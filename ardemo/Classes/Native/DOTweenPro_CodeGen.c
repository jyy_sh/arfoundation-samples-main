﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void DG.Tweening.DOTweenVisualManager::Awake()
extern void DOTweenVisualManager_Awake_mD3829E0071BBBDB7A522EA8C385C36EFB4E8383E (void);
// 0x00000002 System.Void DG.Tweening.DOTweenVisualManager::Update()
extern void DOTweenVisualManager_Update_m01A20A907D4A87C9EA82E16A4A132533053E227A (void);
// 0x00000003 System.Void DG.Tweening.DOTweenVisualManager::OnEnable()
extern void DOTweenVisualManager_OnEnable_m9BC548951D995D467DC0AB1E7BD257F68E9F4420 (void);
// 0x00000004 System.Void DG.Tweening.DOTweenVisualManager::OnDisable()
extern void DOTweenVisualManager_OnDisable_mE365B632F8EB4BD87BC1CBAE72AF84F9462613BC (void);
// 0x00000005 System.Void DG.Tweening.DOTweenVisualManager::.ctor()
extern void DOTweenVisualManager__ctor_mFDA353BA7AB839C6008B8B0014F8FC92093EAF74 (void);
// 0x00000006 System.Void DG.Tweening.DOTweenPath::Awake()
extern void DOTweenPath_Awake_m51A3448F400C8475F54703BDF56036BC5840EC19 (void);
// 0x00000007 System.Void DG.Tweening.DOTweenPath::Reset()
extern void DOTweenPath_Reset_mE0F57DBE1465D5FC758920E14A9F28BFA9CBB907 (void);
// 0x00000008 System.Void DG.Tweening.DOTweenPath::OnDestroy()
extern void DOTweenPath_OnDestroy_m5BA9503DE3961CBB1D5663E2028070CE519AE414 (void);
// 0x00000009 System.Void DG.Tweening.DOTweenPath::DOPlay()
extern void DOTweenPath_DOPlay_m237D396684BF4DE087DFFA44C867C38B68AE5912 (void);
// 0x0000000A System.Void DG.Tweening.DOTweenPath::DOPlayBackwards()
extern void DOTweenPath_DOPlayBackwards_m4C4C04292C1182313E27E97AFCC9D13CD69C0907 (void);
// 0x0000000B System.Void DG.Tweening.DOTweenPath::DOPlayForward()
extern void DOTweenPath_DOPlayForward_mC66732B68D89CE34A6A20B72E6513892AE57E09B (void);
// 0x0000000C System.Void DG.Tweening.DOTweenPath::DOPause()
extern void DOTweenPath_DOPause_m3B8B2A99E7F4AF2C63D866EBD7A14D865244D48A (void);
// 0x0000000D System.Void DG.Tweening.DOTweenPath::DOTogglePause()
extern void DOTweenPath_DOTogglePause_m1E93EF30DA2E9629F0B49349229A39FE4AA6DAB3 (void);
// 0x0000000E System.Void DG.Tweening.DOTweenPath::DORewind()
extern void DOTweenPath_DORewind_mAF3776B4A345F734B6EB8CF74B0CBF0D3B56CB71 (void);
// 0x0000000F System.Void DG.Tweening.DOTweenPath::DORestart(System.Boolean)
extern void DOTweenPath_DORestart_m7CC6C31AC3884C6064F9612E0C025B9DF684952B (void);
// 0x00000010 System.Void DG.Tweening.DOTweenPath::DOComplete()
extern void DOTweenPath_DOComplete_m2D9ADCBA1DED8E57D13892D89D67E86C691D27CD (void);
// 0x00000011 System.Void DG.Tweening.DOTweenPath::DOKill()
extern void DOTweenPath_DOKill_m16CE727B9BF12DCBBA95DD12606832AF5450F154 (void);
// 0x00000012 DG.Tweening.Tween DG.Tweening.DOTweenPath::GetTween()
extern void DOTweenPath_GetTween_mF4DCE3ACAB5A10A6C52EC6381507ACE1F62AD10E (void);
// 0x00000013 UnityEngine.Vector3[] DG.Tweening.DOTweenPath::GetDrawPoints()
extern void DOTweenPath_GetDrawPoints_m0199B745C0B433BA521821316139B4255A467704 (void);
// 0x00000014 UnityEngine.Vector3[] DG.Tweening.DOTweenPath::GetFullWps()
extern void DOTweenPath_GetFullWps_m207122CE008387F966FBA26535D8790D3D5AA6DC (void);
// 0x00000015 System.Void DG.Tweening.DOTweenPath::ReEvaluateRelativeTween()
extern void DOTweenPath_ReEvaluateRelativeTween_mAEF73A49F484B2A0EB04B716D4445AAA171559C9 (void);
// 0x00000016 System.Void DG.Tweening.DOTweenPath::.ctor()
extern void DOTweenPath__ctor_mE5726F4DF128E746378EBCD1346D8E8AAF61CBD9 (void);
// 0x00000017 System.Void DG.Tweening.DOTweenPath::<Awake>b__38_0()
extern void DOTweenPath_U3CAwakeU3Eb__38_0_m499DE7DE14F8DA2F164520621269F9E93F06C8E4 (void);
// 0x00000018 System.Void DG.Tweening.Core.ABSAnimationComponent::DOPlay()
// 0x00000019 System.Void DG.Tweening.Core.ABSAnimationComponent::DOPlayBackwards()
// 0x0000001A System.Void DG.Tweening.Core.ABSAnimationComponent::DOPlayForward()
// 0x0000001B System.Void DG.Tweening.Core.ABSAnimationComponent::DOPause()
// 0x0000001C System.Void DG.Tweening.Core.ABSAnimationComponent::DOTogglePause()
// 0x0000001D System.Void DG.Tweening.Core.ABSAnimationComponent::DORewind()
// 0x0000001E System.Void DG.Tweening.Core.ABSAnimationComponent::DORestart(System.Boolean)
// 0x0000001F System.Void DG.Tweening.Core.ABSAnimationComponent::DOComplete()
// 0x00000020 System.Void DG.Tweening.Core.ABSAnimationComponent::DOKill()
// 0x00000021 System.Void DG.Tweening.Core.ABSAnimationComponent::.ctor()
extern void ABSAnimationComponent__ctor_mF453633C79A30A108832D3F331DAB5E4CA24B272 (void);
static Il2CppMethodPointer s_methodPointers[33] = 
{
	DOTweenVisualManager_Awake_mD3829E0071BBBDB7A522EA8C385C36EFB4E8383E,
	DOTweenVisualManager_Update_m01A20A907D4A87C9EA82E16A4A132533053E227A,
	DOTweenVisualManager_OnEnable_m9BC548951D995D467DC0AB1E7BD257F68E9F4420,
	DOTweenVisualManager_OnDisable_mE365B632F8EB4BD87BC1CBAE72AF84F9462613BC,
	DOTweenVisualManager__ctor_mFDA353BA7AB839C6008B8B0014F8FC92093EAF74,
	DOTweenPath_Awake_m51A3448F400C8475F54703BDF56036BC5840EC19,
	DOTweenPath_Reset_mE0F57DBE1465D5FC758920E14A9F28BFA9CBB907,
	DOTweenPath_OnDestroy_m5BA9503DE3961CBB1D5663E2028070CE519AE414,
	DOTweenPath_DOPlay_m237D396684BF4DE087DFFA44C867C38B68AE5912,
	DOTweenPath_DOPlayBackwards_m4C4C04292C1182313E27E97AFCC9D13CD69C0907,
	DOTweenPath_DOPlayForward_mC66732B68D89CE34A6A20B72E6513892AE57E09B,
	DOTweenPath_DOPause_m3B8B2A99E7F4AF2C63D866EBD7A14D865244D48A,
	DOTweenPath_DOTogglePause_m1E93EF30DA2E9629F0B49349229A39FE4AA6DAB3,
	DOTweenPath_DORewind_mAF3776B4A345F734B6EB8CF74B0CBF0D3B56CB71,
	DOTweenPath_DORestart_m7CC6C31AC3884C6064F9612E0C025B9DF684952B,
	DOTweenPath_DOComplete_m2D9ADCBA1DED8E57D13892D89D67E86C691D27CD,
	DOTweenPath_DOKill_m16CE727B9BF12DCBBA95DD12606832AF5450F154,
	DOTweenPath_GetTween_mF4DCE3ACAB5A10A6C52EC6381507ACE1F62AD10E,
	DOTweenPath_GetDrawPoints_m0199B745C0B433BA521821316139B4255A467704,
	DOTweenPath_GetFullWps_m207122CE008387F966FBA26535D8790D3D5AA6DC,
	DOTweenPath_ReEvaluateRelativeTween_mAEF73A49F484B2A0EB04B716D4445AAA171559C9,
	DOTweenPath__ctor_mE5726F4DF128E746378EBCD1346D8E8AAF61CBD9,
	DOTweenPath_U3CAwakeU3Eb__38_0_m499DE7DE14F8DA2F164520621269F9E93F06C8E4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ABSAnimationComponent__ctor_mF453633C79A30A108832D3F331DAB5E4CA24B272,
};
static const int32_t s_InvokerIndices[33] = 
{
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	2791,
	3418,
	3418,
	3371,
	3371,
	3371,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	2791,
	3418,
	3418,
	3418,
};
extern const CustomAttributesCacheGenerator g_DOTweenPro_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_DOTweenPro_CodeGenModule;
const Il2CppCodeGenModule g_DOTweenPro_CodeGenModule = 
{
	"DOTweenPro.dll",
	33,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_DOTweenPro_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
