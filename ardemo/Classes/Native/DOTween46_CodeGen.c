﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.Vector2 DG.Tweening.DOTweenUtils46::SwitchToRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
extern void DOTweenUtils46_SwitchToRectTransform_m0A393795E7C6FCC971FA44685DBD8C46176BCBB9 (void);
// 0x00000002 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void ShortcutExtensions46_DOFade_m72575C2B2015133EFCC699FE21A2419951AA432A (void);
// 0x00000003 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void ShortcutExtensions46_DOColor_mEFDE35BB24046DCFBAB8DAB12E6B7996C338A719 (void);
// 0x00000004 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern void ShortcutExtensions46_DOFade_mF268968FB793053C5F5230343B2771C9C608C007 (void);
// 0x00000005 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOAnchorPos3D(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Boolean)
extern void ShortcutExtensions46_DOAnchorPos3D_m104305FCB812004D56F3CDBA2327EC0AC8D2CBFF (void);
// 0x00000006 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOSizeDelta(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void ShortcutExtensions46_DOSizeDelta_m0F28C462C6F10CA44C6DDEFBE30838FDB490AB7A (void);
// 0x00000007 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOPunchAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void ShortcutExtensions46_DOPunchAnchorPos_m0E4FBB18152C7EF788723DC36A08081CADF3C3CC (void);
// 0x00000008 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,UnityEngine.Vector2,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void ShortcutExtensions46_DOShakeAnchorPos_mA8018E67BCA3312E54446B02D184BF7BA4656EEC (void);
// 0x00000009 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void ShortcutExtensions46_DOColor_m80A0A452604641CA11960C491CBC3EBE30450BB6 (void);
// 0x0000000A DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
extern void ShortcutExtensions46_DOFade_m560566897759A70C4136B961F83EB89E441E0CBC (void);
// 0x0000000B DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOText(UnityEngine.UI.Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern void ShortcutExtensions46_DOText_mE13EDACBB2B161A6E7BB68A4BFAC148DA825EEA0 (void);
// 0x0000000C System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m11998DA48ADFBDDDCCC821ADBCFE4CA78E356A75 (void);
// 0x0000000D System.Single DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m3CC0E969AEC1C3D19C970E36FA438191A2BF79AB (void);
// 0x0000000E System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mC2A9D8F3FEF369CF5D909EE95A7AF6B67D883CFE (void);
// 0x0000000F System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mEE0BC13E0DD3C22E5C9A54427F2E7F82C1BFDB0A (void);
// 0x00000010 UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m695DB6B9C21E18855DC41D48918F56B64A001919 (void);
// 0x00000011 System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_mBBC54796CF30B8B9E5392B2AD184FD68C77A1B84 (void);
// 0x00000012 System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m78EC2A0728C3BA8730F9C6FD02C266762F9E11ED (void);
// 0x00000013 UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m65423487916C5FE8AA2972B994EF8065218B5E1D (void);
// 0x00000014 System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_mEFED7EB2E070A055614F236CC0BEB52098D40547 (void);
// 0x00000015 System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m7619684E939376756F59B9420341DBE16B2335B1 (void);
// 0x00000016 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::<DOAnchorPos3D>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m365D196C6A7BF617EF5F33C40C6F0CD14CD50244 (void);
// 0x00000017 System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::<DOAnchorPos3D>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m336A27E832E95CA3A8617EBDFF650CF64EA994B0 (void);
// 0x00000018 System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m5712DC8EBA2C2FEEF60489E7C3FB869694CB606B (void);
// 0x00000019 UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0::<DOSizeDelta>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__0_mDD24F37C3CCAEF04700ECAD424D01EFA4F50E229 (void);
// 0x0000001A System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0::<DOSizeDelta>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__1_m7DE913DBF0C3DA77712DE0641B6A2F8ACFA44E67 (void);
// 0x0000001B System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_mB9700D3714BC79BE6CDFDD9CB65961D2F5719076 (void);
// 0x0000001C UnityEngine.Vector3 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::<DOPunchAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__0_m10E9C313E6E91E5D19DB0B6B4FA123ADECCA2B74 (void);
// 0x0000001D System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::<DOPunchAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__1_mF19D13ABBFE03E50E4456264C62E12BB8BEBE098 (void);
// 0x0000001E System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m8221328558696E5701B8036A0371A2537E3B8332 (void);
// 0x0000001F UnityEngine.Vector3 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__0_m26CF751448C2020875DD5E3BD17D3C14216D58AB (void);
// 0x00000020 System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__1_m405D8A53591BDB1F0605A74AA9CEB27AE1E94381 (void);
// 0x00000021 System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_m00B62947195BC1F1C3D00750458BFBF9FEF28059 (void);
// 0x00000022 UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__0_m54F10B78DB2BC0BD8A1392714E902FA3967FD69F (void);
// 0x00000023 System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__1_m4C730A52F0C8DCCEA9ACD409447537B713F6CA9A (void);
// 0x00000024 System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_mA485354C419F9675A100FA688D0DC0C67C5C89CF (void);
// 0x00000025 UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__0_m473A8504F8E60C82DC33C534C91844645F12E6F5 (void);
// 0x00000026 System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__1_m8E3E13E437404AFEC7310F71C49957F7A020FCAE (void);
// 0x00000027 System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m82BBE6802676321D75771BBE72272D53B45FF93B (void);
// 0x00000028 System.String DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::<DOText>b__0()
extern void U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__0_m2C298E2351981C0E0F57ACE1079E233D7A3B6464 (void);
// 0x00000029 System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::<DOText>b__1(System.String)
extern void U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__1_m5080E8829A347C334E2DD9D978768B10707A18FD (void);
static Il2CppMethodPointer s_methodPointers[41] = 
{
	DOTweenUtils46_SwitchToRectTransform_m0A393795E7C6FCC971FA44685DBD8C46176BCBB9,
	ShortcutExtensions46_DOFade_m72575C2B2015133EFCC699FE21A2419951AA432A,
	ShortcutExtensions46_DOColor_mEFDE35BB24046DCFBAB8DAB12E6B7996C338A719,
	ShortcutExtensions46_DOFade_mF268968FB793053C5F5230343B2771C9C608C007,
	ShortcutExtensions46_DOAnchorPos3D_m104305FCB812004D56F3CDBA2327EC0AC8D2CBFF,
	ShortcutExtensions46_DOSizeDelta_m0F28C462C6F10CA44C6DDEFBE30838FDB490AB7A,
	ShortcutExtensions46_DOPunchAnchorPos_m0E4FBB18152C7EF788723DC36A08081CADF3C3CC,
	ShortcutExtensions46_DOShakeAnchorPos_mA8018E67BCA3312E54446B02D184BF7BA4656EEC,
	ShortcutExtensions46_DOColor_m80A0A452604641CA11960C491CBC3EBE30450BB6,
	ShortcutExtensions46_DOFade_m560566897759A70C4136B961F83EB89E441E0CBC,
	ShortcutExtensions46_DOText_mE13EDACBB2B161A6E7BB68A4BFAC148DA825EEA0,
	U3CU3Ec__DisplayClass0_0__ctor_m11998DA48ADFBDDDCCC821ADBCFE4CA78E356A75,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m3CC0E969AEC1C3D19C970E36FA438191A2BF79AB,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mC2A9D8F3FEF369CF5D909EE95A7AF6B67D883CFE,
	U3CU3Ec__DisplayClass3_0__ctor_mEE0BC13E0DD3C22E5C9A54427F2E7F82C1BFDB0A,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m695DB6B9C21E18855DC41D48918F56B64A001919,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_mBBC54796CF30B8B9E5392B2AD184FD68C77A1B84,
	U3CU3Ec__DisplayClass4_0__ctor_m78EC2A0728C3BA8730F9C6FD02C266762F9E11ED,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m65423487916C5FE8AA2972B994EF8065218B5E1D,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_mEFED7EB2E070A055614F236CC0BEB52098D40547,
	U3CU3Ec__DisplayClass16_0__ctor_m7619684E939376756F59B9420341DBE16B2335B1,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m365D196C6A7BF617EF5F33C40C6F0CD14CD50244,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m336A27E832E95CA3A8617EBDFF650CF64EA994B0,
	U3CU3Ec__DisplayClass22_0__ctor_m5712DC8EBA2C2FEEF60489E7C3FB869694CB606B,
	U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__0_mDD24F37C3CCAEF04700ECAD424D01EFA4F50E229,
	U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__1_m7DE913DBF0C3DA77712DE0641B6A2F8ACFA44E67,
	U3CU3Ec__DisplayClass23_0__ctor_mB9700D3714BC79BE6CDFDD9CB65961D2F5719076,
	U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__0_m10E9C313E6E91E5D19DB0B6B4FA123ADECCA2B74,
	U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__1_mF19D13ABBFE03E50E4456264C62E12BB8BEBE098,
	U3CU3Ec__DisplayClass25_0__ctor_m8221328558696E5701B8036A0371A2537E3B8332,
	U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__0_m26CF751448C2020875DD5E3BD17D3C14216D58AB,
	U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__1_m405D8A53591BDB1F0605A74AA9CEB27AE1E94381,
	U3CU3Ec__DisplayClass31_0__ctor_m00B62947195BC1F1C3D00750458BFBF9FEF28059,
	U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__0_m54F10B78DB2BC0BD8A1392714E902FA3967FD69F,
	U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__1_m4C730A52F0C8DCCEA9ACD409447537B713F6CA9A,
	U3CU3Ec__DisplayClass32_0__ctor_mA485354C419F9675A100FA688D0DC0C67C5C89CF,
	U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__0_m473A8504F8E60C82DC33C534C91844645F12E6F5,
	U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__1_m8E3E13E437404AFEC7310F71C49957F7A020FCAE,
	U3CU3Ec__DisplayClass33_0__ctor_m82BBE6802676321D75771BBE72272D53B45FF93B,
	U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__0_m2C298E2351981C0E0F57ACE1079E233D7A3B6464,
	U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__1_m5080E8829A347C334E2DD9D978768B10707A18FD,
};
static const int32_t s_InvokerIndices[41] = 
{
	5068,
	4405,
	4381,
	4405,
	4140,
	4136,
	3602,
	3557,
	4381,
	4405,
	3596,
	3418,
	3398,
	2793,
	3418,
	3300,
	2700,
	3418,
	3300,
	2700,
	3418,
	3416,
	2810,
	3418,
	3414,
	2808,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3300,
	2700,
	3418,
	3300,
	2700,
	3418,
	3371,
	2770,
};
extern const CustomAttributesCacheGenerator g_DOTween46_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_DOTween46_CodeGenModule;
const Il2CppCodeGenModule g_DOTween46_CodeGenModule = 
{
	"DOTween46.dll",
	41,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_DOTween46_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
