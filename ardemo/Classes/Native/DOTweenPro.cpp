﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>
struct ABSTweenPlugin_3_t0048BB94992A96F02D427E2C26C6E92AC9AECD32;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<DG.Tweening.TweenCallback>
struct List_1_t7A220E9BB7539479C818543CE3CBDF252554FD58;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>
struct TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// DG.Tweening.Plugins.Core.PathCore.ControlPoint[]
struct ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// DG.Tweening.TweenCallback[]
struct TweenCallbackU5BU5D_t44475AF4915BDD70BD7FDA0A56F86836DD3D87DE;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// DG.Tweening.Core.ABSAnimationComponent
struct ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7;
// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder
struct ABSPathDecoder_tC2631579BAB41F64C63A0909E8386B88B45F93D1;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder
struct CatmullRomDecoder_t2FA372BD7BB8D2A0CBCBCC812FBCB83703A63F9B;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// DG.Tweening.Core.DOTweenComponent
struct DOTweenComponent_tEFFB81B283458D3C607F5D8960C4E28E10764F29;
// DG.Tweening.DOTweenPath
struct DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989;
// DG.Tweening.DOTweenVisualManager
struct DOTweenVisualManager_t4164D354E77A1A82EB58F804D71C15482404E25C;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// DG.Tweening.EaseFunction
struct EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// DG.Tweening.Plugins.Core.PathCore.LinearDecoder
struct LinearDecoder_tE244F738878748CB8B92F95C686BE0B57B066291;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// DG.Tweening.Plugins.Core.PathCore.Path
struct Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A;
// DG.Tweening.Sequence
struct Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// DG.Tweening.Tween
struct Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941;
// DG.Tweening.TweenCallback
struct TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB;
// System.Type
struct Type_t;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C RuntimeClass* AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral1BCF04EB959108E68CFE0C06D667CF9B7AFC4B36;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7_m49AD20E6B0D8DB63C42C9DF58850542370EACB8C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DOTweenPath_U3CAwakeU3Eb__38_0_m499DE7DE14F8DA2F164520621269F9E93F06C8E4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m49BFDA3CA404A1733B6D6CAAE6F254AD4076414B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_ToArray_mCFFA6AFA30A20FB2FFC3846AD114DF7BC21BFFBC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1__ctor_mABF3CCAC28954E932B026C84D4C0D0F7BF2662F9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Path_Draw_m98F7E4F7EAD1D65843AFCA4C097E0BEF2E5CC783_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenExtensions_Pause_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4A0F398B9290C213D5FED5901234BA973AA0C257_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenExtensions_Pause_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m4AF73672C93CFFCEC64E3E05744EBAE823B216A7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenExtensions_Play_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m1A3AAD3856D80A02DB7F6BB7BBA444FD4C62DA8C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenExtensions_Play_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mE15D9ECA24D6CBE1DE47F46F1233B06A3D32EBF6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_OnComplete_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m0FB1AE97C737B4B59112E68269CEC95EDB23AA1B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_OnKill_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m47F222AC973A667D579E0F8B5BE97093A2E57200_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_OnPlay_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m4064AFE7901DCBAA4382AD1AFC946097BE807A87_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_OnRewind_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m52FD49B2534253B61C8D111A4949B26FE53B13D0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_OnStart_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mFBD4A625C829E10DCAD11FE8633CD49B028D89EC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_OnStepComplete_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m128EB7D25F01AE225EB6397FA4E51044CD8278EE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_OnUpdate_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m3C21D9220857292A08A86EDF81E2F9B4F515C96E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetAutoKill_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mC6B4A0088E7F88B4B988455ACD13CECFD497C22D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m6B8AB1944A21B4D442E2E4B39575F7C6F0F3E460_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m6DD55E5B6755E223CC8B87169782DA90580F2570_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m7C1312703C345C640A4F7F0C6D3D46184F4AFA40_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetId_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m0CCED6BE9804CB6CC45ADE94D53345D7E595CA37_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mF7A6FCD23D0641A47C66CE77CC1A262C48ED4B6E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetSpeedBased_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m687552F99CBA5F1DE88F60CA85E91FC81767E770_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetUpdate_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m0005FC778EB58D20C9D21947236A37B707418F83_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884;
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC;
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t90EA40379E3BAA925D74D641520AE0DDF37A44A7 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<DG.Tweening.TweenCallback>
struct  List_1_t7A220E9BB7539479C818543CE3CBDF252554FD58  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TweenCallbackU5BU5D_t44475AF4915BDD70BD7FDA0A56F86836DD3D87DE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t7A220E9BB7539479C818543CE3CBDF252554FD58, ____items_1)); }
	inline TweenCallbackU5BU5D_t44475AF4915BDD70BD7FDA0A56F86836DD3D87DE* get__items_1() const { return ____items_1; }
	inline TweenCallbackU5BU5D_t44475AF4915BDD70BD7FDA0A56F86836DD3D87DE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TweenCallbackU5BU5D_t44475AF4915BDD70BD7FDA0A56F86836DD3D87DE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t7A220E9BB7539479C818543CE3CBDF252554FD58, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t7A220E9BB7539479C818543CE3CBDF252554FD58, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t7A220E9BB7539479C818543CE3CBDF252554FD58, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t7A220E9BB7539479C818543CE3CBDF252554FD58_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TweenCallbackU5BU5D_t44475AF4915BDD70BD7FDA0A56F86836DD3D87DE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t7A220E9BB7539479C818543CE3CBDF252554FD58_StaticFields, ____emptyArray_5)); }
	inline TweenCallbackU5BU5D_t44475AF4915BDD70BD7FDA0A56F86836DD3D87DE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TweenCallbackU5BU5D_t44475AF4915BDD70BD7FDA0A56F86836DD3D87DE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TweenCallbackU5BU5D_t44475AF4915BDD70BD7FDA0A56F86836DD3D87DE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____items_1)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_StaticFields, ____emptyArray_5)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// DG.Tweening.Core.Debugger
struct  Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F  : public RuntimeObject
{
public:

public:
};

struct Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F_StaticFields
{
public:
	// System.Int32 DG.Tweening.Core.Debugger::logPriority
	int32_t ___logPriority_0;

public:
	inline static int32_t get_offset_of_logPriority_0() { return static_cast<int32_t>(offsetof(Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F_StaticFields, ___logPriority_0)); }
	inline int32_t get_logPriority_0() const { return ___logPriority_0; }
	inline int32_t* get_address_of_logPriority_0() { return &___logPriority_0; }
	inline void set_logPriority_0(int32_t value)
	{
		___logPriority_0 = value;
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Keyframe
struct  Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F 
{
public:
	// System.Single UnityEngine.Keyframe::m_Time
	float ___m_Time_0;
	// System.Single UnityEngine.Keyframe::m_Value
	float ___m_Value_1;
	// System.Single UnityEngine.Keyframe::m_InTangent
	float ___m_InTangent_2;
	// System.Single UnityEngine.Keyframe::m_OutTangent
	float ___m_OutTangent_3;
	// System.Int32 UnityEngine.Keyframe::m_WeightedMode
	int32_t ___m_WeightedMode_4;
	// System.Single UnityEngine.Keyframe::m_InWeight
	float ___m_InWeight_5;
	// System.Single UnityEngine.Keyframe::m_OutWeight
	float ___m_OutWeight_6;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_m_InTangent_2() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_InTangent_2)); }
	inline float get_m_InTangent_2() const { return ___m_InTangent_2; }
	inline float* get_address_of_m_InTangent_2() { return &___m_InTangent_2; }
	inline void set_m_InTangent_2(float value)
	{
		___m_InTangent_2 = value;
	}

	inline static int32_t get_offset_of_m_OutTangent_3() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_OutTangent_3)); }
	inline float get_m_OutTangent_3() const { return ___m_OutTangent_3; }
	inline float* get_address_of_m_OutTangent_3() { return &___m_OutTangent_3; }
	inline void set_m_OutTangent_3(float value)
	{
		___m_OutTangent_3 = value;
	}

	inline static int32_t get_offset_of_m_WeightedMode_4() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_WeightedMode_4)); }
	inline int32_t get_m_WeightedMode_4() const { return ___m_WeightedMode_4; }
	inline int32_t* get_address_of_m_WeightedMode_4() { return &___m_WeightedMode_4; }
	inline void set_m_WeightedMode_4(int32_t value)
	{
		___m_WeightedMode_4 = value;
	}

	inline static int32_t get_offset_of_m_InWeight_5() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_InWeight_5)); }
	inline float get_m_InWeight_5() const { return ___m_InWeight_5; }
	inline float* get_address_of_m_InWeight_5() { return &___m_InWeight_5; }
	inline void set_m_InWeight_5(float value)
	{
		___m_InWeight_5 = value;
	}

	inline static int32_t get_offset_of_m_OutWeight_6() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_OutWeight_6)); }
	inline float get_m_OutWeight_6() const { return ___m_OutWeight_6; }
	inline float* get_address_of_m_OutWeight_6() { return &___m_OutWeight_6; }
	inline void set_m_OutWeight_6(float value)
	{
		___m_OutWeight_6 = value;
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Events.UnityEvent
struct  UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Nullable`1<UnityEngine.Color>
struct  Nullable_1_tA06400BA484934D9CEBAF66D0E71C822EF09A498 
{
public:
	// T System.Nullable`1::value
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tA06400BA484934D9CEBAF66D0E71C822EF09A498, ___value_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_value_0() const { return ___value_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tA06400BA484934D9CEBAF66D0E71C822EF09A498, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Nullable`1<UnityEngine.Vector3>
struct  Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 
{
public:
	// T System.Nullable`1::value
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258, ___value_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_value_0() const { return ___value_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// UnityEngine.AnimationCurve
struct  AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// DG.Tweening.AutoPlay
struct  AutoPlay_t82EC6434A741EB9DED6F3BE07E59E70446ABC80C 
{
public:
	// System.Int32 DG.Tweening.AutoPlay::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AutoPlay_t82EC6434A741EB9DED6F3BE07E59E70446ABC80C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.AxisConstraint
struct  AxisConstraint_tA0D384964013674923F26C7DF2618FB76CD3D7F4 
{
public:
	// System.Int32 DG.Tweening.AxisConstraint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisConstraint_tA0D384964013674923F26C7DF2618FB76CD3D7F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.Plugins.Core.PathCore.ControlPoint
struct  ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD 
{
public:
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.ControlPoint::a
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a_0;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.ControlPoint::b
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD, ___a_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_a_0() const { return ___a_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD, ___b_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_b_1() const { return ___b_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___b_1 = value;
	}
};


// DG.Tweening.Core.DOTweenAnimationType
struct  DOTweenAnimationType_tCFA1A0B80F60515B002DBDD3996379796F3775D6 
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenAnimationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DOTweenAnimationType_tCFA1A0B80F60515B002DBDD3996379796F3775D6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.DOTweenInspectorMode
struct  DOTweenInspectorMode_t14D255801DD873B88DDBDF292B3E806AEB797A54 
{
public:
	// System.Int32 DG.Tweening.DOTweenInspectorMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DOTweenInspectorMode_t14D255801DD873B88DDBDF292B3E806AEB797A54, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// DG.Tweening.Ease
struct  Ease_tB04D625230DDC5B40D74E825C8A9DBBE37A146B4 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Ease_tB04D625230DDC5B40D74E825C8A9DBBE37A146B4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.HandlesDrawMode
struct  HandlesDrawMode_t5159DBDD1D11C7E4C00982C3E8B738858BB87372 
{
public:
	// System.Int32 DG.Tweening.HandlesDrawMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HandlesDrawMode_t5159DBDD1D11C7E4C00982C3E8B738858BB87372, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.HandlesType
struct  HandlesType_t5B69E794484D18DDF58A183C73BEED8DB1D79600 
{
public:
	// System.Int32 DG.Tweening.HandlesType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HandlesType_t5B69E794484D18DDF58A183C73BEED8DB1D79600, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.LogBehaviour
struct  LogBehaviour_tE75FAAAFF4FA56D1EB213B38425928B7FF6B17C1 
{
public:
	// System.Int32 DG.Tweening.LogBehaviour::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogBehaviour_tE75FAAAFF4FA56D1EB213B38425928B7FF6B17C1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.LoopType
struct  LoopType_tF807A5805F6A83F5228BE7D4E633B2572B1B859A 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoopType_tF807A5805F6A83F5228BE7D4E633B2572B1B859A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// DG.Tweening.Core.OnDisableBehaviour
struct  OnDisableBehaviour_tE5585D6BAEB5658BE63FED079380FD834617FEE8 
{
public:
	// System.Int32 DG.Tweening.Core.OnDisableBehaviour::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OnDisableBehaviour_tE5585D6BAEB5658BE63FED079380FD834617FEE8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.Core.OnEnableBehaviour
struct  OnEnableBehaviour_tCE94E78DE6D345D8B5894C8A8BF2650C0B43C838 
{
public:
	// System.Int32 DG.Tweening.Core.OnEnableBehaviour::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OnEnableBehaviour_tCE94E78DE6D345D8B5894C8A8BF2650C0B43C838, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.Plugins.Options.OrientType
struct  OrientType_t98C6DBF0A80C2A938CA315BCE990CF0201D52886 
{
public:
	// System.Int32 DG.Tweening.Plugins.Options.OrientType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OrientType_t98C6DBF0A80C2A938CA315BCE990CF0201D52886, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.PathMode
struct  PathMode_tC3536FD34C73F94ADFB5C7DC9415985E353CE572 
{
public:
	// System.Int32 DG.Tweening.PathMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PathMode_tC3536FD34C73F94ADFB5C7DC9415985E353CE572, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.PathType
struct  PathType_tBA0D4391850F4868EE61BFE9579098DD42D02899 
{
public:
	// System.Int32 DG.Tweening.PathType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PathType_tBA0D4391850F4868EE61BFE9579098DD42D02899, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.Core.Enums.SpecialStartupMode
struct  SpecialStartupMode_t0086D2AE798C217210762DD17C8D3572414ACD92 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.SpecialStartupMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialStartupMode_t0086D2AE798C217210762DD17C8D3572414ACD92, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.Core.TargetType
struct  TargetType_t075843C35C2F3BB06953C27ACCC880643AF35310 
{
public:
	// System.Int32 DG.Tweening.Core.TargetType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TargetType_t075843C35C2F3BB06953C27ACCC880643AF35310, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.TweenType
struct  TweenType_tAB2DEC1268409EA172594368494218E51696EF5D 
{
public:
	// System.Int32 DG.Tweening.TweenType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TweenType_tAB2DEC1268409EA172594368494218E51696EF5D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.UpdateType
struct  UpdateType_t9D838506DD148F29E6183FB298E41921E51CC5ED 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateType_t9D838506DD148F29E6183FB298E41921E51CC5ED, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.Core.VisualManagerPreset
struct  VisualManagerPreset_t85F8E8B19FA55E2706FD4311D9A47AB0588818EE 
{
public:
	// System.Int32 DG.Tweening.Core.VisualManagerPreset::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VisualManagerPreset_t85F8E8B19FA55E2706FD4311D9A47AB0588818EE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.Core.ABSSequentiable
struct  ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76  : public RuntimeObject
{
public:
	// DG.Tweening.TweenType DG.Tweening.Core.ABSSequentiable::tweenType
	int32_t ___tweenType_0;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedPosition
	float ___sequencedPosition_1;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedEndPosition
	float ___sequencedEndPosition_2;
	// DG.Tweening.TweenCallback DG.Tweening.Core.ABSSequentiable::onStart
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onStart_3;

public:
	inline static int32_t get_offset_of_tweenType_0() { return static_cast<int32_t>(offsetof(ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76, ___tweenType_0)); }
	inline int32_t get_tweenType_0() const { return ___tweenType_0; }
	inline int32_t* get_address_of_tweenType_0() { return &___tweenType_0; }
	inline void set_tweenType_0(int32_t value)
	{
		___tweenType_0 = value;
	}

	inline static int32_t get_offset_of_sequencedPosition_1() { return static_cast<int32_t>(offsetof(ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76, ___sequencedPosition_1)); }
	inline float get_sequencedPosition_1() const { return ___sequencedPosition_1; }
	inline float* get_address_of_sequencedPosition_1() { return &___sequencedPosition_1; }
	inline void set_sequencedPosition_1(float value)
	{
		___sequencedPosition_1 = value;
	}

	inline static int32_t get_offset_of_sequencedEndPosition_2() { return static_cast<int32_t>(offsetof(ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76, ___sequencedEndPosition_2)); }
	inline float get_sequencedEndPosition_2() const { return ___sequencedEndPosition_2; }
	inline float* get_address_of_sequencedEndPosition_2() { return &___sequencedEndPosition_2; }
	inline void set_sequencedEndPosition_2(float value)
	{
		___sequencedEndPosition_2 = value;
	}

	inline static int32_t get_offset_of_onStart_3() { return static_cast<int32_t>(offsetof(ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76, ___onStart_3)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onStart_3() const { return ___onStart_3; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onStart_3() { return &___onStart_3; }
	inline void set_onStart_3(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onStart_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStart_3), (void*)value);
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// DG.Tweening.DOTween
struct  DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203  : public RuntimeObject
{
public:

public:
};

struct DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields
{
public:
	// System.String DG.Tweening.DOTween::Version
	String_t* ___Version_0;
	// System.Boolean DG.Tweening.DOTween::useSafeMode
	bool ___useSafeMode_1;
	// System.Boolean DG.Tweening.DOTween::showUnityEditorReport
	bool ___showUnityEditorReport_2;
	// System.Single DG.Tweening.DOTween::timeScale
	float ___timeScale_3;
	// System.Boolean DG.Tweening.DOTween::useSmoothDeltaTime
	bool ___useSmoothDeltaTime_4;
	// System.Single DG.Tweening.DOTween::maxSmoothUnscaledTime
	float ___maxSmoothUnscaledTime_5;
	// DG.Tweening.LogBehaviour DG.Tweening.DOTween::_logBehaviour
	int32_t ____logBehaviour_6;
	// System.Boolean DG.Tweening.DOTween::drawGizmos
	bool ___drawGizmos_7;
	// DG.Tweening.UpdateType DG.Tweening.DOTween::defaultUpdateType
	int32_t ___defaultUpdateType_8;
	// System.Boolean DG.Tweening.DOTween::defaultTimeScaleIndependent
	bool ___defaultTimeScaleIndependent_9;
	// DG.Tweening.AutoPlay DG.Tweening.DOTween::defaultAutoPlay
	int32_t ___defaultAutoPlay_10;
	// System.Boolean DG.Tweening.DOTween::defaultAutoKill
	bool ___defaultAutoKill_11;
	// DG.Tweening.LoopType DG.Tweening.DOTween::defaultLoopType
	int32_t ___defaultLoopType_12;
	// System.Boolean DG.Tweening.DOTween::defaultRecyclable
	bool ___defaultRecyclable_13;
	// DG.Tweening.Ease DG.Tweening.DOTween::defaultEaseType
	int32_t ___defaultEaseType_14;
	// System.Single DG.Tweening.DOTween::defaultEaseOvershootOrAmplitude
	float ___defaultEaseOvershootOrAmplitude_15;
	// System.Single DG.Tweening.DOTween::defaultEasePeriod
	float ___defaultEasePeriod_16;
	// DG.Tweening.Core.DOTweenComponent DG.Tweening.DOTween::instance
	DOTweenComponent_tEFFB81B283458D3C607F5D8960C4E28E10764F29 * ___instance_17;
	// System.Boolean DG.Tweening.DOTween::isUnityEditor
	bool ___isUnityEditor_18;
	// System.Boolean DG.Tweening.DOTween::isDebugBuild
	bool ___isDebugBuild_19;
	// System.Int32 DG.Tweening.DOTween::maxActiveTweenersReached
	int32_t ___maxActiveTweenersReached_20;
	// System.Int32 DG.Tweening.DOTween::maxActiveSequencesReached
	int32_t ___maxActiveSequencesReached_21;
	// System.Collections.Generic.List`1<DG.Tweening.TweenCallback> DG.Tweening.DOTween::GizmosDelegates
	List_1_t7A220E9BB7539479C818543CE3CBDF252554FD58 * ___GizmosDelegates_22;
	// System.Boolean DG.Tweening.DOTween::initialized
	bool ___initialized_23;
	// System.Boolean DG.Tweening.DOTween::isQuitting
	bool ___isQuitting_24;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___Version_0)); }
	inline String_t* get_Version_0() const { return ___Version_0; }
	inline String_t** get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(String_t* value)
	{
		___Version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Version_0), (void*)value);
	}

	inline static int32_t get_offset_of_useSafeMode_1() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___useSafeMode_1)); }
	inline bool get_useSafeMode_1() const { return ___useSafeMode_1; }
	inline bool* get_address_of_useSafeMode_1() { return &___useSafeMode_1; }
	inline void set_useSafeMode_1(bool value)
	{
		___useSafeMode_1 = value;
	}

	inline static int32_t get_offset_of_showUnityEditorReport_2() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___showUnityEditorReport_2)); }
	inline bool get_showUnityEditorReport_2() const { return ___showUnityEditorReport_2; }
	inline bool* get_address_of_showUnityEditorReport_2() { return &___showUnityEditorReport_2; }
	inline void set_showUnityEditorReport_2(bool value)
	{
		___showUnityEditorReport_2 = value;
	}

	inline static int32_t get_offset_of_timeScale_3() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___timeScale_3)); }
	inline float get_timeScale_3() const { return ___timeScale_3; }
	inline float* get_address_of_timeScale_3() { return &___timeScale_3; }
	inline void set_timeScale_3(float value)
	{
		___timeScale_3 = value;
	}

	inline static int32_t get_offset_of_useSmoothDeltaTime_4() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___useSmoothDeltaTime_4)); }
	inline bool get_useSmoothDeltaTime_4() const { return ___useSmoothDeltaTime_4; }
	inline bool* get_address_of_useSmoothDeltaTime_4() { return &___useSmoothDeltaTime_4; }
	inline void set_useSmoothDeltaTime_4(bool value)
	{
		___useSmoothDeltaTime_4 = value;
	}

	inline static int32_t get_offset_of_maxSmoothUnscaledTime_5() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___maxSmoothUnscaledTime_5)); }
	inline float get_maxSmoothUnscaledTime_5() const { return ___maxSmoothUnscaledTime_5; }
	inline float* get_address_of_maxSmoothUnscaledTime_5() { return &___maxSmoothUnscaledTime_5; }
	inline void set_maxSmoothUnscaledTime_5(float value)
	{
		___maxSmoothUnscaledTime_5 = value;
	}

	inline static int32_t get_offset_of__logBehaviour_6() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ____logBehaviour_6)); }
	inline int32_t get__logBehaviour_6() const { return ____logBehaviour_6; }
	inline int32_t* get_address_of__logBehaviour_6() { return &____logBehaviour_6; }
	inline void set__logBehaviour_6(int32_t value)
	{
		____logBehaviour_6 = value;
	}

	inline static int32_t get_offset_of_drawGizmos_7() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___drawGizmos_7)); }
	inline bool get_drawGizmos_7() const { return ___drawGizmos_7; }
	inline bool* get_address_of_drawGizmos_7() { return &___drawGizmos_7; }
	inline void set_drawGizmos_7(bool value)
	{
		___drawGizmos_7 = value;
	}

	inline static int32_t get_offset_of_defaultUpdateType_8() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___defaultUpdateType_8)); }
	inline int32_t get_defaultUpdateType_8() const { return ___defaultUpdateType_8; }
	inline int32_t* get_address_of_defaultUpdateType_8() { return &___defaultUpdateType_8; }
	inline void set_defaultUpdateType_8(int32_t value)
	{
		___defaultUpdateType_8 = value;
	}

	inline static int32_t get_offset_of_defaultTimeScaleIndependent_9() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___defaultTimeScaleIndependent_9)); }
	inline bool get_defaultTimeScaleIndependent_9() const { return ___defaultTimeScaleIndependent_9; }
	inline bool* get_address_of_defaultTimeScaleIndependent_9() { return &___defaultTimeScaleIndependent_9; }
	inline void set_defaultTimeScaleIndependent_9(bool value)
	{
		___defaultTimeScaleIndependent_9 = value;
	}

	inline static int32_t get_offset_of_defaultAutoPlay_10() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___defaultAutoPlay_10)); }
	inline int32_t get_defaultAutoPlay_10() const { return ___defaultAutoPlay_10; }
	inline int32_t* get_address_of_defaultAutoPlay_10() { return &___defaultAutoPlay_10; }
	inline void set_defaultAutoPlay_10(int32_t value)
	{
		___defaultAutoPlay_10 = value;
	}

	inline static int32_t get_offset_of_defaultAutoKill_11() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___defaultAutoKill_11)); }
	inline bool get_defaultAutoKill_11() const { return ___defaultAutoKill_11; }
	inline bool* get_address_of_defaultAutoKill_11() { return &___defaultAutoKill_11; }
	inline void set_defaultAutoKill_11(bool value)
	{
		___defaultAutoKill_11 = value;
	}

	inline static int32_t get_offset_of_defaultLoopType_12() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___defaultLoopType_12)); }
	inline int32_t get_defaultLoopType_12() const { return ___defaultLoopType_12; }
	inline int32_t* get_address_of_defaultLoopType_12() { return &___defaultLoopType_12; }
	inline void set_defaultLoopType_12(int32_t value)
	{
		___defaultLoopType_12 = value;
	}

	inline static int32_t get_offset_of_defaultRecyclable_13() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___defaultRecyclable_13)); }
	inline bool get_defaultRecyclable_13() const { return ___defaultRecyclable_13; }
	inline bool* get_address_of_defaultRecyclable_13() { return &___defaultRecyclable_13; }
	inline void set_defaultRecyclable_13(bool value)
	{
		___defaultRecyclable_13 = value;
	}

	inline static int32_t get_offset_of_defaultEaseType_14() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___defaultEaseType_14)); }
	inline int32_t get_defaultEaseType_14() const { return ___defaultEaseType_14; }
	inline int32_t* get_address_of_defaultEaseType_14() { return &___defaultEaseType_14; }
	inline void set_defaultEaseType_14(int32_t value)
	{
		___defaultEaseType_14 = value;
	}

	inline static int32_t get_offset_of_defaultEaseOvershootOrAmplitude_15() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___defaultEaseOvershootOrAmplitude_15)); }
	inline float get_defaultEaseOvershootOrAmplitude_15() const { return ___defaultEaseOvershootOrAmplitude_15; }
	inline float* get_address_of_defaultEaseOvershootOrAmplitude_15() { return &___defaultEaseOvershootOrAmplitude_15; }
	inline void set_defaultEaseOvershootOrAmplitude_15(float value)
	{
		___defaultEaseOvershootOrAmplitude_15 = value;
	}

	inline static int32_t get_offset_of_defaultEasePeriod_16() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___defaultEasePeriod_16)); }
	inline float get_defaultEasePeriod_16() const { return ___defaultEasePeriod_16; }
	inline float* get_address_of_defaultEasePeriod_16() { return &___defaultEasePeriod_16; }
	inline void set_defaultEasePeriod_16(float value)
	{
		___defaultEasePeriod_16 = value;
	}

	inline static int32_t get_offset_of_instance_17() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___instance_17)); }
	inline DOTweenComponent_tEFFB81B283458D3C607F5D8960C4E28E10764F29 * get_instance_17() const { return ___instance_17; }
	inline DOTweenComponent_tEFFB81B283458D3C607F5D8960C4E28E10764F29 ** get_address_of_instance_17() { return &___instance_17; }
	inline void set_instance_17(DOTweenComponent_tEFFB81B283458D3C607F5D8960C4E28E10764F29 * value)
	{
		___instance_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_17), (void*)value);
	}

	inline static int32_t get_offset_of_isUnityEditor_18() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___isUnityEditor_18)); }
	inline bool get_isUnityEditor_18() const { return ___isUnityEditor_18; }
	inline bool* get_address_of_isUnityEditor_18() { return &___isUnityEditor_18; }
	inline void set_isUnityEditor_18(bool value)
	{
		___isUnityEditor_18 = value;
	}

	inline static int32_t get_offset_of_isDebugBuild_19() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___isDebugBuild_19)); }
	inline bool get_isDebugBuild_19() const { return ___isDebugBuild_19; }
	inline bool* get_address_of_isDebugBuild_19() { return &___isDebugBuild_19; }
	inline void set_isDebugBuild_19(bool value)
	{
		___isDebugBuild_19 = value;
	}

	inline static int32_t get_offset_of_maxActiveTweenersReached_20() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___maxActiveTweenersReached_20)); }
	inline int32_t get_maxActiveTweenersReached_20() const { return ___maxActiveTweenersReached_20; }
	inline int32_t* get_address_of_maxActiveTweenersReached_20() { return &___maxActiveTweenersReached_20; }
	inline void set_maxActiveTweenersReached_20(int32_t value)
	{
		___maxActiveTweenersReached_20 = value;
	}

	inline static int32_t get_offset_of_maxActiveSequencesReached_21() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___maxActiveSequencesReached_21)); }
	inline int32_t get_maxActiveSequencesReached_21() const { return ___maxActiveSequencesReached_21; }
	inline int32_t* get_address_of_maxActiveSequencesReached_21() { return &___maxActiveSequencesReached_21; }
	inline void set_maxActiveSequencesReached_21(int32_t value)
	{
		___maxActiveSequencesReached_21 = value;
	}

	inline static int32_t get_offset_of_GizmosDelegates_22() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___GizmosDelegates_22)); }
	inline List_1_t7A220E9BB7539479C818543CE3CBDF252554FD58 * get_GizmosDelegates_22() const { return ___GizmosDelegates_22; }
	inline List_1_t7A220E9BB7539479C818543CE3CBDF252554FD58 ** get_address_of_GizmosDelegates_22() { return &___GizmosDelegates_22; }
	inline void set_GizmosDelegates_22(List_1_t7A220E9BB7539479C818543CE3CBDF252554FD58 * value)
	{
		___GizmosDelegates_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GizmosDelegates_22), (void*)value);
	}

	inline static int32_t get_offset_of_initialized_23() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___initialized_23)); }
	inline bool get_initialized_23() const { return ___initialized_23; }
	inline bool* get_address_of_initialized_23() { return &___initialized_23; }
	inline void set_initialized_23(bool value)
	{
		___initialized_23 = value;
	}

	inline static int32_t get_offset_of_isQuitting_24() { return static_cast<int32_t>(offsetof(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields, ___isQuitting_24)); }
	inline bool get_isQuitting_24() const { return ___isQuitting_24; }
	inline bool* get_address_of_isQuitting_24() { return &___isQuitting_24; }
	inline void set_isQuitting_24(bool value)
	{
		___isQuitting_24 = value;
	}
};


// UnityEngine.GameObject
struct  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// DG.Tweening.Plugins.Core.PathCore.Path
struct  Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5  : public RuntimeObject
{
public:
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::wpLengths
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___wpLengths_2;
	// DG.Tweening.PathType DG.Tweening.Plugins.Core.PathCore.Path::type
	int32_t ___type_3;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::subdivisionsXSegment
	int32_t ___subdivisionsXSegment_4;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::subdivisions
	int32_t ___subdivisions_5;
	// UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.Path::wps
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___wps_6;
	// DG.Tweening.Plugins.Core.PathCore.ControlPoint[] DG.Tweening.Plugins.Core.PathCore.Path::controlPoints
	ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884* ___controlPoints_7;
	// System.Single DG.Tweening.Plugins.Core.PathCore.Path::length
	float ___length_8;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::isFinalized
	bool ___isFinalized_9;
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::timesTable
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___timesTable_10;
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::lengthsTable
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___lengthsTable_11;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::linearWPIndex
	int32_t ___linearWPIndex_12;
	// DG.Tweening.Plugins.Core.PathCore.Path DG.Tweening.Plugins.Core.PathCore.Path::_incrementalClone
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ____incrementalClone_13;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::_incrementalIndex
	int32_t ____incrementalIndex_14;
	// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder DG.Tweening.Plugins.Core.PathCore.Path::_decoder
	ABSPathDecoder_tC2631579BAB41F64C63A0909E8386B88B45F93D1 * ____decoder_15;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::_changed
	bool ____changed_16;
	// UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.Path::nonLinearDrawWps
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___nonLinearDrawWps_17;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.Path::targetPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___targetPosition_18;
	// System.Nullable`1<UnityEngine.Vector3> DG.Tweening.Plugins.Core.PathCore.Path::lookAtPosition
	Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  ___lookAtPosition_19;
	// UnityEngine.Color DG.Tweening.Plugins.Core.PathCore.Path::gizmoColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___gizmoColor_20;

public:
	inline static int32_t get_offset_of_wpLengths_2() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___wpLengths_2)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_wpLengths_2() const { return ___wpLengths_2; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_wpLengths_2() { return &___wpLengths_2; }
	inline void set_wpLengths_2(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___wpLengths_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wpLengths_2), (void*)value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_subdivisionsXSegment_4() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___subdivisionsXSegment_4)); }
	inline int32_t get_subdivisionsXSegment_4() const { return ___subdivisionsXSegment_4; }
	inline int32_t* get_address_of_subdivisionsXSegment_4() { return &___subdivisionsXSegment_4; }
	inline void set_subdivisionsXSegment_4(int32_t value)
	{
		___subdivisionsXSegment_4 = value;
	}

	inline static int32_t get_offset_of_subdivisions_5() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___subdivisions_5)); }
	inline int32_t get_subdivisions_5() const { return ___subdivisions_5; }
	inline int32_t* get_address_of_subdivisions_5() { return &___subdivisions_5; }
	inline void set_subdivisions_5(int32_t value)
	{
		___subdivisions_5 = value;
	}

	inline static int32_t get_offset_of_wps_6() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___wps_6)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_wps_6() const { return ___wps_6; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_wps_6() { return &___wps_6; }
	inline void set_wps_6(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___wps_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wps_6), (void*)value);
	}

	inline static int32_t get_offset_of_controlPoints_7() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___controlPoints_7)); }
	inline ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884* get_controlPoints_7() const { return ___controlPoints_7; }
	inline ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884** get_address_of_controlPoints_7() { return &___controlPoints_7; }
	inline void set_controlPoints_7(ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884* value)
	{
		___controlPoints_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controlPoints_7), (void*)value);
	}

	inline static int32_t get_offset_of_length_8() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___length_8)); }
	inline float get_length_8() const { return ___length_8; }
	inline float* get_address_of_length_8() { return &___length_8; }
	inline void set_length_8(float value)
	{
		___length_8 = value;
	}

	inline static int32_t get_offset_of_isFinalized_9() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___isFinalized_9)); }
	inline bool get_isFinalized_9() const { return ___isFinalized_9; }
	inline bool* get_address_of_isFinalized_9() { return &___isFinalized_9; }
	inline void set_isFinalized_9(bool value)
	{
		___isFinalized_9 = value;
	}

	inline static int32_t get_offset_of_timesTable_10() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___timesTable_10)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_timesTable_10() const { return ___timesTable_10; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_timesTable_10() { return &___timesTable_10; }
	inline void set_timesTable_10(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___timesTable_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___timesTable_10), (void*)value);
	}

	inline static int32_t get_offset_of_lengthsTable_11() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___lengthsTable_11)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_lengthsTable_11() const { return ___lengthsTable_11; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_lengthsTable_11() { return &___lengthsTable_11; }
	inline void set_lengthsTable_11(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___lengthsTable_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lengthsTable_11), (void*)value);
	}

	inline static int32_t get_offset_of_linearWPIndex_12() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___linearWPIndex_12)); }
	inline int32_t get_linearWPIndex_12() const { return ___linearWPIndex_12; }
	inline int32_t* get_address_of_linearWPIndex_12() { return &___linearWPIndex_12; }
	inline void set_linearWPIndex_12(int32_t value)
	{
		___linearWPIndex_12 = value;
	}

	inline static int32_t get_offset_of__incrementalClone_13() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ____incrementalClone_13)); }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * get__incrementalClone_13() const { return ____incrementalClone_13; }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 ** get_address_of__incrementalClone_13() { return &____incrementalClone_13; }
	inline void set__incrementalClone_13(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * value)
	{
		____incrementalClone_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____incrementalClone_13), (void*)value);
	}

	inline static int32_t get_offset_of__incrementalIndex_14() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ____incrementalIndex_14)); }
	inline int32_t get__incrementalIndex_14() const { return ____incrementalIndex_14; }
	inline int32_t* get_address_of__incrementalIndex_14() { return &____incrementalIndex_14; }
	inline void set__incrementalIndex_14(int32_t value)
	{
		____incrementalIndex_14 = value;
	}

	inline static int32_t get_offset_of__decoder_15() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ____decoder_15)); }
	inline ABSPathDecoder_tC2631579BAB41F64C63A0909E8386B88B45F93D1 * get__decoder_15() const { return ____decoder_15; }
	inline ABSPathDecoder_tC2631579BAB41F64C63A0909E8386B88B45F93D1 ** get_address_of__decoder_15() { return &____decoder_15; }
	inline void set__decoder_15(ABSPathDecoder_tC2631579BAB41F64C63A0909E8386B88B45F93D1 * value)
	{
		____decoder_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____decoder_15), (void*)value);
	}

	inline static int32_t get_offset_of__changed_16() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ____changed_16)); }
	inline bool get__changed_16() const { return ____changed_16; }
	inline bool* get_address_of__changed_16() { return &____changed_16; }
	inline void set__changed_16(bool value)
	{
		____changed_16 = value;
	}

	inline static int32_t get_offset_of_nonLinearDrawWps_17() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___nonLinearDrawWps_17)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_nonLinearDrawWps_17() const { return ___nonLinearDrawWps_17; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_nonLinearDrawWps_17() { return &___nonLinearDrawWps_17; }
	inline void set_nonLinearDrawWps_17(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___nonLinearDrawWps_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nonLinearDrawWps_17), (void*)value);
	}

	inline static int32_t get_offset_of_targetPosition_18() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___targetPosition_18)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_targetPosition_18() const { return ___targetPosition_18; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_targetPosition_18() { return &___targetPosition_18; }
	inline void set_targetPosition_18(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___targetPosition_18 = value;
	}

	inline static int32_t get_offset_of_lookAtPosition_19() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___lookAtPosition_19)); }
	inline Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  get_lookAtPosition_19() const { return ___lookAtPosition_19; }
	inline Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 * get_address_of_lookAtPosition_19() { return &___lookAtPosition_19; }
	inline void set_lookAtPosition_19(Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  value)
	{
		___lookAtPosition_19 = value;
	}

	inline static int32_t get_offset_of_gizmoColor_20() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5, ___gizmoColor_20)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_gizmoColor_20() const { return ___gizmoColor_20; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_gizmoColor_20() { return &___gizmoColor_20; }
	inline void set_gizmoColor_20(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___gizmoColor_20 = value;
	}
};

struct Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_StaticFields
{
public:
	// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder DG.Tweening.Plugins.Core.PathCore.Path::_catmullRomDecoder
	CatmullRomDecoder_t2FA372BD7BB8D2A0CBCBCC812FBCB83703A63F9B * ____catmullRomDecoder_0;
	// DG.Tweening.Plugins.Core.PathCore.LinearDecoder DG.Tweening.Plugins.Core.PathCore.Path::_linearDecoder
	LinearDecoder_tE244F738878748CB8B92F95C686BE0B57B066291 * ____linearDecoder_1;

public:
	inline static int32_t get_offset_of__catmullRomDecoder_0() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_StaticFields, ____catmullRomDecoder_0)); }
	inline CatmullRomDecoder_t2FA372BD7BB8D2A0CBCBCC812FBCB83703A63F9B * get__catmullRomDecoder_0() const { return ____catmullRomDecoder_0; }
	inline CatmullRomDecoder_t2FA372BD7BB8D2A0CBCBCC812FBCB83703A63F9B ** get_address_of__catmullRomDecoder_0() { return &____catmullRomDecoder_0; }
	inline void set__catmullRomDecoder_0(CatmullRomDecoder_t2FA372BD7BB8D2A0CBCBCC812FBCB83703A63F9B * value)
	{
		____catmullRomDecoder_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____catmullRomDecoder_0), (void*)value);
	}

	inline static int32_t get_offset_of__linearDecoder_1() { return static_cast<int32_t>(offsetof(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_StaticFields, ____linearDecoder_1)); }
	inline LinearDecoder_tE244F738878748CB8B92F95C686BE0B57B066291 * get__linearDecoder_1() const { return ____linearDecoder_1; }
	inline LinearDecoder_tE244F738878748CB8B92F95C686BE0B57B066291 ** get_address_of__linearDecoder_1() { return &____linearDecoder_1; }
	inline void set__linearDecoder_1(LinearDecoder_tE244F738878748CB8B92F95C686BE0B57B066291 * value)
	{
		____linearDecoder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____linearDecoder_1), (void*)value);
	}
};


// DG.Tweening.Plugins.Options.PathOptions
struct  PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A 
{
public:
	// DG.Tweening.PathMode DG.Tweening.Plugins.Options.PathOptions::mode
	int32_t ___mode_0;
	// DG.Tweening.Plugins.Options.OrientType DG.Tweening.Plugins.Options.PathOptions::orientType
	int32_t ___orientType_1;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.PathOptions::lockPositionAxis
	int32_t ___lockPositionAxis_2;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.PathOptions::lockRotationAxis
	int32_t ___lockRotationAxis_3;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::isClosedPath
	bool ___isClosedPath_4;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Options.PathOptions::lookAtPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookAtPosition_5;
	// UnityEngine.Transform DG.Tweening.Plugins.Options.PathOptions::lookAtTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___lookAtTransform_6;
	// System.Single DG.Tweening.Plugins.Options.PathOptions::lookAhead
	float ___lookAhead_7;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::hasCustomForwardDirection
	bool ___hasCustomForwardDirection_8;
	// UnityEngine.Quaternion DG.Tweening.Plugins.Options.PathOptions::forward
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___forward_9;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::useLocalPosition
	bool ___useLocalPosition_10;
	// UnityEngine.Transform DG.Tweening.Plugins.Options.PathOptions::parent
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent_11;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::isRigidbody
	bool ___isRigidbody_12;
	// UnityEngine.Quaternion DG.Tweening.Plugins.Options.PathOptions::startupRot
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___startupRot_13;
	// System.Single DG.Tweening.Plugins.Options.PathOptions::startupZRot
	float ___startupZRot_14;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_orientType_1() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___orientType_1)); }
	inline int32_t get_orientType_1() const { return ___orientType_1; }
	inline int32_t* get_address_of_orientType_1() { return &___orientType_1; }
	inline void set_orientType_1(int32_t value)
	{
		___orientType_1 = value;
	}

	inline static int32_t get_offset_of_lockPositionAxis_2() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___lockPositionAxis_2)); }
	inline int32_t get_lockPositionAxis_2() const { return ___lockPositionAxis_2; }
	inline int32_t* get_address_of_lockPositionAxis_2() { return &___lockPositionAxis_2; }
	inline void set_lockPositionAxis_2(int32_t value)
	{
		___lockPositionAxis_2 = value;
	}

	inline static int32_t get_offset_of_lockRotationAxis_3() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___lockRotationAxis_3)); }
	inline int32_t get_lockRotationAxis_3() const { return ___lockRotationAxis_3; }
	inline int32_t* get_address_of_lockRotationAxis_3() { return &___lockRotationAxis_3; }
	inline void set_lockRotationAxis_3(int32_t value)
	{
		___lockRotationAxis_3 = value;
	}

	inline static int32_t get_offset_of_isClosedPath_4() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___isClosedPath_4)); }
	inline bool get_isClosedPath_4() const { return ___isClosedPath_4; }
	inline bool* get_address_of_isClosedPath_4() { return &___isClosedPath_4; }
	inline void set_isClosedPath_4(bool value)
	{
		___isClosedPath_4 = value;
	}

	inline static int32_t get_offset_of_lookAtPosition_5() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___lookAtPosition_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_lookAtPosition_5() const { return ___lookAtPosition_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_lookAtPosition_5() { return &___lookAtPosition_5; }
	inline void set_lookAtPosition_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___lookAtPosition_5 = value;
	}

	inline static int32_t get_offset_of_lookAtTransform_6() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___lookAtTransform_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_lookAtTransform_6() const { return ___lookAtTransform_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_lookAtTransform_6() { return &___lookAtTransform_6; }
	inline void set_lookAtTransform_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___lookAtTransform_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lookAtTransform_6), (void*)value);
	}

	inline static int32_t get_offset_of_lookAhead_7() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___lookAhead_7)); }
	inline float get_lookAhead_7() const { return ___lookAhead_7; }
	inline float* get_address_of_lookAhead_7() { return &___lookAhead_7; }
	inline void set_lookAhead_7(float value)
	{
		___lookAhead_7 = value;
	}

	inline static int32_t get_offset_of_hasCustomForwardDirection_8() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___hasCustomForwardDirection_8)); }
	inline bool get_hasCustomForwardDirection_8() const { return ___hasCustomForwardDirection_8; }
	inline bool* get_address_of_hasCustomForwardDirection_8() { return &___hasCustomForwardDirection_8; }
	inline void set_hasCustomForwardDirection_8(bool value)
	{
		___hasCustomForwardDirection_8 = value;
	}

	inline static int32_t get_offset_of_forward_9() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___forward_9)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_forward_9() const { return ___forward_9; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_forward_9() { return &___forward_9; }
	inline void set_forward_9(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___forward_9 = value;
	}

	inline static int32_t get_offset_of_useLocalPosition_10() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___useLocalPosition_10)); }
	inline bool get_useLocalPosition_10() const { return ___useLocalPosition_10; }
	inline bool* get_address_of_useLocalPosition_10() { return &___useLocalPosition_10; }
	inline void set_useLocalPosition_10(bool value)
	{
		___useLocalPosition_10 = value;
	}

	inline static int32_t get_offset_of_parent_11() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___parent_11)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_parent_11() const { return ___parent_11; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_parent_11() { return &___parent_11; }
	inline void set_parent_11(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___parent_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_11), (void*)value);
	}

	inline static int32_t get_offset_of_isRigidbody_12() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___isRigidbody_12)); }
	inline bool get_isRigidbody_12() const { return ___isRigidbody_12; }
	inline bool* get_address_of_isRigidbody_12() { return &___isRigidbody_12; }
	inline void set_isRigidbody_12(bool value)
	{
		___isRigidbody_12 = value;
	}

	inline static int32_t get_offset_of_startupRot_13() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___startupRot_13)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_startupRot_13() const { return ___startupRot_13; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_startupRot_13() { return &___startupRot_13; }
	inline void set_startupRot_13(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___startupRot_13 = value;
	}

	inline static int32_t get_offset_of_startupZRot_14() { return static_cast<int32_t>(offsetof(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A, ___startupZRot_14)); }
	inline float get_startupZRot_14() const { return ___startupZRot_14; }
	inline float* get_address_of_startupZRot_14() { return &___startupZRot_14; }
	inline void set_startupZRot_14(float value)
	{
		___startupZRot_14 = value;
	}
};

// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.PathOptions
struct PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A_marshaled_pinvoke
{
	int32_t ___mode_0;
	int32_t ___orientType_1;
	int32_t ___lockPositionAxis_2;
	int32_t ___lockRotationAxis_3;
	int32_t ___isClosedPath_4;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookAtPosition_5;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___lookAtTransform_6;
	float ___lookAhead_7;
	int32_t ___hasCustomForwardDirection_8;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___forward_9;
	int32_t ___useLocalPosition_10;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent_11;
	int32_t ___isRigidbody_12;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___startupRot_13;
	float ___startupZRot_14;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.PathOptions
struct PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A_marshaled_com
{
	int32_t ___mode_0;
	int32_t ___orientType_1;
	int32_t ___lockPositionAxis_2;
	int32_t ___lockRotationAxis_3;
	int32_t ___isClosedPath_4;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookAtPosition_5;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___lookAtTransform_6;
	float ___lookAhead_7;
	int32_t ___hasCustomForwardDirection_8;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___forward_9;
	int32_t ___useLocalPosition_10;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent_11;
	int32_t ___isRigidbody_12;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___startupRot_13;
	float ___startupZRot_14;
};

// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Renderer
struct  Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Rigidbody
struct  Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// DG.Tweening.Tween
struct  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941  : public ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76
{
public:
	// System.Single DG.Tweening.Tween::timeScale
	float ___timeScale_4;
	// System.Boolean DG.Tweening.Tween::isBackwards
	bool ___isBackwards_5;
	// System.Object DG.Tweening.Tween::id
	RuntimeObject * ___id_6;
	// System.Object DG.Tweening.Tween::target
	RuntimeObject * ___target_7;
	// DG.Tweening.UpdateType DG.Tweening.Tween::updateType
	int32_t ___updateType_8;
	// System.Boolean DG.Tweening.Tween::isIndependentUpdate
	bool ___isIndependentUpdate_9;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPlay
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onPlay_10;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPause
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onPause_11;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onRewind
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onRewind_12;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onUpdate
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onUpdate_13;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onStepComplete
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onStepComplete_14;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onComplete
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onComplete_15;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onKill
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onKill_16;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.Tween::onWaypointChange
	TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B * ___onWaypointChange_17;
	// System.Boolean DG.Tweening.Tween::isFrom
	bool ___isFrom_18;
	// System.Boolean DG.Tweening.Tween::isBlendable
	bool ___isBlendable_19;
	// System.Boolean DG.Tweening.Tween::isRecyclable
	bool ___isRecyclable_20;
	// System.Boolean DG.Tweening.Tween::isSpeedBased
	bool ___isSpeedBased_21;
	// System.Boolean DG.Tweening.Tween::autoKill
	bool ___autoKill_22;
	// System.Single DG.Tweening.Tween::duration
	float ___duration_23;
	// System.Int32 DG.Tweening.Tween::loops
	int32_t ___loops_24;
	// DG.Tweening.LoopType DG.Tweening.Tween::loopType
	int32_t ___loopType_25;
	// System.Single DG.Tweening.Tween::delay
	float ___delay_26;
	// System.Boolean DG.Tweening.Tween::isRelative
	bool ___isRelative_27;
	// DG.Tweening.Ease DG.Tweening.Tween::easeType
	int32_t ___easeType_28;
	// DG.Tweening.EaseFunction DG.Tweening.Tween::customEase
	EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9 * ___customEase_29;
	// System.Single DG.Tweening.Tween::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_30;
	// System.Single DG.Tweening.Tween::easePeriod
	float ___easePeriod_31;
	// System.Type DG.Tweening.Tween::typeofT1
	Type_t * ___typeofT1_32;
	// System.Type DG.Tweening.Tween::typeofT2
	Type_t * ___typeofT2_33;
	// System.Type DG.Tweening.Tween::typeofTPlugOptions
	Type_t * ___typeofTPlugOptions_34;
	// System.Boolean DG.Tweening.Tween::active
	bool ___active_35;
	// System.Boolean DG.Tweening.Tween::isSequenced
	bool ___isSequenced_36;
	// DG.Tweening.Sequence DG.Tweening.Tween::sequenceParent
	Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * ___sequenceParent_37;
	// System.Int32 DG.Tweening.Tween::activeId
	int32_t ___activeId_38;
	// DG.Tweening.Core.Enums.SpecialStartupMode DG.Tweening.Tween::specialStartupMode
	int32_t ___specialStartupMode_39;
	// System.Boolean DG.Tweening.Tween::creationLocked
	bool ___creationLocked_40;
	// System.Boolean DG.Tweening.Tween::startupDone
	bool ___startupDone_41;
	// System.Boolean DG.Tweening.Tween::playedOnce
	bool ___playedOnce_42;
	// System.Single DG.Tweening.Tween::position
	float ___position_43;
	// System.Single DG.Tweening.Tween::fullDuration
	float ___fullDuration_44;
	// System.Int32 DG.Tweening.Tween::completedLoops
	int32_t ___completedLoops_45;
	// System.Boolean DG.Tweening.Tween::isPlaying
	bool ___isPlaying_46;
	// System.Boolean DG.Tweening.Tween::isComplete
	bool ___isComplete_47;
	// System.Single DG.Tweening.Tween::elapsedDelay
	float ___elapsedDelay_48;
	// System.Boolean DG.Tweening.Tween::delayComplete
	bool ___delayComplete_49;
	// System.Int32 DG.Tweening.Tween::miscInt
	int32_t ___miscInt_50;

public:
	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_isBackwards_5() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isBackwards_5)); }
	inline bool get_isBackwards_5() const { return ___isBackwards_5; }
	inline bool* get_address_of_isBackwards_5() { return &___isBackwards_5; }
	inline void set_isBackwards_5(bool value)
	{
		___isBackwards_5 = value;
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___id_6)); }
	inline RuntimeObject * get_id_6() const { return ___id_6; }
	inline RuntimeObject ** get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(RuntimeObject * value)
	{
		___id_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___id_6), (void*)value);
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___target_7)); }
	inline RuntimeObject * get_target_7() const { return ___target_7; }
	inline RuntimeObject ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(RuntimeObject * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_7), (void*)value);
	}

	inline static int32_t get_offset_of_updateType_8() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___updateType_8)); }
	inline int32_t get_updateType_8() const { return ___updateType_8; }
	inline int32_t* get_address_of_updateType_8() { return &___updateType_8; }
	inline void set_updateType_8(int32_t value)
	{
		___updateType_8 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_9() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isIndependentUpdate_9)); }
	inline bool get_isIndependentUpdate_9() const { return ___isIndependentUpdate_9; }
	inline bool* get_address_of_isIndependentUpdate_9() { return &___isIndependentUpdate_9; }
	inline void set_isIndependentUpdate_9(bool value)
	{
		___isIndependentUpdate_9 = value;
	}

	inline static int32_t get_offset_of_onPlay_10() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onPlay_10)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onPlay_10() const { return ___onPlay_10; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onPlay_10() { return &___onPlay_10; }
	inline void set_onPlay_10(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onPlay_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPlay_10), (void*)value);
	}

	inline static int32_t get_offset_of_onPause_11() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onPause_11)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onPause_11() const { return ___onPause_11; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onPause_11() { return &___onPause_11; }
	inline void set_onPause_11(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onPause_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPause_11), (void*)value);
	}

	inline static int32_t get_offset_of_onRewind_12() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onRewind_12)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onRewind_12() const { return ___onRewind_12; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onRewind_12() { return &___onRewind_12; }
	inline void set_onRewind_12(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onRewind_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onRewind_12), (void*)value);
	}

	inline static int32_t get_offset_of_onUpdate_13() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onUpdate_13)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onUpdate_13() const { return ___onUpdate_13; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onUpdate_13() { return &___onUpdate_13; }
	inline void set_onUpdate_13(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onUpdate_13), (void*)value);
	}

	inline static int32_t get_offset_of_onStepComplete_14() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onStepComplete_14)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onStepComplete_14() const { return ___onStepComplete_14; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onStepComplete_14() { return &___onStepComplete_14; }
	inline void set_onStepComplete_14(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onStepComplete_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStepComplete_14), (void*)value);
	}

	inline static int32_t get_offset_of_onComplete_15() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onComplete_15)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onComplete_15() const { return ___onComplete_15; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onComplete_15() { return &___onComplete_15; }
	inline void set_onComplete_15(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onComplete_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onComplete_15), (void*)value);
	}

	inline static int32_t get_offset_of_onKill_16() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onKill_16)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onKill_16() const { return ___onKill_16; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onKill_16() { return &___onKill_16; }
	inline void set_onKill_16(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onKill_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onKill_16), (void*)value);
	}

	inline static int32_t get_offset_of_onWaypointChange_17() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onWaypointChange_17)); }
	inline TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B * get_onWaypointChange_17() const { return ___onWaypointChange_17; }
	inline TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B ** get_address_of_onWaypointChange_17() { return &___onWaypointChange_17; }
	inline void set_onWaypointChange_17(TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B * value)
	{
		___onWaypointChange_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onWaypointChange_17), (void*)value);
	}

	inline static int32_t get_offset_of_isFrom_18() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isFrom_18)); }
	inline bool get_isFrom_18() const { return ___isFrom_18; }
	inline bool* get_address_of_isFrom_18() { return &___isFrom_18; }
	inline void set_isFrom_18(bool value)
	{
		___isFrom_18 = value;
	}

	inline static int32_t get_offset_of_isBlendable_19() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isBlendable_19)); }
	inline bool get_isBlendable_19() const { return ___isBlendable_19; }
	inline bool* get_address_of_isBlendable_19() { return &___isBlendable_19; }
	inline void set_isBlendable_19(bool value)
	{
		___isBlendable_19 = value;
	}

	inline static int32_t get_offset_of_isRecyclable_20() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isRecyclable_20)); }
	inline bool get_isRecyclable_20() const { return ___isRecyclable_20; }
	inline bool* get_address_of_isRecyclable_20() { return &___isRecyclable_20; }
	inline void set_isRecyclable_20(bool value)
	{
		___isRecyclable_20 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_21() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isSpeedBased_21)); }
	inline bool get_isSpeedBased_21() const { return ___isSpeedBased_21; }
	inline bool* get_address_of_isSpeedBased_21() { return &___isSpeedBased_21; }
	inline void set_isSpeedBased_21(bool value)
	{
		___isSpeedBased_21 = value;
	}

	inline static int32_t get_offset_of_autoKill_22() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___autoKill_22)); }
	inline bool get_autoKill_22() const { return ___autoKill_22; }
	inline bool* get_address_of_autoKill_22() { return &___autoKill_22; }
	inline void set_autoKill_22(bool value)
	{
		___autoKill_22 = value;
	}

	inline static int32_t get_offset_of_duration_23() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___duration_23)); }
	inline float get_duration_23() const { return ___duration_23; }
	inline float* get_address_of_duration_23() { return &___duration_23; }
	inline void set_duration_23(float value)
	{
		___duration_23 = value;
	}

	inline static int32_t get_offset_of_loops_24() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___loops_24)); }
	inline int32_t get_loops_24() const { return ___loops_24; }
	inline int32_t* get_address_of_loops_24() { return &___loops_24; }
	inline void set_loops_24(int32_t value)
	{
		___loops_24 = value;
	}

	inline static int32_t get_offset_of_loopType_25() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___loopType_25)); }
	inline int32_t get_loopType_25() const { return ___loopType_25; }
	inline int32_t* get_address_of_loopType_25() { return &___loopType_25; }
	inline void set_loopType_25(int32_t value)
	{
		___loopType_25 = value;
	}

	inline static int32_t get_offset_of_delay_26() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___delay_26)); }
	inline float get_delay_26() const { return ___delay_26; }
	inline float* get_address_of_delay_26() { return &___delay_26; }
	inline void set_delay_26(float value)
	{
		___delay_26 = value;
	}

	inline static int32_t get_offset_of_isRelative_27() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isRelative_27)); }
	inline bool get_isRelative_27() const { return ___isRelative_27; }
	inline bool* get_address_of_isRelative_27() { return &___isRelative_27; }
	inline void set_isRelative_27(bool value)
	{
		___isRelative_27 = value;
	}

	inline static int32_t get_offset_of_easeType_28() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___easeType_28)); }
	inline int32_t get_easeType_28() const { return ___easeType_28; }
	inline int32_t* get_address_of_easeType_28() { return &___easeType_28; }
	inline void set_easeType_28(int32_t value)
	{
		___easeType_28 = value;
	}

	inline static int32_t get_offset_of_customEase_29() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___customEase_29)); }
	inline EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9 * get_customEase_29() const { return ___customEase_29; }
	inline EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9 ** get_address_of_customEase_29() { return &___customEase_29; }
	inline void set_customEase_29(EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9 * value)
	{
		___customEase_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___customEase_29), (void*)value);
	}

	inline static int32_t get_offset_of_easeOvershootOrAmplitude_30() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___easeOvershootOrAmplitude_30)); }
	inline float get_easeOvershootOrAmplitude_30() const { return ___easeOvershootOrAmplitude_30; }
	inline float* get_address_of_easeOvershootOrAmplitude_30() { return &___easeOvershootOrAmplitude_30; }
	inline void set_easeOvershootOrAmplitude_30(float value)
	{
		___easeOvershootOrAmplitude_30 = value;
	}

	inline static int32_t get_offset_of_easePeriod_31() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___easePeriod_31)); }
	inline float get_easePeriod_31() const { return ___easePeriod_31; }
	inline float* get_address_of_easePeriod_31() { return &___easePeriod_31; }
	inline void set_easePeriod_31(float value)
	{
		___easePeriod_31 = value;
	}

	inline static int32_t get_offset_of_typeofT1_32() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___typeofT1_32)); }
	inline Type_t * get_typeofT1_32() const { return ___typeofT1_32; }
	inline Type_t ** get_address_of_typeofT1_32() { return &___typeofT1_32; }
	inline void set_typeofT1_32(Type_t * value)
	{
		___typeofT1_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeofT1_32), (void*)value);
	}

	inline static int32_t get_offset_of_typeofT2_33() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___typeofT2_33)); }
	inline Type_t * get_typeofT2_33() const { return ___typeofT2_33; }
	inline Type_t ** get_address_of_typeofT2_33() { return &___typeofT2_33; }
	inline void set_typeofT2_33(Type_t * value)
	{
		___typeofT2_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeofT2_33), (void*)value);
	}

	inline static int32_t get_offset_of_typeofTPlugOptions_34() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___typeofTPlugOptions_34)); }
	inline Type_t * get_typeofTPlugOptions_34() const { return ___typeofTPlugOptions_34; }
	inline Type_t ** get_address_of_typeofTPlugOptions_34() { return &___typeofTPlugOptions_34; }
	inline void set_typeofTPlugOptions_34(Type_t * value)
	{
		___typeofTPlugOptions_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeofTPlugOptions_34), (void*)value);
	}

	inline static int32_t get_offset_of_active_35() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___active_35)); }
	inline bool get_active_35() const { return ___active_35; }
	inline bool* get_address_of_active_35() { return &___active_35; }
	inline void set_active_35(bool value)
	{
		___active_35 = value;
	}

	inline static int32_t get_offset_of_isSequenced_36() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isSequenced_36)); }
	inline bool get_isSequenced_36() const { return ___isSequenced_36; }
	inline bool* get_address_of_isSequenced_36() { return &___isSequenced_36; }
	inline void set_isSequenced_36(bool value)
	{
		___isSequenced_36 = value;
	}

	inline static int32_t get_offset_of_sequenceParent_37() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___sequenceParent_37)); }
	inline Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * get_sequenceParent_37() const { return ___sequenceParent_37; }
	inline Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E ** get_address_of_sequenceParent_37() { return &___sequenceParent_37; }
	inline void set_sequenceParent_37(Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * value)
	{
		___sequenceParent_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sequenceParent_37), (void*)value);
	}

	inline static int32_t get_offset_of_activeId_38() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___activeId_38)); }
	inline int32_t get_activeId_38() const { return ___activeId_38; }
	inline int32_t* get_address_of_activeId_38() { return &___activeId_38; }
	inline void set_activeId_38(int32_t value)
	{
		___activeId_38 = value;
	}

	inline static int32_t get_offset_of_specialStartupMode_39() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___specialStartupMode_39)); }
	inline int32_t get_specialStartupMode_39() const { return ___specialStartupMode_39; }
	inline int32_t* get_address_of_specialStartupMode_39() { return &___specialStartupMode_39; }
	inline void set_specialStartupMode_39(int32_t value)
	{
		___specialStartupMode_39 = value;
	}

	inline static int32_t get_offset_of_creationLocked_40() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___creationLocked_40)); }
	inline bool get_creationLocked_40() const { return ___creationLocked_40; }
	inline bool* get_address_of_creationLocked_40() { return &___creationLocked_40; }
	inline void set_creationLocked_40(bool value)
	{
		___creationLocked_40 = value;
	}

	inline static int32_t get_offset_of_startupDone_41() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___startupDone_41)); }
	inline bool get_startupDone_41() const { return ___startupDone_41; }
	inline bool* get_address_of_startupDone_41() { return &___startupDone_41; }
	inline void set_startupDone_41(bool value)
	{
		___startupDone_41 = value;
	}

	inline static int32_t get_offset_of_playedOnce_42() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___playedOnce_42)); }
	inline bool get_playedOnce_42() const { return ___playedOnce_42; }
	inline bool* get_address_of_playedOnce_42() { return &___playedOnce_42; }
	inline void set_playedOnce_42(bool value)
	{
		___playedOnce_42 = value;
	}

	inline static int32_t get_offset_of_position_43() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___position_43)); }
	inline float get_position_43() const { return ___position_43; }
	inline float* get_address_of_position_43() { return &___position_43; }
	inline void set_position_43(float value)
	{
		___position_43 = value;
	}

	inline static int32_t get_offset_of_fullDuration_44() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___fullDuration_44)); }
	inline float get_fullDuration_44() const { return ___fullDuration_44; }
	inline float* get_address_of_fullDuration_44() { return &___fullDuration_44; }
	inline void set_fullDuration_44(float value)
	{
		___fullDuration_44 = value;
	}

	inline static int32_t get_offset_of_completedLoops_45() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___completedLoops_45)); }
	inline int32_t get_completedLoops_45() const { return ___completedLoops_45; }
	inline int32_t* get_address_of_completedLoops_45() { return &___completedLoops_45; }
	inline void set_completedLoops_45(int32_t value)
	{
		___completedLoops_45 = value;
	}

	inline static int32_t get_offset_of_isPlaying_46() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isPlaying_46)); }
	inline bool get_isPlaying_46() const { return ___isPlaying_46; }
	inline bool* get_address_of_isPlaying_46() { return &___isPlaying_46; }
	inline void set_isPlaying_46(bool value)
	{
		___isPlaying_46 = value;
	}

	inline static int32_t get_offset_of_isComplete_47() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isComplete_47)); }
	inline bool get_isComplete_47() const { return ___isComplete_47; }
	inline bool* get_address_of_isComplete_47() { return &___isComplete_47; }
	inline void set_isComplete_47(bool value)
	{
		___isComplete_47 = value;
	}

	inline static int32_t get_offset_of_elapsedDelay_48() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___elapsedDelay_48)); }
	inline float get_elapsedDelay_48() const { return ___elapsedDelay_48; }
	inline float* get_address_of_elapsedDelay_48() { return &___elapsedDelay_48; }
	inline void set_elapsedDelay_48(float value)
	{
		___elapsedDelay_48 = value;
	}

	inline static int32_t get_offset_of_delayComplete_49() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___delayComplete_49)); }
	inline bool get_delayComplete_49() const { return ___delayComplete_49; }
	inline bool* get_address_of_delayComplete_49() { return &___delayComplete_49; }
	inline void set_delayComplete_49(bool value)
	{
		___delayComplete_49 = value;
	}

	inline static int32_t get_offset_of_miscInt_50() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___miscInt_50)); }
	inline int32_t get_miscInt_50() const { return ___miscInt_50; }
	inline int32_t* get_address_of_miscInt_50() { return &___miscInt_50; }
	inline void set_miscInt_50(int32_t value)
	{
		___miscInt_50 = value;
	}
};


// DG.Tweening.TweenCallback
struct  TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// DG.Tweening.Tweener
struct  Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8  : public Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941
{
public:
	// System.Boolean DG.Tweening.Tweener::hasManuallySetStartValue
	bool ___hasManuallySetStartValue_51;
	// System.Boolean DG.Tweening.Tweener::isFromAllowed
	bool ___isFromAllowed_52;

public:
	inline static int32_t get_offset_of_hasManuallySetStartValue_51() { return static_cast<int32_t>(offsetof(Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8, ___hasManuallySetStartValue_51)); }
	inline bool get_hasManuallySetStartValue_51() const { return ___hasManuallySetStartValue_51; }
	inline bool* get_address_of_hasManuallySetStartValue_51() { return &___hasManuallySetStartValue_51; }
	inline void set_hasManuallySetStartValue_51(bool value)
	{
		___hasManuallySetStartValue_51 = value;
	}

	inline static int32_t get_offset_of_isFromAllowed_52() { return static_cast<int32_t>(offsetof(Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8, ___isFromAllowed_52)); }
	inline bool get_isFromAllowed_52() const { return ___isFromAllowed_52; }
	inline bool* get_address_of_isFromAllowed_52() { return &___isFromAllowed_52; }
	inline void set_isFromAllowed_52(bool value)
	{
		___isFromAllowed_52 = value;
	}
};


// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>
struct  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A  : public Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_t0048BB94992A96F02D427E2C26C6E92AC9AECD32 * ___tweenPlugin_59;

public:
	inline static int32_t get_offset_of_startValue_53() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A, ___startValue_53)); }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * get_startValue_53() const { return ___startValue_53; }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 ** get_address_of_startValue_53() { return &___startValue_53; }
	inline void set_startValue_53(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * value)
	{
		___startValue_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___startValue_53), (void*)value);
	}

	inline static int32_t get_offset_of_endValue_54() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A, ___endValue_54)); }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * get_endValue_54() const { return ___endValue_54; }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 ** get_address_of_endValue_54() { return &___endValue_54; }
	inline void set_endValue_54(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * value)
	{
		___endValue_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___endValue_54), (void*)value);
	}

	inline static int32_t get_offset_of_changeValue_55() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A, ___changeValue_55)); }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * get_changeValue_55() const { return ___changeValue_55; }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 ** get_address_of_changeValue_55() { return &___changeValue_55; }
	inline void set_changeValue_55(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * value)
	{
		___changeValue_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___changeValue_55), (void*)value);
	}

	inline static int32_t get_offset_of_plugOptions_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A, ___plugOptions_56)); }
	inline PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A  get_plugOptions_56() const { return ___plugOptions_56; }
	inline PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A * get_address_of_plugOptions_56() { return &___plugOptions_56; }
	inline void set_plugOptions_56(PathOptions_tA9BC8C9E92253DB6E8C05DA62E3745EDFC06E32A  value)
	{
		___plugOptions_56 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___plugOptions_56))->___lookAtTransform_6), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___plugOptions_56))->___parent_11), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_getter_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A, ___getter_57)); }
	inline DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A * get_getter_57() const { return ___getter_57; }
	inline DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A ** get_address_of_getter_57() { return &___getter_57; }
	inline void set_getter_57(DOGetter_1_tE8D2419A0219EB945463B02351F4905C77235E1A * value)
	{
		___getter_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getter_57), (void*)value);
	}

	inline static int32_t get_offset_of_setter_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A, ___setter_58)); }
	inline DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 * get_setter_58() const { return ___setter_58; }
	inline DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 ** get_address_of_setter_58() { return &___setter_58; }
	inline void set_setter_58(DOSetter_1_tAEE38AA9321AE4A0A4A4E5D496EA753BFB746B85 * value)
	{
		___setter_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___setter_58), (void*)value);
	}

	inline static int32_t get_offset_of_tweenPlugin_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A, ___tweenPlugin_59)); }
	inline ABSTweenPlugin_3_t0048BB94992A96F02D427E2C26C6E92AC9AECD32 * get_tweenPlugin_59() const { return ___tweenPlugin_59; }
	inline ABSTweenPlugin_3_t0048BB94992A96F02D427E2C26C6E92AC9AECD32 ** get_address_of_tweenPlugin_59() { return &___tweenPlugin_59; }
	inline void set_tweenPlugin_59(ABSTweenPlugin_3_t0048BB94992A96F02D427E2C26C6E92AC9AECD32 * value)
	{
		___tweenPlugin_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tweenPlugin_59), (void*)value);
	}
};


// DG.Tweening.Core.ABSAnimationComponent
struct  ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// DG.Tweening.UpdateType DG.Tweening.Core.ABSAnimationComponent::updateType
	int32_t ___updateType_4;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::isSpeedBased
	bool ___isSpeedBased_5;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnStart
	bool ___hasOnStart_6;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnPlay
	bool ___hasOnPlay_7;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnUpdate
	bool ___hasOnUpdate_8;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnStepComplete
	bool ___hasOnStepComplete_9;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnComplete
	bool ___hasOnComplete_10;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnTweenCreated
	bool ___hasOnTweenCreated_11;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnRewind
	bool ___hasOnRewind_12;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onStart
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onStart_13;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onPlay
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onPlay_14;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onUpdate
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onUpdate_15;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onStepComplete
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onStepComplete_16;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onComplete
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onComplete_17;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onTweenCreated
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onTweenCreated_18;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onRewind
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onRewind_19;
	// DG.Tweening.Tween DG.Tweening.Core.ABSAnimationComponent::tween
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___tween_20;

public:
	inline static int32_t get_offset_of_updateType_4() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___updateType_4)); }
	inline int32_t get_updateType_4() const { return ___updateType_4; }
	inline int32_t* get_address_of_updateType_4() { return &___updateType_4; }
	inline void set_updateType_4(int32_t value)
	{
		___updateType_4 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_5() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___isSpeedBased_5)); }
	inline bool get_isSpeedBased_5() const { return ___isSpeedBased_5; }
	inline bool* get_address_of_isSpeedBased_5() { return &___isSpeedBased_5; }
	inline void set_isSpeedBased_5(bool value)
	{
		___isSpeedBased_5 = value;
	}

	inline static int32_t get_offset_of_hasOnStart_6() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___hasOnStart_6)); }
	inline bool get_hasOnStart_6() const { return ___hasOnStart_6; }
	inline bool* get_address_of_hasOnStart_6() { return &___hasOnStart_6; }
	inline void set_hasOnStart_6(bool value)
	{
		___hasOnStart_6 = value;
	}

	inline static int32_t get_offset_of_hasOnPlay_7() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___hasOnPlay_7)); }
	inline bool get_hasOnPlay_7() const { return ___hasOnPlay_7; }
	inline bool* get_address_of_hasOnPlay_7() { return &___hasOnPlay_7; }
	inline void set_hasOnPlay_7(bool value)
	{
		___hasOnPlay_7 = value;
	}

	inline static int32_t get_offset_of_hasOnUpdate_8() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___hasOnUpdate_8)); }
	inline bool get_hasOnUpdate_8() const { return ___hasOnUpdate_8; }
	inline bool* get_address_of_hasOnUpdate_8() { return &___hasOnUpdate_8; }
	inline void set_hasOnUpdate_8(bool value)
	{
		___hasOnUpdate_8 = value;
	}

	inline static int32_t get_offset_of_hasOnStepComplete_9() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___hasOnStepComplete_9)); }
	inline bool get_hasOnStepComplete_9() const { return ___hasOnStepComplete_9; }
	inline bool* get_address_of_hasOnStepComplete_9() { return &___hasOnStepComplete_9; }
	inline void set_hasOnStepComplete_9(bool value)
	{
		___hasOnStepComplete_9 = value;
	}

	inline static int32_t get_offset_of_hasOnComplete_10() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___hasOnComplete_10)); }
	inline bool get_hasOnComplete_10() const { return ___hasOnComplete_10; }
	inline bool* get_address_of_hasOnComplete_10() { return &___hasOnComplete_10; }
	inline void set_hasOnComplete_10(bool value)
	{
		___hasOnComplete_10 = value;
	}

	inline static int32_t get_offset_of_hasOnTweenCreated_11() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___hasOnTweenCreated_11)); }
	inline bool get_hasOnTweenCreated_11() const { return ___hasOnTweenCreated_11; }
	inline bool* get_address_of_hasOnTweenCreated_11() { return &___hasOnTweenCreated_11; }
	inline void set_hasOnTweenCreated_11(bool value)
	{
		___hasOnTweenCreated_11 = value;
	}

	inline static int32_t get_offset_of_hasOnRewind_12() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___hasOnRewind_12)); }
	inline bool get_hasOnRewind_12() const { return ___hasOnRewind_12; }
	inline bool* get_address_of_hasOnRewind_12() { return &___hasOnRewind_12; }
	inline void set_hasOnRewind_12(bool value)
	{
		___hasOnRewind_12 = value;
	}

	inline static int32_t get_offset_of_onStart_13() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___onStart_13)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onStart_13() const { return ___onStart_13; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onStart_13() { return &___onStart_13; }
	inline void set_onStart_13(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onStart_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStart_13), (void*)value);
	}

	inline static int32_t get_offset_of_onPlay_14() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___onPlay_14)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onPlay_14() const { return ___onPlay_14; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onPlay_14() { return &___onPlay_14; }
	inline void set_onPlay_14(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onPlay_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPlay_14), (void*)value);
	}

	inline static int32_t get_offset_of_onUpdate_15() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___onUpdate_15)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onUpdate_15() const { return ___onUpdate_15; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onUpdate_15() { return &___onUpdate_15; }
	inline void set_onUpdate_15(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onUpdate_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onUpdate_15), (void*)value);
	}

	inline static int32_t get_offset_of_onStepComplete_16() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___onStepComplete_16)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onStepComplete_16() const { return ___onStepComplete_16; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onStepComplete_16() { return &___onStepComplete_16; }
	inline void set_onStepComplete_16(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onStepComplete_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStepComplete_16), (void*)value);
	}

	inline static int32_t get_offset_of_onComplete_17() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___onComplete_17)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onComplete_17() const { return ___onComplete_17; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onComplete_17() { return &___onComplete_17; }
	inline void set_onComplete_17(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onComplete_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onComplete_17), (void*)value);
	}

	inline static int32_t get_offset_of_onTweenCreated_18() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___onTweenCreated_18)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onTweenCreated_18() const { return ___onTweenCreated_18; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onTweenCreated_18() { return &___onTweenCreated_18; }
	inline void set_onTweenCreated_18(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onTweenCreated_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onTweenCreated_18), (void*)value);
	}

	inline static int32_t get_offset_of_onRewind_19() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___onRewind_19)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onRewind_19() const { return ___onRewind_19; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onRewind_19() { return &___onRewind_19; }
	inline void set_onRewind_19(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onRewind_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onRewind_19), (void*)value);
	}

	inline static int32_t get_offset_of_tween_20() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___tween_20)); }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * get_tween_20() const { return ___tween_20; }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 ** get_address_of_tween_20() { return &___tween_20; }
	inline void set_tween_20(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * value)
	{
		___tween_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tween_20), (void*)value);
	}
};


// DG.Tweening.DOTweenVisualManager
struct  DOTweenVisualManager_t4164D354E77A1A82EB58F804D71C15482404E25C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// DG.Tweening.Core.VisualManagerPreset DG.Tweening.DOTweenVisualManager::preset
	int32_t ___preset_4;
	// DG.Tweening.Core.OnEnableBehaviour DG.Tweening.DOTweenVisualManager::onEnableBehaviour
	int32_t ___onEnableBehaviour_5;
	// DG.Tweening.Core.OnDisableBehaviour DG.Tweening.DOTweenVisualManager::onDisableBehaviour
	int32_t ___onDisableBehaviour_6;
	// System.Boolean DG.Tweening.DOTweenVisualManager::_requiresRestartFromSpawnPoint
	bool ____requiresRestartFromSpawnPoint_7;
	// DG.Tweening.Core.ABSAnimationComponent DG.Tweening.DOTweenVisualManager::_animComponent
	ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * ____animComponent_8;

public:
	inline static int32_t get_offset_of_preset_4() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t4164D354E77A1A82EB58F804D71C15482404E25C, ___preset_4)); }
	inline int32_t get_preset_4() const { return ___preset_4; }
	inline int32_t* get_address_of_preset_4() { return &___preset_4; }
	inline void set_preset_4(int32_t value)
	{
		___preset_4 = value;
	}

	inline static int32_t get_offset_of_onEnableBehaviour_5() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t4164D354E77A1A82EB58F804D71C15482404E25C, ___onEnableBehaviour_5)); }
	inline int32_t get_onEnableBehaviour_5() const { return ___onEnableBehaviour_5; }
	inline int32_t* get_address_of_onEnableBehaviour_5() { return &___onEnableBehaviour_5; }
	inline void set_onEnableBehaviour_5(int32_t value)
	{
		___onEnableBehaviour_5 = value;
	}

	inline static int32_t get_offset_of_onDisableBehaviour_6() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t4164D354E77A1A82EB58F804D71C15482404E25C, ___onDisableBehaviour_6)); }
	inline int32_t get_onDisableBehaviour_6() const { return ___onDisableBehaviour_6; }
	inline int32_t* get_address_of_onDisableBehaviour_6() { return &___onDisableBehaviour_6; }
	inline void set_onDisableBehaviour_6(int32_t value)
	{
		___onDisableBehaviour_6 = value;
	}

	inline static int32_t get_offset_of__requiresRestartFromSpawnPoint_7() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t4164D354E77A1A82EB58F804D71C15482404E25C, ____requiresRestartFromSpawnPoint_7)); }
	inline bool get__requiresRestartFromSpawnPoint_7() const { return ____requiresRestartFromSpawnPoint_7; }
	inline bool* get_address_of__requiresRestartFromSpawnPoint_7() { return &____requiresRestartFromSpawnPoint_7; }
	inline void set__requiresRestartFromSpawnPoint_7(bool value)
	{
		____requiresRestartFromSpawnPoint_7 = value;
	}

	inline static int32_t get_offset_of__animComponent_8() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t4164D354E77A1A82EB58F804D71C15482404E25C, ____animComponent_8)); }
	inline ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * get__animComponent_8() const { return ____animComponent_8; }
	inline ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 ** get_address_of__animComponent_8() { return &____animComponent_8; }
	inline void set__animComponent_8(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * value)
	{
		____animComponent_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____animComponent_8), (void*)value);
	}
};


// DG.Tweening.DOTweenPath
struct  DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989  : public ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7
{
public:
	// System.Single DG.Tweening.DOTweenPath::delay
	float ___delay_21;
	// System.Single DG.Tweening.DOTweenPath::duration
	float ___duration_22;
	// DG.Tweening.Ease DG.Tweening.DOTweenPath::easeType
	int32_t ___easeType_23;
	// UnityEngine.AnimationCurve DG.Tweening.DOTweenPath::easeCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___easeCurve_24;
	// System.Int32 DG.Tweening.DOTweenPath::loops
	int32_t ___loops_25;
	// System.String DG.Tweening.DOTweenPath::id
	String_t* ___id_26;
	// DG.Tweening.LoopType DG.Tweening.DOTweenPath::loopType
	int32_t ___loopType_27;
	// DG.Tweening.Plugins.Options.OrientType DG.Tweening.DOTweenPath::orientType
	int32_t ___orientType_28;
	// UnityEngine.Transform DG.Tweening.DOTweenPath::lookAtTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___lookAtTransform_29;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::lookAtPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookAtPosition_30;
	// System.Single DG.Tweening.DOTweenPath::lookAhead
	float ___lookAhead_31;
	// System.Boolean DG.Tweening.DOTweenPath::autoPlay
	bool ___autoPlay_32;
	// System.Boolean DG.Tweening.DOTweenPath::autoKill
	bool ___autoKill_33;
	// System.Boolean DG.Tweening.DOTweenPath::relative
	bool ___relative_34;
	// System.Boolean DG.Tweening.DOTweenPath::isLocal
	bool ___isLocal_35;
	// System.Boolean DG.Tweening.DOTweenPath::isClosedPath
	bool ___isClosedPath_36;
	// System.Int32 DG.Tweening.DOTweenPath::pathResolution
	int32_t ___pathResolution_37;
	// DG.Tweening.PathMode DG.Tweening.DOTweenPath::pathMode
	int32_t ___pathMode_38;
	// DG.Tweening.AxisConstraint DG.Tweening.DOTweenPath::lockRotation
	int32_t ___lockRotation_39;
	// System.Boolean DG.Tweening.DOTweenPath::assignForwardAndUp
	bool ___assignForwardAndUp_40;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::forwardDirection
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardDirection_41;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::upDirection
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upDirection_42;
	// System.Boolean DG.Tweening.DOTweenPath::tweenRigidbody
	bool ___tweenRigidbody_43;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> DG.Tweening.DOTweenPath::wps
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___wps_44;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> DG.Tweening.DOTweenPath::fullWps
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___fullWps_45;
	// DG.Tweening.Plugins.Core.PathCore.Path DG.Tweening.DOTweenPath::path
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___path_46;
	// DG.Tweening.DOTweenInspectorMode DG.Tweening.DOTweenPath::inspectorMode
	int32_t ___inspectorMode_47;
	// DG.Tweening.PathType DG.Tweening.DOTweenPath::pathType
	int32_t ___pathType_48;
	// DG.Tweening.HandlesType DG.Tweening.DOTweenPath::handlesType
	int32_t ___handlesType_49;
	// System.Boolean DG.Tweening.DOTweenPath::livePreview
	bool ___livePreview_50;
	// DG.Tweening.HandlesDrawMode DG.Tweening.DOTweenPath::handlesDrawMode
	int32_t ___handlesDrawMode_51;
	// System.Single DG.Tweening.DOTweenPath::perspectiveHandleSize
	float ___perspectiveHandleSize_52;
	// System.Boolean DG.Tweening.DOTweenPath::showIndexes
	bool ___showIndexes_53;
	// System.Boolean DG.Tweening.DOTweenPath::showWpLength
	bool ___showWpLength_54;
	// UnityEngine.Color DG.Tweening.DOTweenPath::pathColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___pathColor_55;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::lastSrcPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lastSrcPosition_56;
	// System.Boolean DG.Tweening.DOTweenPath::wpsDropdown
	bool ___wpsDropdown_57;
	// System.Single DG.Tweening.DOTweenPath::dropToFloorOffset
	float ___dropToFloorOffset_58;

public:
	inline static int32_t get_offset_of_delay_21() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___delay_21)); }
	inline float get_delay_21() const { return ___delay_21; }
	inline float* get_address_of_delay_21() { return &___delay_21; }
	inline void set_delay_21(float value)
	{
		___delay_21 = value;
	}

	inline static int32_t get_offset_of_duration_22() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___duration_22)); }
	inline float get_duration_22() const { return ___duration_22; }
	inline float* get_address_of_duration_22() { return &___duration_22; }
	inline void set_duration_22(float value)
	{
		___duration_22 = value;
	}

	inline static int32_t get_offset_of_easeType_23() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___easeType_23)); }
	inline int32_t get_easeType_23() const { return ___easeType_23; }
	inline int32_t* get_address_of_easeType_23() { return &___easeType_23; }
	inline void set_easeType_23(int32_t value)
	{
		___easeType_23 = value;
	}

	inline static int32_t get_offset_of_easeCurve_24() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___easeCurve_24)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_easeCurve_24() const { return ___easeCurve_24; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_easeCurve_24() { return &___easeCurve_24; }
	inline void set_easeCurve_24(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___easeCurve_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___easeCurve_24), (void*)value);
	}

	inline static int32_t get_offset_of_loops_25() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___loops_25)); }
	inline int32_t get_loops_25() const { return ___loops_25; }
	inline int32_t* get_address_of_loops_25() { return &___loops_25; }
	inline void set_loops_25(int32_t value)
	{
		___loops_25 = value;
	}

	inline static int32_t get_offset_of_id_26() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___id_26)); }
	inline String_t* get_id_26() const { return ___id_26; }
	inline String_t** get_address_of_id_26() { return &___id_26; }
	inline void set_id_26(String_t* value)
	{
		___id_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___id_26), (void*)value);
	}

	inline static int32_t get_offset_of_loopType_27() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___loopType_27)); }
	inline int32_t get_loopType_27() const { return ___loopType_27; }
	inline int32_t* get_address_of_loopType_27() { return &___loopType_27; }
	inline void set_loopType_27(int32_t value)
	{
		___loopType_27 = value;
	}

	inline static int32_t get_offset_of_orientType_28() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___orientType_28)); }
	inline int32_t get_orientType_28() const { return ___orientType_28; }
	inline int32_t* get_address_of_orientType_28() { return &___orientType_28; }
	inline void set_orientType_28(int32_t value)
	{
		___orientType_28 = value;
	}

	inline static int32_t get_offset_of_lookAtTransform_29() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___lookAtTransform_29)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_lookAtTransform_29() const { return ___lookAtTransform_29; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_lookAtTransform_29() { return &___lookAtTransform_29; }
	inline void set_lookAtTransform_29(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___lookAtTransform_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lookAtTransform_29), (void*)value);
	}

	inline static int32_t get_offset_of_lookAtPosition_30() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___lookAtPosition_30)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_lookAtPosition_30() const { return ___lookAtPosition_30; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_lookAtPosition_30() { return &___lookAtPosition_30; }
	inline void set_lookAtPosition_30(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___lookAtPosition_30 = value;
	}

	inline static int32_t get_offset_of_lookAhead_31() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___lookAhead_31)); }
	inline float get_lookAhead_31() const { return ___lookAhead_31; }
	inline float* get_address_of_lookAhead_31() { return &___lookAhead_31; }
	inline void set_lookAhead_31(float value)
	{
		___lookAhead_31 = value;
	}

	inline static int32_t get_offset_of_autoPlay_32() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___autoPlay_32)); }
	inline bool get_autoPlay_32() const { return ___autoPlay_32; }
	inline bool* get_address_of_autoPlay_32() { return &___autoPlay_32; }
	inline void set_autoPlay_32(bool value)
	{
		___autoPlay_32 = value;
	}

	inline static int32_t get_offset_of_autoKill_33() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___autoKill_33)); }
	inline bool get_autoKill_33() const { return ___autoKill_33; }
	inline bool* get_address_of_autoKill_33() { return &___autoKill_33; }
	inline void set_autoKill_33(bool value)
	{
		___autoKill_33 = value;
	}

	inline static int32_t get_offset_of_relative_34() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___relative_34)); }
	inline bool get_relative_34() const { return ___relative_34; }
	inline bool* get_address_of_relative_34() { return &___relative_34; }
	inline void set_relative_34(bool value)
	{
		___relative_34 = value;
	}

	inline static int32_t get_offset_of_isLocal_35() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___isLocal_35)); }
	inline bool get_isLocal_35() const { return ___isLocal_35; }
	inline bool* get_address_of_isLocal_35() { return &___isLocal_35; }
	inline void set_isLocal_35(bool value)
	{
		___isLocal_35 = value;
	}

	inline static int32_t get_offset_of_isClosedPath_36() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___isClosedPath_36)); }
	inline bool get_isClosedPath_36() const { return ___isClosedPath_36; }
	inline bool* get_address_of_isClosedPath_36() { return &___isClosedPath_36; }
	inline void set_isClosedPath_36(bool value)
	{
		___isClosedPath_36 = value;
	}

	inline static int32_t get_offset_of_pathResolution_37() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___pathResolution_37)); }
	inline int32_t get_pathResolution_37() const { return ___pathResolution_37; }
	inline int32_t* get_address_of_pathResolution_37() { return &___pathResolution_37; }
	inline void set_pathResolution_37(int32_t value)
	{
		___pathResolution_37 = value;
	}

	inline static int32_t get_offset_of_pathMode_38() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___pathMode_38)); }
	inline int32_t get_pathMode_38() const { return ___pathMode_38; }
	inline int32_t* get_address_of_pathMode_38() { return &___pathMode_38; }
	inline void set_pathMode_38(int32_t value)
	{
		___pathMode_38 = value;
	}

	inline static int32_t get_offset_of_lockRotation_39() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___lockRotation_39)); }
	inline int32_t get_lockRotation_39() const { return ___lockRotation_39; }
	inline int32_t* get_address_of_lockRotation_39() { return &___lockRotation_39; }
	inline void set_lockRotation_39(int32_t value)
	{
		___lockRotation_39 = value;
	}

	inline static int32_t get_offset_of_assignForwardAndUp_40() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___assignForwardAndUp_40)); }
	inline bool get_assignForwardAndUp_40() const { return ___assignForwardAndUp_40; }
	inline bool* get_address_of_assignForwardAndUp_40() { return &___assignForwardAndUp_40; }
	inline void set_assignForwardAndUp_40(bool value)
	{
		___assignForwardAndUp_40 = value;
	}

	inline static int32_t get_offset_of_forwardDirection_41() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___forwardDirection_41)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardDirection_41() const { return ___forwardDirection_41; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardDirection_41() { return &___forwardDirection_41; }
	inline void set_forwardDirection_41(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardDirection_41 = value;
	}

	inline static int32_t get_offset_of_upDirection_42() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___upDirection_42)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upDirection_42() const { return ___upDirection_42; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upDirection_42() { return &___upDirection_42; }
	inline void set_upDirection_42(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upDirection_42 = value;
	}

	inline static int32_t get_offset_of_tweenRigidbody_43() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___tweenRigidbody_43)); }
	inline bool get_tweenRigidbody_43() const { return ___tweenRigidbody_43; }
	inline bool* get_address_of_tweenRigidbody_43() { return &___tweenRigidbody_43; }
	inline void set_tweenRigidbody_43(bool value)
	{
		___tweenRigidbody_43 = value;
	}

	inline static int32_t get_offset_of_wps_44() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___wps_44)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_wps_44() const { return ___wps_44; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_wps_44() { return &___wps_44; }
	inline void set_wps_44(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___wps_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___wps_44), (void*)value);
	}

	inline static int32_t get_offset_of_fullWps_45() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___fullWps_45)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_fullWps_45() const { return ___fullWps_45; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_fullWps_45() { return &___fullWps_45; }
	inline void set_fullWps_45(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___fullWps_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fullWps_45), (void*)value);
	}

	inline static int32_t get_offset_of_path_46() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___path_46)); }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * get_path_46() const { return ___path_46; }
	inline Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 ** get_address_of_path_46() { return &___path_46; }
	inline void set_path_46(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * value)
	{
		___path_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_46), (void*)value);
	}

	inline static int32_t get_offset_of_inspectorMode_47() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___inspectorMode_47)); }
	inline int32_t get_inspectorMode_47() const { return ___inspectorMode_47; }
	inline int32_t* get_address_of_inspectorMode_47() { return &___inspectorMode_47; }
	inline void set_inspectorMode_47(int32_t value)
	{
		___inspectorMode_47 = value;
	}

	inline static int32_t get_offset_of_pathType_48() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___pathType_48)); }
	inline int32_t get_pathType_48() const { return ___pathType_48; }
	inline int32_t* get_address_of_pathType_48() { return &___pathType_48; }
	inline void set_pathType_48(int32_t value)
	{
		___pathType_48 = value;
	}

	inline static int32_t get_offset_of_handlesType_49() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___handlesType_49)); }
	inline int32_t get_handlesType_49() const { return ___handlesType_49; }
	inline int32_t* get_address_of_handlesType_49() { return &___handlesType_49; }
	inline void set_handlesType_49(int32_t value)
	{
		___handlesType_49 = value;
	}

	inline static int32_t get_offset_of_livePreview_50() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___livePreview_50)); }
	inline bool get_livePreview_50() const { return ___livePreview_50; }
	inline bool* get_address_of_livePreview_50() { return &___livePreview_50; }
	inline void set_livePreview_50(bool value)
	{
		___livePreview_50 = value;
	}

	inline static int32_t get_offset_of_handlesDrawMode_51() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___handlesDrawMode_51)); }
	inline int32_t get_handlesDrawMode_51() const { return ___handlesDrawMode_51; }
	inline int32_t* get_address_of_handlesDrawMode_51() { return &___handlesDrawMode_51; }
	inline void set_handlesDrawMode_51(int32_t value)
	{
		___handlesDrawMode_51 = value;
	}

	inline static int32_t get_offset_of_perspectiveHandleSize_52() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___perspectiveHandleSize_52)); }
	inline float get_perspectiveHandleSize_52() const { return ___perspectiveHandleSize_52; }
	inline float* get_address_of_perspectiveHandleSize_52() { return &___perspectiveHandleSize_52; }
	inline void set_perspectiveHandleSize_52(float value)
	{
		___perspectiveHandleSize_52 = value;
	}

	inline static int32_t get_offset_of_showIndexes_53() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___showIndexes_53)); }
	inline bool get_showIndexes_53() const { return ___showIndexes_53; }
	inline bool* get_address_of_showIndexes_53() { return &___showIndexes_53; }
	inline void set_showIndexes_53(bool value)
	{
		___showIndexes_53 = value;
	}

	inline static int32_t get_offset_of_showWpLength_54() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___showWpLength_54)); }
	inline bool get_showWpLength_54() const { return ___showWpLength_54; }
	inline bool* get_address_of_showWpLength_54() { return &___showWpLength_54; }
	inline void set_showWpLength_54(bool value)
	{
		___showWpLength_54 = value;
	}

	inline static int32_t get_offset_of_pathColor_55() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___pathColor_55)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_pathColor_55() const { return ___pathColor_55; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_pathColor_55() { return &___pathColor_55; }
	inline void set_pathColor_55(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___pathColor_55 = value;
	}

	inline static int32_t get_offset_of_lastSrcPosition_56() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___lastSrcPosition_56)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_lastSrcPosition_56() const { return ___lastSrcPosition_56; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_lastSrcPosition_56() { return &___lastSrcPosition_56; }
	inline void set_lastSrcPosition_56(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___lastSrcPosition_56 = value;
	}

	inline static int32_t get_offset_of_wpsDropdown_57() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___wpsDropdown_57)); }
	inline bool get_wpsDropdown_57() const { return ___wpsDropdown_57; }
	inline bool* get_address_of_wpsDropdown_57() { return &___wpsDropdown_57; }
	inline void set_wpsDropdown_57(bool value)
	{
		___wpsDropdown_57 = value;
	}

	inline static int32_t get_offset_of_dropToFloorOffset_58() { return static_cast<int32_t>(offsetof(DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989, ___dropToFloorOffset_58)); }
	inline float get_dropToFloorOffset_58() const { return ___dropToFloorOffset_58; }
	inline float* get_address_of_dropToFloorOffset_58() { return &___dropToFloorOffset_58; }
	inline void set_dropToFloorOffset_58(float value)
	{
		___dropToFloorOffset_58 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  m_Items[1];

public:
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		m_Items[index] = value;
	}
};
// DG.Tweening.Plugins.Core.PathCore.ControlPoint[]
struct ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD  m_Items[1];

public:
	inline ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  m_Items[1];

public:
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  value)
	{
		m_Items[index] = value;
	}
};


// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void System.Nullable`1<UnityEngine.Vector3>::.ctor(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830_gshared (Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetDelay<System.Object>(!!0,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetDelay_TisRuntimeObject_mAD79DB59BFAAA02166DDBB7C147C9E310E34A257_gshared (RuntimeObject * ___t0, float ___delay1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<System.Object>(!!0,System.Int32,DG.Tweening.LoopType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetLoops_TisRuntimeObject_m65FD37B61EFA696D5A4EB23ABA3C93444E4F9B4B_gshared (RuntimeObject * ___t0, int32_t ___loops1, int32_t ___loopType2, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetAutoKill<System.Object>(!!0,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetAutoKill_TisRuntimeObject_m0398884E97953A082C222383BE97404B2BF254AD_gshared (RuntimeObject * ___t0, bool ___autoKillOnCompletion1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetUpdate<System.Object>(!!0,DG.Tweening.UpdateType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetUpdate_TisRuntimeObject_mE74D504CEE2AB6BBE1A7C72B29AC8C8DD5188824_gshared (RuntimeObject * ___t0, int32_t ___updateType1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnKill<System.Object>(!!0,DG.Tweening.TweenCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_OnKill_TisRuntimeObject_m3D44E377E810E472132FC1F0F405DCA565E6DCAE_gshared (RuntimeObject * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetSpeedBased<System.Object>(!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetSpeedBased_TisRuntimeObject_m808BBC2133B2235E29198F54D5146FEF34ABB8D7_gshared (RuntimeObject * ___t0, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<System.Object>(!!0,UnityEngine.AnimationCurve)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetEase_TisRuntimeObject_mDFD21B66D89B1600A547B64E3F973F63D9F42B98_gshared (RuntimeObject * ___t0, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___animCurve1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<System.Object>(!!0,DG.Tweening.Ease)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetEase_TisRuntimeObject_m17D546F0CC41676B108187269B5BB10959EFA2A4_gshared (RuntimeObject * ___t0, int32_t ___ease1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetId<System.Object>(!!0,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetId_TisRuntimeObject_m72E09063B9C56E443140E7E01F32618F28131087_gshared (RuntimeObject * ___t0, RuntimeObject * ___id1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnStart<System.Object>(!!0,DG.Tweening.TweenCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_OnStart_TisRuntimeObject_mE8EF10BC4D36C32FCEFB7DB33732E45775D2A986_gshared (RuntimeObject * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnPlay<System.Object>(!!0,DG.Tweening.TweenCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_OnPlay_TisRuntimeObject_mC8DCD120A7EF0DCBF14EDF1852CF047A6632BBC9_gshared (RuntimeObject * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnUpdate<System.Object>(!!0,DG.Tweening.TweenCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_OnUpdate_TisRuntimeObject_m9833BEBF80F4D5CFBD371351F053310B5758AE99_gshared (RuntimeObject * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnStepComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_OnStepComplete_TisRuntimeObject_mA199BA498C4CD73FBE20E1CA62E6F658AE492D0A_gshared (RuntimeObject * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_OnComplete_TisRuntimeObject_m23E1F90FEE2A0CA4D04C244B7A7A83A541855DC0_gshared (RuntimeObject * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnRewind<System.Object>(!!0,DG.Tweening.TweenCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_OnRewind_TisRuntimeObject_mBFA5258FEF4DA74AC2ABC3C8025506F1EB866AF0_gshared (RuntimeObject * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenExtensions::Play<System.Object>(!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenExtensions_Play_TisRuntimeObject_m3E3C5EA201897EC0A7D33C27152A94B0C6BA54AD_gshared (RuntimeObject * ___t0, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenExtensions::Pause<System.Object>(!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenExtensions_Pause_TisRuntimeObject_mB5DB00EFAF4AD1C64193EBAD6A86680274AD57EA_gshared (RuntimeObject * ___t0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* List_1_ToArray_mCFFA6AFA30A20FB2FFC3846AD114DF7BC21BFFBC_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method);
// System.Void System.Nullable`1<UnityEngine.Color>::.ctor(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Nullable_1__ctor_mABF3CCAC28954E932B026C84D4C0D0F7BF2662F9_gshared (Nullable_1_tA06400BA484934D9CEBAF66D0E71C822EF09A498 * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
inline int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, const RuntimeMethod*))List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline)(__this, method);
}
// System.Void DG.Tweening.Plugins.Core.PathCore.Path::AssignDecoder(DG.Tweening.PathType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Path_AssignDecoder_mD999EA9D655C222C11AF74D1F24EBD51AFC37097 (Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * __this, int32_t ___pathType0, const RuntimeMethod* method);
// System.Void DG.Tweening.TweenCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662 (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Add(!0)
inline void List_1_Add_m49BFDA3CA404A1733B6D6CAAE6F254AD4076414B (List_1_t7A220E9BB7539479C818543CE3CBDF252554FD58 * __this, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t7A220E9BB7539479C818543CE3CBDF252554FD58 *, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Void DG.Tweening.DOTweenPath::ReEvaluateRelativeTween()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenPath_ReEvaluateRelativeTween_mAEF73A49F484B2A0EB04B716D4445AAA171559C9 (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ShortcutExtensions_DOPath_m01EDCCA0EE21B7E554FF28BEA409198E50CFD1FC (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target0, Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___path1, float ___duration2, int32_t ___pathMode3, const RuntimeMethod* method);
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,System.Boolean,DG.Tweening.AxisConstraint,DG.Tweening.AxisConstraint)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_SetOptions_m232500C0CAFAD89AFE0A437A6015F2E59ED3D8FF (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, bool ___closePath1, int32_t ___lockPosition2, int32_t ___lockRotation3, const RuntimeMethod* method);
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOLocalPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ShortcutExtensions_DOLocalPath_mE0136EA4E054DF88A8322A664D3CCEFAFABEAE4C (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target0, Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___path1, float ___duration2, int32_t ___pathMode3, const RuntimeMethod* method);
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOPath(UnityEngine.Transform,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ShortcutExtensions_DOPath_m7AB84619455A541B79164E1D270A5AB511533B24 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___path1, float ___duration2, int32_t ___pathMode3, const RuntimeMethod* method);
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOLocalPath(UnityEngine.Transform,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ShortcutExtensions_DOLocalPath_m28185CD9787640FBB679A0B46F144E1BB0DB43F8 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * ___path1, float ___duration2, int32_t ___pathMode3, const RuntimeMethod* method);
// System.Void System.Nullable`1<UnityEngine.Vector3>::.ctor(!0)
inline void Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830 (Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method)
{
	((  void (*) (Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , const RuntimeMethod*))Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830_gshared)(__this, ___value0, method);
}
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetLookAt(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,UnityEngine.Transform,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Vector3>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_SetLookAt_m3BFF34B7765FCB4B3DDE2B3B90869FE9086DDD01 (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___lookAtTransform1, Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  ___forwardDirection2, Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  ___up3, const RuntimeMethod* method);
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetLookAt(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,UnityEngine.Vector3,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Vector3>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_SetLookAt_m31B2E7ECAADA53B8C6EDA9BC760AFA2F6155B382 (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookAtPosition1, Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  ___forwardDirection2, Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  ___up3, const RuntimeMethod* method);
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetLookAt(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,System.Single,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Vector3>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_SetLookAt_mAD3CC7329671C9A6D0FA41A9AB2D3B236769C2AF (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, float ___lookAhead1, Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  ___forwardDirection2, Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  ___up3, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetDelay<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,System.Single)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m6B8AB1944A21B4D442E2E4B39575F7C6F0F3E460 (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, float ___delay1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, float, const RuntimeMethod*))TweenSettingsExtensions_SetDelay_TisRuntimeObject_mAD79DB59BFAAA02166DDBB7C147C9E310E34A257_gshared)(___t0, ___delay1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,System.Int32,DG.Tweening.LoopType)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mF7A6FCD23D0641A47C66CE77CC1A262C48ED4B6E (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, int32_t ___loops1, int32_t ___loopType2, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, int32_t, int32_t, const RuntimeMethod*))TweenSettingsExtensions_SetLoops_TisRuntimeObject_m65FD37B61EFA696D5A4EB23ABA3C93444E4F9B4B_gshared)(___t0, ___loops1, ___loopType2, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetAutoKill<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,System.Boolean)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_SetAutoKill_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mC6B4A0088E7F88B4B988455ACD13CECFD497C22D (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, bool ___autoKillOnCompletion1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, bool, const RuntimeMethod*))TweenSettingsExtensions_SetAutoKill_TisRuntimeObject_m0398884E97953A082C222383BE97404B2BF254AD_gshared)(___t0, ___autoKillOnCompletion1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetUpdate<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.UpdateType)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_SetUpdate_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m0005FC778EB58D20C9D21947236A37B707418F83 (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, int32_t ___updateType1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, int32_t, const RuntimeMethod*))TweenSettingsExtensions_SetUpdate_TisRuntimeObject_mE74D504CEE2AB6BBE1A7C72B29AC8C8DD5188824_gshared)(___t0, ___updateType1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::OnKill<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_OnKill_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m47F222AC973A667D579E0F8B5BE97093A2E57200 (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *, const RuntimeMethod*))TweenSettingsExtensions_OnKill_TisRuntimeObject_m3D44E377E810E472132FC1F0F405DCA565E6DCAE_gshared)(___t0, ___action1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetSpeedBased<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_SetSpeedBased_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m687552F99CBA5F1DE88F60CA85E91FC81767E770 (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, const RuntimeMethod*))TweenSettingsExtensions_SetSpeedBased_TisRuntimeObject_m808BBC2133B2235E29198F54D5146FEF34ABB8D7_gshared)(___t0, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,UnityEngine.AnimationCurve)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m7C1312703C345C640A4F7F0C6D3D46184F4AFA40 (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___animCurve1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, const RuntimeMethod*))TweenSettingsExtensions_SetEase_TisRuntimeObject_mDFD21B66D89B1600A547B64E3F973F63D9F42B98_gshared)(___t0, ___animCurve1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.Ease)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m6DD55E5B6755E223CC8B87169782DA90580F2570 (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, int32_t ___ease1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, int32_t, const RuntimeMethod*))TweenSettingsExtensions_SetEase_TisRuntimeObject_m17D546F0CC41676B108187269B5BB10959EFA2A4_gshared)(___t0, ___ease1, method);
}
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C (String_t* ___value0, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetId<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,System.Object)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_SetId_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m0CCED6BE9804CB6CC45ADE94D53345D7E595CA37 (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, RuntimeObject * ___id1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, RuntimeObject *, const RuntimeMethod*))TweenSettingsExtensions_SetId_TisRuntimeObject_m72E09063B9C56E443140E7E01F32618F28131087_gshared)(___t0, ___id1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::OnStart<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_OnStart_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mFBD4A625C829E10DCAD11FE8633CD49B028D89EC (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *, const RuntimeMethod*))TweenSettingsExtensions_OnStart_TisRuntimeObject_mE8EF10BC4D36C32FCEFB7DB33732E45775D2A986_gshared)(___t0, ___action1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::OnPlay<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_OnPlay_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m4064AFE7901DCBAA4382AD1AFC946097BE807A87 (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *, const RuntimeMethod*))TweenSettingsExtensions_OnPlay_TisRuntimeObject_mC8DCD120A7EF0DCBF14EDF1852CF047A6632BBC9_gshared)(___t0, ___action1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::OnUpdate<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_OnUpdate_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m3C21D9220857292A08A86EDF81E2F9B4F515C96E (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *, const RuntimeMethod*))TweenSettingsExtensions_OnUpdate_TisRuntimeObject_m9833BEBF80F4D5CFBD371351F053310B5758AE99_gshared)(___t0, ___action1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::OnStepComplete<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_OnStepComplete_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m128EB7D25F01AE225EB6397FA4E51044CD8278EE (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *, const RuntimeMethod*))TweenSettingsExtensions_OnStepComplete_TisRuntimeObject_mA199BA498C4CD73FBE20E1CA62E6F658AE492D0A_gshared)(___t0, ___action1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_OnComplete_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m0FB1AE97C737B4B59112E68269CEC95EDB23AA1B (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *, const RuntimeMethod*))TweenSettingsExtensions_OnComplete_TisRuntimeObject_m23E1F90FEE2A0CA4D04C244B7A7A83A541855DC0_gshared)(___t0, ___action1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::OnRewind<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenSettingsExtensions_OnRewind_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m52FD49B2534253B61C8D111A4949B26FE53B13D0 (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *, const RuntimeMethod*))TweenSettingsExtensions_OnRewind_TisRuntimeObject_mBFA5258FEF4DA74AC2ABC3C8025506F1EB866AF0_gshared)(___t0, ___action1, method);
}
// !!0 DG.Tweening.TweenExtensions::Play<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenExtensions_Play_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mE15D9ECA24D6CBE1DE47F46F1233B06A3D32EBF6 (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, const RuntimeMethod*))TweenExtensions_Play_TisRuntimeObject_m3E3C5EA201897EC0A7D33C27152A94B0C6BA54AD_gshared)(___t0, method);
}
// !!0 DG.Tweening.TweenExtensions::Pause<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0)
inline TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * TweenExtensions_Pause_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m4AF73672C93CFFCEC64E3E05744EBAE823B216A7 (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * ___t0, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * (*) (TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A *, const RuntimeMethod*))TweenExtensions_Pause_TisRuntimeObject_mB5DB00EFAF4AD1C64193EBAD6A86680274AD57EA_gshared)(___t0, method);
}
// System.Void UnityEngine.Events.UnityEvent::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5 (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* List_1_ToArray_mCFFA6AFA30A20FB2FFC3846AD114DF7BC21BFFBC (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	return ((  Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, const RuntimeMethod*))List_1_ToArray_mCFFA6AFA30A20FB2FFC3846AD114DF7BC21BFFBC_gshared)(__this, method);
}
// System.Void System.Nullable`1<UnityEngine.Color>::.ctor(!0)
inline void Nullable_1__ctor_mABF3CCAC28954E932B026C84D4C0D0F7BF2662F9 (Nullable_1_tA06400BA484934D9CEBAF66D0E71C822EF09A498 * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method)
{
	((  void (*) (Nullable_1_tA06400BA484934D9CEBAF66D0E71C822EF09A498 *, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 , const RuntimeMethod*))Nullable_1__ctor_mABF3CCAC28954E932B026C84D4C0D0F7BF2662F9_gshared)(__this, ___value0, method);
}
// System.Void DG.Tweening.Plugins.Core.PathCore.Path::.ctor(DG.Tweening.PathType,UnityEngine.Vector3[],System.Int32,System.Nullable`1<UnityEngine.Color>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Path__ctor_mFC92041030E41315A55CFE293F4500B2D36DBE20 (Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * __this, int32_t ___type0, Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___waypoints1, int32_t ___subdivisionsXSegment2, Nullable_1_tA06400BA484934D9CEBAF66D0E71C822EF09A498  ___gizmoColor3, const RuntimeMethod* method);
// System.Void DG.Tweening.TweenExtensions::Kill(DG.Tweening.Tween,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenExtensions_Kill_m8A79B9D5D31C46E9669C2EFEDF26BF4F7EB02D10 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, bool ___complete1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenExtensions::Play<DG.Tweening.Tween>(!!0)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenExtensions_Play_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m1A3AAD3856D80A02DB7F6BB7BBA444FD4C62DA8C (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, const RuntimeMethod*))TweenExtensions_Play_TisRuntimeObject_m3E3C5EA201897EC0A7D33C27152A94B0C6BA54AD_gshared)(___t0, method);
}
// System.Void DG.Tweening.TweenExtensions::PlayBackwards(DG.Tweening.Tween)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenExtensions_PlayBackwards_m147A9C8A47E881FA94B137394EF6F4EA53BFD1ED (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method);
// System.Void DG.Tweening.TweenExtensions::PlayForward(DG.Tweening.Tween)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenExtensions_PlayForward_m72684DEC2FE49A30A71B00FF442CCBBE3933F547 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenExtensions::Pause<DG.Tweening.Tween>(!!0)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenExtensions_Pause_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4A0F398B9290C213D5FED5901234BA973AA0C257 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, const RuntimeMethod*))TweenExtensions_Pause_TisRuntimeObject_mB5DB00EFAF4AD1C64193EBAD6A86680274AD57EA_gshared)(___t0, method);
}
// System.Void DG.Tweening.TweenExtensions::TogglePause(DG.Tweening.Tween)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenExtensions_TogglePause_mF6A1C2093B76873D90C8C36DF3E16528B227960B (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method);
// System.Void DG.Tweening.TweenExtensions::Rewind(DG.Tweening.Tween,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenExtensions_Rewind_m911EFDC4DAAEB2DC3C80112FE4C14F01BCF558C8 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, bool ___includeDelay1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.Debugger::LogNullTween(DG.Tweening.Tween)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debugger_LogNullTween_m6A9B33DC41AC0A765DD49C9AC2DDABECD22F476B (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method);
// System.Void DG.Tweening.TweenExtensions::Restart(DG.Tweening.Tween,System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenExtensions_Restart_mE904E3AB313B9CF8977E8BA3A2A191B821FC95CC (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, bool ___includeDelay1, float ___changeDelayTo2, const RuntimeMethod* method);
// System.Void DG.Tweening.TweenExtensions::Complete(DG.Tweening.Tween)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenExtensions_Complete_m1006012CD6C2AED2F84C6F04C16FF5D6DCEB0188 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.Debugger::LogInvalidTween(DG.Tweening.Tween)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debugger_LogInvalidTween_m89B8B151DC18203462D28B7A6FC9E5382BE7B5B2 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.Debugger::LogWarning(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debugger_LogWarning_m0B6001D682A2F01BB835AE4AD56405EE3FB146ED (RuntimeObject * ___message0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, int32_t, const RuntimeMethod*))List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline)(__this, ___index0, method);
}
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Vector3_op_Equality_m8A98C7F38641110A2F90445EF8E98ECE14B08296 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lhs0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * __this, float ___time0, float ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0 (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * __this, KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* ___keys0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_forward_m3082920F8A24AA02E4F542B6771EB0B63A91AC90 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50 (const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
inline void List_1__ctor_mF8F23D572031748AD428623AE16803455997E297 (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, const RuntimeMethod*))List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_gshared)(__this, method);
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.ABSAnimationComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ABSAnimationComponent__ctor_mF453633C79A30A108832D3F331DAB5E4CA24B272 (ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<DG.Tweening.Core.ABSAnimationComponent>()
inline ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * Component_GetComponent_TisABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7_m49AD20E6B0D8DB63C42C9DF58850542370EACB8C (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.Core.ABSAnimationComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ABSAnimationComponent__ctor_mF453633C79A30A108832D3F331DAB5E4CA24B272 (ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.DOTweenPath::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenPath_Awake_m51A3448F400C8475F54703BDF56036BC5840EC19 (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTweenPath_U3CAwakeU3Eb__38_0_m499DE7DE14F8DA2F164520621269F9E93F06C8E4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m49BFDA3CA404A1733B6D6CAAE6F254AD4076414B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_Draw_m98F7E4F7EAD1D65843AFCA4C097E0BEF2E5CC783_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenExtensions_Pause_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m4AF73672C93CFFCEC64E3E05744EBAE823B216A7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenExtensions_Play_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mE15D9ECA24D6CBE1DE47F46F1233B06A3D32EBF6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_OnComplete_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m0FB1AE97C737B4B59112E68269CEC95EDB23AA1B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_OnKill_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m47F222AC973A667D579E0F8B5BE97093A2E57200_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_OnPlay_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m4064AFE7901DCBAA4382AD1AFC946097BE807A87_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_OnRewind_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m52FD49B2534253B61C8D111A4949B26FE53B13D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_OnStart_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mFBD4A625C829E10DCAD11FE8633CD49B028D89EC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_OnStepComplete_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m128EB7D25F01AE225EB6397FA4E51044CD8278EE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_OnUpdate_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m3C21D9220857292A08A86EDF81E2F9B4F515C96E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetAutoKill_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mC6B4A0088E7F88B4B988455ACD13CECFD497C22D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m6B8AB1944A21B4D442E2E4B39575F7C6F0F3E460_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m6DD55E5B6755E223CC8B87169782DA90580F2570_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m7C1312703C345C640A4F7F0C6D3D46184F4AFA40_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetId_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m0CCED6BE9804CB6CC45ADE94D53345D7E595CA37_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mF7A6FCD23D0641A47C66CE77CC1A262C48ED4B6E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetSpeedBased_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m687552F99CBA5F1DE88F60CA85E91FC81767E770_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetUpdate_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m0005FC778EB58D20C9D21947236A37B707418F83_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * V_0 = NULL;
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * V_1 = NULL;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_2 = NULL;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_3;
	memset((&V_3), 0, sizeof(V_3));
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD  V_7;
	memset((&V_7), 0, sizeof(V_7));
	int32_t V_8 = 0;
	Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  V_9;
	memset((&V_9), 0, sizeof(V_9));
	TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * G_B24_0 = NULL;
	TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * G_B28_0 = NULL;
	{
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_0 = __this->get_path_46();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_1 = __this->get_wps_44();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_1, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		if ((((int32_t)L_2) < ((int32_t)1)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = __this->get_inspectorMode_47();
		if ((!(((uint32_t)L_3) == ((uint32_t)3))))
		{
			goto IL_0020;
		}
	}

IL_001f:
	{
		return;
	}

IL_0020:
	{
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_4 = __this->get_path_46();
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_5 = __this->get_path_46();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_type_3();
		NullCheck(L_4);
		Path_AssignDecoder_mD999EA9D655C222C11AF74D1F24EBD51AFC37097(L_4, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		bool L_7 = ((DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields*)il2cpp_codegen_static_fields_for(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var))->get_isUnityEditor_18();
		if (!L_7)
		{
			goto IL_0069;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		List_1_t7A220E9BB7539479C818543CE3CBDF252554FD58 * L_8 = ((DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_StaticFields*)il2cpp_codegen_static_fields_for(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var))->get_GizmosDelegates_22();
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_9 = __this->get_path_46();
		TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * L_10 = (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *)il2cpp_codegen_object_new(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662(L_10, L_9, (intptr_t)((intptr_t)Path_Draw_m98F7E4F7EAD1D65843AFCA4C097E0BEF2E5CC783_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_8);
		List_1_Add_m49BFDA3CA404A1733B6D6CAAE6F254AD4076414B(L_8, L_10, /*hidden argument*/List_1_Add_m49BFDA3CA404A1733B6D6CAAE6F254AD4076414B_RuntimeMethod_var);
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_11 = __this->get_path_46();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_12 = __this->get_pathColor_55();
		NullCheck(L_11);
		L_11->set_gizmoColor_20(L_12);
	}

IL_0069:
	{
		bool L_13 = __this->get_isLocal_35();
		if (!L_13)
		{
			goto IL_015c;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14;
		L_14 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		V_2 = L_14;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_15 = V_2;
		NullCheck(L_15);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9(L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_17;
		L_17 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_16, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_015c;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18 = V_2;
		NullCheck(L_18);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_19;
		L_19 = Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9(L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20 = V_2;
		NullCheck(L_20);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_22 = __this->get_path_46();
		NullCheck(L_22);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_23 = L_22->get_wps_6();
		NullCheck(L_23);
		V_4 = ((int32_t)((int32_t)(((RuntimeArray*)L_23)->max_length)));
		V_5 = 0;
		goto IL_00de;
	}

IL_00ae:
	{
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_24 = __this->get_path_46();
		NullCheck(L_24);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_25 = L_24->get_wps_6();
		int32_t L_26 = V_5;
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_27 = __this->get_path_46();
		NullCheck(L_27);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_28 = L_27->get_wps_6();
		int32_t L_29 = V_5;
		NullCheck(L_28);
		int32_t L_30 = L_29;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32 = V_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33;
		L_33 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_25);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(L_26), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_33);
		int32_t L_34 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_34, (int32_t)1));
	}

IL_00de:
	{
		int32_t L_35 = V_5;
		int32_t L_36 = V_4;
		if ((((int32_t)L_35) < ((int32_t)L_36)))
		{
			goto IL_00ae;
		}
	}
	{
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_37 = __this->get_path_46();
		NullCheck(L_37);
		ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884* L_38 = L_37->get_controlPoints_7();
		NullCheck(L_38);
		V_4 = ((int32_t)((int32_t)(((RuntimeArray*)L_38)->max_length)));
		V_6 = 0;
		goto IL_0156;
	}

IL_00f8:
	{
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_39 = __this->get_path_46();
		NullCheck(L_39);
		ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884* L_40 = L_39->get_controlPoints_7();
		int32_t L_41 = V_6;
		NullCheck(L_40);
		int32_t L_42 = L_41;
		ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD  L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		V_7 = L_43;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_44 = (&V_7)->get_address_of_a_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_45 = L_44;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_46 = (*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_45);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_47 = V_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_48;
		L_48 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_46, L_47, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_45 = L_48;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_49 = (&V_7)->get_address_of_b_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_50 = L_49;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_51 = (*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_50);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_52 = V_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_53;
		L_53 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_51, L_52, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_50 = L_53;
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_54 = __this->get_path_46();
		NullCheck(L_54);
		ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884* L_55 = L_54->get_controlPoints_7();
		int32_t L_56 = V_6;
		ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD  L_57 = V_7;
		NullCheck(L_55);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(L_56), (ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD )L_57);
		int32_t L_58 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_58, (int32_t)1));
	}

IL_0156:
	{
		int32_t L_59 = V_6;
		int32_t L_60 = V_4;
		if ((((int32_t)L_59) < ((int32_t)L_60)))
		{
			goto IL_00f8;
		}
	}

IL_015c:
	{
		bool L_61 = __this->get_relative_34();
		if (!L_61)
		{
			goto IL_016a;
		}
	}
	{
		DOTweenPath_ReEvaluateRelativeTween_mAEF73A49F484B2A0EB04B716D4445AAA171559C9(__this, /*hidden argument*/NULL);
	}

IL_016a:
	{
		int32_t L_62 = __this->get_pathMode_38();
		if ((!(((uint32_t)L_62) == ((uint32_t)1))))
		{
			goto IL_0188;
		}
	}
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_63;
		L_63 = Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_64;
		L_64 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_63, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_0188;
		}
	}
	{
		__this->set_pathMode_38(2);
	}

IL_0188:
	{
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_65;
		L_65 = Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var);
		V_1 = L_65;
		bool L_66 = __this->get_tweenRigidbody_43();
		if (!L_66)
		{
			goto IL_0201;
		}
	}
	{
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_67 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_68;
		L_68 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_67, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_0201;
		}
	}
	{
		bool L_69 = __this->get_isLocal_35();
		if (L_69)
		{
			goto IL_01d4;
		}
	}
	{
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_70 = V_1;
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_71 = __this->get_path_46();
		float L_72 = __this->get_duration_22();
		int32_t L_73 = __this->get_pathMode_38();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_74;
		L_74 = ShortcutExtensions_DOPath_m01EDCCA0EE21B7E554FF28BEA409198E50CFD1FC(L_70, L_71, L_72, L_73, /*hidden argument*/NULL);
		bool L_75 = __this->get_isClosedPath_36();
		int32_t L_76 = __this->get_lockRotation_39();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_77;
		L_77 = TweenSettingsExtensions_SetOptions_m232500C0CAFAD89AFE0A437A6015F2E59ED3D8FF(L_74, L_75, 0, L_76, /*hidden argument*/NULL);
		G_B24_0 = L_77;
		goto IL_01fe;
	}

IL_01d4:
	{
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_78 = V_1;
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_79 = __this->get_path_46();
		float L_80 = __this->get_duration_22();
		int32_t L_81 = __this->get_pathMode_38();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_82;
		L_82 = ShortcutExtensions_DOLocalPath_mE0136EA4E054DF88A8322A664D3CCEFAFABEAE4C(L_78, L_79, L_80, L_81, /*hidden argument*/NULL);
		bool L_83 = __this->get_isClosedPath_36();
		int32_t L_84 = __this->get_lockRotation_39();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_85;
		L_85 = TweenSettingsExtensions_SetOptions_m232500C0CAFAD89AFE0A437A6015F2E59ED3D8FF(L_82, L_83, 0, L_84, /*hidden argument*/NULL);
		G_B24_0 = L_85;
	}

IL_01fe:
	{
		V_0 = G_B24_0;
		goto IL_026a;
	}

IL_0201:
	{
		bool L_86 = __this->get_isLocal_35();
		if (L_86)
		{
			goto IL_023a;
		}
	}
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_87;
		L_87 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_88 = __this->get_path_46();
		float L_89 = __this->get_duration_22();
		int32_t L_90 = __this->get_pathMode_38();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_91;
		L_91 = ShortcutExtensions_DOPath_m7AB84619455A541B79164E1D270A5AB511533B24(L_87, L_88, L_89, L_90, /*hidden argument*/NULL);
		bool L_92 = __this->get_isClosedPath_36();
		int32_t L_93 = __this->get_lockRotation_39();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_94;
		L_94 = TweenSettingsExtensions_SetOptions_m232500C0CAFAD89AFE0A437A6015F2E59ED3D8FF(L_91, L_92, 0, L_93, /*hidden argument*/NULL);
		G_B28_0 = L_94;
		goto IL_0269;
	}

IL_023a:
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_95;
		L_95 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_96 = __this->get_path_46();
		float L_97 = __this->get_duration_22();
		int32_t L_98 = __this->get_pathMode_38();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_99;
		L_99 = ShortcutExtensions_DOLocalPath_m28185CD9787640FBB679A0B46F144E1BB0DB43F8(L_95, L_96, L_97, L_98, /*hidden argument*/NULL);
		bool L_100 = __this->get_isClosedPath_36();
		int32_t L_101 = __this->get_lockRotation_39();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_102;
		L_102 = TweenSettingsExtensions_SetOptions_m232500C0CAFAD89AFE0A437A6015F2E59ED3D8FF(L_99, L_100, 0, L_101, /*hidden argument*/NULL);
		G_B28_0 = L_102;
	}

IL_0269:
	{
		V_0 = G_B28_0;
	}

IL_026a:
	{
		int32_t L_103 = __this->get_orientType_28();
		V_8 = L_103;
		int32_t L_104 = V_8;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_104, (int32_t)1)))
		{
			case 0:
			{
				goto IL_0343;
			}
			case 1:
			{
				goto IL_028c;
			}
			case 2:
			{
				goto IL_02f3;
			}
		}
	}
	{
		goto IL_0391;
	}

IL_028c:
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_105 = __this->get_lookAtTransform_29();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_106;
		L_106 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_105, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_106)
		{
			goto IL_0391;
		}
	}
	{
		bool L_107 = __this->get_assignForwardAndUp_40();
		if (!L_107)
		{
			goto IL_02cd;
		}
	}
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_108 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_109 = __this->get_lookAtTransform_29();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_110 = __this->get_forwardDirection_41();
		Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  L_111;
		memset((&L_111), 0, sizeof(L_111));
		Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830((&L_111), L_110, /*hidden argument*/Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_112 = __this->get_upDirection_42();
		Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  L_113;
		memset((&L_113), 0, sizeof(L_113));
		Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830((&L_113), L_112, /*hidden argument*/Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830_RuntimeMethod_var);
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_114;
		L_114 = TweenSettingsExtensions_SetLookAt_m3BFF34B7765FCB4B3DDE2B3B90869FE9086DDD01(L_108, L_109, L_111, L_113, /*hidden argument*/NULL);
		goto IL_0391;
	}

IL_02cd:
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_115 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_116 = __this->get_lookAtTransform_29();
		il2cpp_codegen_initobj((&V_9), sizeof(Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 ));
		Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  L_117 = V_9;
		il2cpp_codegen_initobj((&V_9), sizeof(Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 ));
		Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  L_118 = V_9;
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_119;
		L_119 = TweenSettingsExtensions_SetLookAt_m3BFF34B7765FCB4B3DDE2B3B90869FE9086DDD01(L_115, L_116, L_117, L_118, /*hidden argument*/NULL);
		goto IL_0391;
	}

IL_02f3:
	{
		bool L_120 = __this->get_assignForwardAndUp_40();
		if (!L_120)
		{
			goto IL_0320;
		}
	}
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_121 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_122 = __this->get_lookAtPosition_30();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_123 = __this->get_forwardDirection_41();
		Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  L_124;
		memset((&L_124), 0, sizeof(L_124));
		Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830((&L_124), L_123, /*hidden argument*/Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_125 = __this->get_upDirection_42();
		Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  L_126;
		memset((&L_126), 0, sizeof(L_126));
		Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830((&L_126), L_125, /*hidden argument*/Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830_RuntimeMethod_var);
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_127;
		L_127 = TweenSettingsExtensions_SetLookAt_m31B2E7ECAADA53B8C6EDA9BC760AFA2F6155B382(L_121, L_122, L_124, L_126, /*hidden argument*/NULL);
		goto IL_0391;
	}

IL_0320:
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_128 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_129 = __this->get_lookAtPosition_30();
		il2cpp_codegen_initobj((&V_9), sizeof(Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 ));
		Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  L_130 = V_9;
		il2cpp_codegen_initobj((&V_9), sizeof(Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 ));
		Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  L_131 = V_9;
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_132;
		L_132 = TweenSettingsExtensions_SetLookAt_m31B2E7ECAADA53B8C6EDA9BC760AFA2F6155B382(L_128, L_129, L_130, L_131, /*hidden argument*/NULL);
		goto IL_0391;
	}

IL_0343:
	{
		bool L_133 = __this->get_assignForwardAndUp_40();
		if (!L_133)
		{
			goto IL_0370;
		}
	}
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_134 = V_0;
		float L_135 = __this->get_lookAhead_31();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_136 = __this->get_forwardDirection_41();
		Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  L_137;
		memset((&L_137), 0, sizeof(L_137));
		Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830((&L_137), L_136, /*hidden argument*/Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_138 = __this->get_upDirection_42();
		Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  L_139;
		memset((&L_139), 0, sizeof(L_139));
		Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830((&L_139), L_138, /*hidden argument*/Nullable_1__ctor_m6F1238037ACD87D7DC2C911C35EEC7D1B73F5830_RuntimeMethod_var);
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_140;
		L_140 = TweenSettingsExtensions_SetLookAt_mAD3CC7329671C9A6D0FA41A9AB2D3B236769C2AF(L_134, L_135, L_137, L_139, /*hidden argument*/NULL);
		goto IL_0391;
	}

IL_0370:
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_141 = V_0;
		float L_142 = __this->get_lookAhead_31();
		il2cpp_codegen_initobj((&V_9), sizeof(Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 ));
		Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  L_143 = V_9;
		il2cpp_codegen_initobj((&V_9), sizeof(Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 ));
		Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  L_144 = V_9;
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_145;
		L_145 = TweenSettingsExtensions_SetLookAt_mAD3CC7329671C9A6D0FA41A9AB2D3B236769C2AF(L_141, L_142, L_143, L_144, /*hidden argument*/NULL);
	}

IL_0391:
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_146 = V_0;
		float L_147 = __this->get_delay_21();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_148;
		L_148 = TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m6B8AB1944A21B4D442E2E4B39575F7C6F0F3E460(L_146, L_147, /*hidden argument*/TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m6B8AB1944A21B4D442E2E4B39575F7C6F0F3E460_RuntimeMethod_var);
		int32_t L_149 = __this->get_loops_25();
		int32_t L_150 = __this->get_loopType_27();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_151;
		L_151 = TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mF7A6FCD23D0641A47C66CE77CC1A262C48ED4B6E(L_148, L_149, L_150, /*hidden argument*/TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mF7A6FCD23D0641A47C66CE77CC1A262C48ED4B6E_RuntimeMethod_var);
		bool L_152 = __this->get_autoKill_33();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_153;
		L_153 = TweenSettingsExtensions_SetAutoKill_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mC6B4A0088E7F88B4B988455ACD13CECFD497C22D(L_151, L_152, /*hidden argument*/TweenSettingsExtensions_SetAutoKill_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mC6B4A0088E7F88B4B988455ACD13CECFD497C22D_RuntimeMethod_var);
		int32_t L_154 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_updateType_4();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_155;
		L_155 = TweenSettingsExtensions_SetUpdate_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m0005FC778EB58D20C9D21947236A37B707418F83(L_153, L_154, /*hidden argument*/TweenSettingsExtensions_SetUpdate_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m0005FC778EB58D20C9D21947236A37B707418F83_RuntimeMethod_var);
		TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * L_156 = (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *)il2cpp_codegen_object_new(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662(L_156, __this, (intptr_t)((intptr_t)DOTweenPath_U3CAwakeU3Eb__38_0_m499DE7DE14F8DA2F164520621269F9E93F06C8E4_RuntimeMethod_var), /*hidden argument*/NULL);
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_157;
		L_157 = TweenSettingsExtensions_OnKill_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m47F222AC973A667D579E0F8B5BE97093A2E57200(L_155, L_156, /*hidden argument*/TweenSettingsExtensions_OnKill_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m47F222AC973A667D579E0F8B5BE97093A2E57200_RuntimeMethod_var);
		bool L_158 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_isSpeedBased_5();
		if (!L_158)
		{
			goto IL_03e5;
		}
	}
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_159 = V_0;
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_160;
		L_160 = TweenSettingsExtensions_SetSpeedBased_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m687552F99CBA5F1DE88F60CA85E91FC81767E770(L_159, /*hidden argument*/TweenSettingsExtensions_SetSpeedBased_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m687552F99CBA5F1DE88F60CA85E91FC81767E770_RuntimeMethod_var);
	}

IL_03e5:
	{
		int32_t L_161 = __this->get_easeType_23();
		if ((!(((uint32_t)L_161) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_03fe;
		}
	}
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_162 = V_0;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_163 = __this->get_easeCurve_24();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_164;
		L_164 = TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m7C1312703C345C640A4F7F0C6D3D46184F4AFA40(L_162, L_163, /*hidden argument*/TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m7C1312703C345C640A4F7F0C6D3D46184F4AFA40_RuntimeMethod_var);
		goto IL_040b;
	}

IL_03fe:
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_165 = V_0;
		int32_t L_166 = __this->get_easeType_23();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_167;
		L_167 = TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m6DD55E5B6755E223CC8B87169782DA90580F2570(L_165, L_166, /*hidden argument*/TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m6DD55E5B6755E223CC8B87169782DA90580F2570_RuntimeMethod_var);
	}

IL_040b:
	{
		String_t* L_168 = __this->get_id_26();
		bool L_169;
		L_169 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_168, /*hidden argument*/NULL);
		if (L_169)
		{
			goto IL_0425;
		}
	}
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_170 = V_0;
		String_t* L_171 = __this->get_id_26();
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_172;
		L_172 = TweenSettingsExtensions_SetId_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m0CCED6BE9804CB6CC45ADE94D53345D7E595CA37(L_170, L_171, /*hidden argument*/TweenSettingsExtensions_SetId_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m0CCED6BE9804CB6CC45ADE94D53345D7E595CA37_RuntimeMethod_var);
	}

IL_0425:
	{
		bool L_173 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_hasOnStart_6();
		if (!L_173)
		{
			goto IL_044f;
		}
	}
	{
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_174 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onStart_13();
		if (!L_174)
		{
			goto IL_0456;
		}
	}
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_175 = V_0;
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_176 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onStart_13();
		TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * L_177 = (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *)il2cpp_codegen_object_new(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662(L_177, L_176, (intptr_t)((intptr_t)UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5_RuntimeMethod_var), /*hidden argument*/NULL);
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_178;
		L_178 = TweenSettingsExtensions_OnStart_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mFBD4A625C829E10DCAD11FE8633CD49B028D89EC(L_175, L_177, /*hidden argument*/TweenSettingsExtensions_OnStart_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mFBD4A625C829E10DCAD11FE8633CD49B028D89EC_RuntimeMethod_var);
		goto IL_0456;
	}

IL_044f:
	{
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_onStart_13((UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 *)NULL);
	}

IL_0456:
	{
		bool L_179 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_hasOnPlay_7();
		if (!L_179)
		{
			goto IL_0480;
		}
	}
	{
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_180 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onPlay_14();
		if (!L_180)
		{
			goto IL_0487;
		}
	}
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_181 = V_0;
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_182 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onPlay_14();
		TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * L_183 = (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *)il2cpp_codegen_object_new(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662(L_183, L_182, (intptr_t)((intptr_t)UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5_RuntimeMethod_var), /*hidden argument*/NULL);
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_184;
		L_184 = TweenSettingsExtensions_OnPlay_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m4064AFE7901DCBAA4382AD1AFC946097BE807A87(L_181, L_183, /*hidden argument*/TweenSettingsExtensions_OnPlay_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m4064AFE7901DCBAA4382AD1AFC946097BE807A87_RuntimeMethod_var);
		goto IL_0487;
	}

IL_0480:
	{
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_onPlay_14((UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 *)NULL);
	}

IL_0487:
	{
		bool L_185 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_hasOnUpdate_8();
		if (!L_185)
		{
			goto IL_04b1;
		}
	}
	{
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_186 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onUpdate_15();
		if (!L_186)
		{
			goto IL_04b8;
		}
	}
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_187 = V_0;
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_188 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onUpdate_15();
		TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * L_189 = (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *)il2cpp_codegen_object_new(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662(L_189, L_188, (intptr_t)((intptr_t)UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5_RuntimeMethod_var), /*hidden argument*/NULL);
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_190;
		L_190 = TweenSettingsExtensions_OnUpdate_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m3C21D9220857292A08A86EDF81E2F9B4F515C96E(L_187, L_189, /*hidden argument*/TweenSettingsExtensions_OnUpdate_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m3C21D9220857292A08A86EDF81E2F9B4F515C96E_RuntimeMethod_var);
		goto IL_04b8;
	}

IL_04b1:
	{
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_onUpdate_15((UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 *)NULL);
	}

IL_04b8:
	{
		bool L_191 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_hasOnStepComplete_9();
		if (!L_191)
		{
			goto IL_04e2;
		}
	}
	{
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_192 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onStepComplete_16();
		if (!L_192)
		{
			goto IL_04e9;
		}
	}
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_193 = V_0;
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_194 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onStepComplete_16();
		TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * L_195 = (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *)il2cpp_codegen_object_new(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662(L_195, L_194, (intptr_t)((intptr_t)UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5_RuntimeMethod_var), /*hidden argument*/NULL);
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_196;
		L_196 = TweenSettingsExtensions_OnStepComplete_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m128EB7D25F01AE225EB6397FA4E51044CD8278EE(L_193, L_195, /*hidden argument*/TweenSettingsExtensions_OnStepComplete_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m128EB7D25F01AE225EB6397FA4E51044CD8278EE_RuntimeMethod_var);
		goto IL_04e9;
	}

IL_04e2:
	{
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_onStepComplete_16((UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 *)NULL);
	}

IL_04e9:
	{
		bool L_197 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_hasOnComplete_10();
		if (!L_197)
		{
			goto IL_0513;
		}
	}
	{
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_198 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onComplete_17();
		if (!L_198)
		{
			goto IL_051a;
		}
	}
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_199 = V_0;
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_200 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onComplete_17();
		TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * L_201 = (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *)il2cpp_codegen_object_new(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662(L_201, L_200, (intptr_t)((intptr_t)UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5_RuntimeMethod_var), /*hidden argument*/NULL);
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_202;
		L_202 = TweenSettingsExtensions_OnComplete_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m0FB1AE97C737B4B59112E68269CEC95EDB23AA1B(L_199, L_201, /*hidden argument*/TweenSettingsExtensions_OnComplete_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m0FB1AE97C737B4B59112E68269CEC95EDB23AA1B_RuntimeMethod_var);
		goto IL_051a;
	}

IL_0513:
	{
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_onComplete_17((UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 *)NULL);
	}

IL_051a:
	{
		bool L_203 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_hasOnRewind_12();
		if (!L_203)
		{
			goto IL_0544;
		}
	}
	{
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_204 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onRewind_19();
		if (!L_204)
		{
			goto IL_054b;
		}
	}
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_205 = V_0;
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_206 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onRewind_19();
		TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * L_207 = (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *)il2cpp_codegen_object_new(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662(L_207, L_206, (intptr_t)((intptr_t)UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5_RuntimeMethod_var), /*hidden argument*/NULL);
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_208;
		L_208 = TweenSettingsExtensions_OnRewind_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m52FD49B2534253B61C8D111A4949B26FE53B13D0(L_205, L_207, /*hidden argument*/TweenSettingsExtensions_OnRewind_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m52FD49B2534253B61C8D111A4949B26FE53B13D0_RuntimeMethod_var);
		goto IL_054b;
	}

IL_0544:
	{
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_onRewind_19((UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 *)NULL);
	}

IL_054b:
	{
		bool L_209 = __this->get_autoPlay_32();
		if (!L_209)
		{
			goto IL_055c;
		}
	}
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_210 = V_0;
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_211;
		L_211 = TweenExtensions_Play_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mE15D9ECA24D6CBE1DE47F46F1233B06A3D32EBF6(L_210, /*hidden argument*/TweenExtensions_Play_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_mE15D9ECA24D6CBE1DE47F46F1233B06A3D32EBF6_RuntimeMethod_var);
		goto IL_0563;
	}

IL_055c:
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_212 = V_0;
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_213;
		L_213 = TweenExtensions_Pause_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m4AF73672C93CFFCEC64E3E05744EBAE823B216A7(L_212, /*hidden argument*/TweenExtensions_Pause_TisTweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A_m4AF73672C93CFFCEC64E3E05744EBAE823B216A7_RuntimeMethod_var);
	}

IL_0563:
	{
		TweenerCore_3_t3502A54CD60E18ECC9C6BED38B76E85B1EF67C9A * L_214 = V_0;
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_214);
		bool L_215 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_hasOnTweenCreated_11();
		if (!L_215)
		{
			goto IL_0585;
		}
	}
	{
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_216 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onTweenCreated_18();
		if (!L_216)
		{
			goto IL_0585;
		}
	}
	{
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_217 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onTweenCreated_18();
		NullCheck(L_217);
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(L_217, /*hidden argument*/NULL);
	}

IL_0585:
	{
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenPath_Reset_mE0F57DBE1465D5FC758920E14A9F28BFA9CBB907 (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_ToArray_mCFFA6AFA30A20FB2FFC3846AD114DF7BC21BFFBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1__ctor_mABF3CCAC28954E932B026C84D4C0D0F7BF2662F9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_pathType_48();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_1 = __this->get_wps_44();
		NullCheck(L_1);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_2;
		L_2 = List_1_ToArray_mCFFA6AFA30A20FB2FFC3846AD114DF7BC21BFFBC(L_1, /*hidden argument*/List_1_ToArray_mCFFA6AFA30A20FB2FFC3846AD114DF7BC21BFFBC_RuntimeMethod_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_3 = __this->get_pathColor_55();
		Nullable_1_tA06400BA484934D9CEBAF66D0E71C822EF09A498  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Nullable_1__ctor_mABF3CCAC28954E932B026C84D4C0D0F7BF2662F9((&L_4), L_3, /*hidden argument*/Nullable_1__ctor_mABF3CCAC28954E932B026C84D4C0D0F7BF2662F9_RuntimeMethod_var);
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_5 = (Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 *)il2cpp_codegen_object_new(Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_il2cpp_TypeInfo_var);
		Path__ctor_mFC92041030E41315A55CFE293F4500B2D36DBE20(L_5, L_0, L_2, ((int32_t)10), L_4, /*hidden argument*/NULL);
		__this->set_path_46(L_5);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenPath_OnDestroy_m5BA9503DE3961CBB1D5663E2028070CE519AE414 (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_0 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_1 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		NullCheck(L_1);
		bool L_2 = L_1->get_active_35();
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_3 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		TweenExtensions_Kill_m8A79B9D5D31C46E9669C2EFEDF26BF4F7EB02D10(L_3, (bool)0, /*hidden argument*/NULL);
	}

IL_0021:
	{
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20((Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *)NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOPlay()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenPath_DOPlay_m237D396684BF4DE087DFFA44C867C38B68AE5912 (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenExtensions_Play_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m1A3AAD3856D80A02DB7F6BB7BBA444FD4C62DA8C_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_0 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_1;
		L_1 = TweenExtensions_Play_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m1A3AAD3856D80A02DB7F6BB7BBA444FD4C62DA8C(L_0, /*hidden argument*/TweenExtensions_Play_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m1A3AAD3856D80A02DB7F6BB7BBA444FD4C62DA8C_RuntimeMethod_var);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOPlayBackwards()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenPath_DOPlayBackwards_m4C4C04292C1182313E27E97AFCC9D13CD69C0907 (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_0 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		TweenExtensions_PlayBackwards_m147A9C8A47E881FA94B137394EF6F4EA53BFD1ED(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOPlayForward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenPath_DOPlayForward_mC66732B68D89CE34A6A20B72E6513892AE57E09B (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_0 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		TweenExtensions_PlayForward_m72684DEC2FE49A30A71B00FF442CCBBE3933F547(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOPause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenPath_DOPause_m3B8B2A99E7F4AF2C63D866EBD7A14D865244D48A (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenExtensions_Pause_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4A0F398B9290C213D5FED5901234BA973AA0C257_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_0 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_1;
		L_1 = TweenExtensions_Pause_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4A0F398B9290C213D5FED5901234BA973AA0C257(L_0, /*hidden argument*/TweenExtensions_Pause_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4A0F398B9290C213D5FED5901234BA973AA0C257_RuntimeMethod_var);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOTogglePause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenPath_DOTogglePause_m1E93EF30DA2E9629F0B49349229A39FE4AA6DAB3 (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_0 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		TweenExtensions_TogglePause_mF6A1C2093B76873D90C8C36DF3E16528B227960B(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DORewind()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenPath_DORewind_mAF3776B4A345F734B6EB8CF74B0CBF0D3B56CB71 (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_0 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		TweenExtensions_Rewind_m911EFDC4DAAEB2DC3C80112FE4C14F01BCF558C8(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DORestart(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenPath_DORestart_m7CC6C31AC3884C6064F9612E0C025B9DF684952B (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, bool ___fromHere0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_0 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F_StaticFields*)il2cpp_codegen_static_fields_for(Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F_il2cpp_TypeInfo_var))->get_logPriority_0();
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_001b;
		}
	}
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_2 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		Debugger_LogNullTween_m6A9B33DC41AC0A765DD49C9AC2DDABECD22F476B(L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}

IL_001c:
	{
		bool L_3 = ___fromHere0;
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		bool L_4 = __this->get_relative_34();
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		bool L_5 = __this->get_isLocal_35();
		if (L_5)
		{
			goto IL_0035;
		}
	}
	{
		DOTweenPath_ReEvaluateRelativeTween_mAEF73A49F484B2A0EB04B716D4445AAA171559C9(__this, /*hidden argument*/NULL);
	}

IL_0035:
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_6 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		TweenExtensions_Restart_mE904E3AB313B9CF8977E8BA3A2A191B821FC95CC(L_6, (bool)1, (-1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOComplete()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenPath_DOComplete_m2D9ADCBA1DED8E57D13892D89D67E86C691D27CD (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_0 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		TweenExtensions_Complete_m1006012CD6C2AED2F84C6F04C16FF5D6DCEB0188(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOKill()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenPath_DOKill_m16CE727B9BF12DCBBA95DD12606832AF5450F154 (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_0 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		TweenExtensions_Kill_m8A79B9D5D31C46E9669C2EFEDF26BF4F7EB02D10(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// DG.Tweening.Tween DG.Tweening.DOTweenPath::GetTween()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * DOTweenPath_GetTween_mF4DCE3ACAB5A10A6C52EC6381507ACE1F62AD10E (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_0 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_1 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		NullCheck(L_1);
		bool L_2 = L_1->get_active_35();
		if (L_2)
		{
			goto IL_003f;
		}
	}

IL_0015:
	{
		int32_t L_3 = ((Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F_StaticFields*)il2cpp_codegen_static_fields_for(Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F_il2cpp_TypeInfo_var))->get_logPriority_0();
		if ((((int32_t)L_3) <= ((int32_t)1)))
		{
			goto IL_003d;
		}
	}
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_4 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		if (L_4)
		{
			goto IL_0032;
		}
	}
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_5 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		Debugger_LogNullTween_m6A9B33DC41AC0A765DD49C9AC2DDABECD22F476B(L_5, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_0032:
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_6 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		Debugger_LogInvalidTween_m89B8B151DC18203462D28B7A6FC9E5382BE7B5B2(L_6, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *)NULL;
	}

IL_003f:
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_7 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		return L_7;
	}
}
// UnityEngine.Vector3[] DG.Tweening.DOTweenPath::GetDrawPoints()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* DOTweenPath_GetDrawPoints_m0199B745C0B433BA521821316139B4255A467704 (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1BCF04EB959108E68CFE0C06D667CF9B7AFC4B36);
		s_Il2CppMethodInitialized = true;
	}
	{
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_0 = __this->get_path_46();
		NullCheck(L_0);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_1 = L_0->get_wps_6();
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_2 = __this->get_path_46();
		NullCheck(L_2);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_3 = L_2->get_nonLinearDrawWps_17();
		if (L_3)
		{
			goto IL_0026;
		}
	}

IL_001a:
	{
		Debugger_LogWarning_m0B6001D682A2F01BB835AE4AD56405EE3FB146ED(_stringLiteral1BCF04EB959108E68CFE0C06D667CF9B7AFC4B36, /*hidden argument*/NULL);
		return (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)NULL;
	}

IL_0026:
	{
		int32_t L_4 = __this->get_pathType_48();
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_5 = __this->get_path_46();
		NullCheck(L_5);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_6 = L_5->get_wps_6();
		return L_6;
	}

IL_003a:
	{
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_7 = __this->get_path_46();
		NullCheck(L_7);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_8 = L_7->get_nonLinearDrawWps_17();
		return L_8;
	}
}
// UnityEngine.Vector3[] DG.Tweening.DOTweenPath::GetFullWps()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* DOTweenPath_GetFullWps_m207122CE008387F966FBA26535D8790D3D5AA6DC (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* V_2 = NULL;
	int32_t V_3 = 0;
	{
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_0 = __this->get_wps_44();
		NullCheck(L_0);
		int32_t L_1;
		L_1 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_0, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)1));
		bool L_3 = __this->get_isClosedPath_36();
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_4 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_001c:
	{
		int32_t L_5 = V_1;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_6 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)SZArrayNew(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var, (uint32_t)L_5);
		V_2 = L_6;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_7 = V_2;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_9);
		V_3 = 0;
		goto IL_0052;
	}

IL_0039:
	{
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_10 = V_2;
		int32_t L_11 = V_3;
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_12 = __this->get_wps_44();
		int32_t L_13 = V_3;
		NullCheck(L_12);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_12, L_13, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		NullCheck(L_10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1))), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_14);
		int32_t L_15 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0052:
	{
		int32_t L_16 = V_3;
		int32_t L_17 = V_0;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0039;
		}
	}
	{
		bool L_18 = __this->get_isClosedPath_36();
		if (!L_18)
		{
			goto IL_006e;
		}
	}
	{
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_19 = V_2;
		int32_t L_20 = V_1;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_21 = V_2;
		NullCheck(L_21);
		int32_t L_22 = 0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_19);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_subtract((int32_t)L_20, (int32_t)1))), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_23);
	}

IL_006e:
	{
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_24 = V_2;
		return L_24;
	}
}
// System.Void DG.Tweening.DOTweenPath::ReEvaluateRelativeTween()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenPath_ReEvaluateRelativeTween_mAEF73A49F484B2A0EB04B716D4445AAA171559C9 (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD  V_5;
	memset((&V_5), 0, sizeof(V_5));
	{
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = __this->get_lastSrcPosition_56();
		bool L_4;
		L_4 = Vector3_op_Equality_m8A98C7F38641110A2F90445EF8E98ECE14B08296(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001b;
		}
	}
	{
		return;
	}

IL_001b:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = __this->get_lastSrcPosition_56();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_8 = __this->get_path_46();
		NullCheck(L_8);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_9 = L_8->get_wps_6();
		NullCheck(L_9);
		V_2 = ((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length)));
		V_3 = 0;
		goto IL_0066;
	}

IL_003a:
	{
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_10 = __this->get_path_46();
		NullCheck(L_10);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_11 = L_10->get_wps_6();
		int32_t L_12 = V_3;
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_13 = __this->get_path_46();
		NullCheck(L_13);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_14 = L_13->get_wps_6();
		int32_t L_15 = V_3;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(L_12), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_19);
		int32_t L_20 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
	}

IL_0066:
	{
		int32_t L_21 = V_3;
		int32_t L_22 = V_2;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_003a;
		}
	}
	{
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_23 = __this->get_path_46();
		NullCheck(L_23);
		ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884* L_24 = L_23->get_controlPoints_7();
		NullCheck(L_24);
		V_2 = ((int32_t)((int32_t)(((RuntimeArray*)L_24)->max_length)));
		V_4 = 0;
		goto IL_00db;
	}

IL_007d:
	{
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_25 = __this->get_path_46();
		NullCheck(L_25);
		ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884* L_26 = L_25->get_controlPoints_7();
		int32_t L_27 = V_4;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD  L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		V_5 = L_29;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_30 = (&V_5)->get_address_of_a_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_31 = L_30;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32 = (*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_31);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_34;
		L_34 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_32, L_33, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_31 = L_34;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_35 = (&V_5)->get_address_of_b_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_36 = L_35;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37 = (*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_36);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_38 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_39;
		L_39 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_37, L_38, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_36 = L_39;
		Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5 * L_40 = __this->get_path_46();
		NullCheck(L_40);
		ControlPointU5BU5D_t9F61665C9F79DD5715B9C6C357BC1A9F16326884* L_41 = L_40->get_controlPoints_7();
		int32_t L_42 = V_4;
		ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD  L_43 = V_5;
		NullCheck(L_41);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(L_42), (ControlPoint_tA7D3372AB7B8CC893FB9A9FC635A245CD4D373CD )L_43);
		int32_t L_44 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_44, (int32_t)1));
	}

IL_00db:
	{
		int32_t L_45 = V_4;
		int32_t L_46 = V_2;
		if ((((int32_t)L_45) < ((int32_t)L_46)))
		{
			goto IL_007d;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_47 = V_0;
		__this->set_lastSrcPosition_56(L_47);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenPath__ctor_mE5726F4DF128E746378EBCD1346D8E8AAF61CBD9 (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_duration_22((1.0f));
		__this->set_easeType_23(6);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_5 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_5, L_3, /*hidden argument*/NULL);
		__this->set_easeCurve_24(L_5);
		__this->set_loops_25(1);
		__this->set_id_26(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		__this->set_lookAhead_31((0.00999999978f));
		__this->set_autoPlay_32((bool)1);
		__this->set_autoKill_33((bool)1);
		__this->set_pathResolution_37(((int32_t)10));
		__this->set_pathMode_38(1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Vector3_get_forward_m3082920F8A24AA02E4F542B6771EB0B63A91AC90(/*hidden argument*/NULL);
		__this->set_forwardDirection_41(L_6);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		__this->set_upDirection_42(L_7);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_8 = (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *)il2cpp_codegen_object_new(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var);
		List_1__ctor_mF8F23D572031748AD428623AE16803455997E297(L_8, /*hidden argument*/List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_RuntimeMethod_var);
		__this->set_wps_44(L_8);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_9 = (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *)il2cpp_codegen_object_new(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var);
		List_1__ctor_mF8F23D572031748AD428623AE16803455997E297(L_9, /*hidden argument*/List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_RuntimeMethod_var);
		__this->set_fullWps_45(L_9);
		__this->set_livePreview_50((bool)1);
		__this->set_perspectiveHandleSize_52((0.5f));
		__this->set_showIndexes_53((bool)1);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_10), (1.0f), (1.0f), (1.0f), (0.5f), /*hidden argument*/NULL);
		__this->set_pathColor_55(L_10);
		ABSAnimationComponent__ctor_mF453633C79A30A108832D3F331DAB5E4CA24B272(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::<Awake>b__38_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenPath_U3CAwakeU3Eb__38_0_m499DE7DE14F8DA2F164520621269F9E93F06C8E4 (DOTweenPath_t07E31AF1CF22D971C83FA3171C5C74EB86812989 * __this, const RuntimeMethod* method)
{
	{
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20((Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *)NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.DOTweenVisualManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenVisualManager_Awake_mD3829E0071BBBDB7A522EA8C385C36EFB4E8383E (DOTweenVisualManager_t4164D354E77A1A82EB58F804D71C15482404E25C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7_m49AD20E6B0D8DB63C42C9DF58850542370EACB8C_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_0;
		L_0 = Component_GetComponent_TisABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7_m49AD20E6B0D8DB63C42C9DF58850542370EACB8C(__this, /*hidden argument*/Component_GetComponent_TisABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7_m49AD20E6B0D8DB63C42C9DF58850542370EACB8C_RuntimeMethod_var);
		__this->set__animComponent_8(L_0);
		return;
	}
}
// System.Void DG.Tweening.DOTweenVisualManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenVisualManager_Update_m01A20A907D4A87C9EA82E16A4A132533053E227A (DOTweenVisualManager_t4164D354E77A1A82EB58F804D71C15482404E25C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__requiresRestartFromSpawnPoint_7();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_1 = __this->get__animComponent_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}

IL_0016:
	{
		return;
	}

IL_0017:
	{
		__this->set__requiresRestartFromSpawnPoint_7((bool)0);
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_3 = __this->get__animComponent_8();
		NullCheck(L_3);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DORestart(System.Boolean) */, L_3, (bool)1);
		return;
	}
}
// System.Void DG.Tweening.DOTweenVisualManager::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenVisualManager_OnEnable_m9BC548951D995D467DC0AB1E7BD257F68E9F4420 (DOTweenVisualManager_t4164D354E77A1A82EB58F804D71C15482404E25C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_onEnableBehaviour_5();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1)))
		{
			case 0:
			{
				goto IL_001c;
			}
			case 1:
			{
				goto IL_0036;
			}
			case 2:
			{
				goto IL_0051;
			}
		}
	}
	{
		return;
	}

IL_001c:
	{
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_2 = __this->get__animComponent_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0058;
		}
	}
	{
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_4 = __this->get__animComponent_8();
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOPlay() */, L_4);
		return;
	}

IL_0036:
	{
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_5 = __this->get__animComponent_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_5, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0058;
		}
	}
	{
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_7 = __this->get__animComponent_8();
		NullCheck(L_7);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DORestart(System.Boolean) */, L_7, (bool)0);
		return;
	}

IL_0051:
	{
		__this->set__requiresRestartFromSpawnPoint_7((bool)1);
	}

IL_0058:
	{
		return;
	}
}
// System.Void DG.Tweening.DOTweenVisualManager::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenVisualManager_OnDisable_mE365B632F8EB4BD87BC1CBAE72AF84F9462613BC (DOTweenVisualManager_t4164D354E77A1A82EB58F804D71C15482404E25C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		__this->set__requiresRestartFromSpawnPoint_7((bool)0);
		int32_t L_0 = __this->get_onDisableBehaviour_6();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1)))
		{
			case 0:
			{
				goto IL_002b;
			}
			case 1:
			{
				goto IL_0048;
			}
			case 2:
			{
				goto IL_0062;
			}
			case 3:
			{
				goto IL_007c;
			}
			case 4:
			{
				goto IL_00a1;
			}
		}
	}
	{
		return;
	}

IL_002b:
	{
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_2 = __this->get__animComponent_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00c5;
		}
	}
	{
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_4 = __this->get__animComponent_8();
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(7 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOPause() */, L_4);
		return;
	}

IL_0048:
	{
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_5 = __this->get__animComponent_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_5, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_00c5;
		}
	}
	{
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_7 = __this->get__animComponent_8();
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(9 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DORewind() */, L_7);
		return;
	}

IL_0062:
	{
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_8 = __this->get__animComponent_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_8, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00c5;
		}
	}
	{
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_10 = __this->get__animComponent_8();
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(12 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOKill() */, L_10);
		return;
	}

IL_007c:
	{
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_11 = __this->get__animComponent_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_12;
		L_12 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_11, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00c5;
		}
	}
	{
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_13 = __this->get__animComponent_8();
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(11 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOComplete() */, L_13);
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_14 = __this->get__animComponent_8();
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(12 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOKill() */, L_14);
		return;
	}

IL_00a1:
	{
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_15 = __this->get__animComponent_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_16;
		L_16 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_15, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00ba;
		}
	}
	{
		ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * L_17 = __this->get__animComponent_8();
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(12 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOKill() */, L_17);
	}

IL_00ba:
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_18;
		L_18 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_18, /*hidden argument*/NULL);
	}

IL_00c5:
	{
		return;
	}
}
// System.Void DG.Tweening.DOTweenVisualManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenVisualManager__ctor_mFDA353BA7AB839C6008B8B0014F8FC92093EAF74 (DOTweenVisualManager_t4164D354E77A1A82EB58F804D71C15482404E25C * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_2 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)__this->get__items_1();
		int32_t L_3 = ___index0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)L_2, (int32_t)L_3);
		return (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
