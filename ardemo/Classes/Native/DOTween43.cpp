﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct ABSTweenPlugin_3_t6A1019F3F26DC4DFAFFA4439E8D04997B8A86E75;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct ABSTweenPlugin_3_tCBAE7B953324A17FF3FA58E62DF7FFF2F5870916;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_tEF220F6242890F93B1000E3697D41B1262D65ED1;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
struct DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10;
// DG.Tweening.Core.DOGetter`1<System.Single>
struct DOGetter_1_tF0D36C857825240D5F2039FD835743757A8410EF;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
struct DOGetter_1_t124A8173423DDFE7DBFE2D18475606BF47E278FF;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
struct DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF;
// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_tB3077E9EB59C7ADF9A3076747F40B1F3F83296FC;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
struct DOSetter_1_tB633699E0271976DA4B170D01AF6535DE81F46CC;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597;
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct TweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_tC048346C0B552E5B61037FE375271319C0EB9564;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// DG.Tweening.EaseFunction
struct EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5;
// DG.Tweening.Sequence
struct Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF;
// System.String
struct String_t;
// DG.Tweening.TweenCallback
struct TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB;
// DG.Tweening.Tweener
struct Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96;
// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392;
// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_tB8834D1D0E93BA282464F6636FE1B460D73E7AF8;
// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_tAF5B6371344DA9513471031395C57B5466607F3E;

IL2CPP_EXTERN_C RuntimeClass* DOGetter_1_t124A8173423DDFE7DBFE2D18475606BF47E278FF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DOGetter_1_tF0D36C857825240D5F2039FD835743757A8410EF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DOSetter_1_tB3077E9EB59C7ADF9A3076747F40B1F3F83296FC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DOSetter_1_tB633699E0271976DA4B170D01AF6535DE81F46CC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass5_0_tB8834D1D0E93BA282464F6636FE1B460D73E7AF8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass8_0_tAF5B6371344DA9513471031395C57B5466607F3E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeMethod* DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DOGetter_1__ctor_mCB309EC70189CBE3BF3FE2E6A96AA71716359C08_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DOGetter_1__ctor_mCDCE48F97A38C7D3830E1484A33779D9C16CB99D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DOSetter_1__ctor_m39F0112BD65B9E72E1A4A7BAD2BCD56AC56CA736_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DOSetter_1__ctor_m5EA89B43642DA9193568B0D408DC288C2890D76E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Rigidbody2D_MovePosition_mB4493BFC30B2FEBB02C7819AAE626871939D5BD9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Rigidbody2D_MoveRotation_mB550025C3B09DB909CB895609BF642EF768C5B22_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597_mDB9A07D9A7A3EFAC4B1B27A61BFB1FF1492809C4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6_m3ABE5A45167B552A625305CCCFF29BC87F315688_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetTarget_TisTweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_mD9FAB53299A02D8E43256A69277B92879FD3C8AC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__0_m1D7DF9711656BBF94B94806F1F8BCEBD054C71E4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__1_m0DFCE7EB3897535D529FA90C2B100ED244F383FC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__0_m8BD6A8476E7D96B335DCCED905B443EE68DADEF7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__1_mAF96917D873B7CE5960E949FF567DB4C23DC2961_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass5_0_U3CDOMoveU3Eb__0_m9D8CAC5B7FAEBAACD75489B96693E2066A998218_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass8_0_U3CDORotateU3Eb__0_mD311B368E4BB9122A2784911F2CA4F2846AC1439_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t58800F9F376F3DAC8FEF6D270699C75FF9420324 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// DG.Tweening.ShortcutExtensions43
struct  ShortcutExtensions43_t6885A075A1BAE79A8D38314262924D3DB0106BF0  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96  : public RuntimeObject
{
public:
	// UnityEngine.SpriteRenderer DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::target
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96, ___target_0)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_target_0() const { return ___target_0; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392  : public RuntimeObject
{
public:
	// UnityEngine.SpriteRenderer DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::target
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392, ___target_0)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_target_0() const { return ___target_0; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_tB8834D1D0E93BA282464F6636FE1B460D73E7AF8  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0::target
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tB8834D1D0E93BA282464F6636FE1B460D73E7AF8, ___target_0)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_tAF5B6371344DA9513471031395C57B5466607F3E  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0::target
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tAF5B6371344DA9513471031395C57B5466607F3E, ___target_0)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// DG.Tweening.Plugins.Options.ColorOptions
struct  ColorOptions_t25AF005F398643658A000DBAD00EFF82C944355A 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.ColorOptions::alphaOnly
	bool ___alphaOnly_0;

public:
	inline static int32_t get_offset_of_alphaOnly_0() { return static_cast<int32_t>(offsetof(ColorOptions_t25AF005F398643658A000DBAD00EFF82C944355A, ___alphaOnly_0)); }
	inline bool get_alphaOnly_0() const { return ___alphaOnly_0; }
	inline bool* get_address_of_alphaOnly_0() { return &___alphaOnly_0; }
	inline void set_alphaOnly_0(bool value)
	{
		___alphaOnly_0 = value;
	}
};

// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t25AF005F398643658A000DBAD00EFF82C944355A_marshaled_pinvoke
{
	int32_t ___alphaOnly_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t25AF005F398643658A000DBAD00EFF82C944355A_marshaled_com
{
	int32_t ___alphaOnly_0;
};

// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// DG.Tweening.Plugins.Options.FloatOptions
struct  FloatOptions_t83EDE0C4170937A97158EC0DE5DBBABB89818603 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.FloatOptions::snapping
	bool ___snapping_0;

public:
	inline static int32_t get_offset_of_snapping_0() { return static_cast<int32_t>(offsetof(FloatOptions_t83EDE0C4170937A97158EC0DE5DBBABB89818603, ___snapping_0)); }
	inline bool get_snapping_0() const { return ___snapping_0; }
	inline bool* get_address_of_snapping_0() { return &___snapping_0; }
	inline void set_snapping_0(bool value)
	{
		___snapping_0 = value;
	}
};

// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t83EDE0C4170937A97158EC0DE5DBBABB89818603_marshaled_pinvoke
{
	int32_t ___snapping_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t83EDE0C4170937A97158EC0DE5DBBABB89818603_marshaled_com
{
	int32_t ___snapping_0;
};

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// DG.Tweening.AxisConstraint
struct  AxisConstraint_tA0D384964013674923F26C7DF2618FB76CD3D7F4 
{
public:
	// System.Int32 DG.Tweening.AxisConstraint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisConstraint_tA0D384964013674923F26C7DF2618FB76CD3D7F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// DG.Tweening.Ease
struct  Ease_tB04D625230DDC5B40D74E825C8A9DBBE37A146B4 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Ease_tB04D625230DDC5B40D74E825C8A9DBBE37A146B4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.LoopType
struct  LoopType_tF807A5805F6A83F5228BE7D4E633B2572B1B859A 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoopType_tF807A5805F6A83F5228BE7D4E633B2572B1B859A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// DG.Tweening.Core.Enums.SpecialStartupMode
struct  SpecialStartupMode_t0086D2AE798C217210762DD17C8D3572414ACD92 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.SpecialStartupMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialStartupMode_t0086D2AE798C217210762DD17C8D3572414ACD92, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.TweenType
struct  TweenType_tAB2DEC1268409EA172594368494218E51696EF5D 
{
public:
	// System.Int32 DG.Tweening.TweenType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TweenType_tAB2DEC1268409EA172594368494218E51696EF5D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.UpdateType
struct  UpdateType_t9D838506DD148F29E6183FB298E41921E51CC5ED 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateType_t9D838506DD148F29E6183FB298E41921E51CC5ED, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.Core.ABSSequentiable
struct  ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76  : public RuntimeObject
{
public:
	// DG.Tweening.TweenType DG.Tweening.Core.ABSSequentiable::tweenType
	int32_t ___tweenType_0;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedPosition
	float ___sequencedPosition_1;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedEndPosition
	float ___sequencedEndPosition_2;
	// DG.Tweening.TweenCallback DG.Tweening.Core.ABSSequentiable::onStart
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onStart_3;

public:
	inline static int32_t get_offset_of_tweenType_0() { return static_cast<int32_t>(offsetof(ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76, ___tweenType_0)); }
	inline int32_t get_tweenType_0() const { return ___tweenType_0; }
	inline int32_t* get_address_of_tweenType_0() { return &___tweenType_0; }
	inline void set_tweenType_0(int32_t value)
	{
		___tweenType_0 = value;
	}

	inline static int32_t get_offset_of_sequencedPosition_1() { return static_cast<int32_t>(offsetof(ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76, ___sequencedPosition_1)); }
	inline float get_sequencedPosition_1() const { return ___sequencedPosition_1; }
	inline float* get_address_of_sequencedPosition_1() { return &___sequencedPosition_1; }
	inline void set_sequencedPosition_1(float value)
	{
		___sequencedPosition_1 = value;
	}

	inline static int32_t get_offset_of_sequencedEndPosition_2() { return static_cast<int32_t>(offsetof(ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76, ___sequencedEndPosition_2)); }
	inline float get_sequencedEndPosition_2() const { return ___sequencedEndPosition_2; }
	inline float* get_address_of_sequencedEndPosition_2() { return &___sequencedEndPosition_2; }
	inline void set_sequencedEndPosition_2(float value)
	{
		___sequencedEndPosition_2 = value;
	}

	inline static int32_t get_offset_of_onStart_3() { return static_cast<int32_t>(offsetof(ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76, ___onStart_3)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onStart_3() const { return ___onStart_3; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onStart_3() { return &___onStart_3; }
	inline void set_onStart_3(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onStart_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStart_3), (void*)value);
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// DG.Tweening.Plugins.Options.VectorOptions
struct  VectorOptions_t75B437FFE9996394BC720A3F95ABFF81F338B0AB 
{
public:
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.VectorOptions::axisConstraint
	int32_t ___axisConstraint_0;
	// System.Boolean DG.Tweening.Plugins.Options.VectorOptions::snapping
	bool ___snapping_1;

public:
	inline static int32_t get_offset_of_axisConstraint_0() { return static_cast<int32_t>(offsetof(VectorOptions_t75B437FFE9996394BC720A3F95ABFF81F338B0AB, ___axisConstraint_0)); }
	inline int32_t get_axisConstraint_0() const { return ___axisConstraint_0; }
	inline int32_t* get_address_of_axisConstraint_0() { return &___axisConstraint_0; }
	inline void set_axisConstraint_0(int32_t value)
	{
		___axisConstraint_0 = value;
	}

	inline static int32_t get_offset_of_snapping_1() { return static_cast<int32_t>(offsetof(VectorOptions_t75B437FFE9996394BC720A3F95ABFF81F338B0AB, ___snapping_1)); }
	inline bool get_snapping_1() const { return ___snapping_1; }
	inline bool* get_address_of_snapping_1() { return &___snapping_1; }
	inline void set_snapping_1(bool value)
	{
		___snapping_1 = value;
	}
};

// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t75B437FFE9996394BC720A3F95ABFF81F338B0AB_marshaled_pinvoke
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t75B437FFE9996394BC720A3F95ABFF81F338B0AB_marshaled_com
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};

// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
struct  DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10  : public MulticastDelegate_t
{
public:

public:
};


// DG.Tweening.Core.DOGetter`1<System.Single>
struct  DOGetter_1_tF0D36C857825240D5F2039FD835743757A8410EF  : public MulticastDelegate_t
{
public:

public:
};


// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
struct  DOGetter_1_t124A8173423DDFE7DBFE2D18475606BF47E278FF  : public MulticastDelegate_t
{
public:

public:
};


// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
struct  DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF  : public MulticastDelegate_t
{
public:

public:
};


// DG.Tweening.Core.DOSetter`1<System.Single>
struct  DOSetter_1_tB3077E9EB59C7ADF9A3076747F40B1F3F83296FC  : public MulticastDelegate_t
{
public:

public:
};


// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
struct  DOSetter_1_tB633699E0271976DA4B170D01AF6535DE81F46CC  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Renderer
struct  Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Rigidbody2D
struct  Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// DG.Tweening.Tween
struct  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941  : public ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76
{
public:
	// System.Single DG.Tweening.Tween::timeScale
	float ___timeScale_4;
	// System.Boolean DG.Tweening.Tween::isBackwards
	bool ___isBackwards_5;
	// System.Object DG.Tweening.Tween::id
	RuntimeObject * ___id_6;
	// System.Object DG.Tweening.Tween::target
	RuntimeObject * ___target_7;
	// DG.Tweening.UpdateType DG.Tweening.Tween::updateType
	int32_t ___updateType_8;
	// System.Boolean DG.Tweening.Tween::isIndependentUpdate
	bool ___isIndependentUpdate_9;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPlay
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onPlay_10;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPause
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onPause_11;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onRewind
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onRewind_12;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onUpdate
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onUpdate_13;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onStepComplete
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onStepComplete_14;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onComplete
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onComplete_15;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onKill
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onKill_16;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.Tween::onWaypointChange
	TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B * ___onWaypointChange_17;
	// System.Boolean DG.Tweening.Tween::isFrom
	bool ___isFrom_18;
	// System.Boolean DG.Tweening.Tween::isBlendable
	bool ___isBlendable_19;
	// System.Boolean DG.Tweening.Tween::isRecyclable
	bool ___isRecyclable_20;
	// System.Boolean DG.Tweening.Tween::isSpeedBased
	bool ___isSpeedBased_21;
	// System.Boolean DG.Tweening.Tween::autoKill
	bool ___autoKill_22;
	// System.Single DG.Tweening.Tween::duration
	float ___duration_23;
	// System.Int32 DG.Tweening.Tween::loops
	int32_t ___loops_24;
	// DG.Tweening.LoopType DG.Tweening.Tween::loopType
	int32_t ___loopType_25;
	// System.Single DG.Tweening.Tween::delay
	float ___delay_26;
	// System.Boolean DG.Tweening.Tween::isRelative
	bool ___isRelative_27;
	// DG.Tweening.Ease DG.Tweening.Tween::easeType
	int32_t ___easeType_28;
	// DG.Tweening.EaseFunction DG.Tweening.Tween::customEase
	EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9 * ___customEase_29;
	// System.Single DG.Tweening.Tween::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_30;
	// System.Single DG.Tweening.Tween::easePeriod
	float ___easePeriod_31;
	// System.Type DG.Tweening.Tween::typeofT1
	Type_t * ___typeofT1_32;
	// System.Type DG.Tweening.Tween::typeofT2
	Type_t * ___typeofT2_33;
	// System.Type DG.Tweening.Tween::typeofTPlugOptions
	Type_t * ___typeofTPlugOptions_34;
	// System.Boolean DG.Tweening.Tween::active
	bool ___active_35;
	// System.Boolean DG.Tweening.Tween::isSequenced
	bool ___isSequenced_36;
	// DG.Tweening.Sequence DG.Tweening.Tween::sequenceParent
	Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * ___sequenceParent_37;
	// System.Int32 DG.Tweening.Tween::activeId
	int32_t ___activeId_38;
	// DG.Tweening.Core.Enums.SpecialStartupMode DG.Tweening.Tween::specialStartupMode
	int32_t ___specialStartupMode_39;
	// System.Boolean DG.Tweening.Tween::creationLocked
	bool ___creationLocked_40;
	// System.Boolean DG.Tweening.Tween::startupDone
	bool ___startupDone_41;
	// System.Boolean DG.Tweening.Tween::playedOnce
	bool ___playedOnce_42;
	// System.Single DG.Tweening.Tween::position
	float ___position_43;
	// System.Single DG.Tweening.Tween::fullDuration
	float ___fullDuration_44;
	// System.Int32 DG.Tweening.Tween::completedLoops
	int32_t ___completedLoops_45;
	// System.Boolean DG.Tweening.Tween::isPlaying
	bool ___isPlaying_46;
	// System.Boolean DG.Tweening.Tween::isComplete
	bool ___isComplete_47;
	// System.Single DG.Tweening.Tween::elapsedDelay
	float ___elapsedDelay_48;
	// System.Boolean DG.Tweening.Tween::delayComplete
	bool ___delayComplete_49;
	// System.Int32 DG.Tweening.Tween::miscInt
	int32_t ___miscInt_50;

public:
	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_isBackwards_5() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isBackwards_5)); }
	inline bool get_isBackwards_5() const { return ___isBackwards_5; }
	inline bool* get_address_of_isBackwards_5() { return &___isBackwards_5; }
	inline void set_isBackwards_5(bool value)
	{
		___isBackwards_5 = value;
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___id_6)); }
	inline RuntimeObject * get_id_6() const { return ___id_6; }
	inline RuntimeObject ** get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(RuntimeObject * value)
	{
		___id_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___id_6), (void*)value);
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___target_7)); }
	inline RuntimeObject * get_target_7() const { return ___target_7; }
	inline RuntimeObject ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(RuntimeObject * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_7), (void*)value);
	}

	inline static int32_t get_offset_of_updateType_8() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___updateType_8)); }
	inline int32_t get_updateType_8() const { return ___updateType_8; }
	inline int32_t* get_address_of_updateType_8() { return &___updateType_8; }
	inline void set_updateType_8(int32_t value)
	{
		___updateType_8 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_9() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isIndependentUpdate_9)); }
	inline bool get_isIndependentUpdate_9() const { return ___isIndependentUpdate_9; }
	inline bool* get_address_of_isIndependentUpdate_9() { return &___isIndependentUpdate_9; }
	inline void set_isIndependentUpdate_9(bool value)
	{
		___isIndependentUpdate_9 = value;
	}

	inline static int32_t get_offset_of_onPlay_10() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onPlay_10)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onPlay_10() const { return ___onPlay_10; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onPlay_10() { return &___onPlay_10; }
	inline void set_onPlay_10(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onPlay_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPlay_10), (void*)value);
	}

	inline static int32_t get_offset_of_onPause_11() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onPause_11)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onPause_11() const { return ___onPause_11; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onPause_11() { return &___onPause_11; }
	inline void set_onPause_11(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onPause_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPause_11), (void*)value);
	}

	inline static int32_t get_offset_of_onRewind_12() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onRewind_12)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onRewind_12() const { return ___onRewind_12; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onRewind_12() { return &___onRewind_12; }
	inline void set_onRewind_12(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onRewind_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onRewind_12), (void*)value);
	}

	inline static int32_t get_offset_of_onUpdate_13() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onUpdate_13)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onUpdate_13() const { return ___onUpdate_13; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onUpdate_13() { return &___onUpdate_13; }
	inline void set_onUpdate_13(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onUpdate_13), (void*)value);
	}

	inline static int32_t get_offset_of_onStepComplete_14() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onStepComplete_14)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onStepComplete_14() const { return ___onStepComplete_14; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onStepComplete_14() { return &___onStepComplete_14; }
	inline void set_onStepComplete_14(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onStepComplete_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStepComplete_14), (void*)value);
	}

	inline static int32_t get_offset_of_onComplete_15() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onComplete_15)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onComplete_15() const { return ___onComplete_15; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onComplete_15() { return &___onComplete_15; }
	inline void set_onComplete_15(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onComplete_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onComplete_15), (void*)value);
	}

	inline static int32_t get_offset_of_onKill_16() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onKill_16)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onKill_16() const { return ___onKill_16; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onKill_16() { return &___onKill_16; }
	inline void set_onKill_16(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onKill_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onKill_16), (void*)value);
	}

	inline static int32_t get_offset_of_onWaypointChange_17() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onWaypointChange_17)); }
	inline TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B * get_onWaypointChange_17() const { return ___onWaypointChange_17; }
	inline TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B ** get_address_of_onWaypointChange_17() { return &___onWaypointChange_17; }
	inline void set_onWaypointChange_17(TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B * value)
	{
		___onWaypointChange_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onWaypointChange_17), (void*)value);
	}

	inline static int32_t get_offset_of_isFrom_18() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isFrom_18)); }
	inline bool get_isFrom_18() const { return ___isFrom_18; }
	inline bool* get_address_of_isFrom_18() { return &___isFrom_18; }
	inline void set_isFrom_18(bool value)
	{
		___isFrom_18 = value;
	}

	inline static int32_t get_offset_of_isBlendable_19() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isBlendable_19)); }
	inline bool get_isBlendable_19() const { return ___isBlendable_19; }
	inline bool* get_address_of_isBlendable_19() { return &___isBlendable_19; }
	inline void set_isBlendable_19(bool value)
	{
		___isBlendable_19 = value;
	}

	inline static int32_t get_offset_of_isRecyclable_20() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isRecyclable_20)); }
	inline bool get_isRecyclable_20() const { return ___isRecyclable_20; }
	inline bool* get_address_of_isRecyclable_20() { return &___isRecyclable_20; }
	inline void set_isRecyclable_20(bool value)
	{
		___isRecyclable_20 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_21() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isSpeedBased_21)); }
	inline bool get_isSpeedBased_21() const { return ___isSpeedBased_21; }
	inline bool* get_address_of_isSpeedBased_21() { return &___isSpeedBased_21; }
	inline void set_isSpeedBased_21(bool value)
	{
		___isSpeedBased_21 = value;
	}

	inline static int32_t get_offset_of_autoKill_22() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___autoKill_22)); }
	inline bool get_autoKill_22() const { return ___autoKill_22; }
	inline bool* get_address_of_autoKill_22() { return &___autoKill_22; }
	inline void set_autoKill_22(bool value)
	{
		___autoKill_22 = value;
	}

	inline static int32_t get_offset_of_duration_23() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___duration_23)); }
	inline float get_duration_23() const { return ___duration_23; }
	inline float* get_address_of_duration_23() { return &___duration_23; }
	inline void set_duration_23(float value)
	{
		___duration_23 = value;
	}

	inline static int32_t get_offset_of_loops_24() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___loops_24)); }
	inline int32_t get_loops_24() const { return ___loops_24; }
	inline int32_t* get_address_of_loops_24() { return &___loops_24; }
	inline void set_loops_24(int32_t value)
	{
		___loops_24 = value;
	}

	inline static int32_t get_offset_of_loopType_25() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___loopType_25)); }
	inline int32_t get_loopType_25() const { return ___loopType_25; }
	inline int32_t* get_address_of_loopType_25() { return &___loopType_25; }
	inline void set_loopType_25(int32_t value)
	{
		___loopType_25 = value;
	}

	inline static int32_t get_offset_of_delay_26() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___delay_26)); }
	inline float get_delay_26() const { return ___delay_26; }
	inline float* get_address_of_delay_26() { return &___delay_26; }
	inline void set_delay_26(float value)
	{
		___delay_26 = value;
	}

	inline static int32_t get_offset_of_isRelative_27() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isRelative_27)); }
	inline bool get_isRelative_27() const { return ___isRelative_27; }
	inline bool* get_address_of_isRelative_27() { return &___isRelative_27; }
	inline void set_isRelative_27(bool value)
	{
		___isRelative_27 = value;
	}

	inline static int32_t get_offset_of_easeType_28() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___easeType_28)); }
	inline int32_t get_easeType_28() const { return ___easeType_28; }
	inline int32_t* get_address_of_easeType_28() { return &___easeType_28; }
	inline void set_easeType_28(int32_t value)
	{
		___easeType_28 = value;
	}

	inline static int32_t get_offset_of_customEase_29() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___customEase_29)); }
	inline EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9 * get_customEase_29() const { return ___customEase_29; }
	inline EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9 ** get_address_of_customEase_29() { return &___customEase_29; }
	inline void set_customEase_29(EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9 * value)
	{
		___customEase_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___customEase_29), (void*)value);
	}

	inline static int32_t get_offset_of_easeOvershootOrAmplitude_30() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___easeOvershootOrAmplitude_30)); }
	inline float get_easeOvershootOrAmplitude_30() const { return ___easeOvershootOrAmplitude_30; }
	inline float* get_address_of_easeOvershootOrAmplitude_30() { return &___easeOvershootOrAmplitude_30; }
	inline void set_easeOvershootOrAmplitude_30(float value)
	{
		___easeOvershootOrAmplitude_30 = value;
	}

	inline static int32_t get_offset_of_easePeriod_31() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___easePeriod_31)); }
	inline float get_easePeriod_31() const { return ___easePeriod_31; }
	inline float* get_address_of_easePeriod_31() { return &___easePeriod_31; }
	inline void set_easePeriod_31(float value)
	{
		___easePeriod_31 = value;
	}

	inline static int32_t get_offset_of_typeofT1_32() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___typeofT1_32)); }
	inline Type_t * get_typeofT1_32() const { return ___typeofT1_32; }
	inline Type_t ** get_address_of_typeofT1_32() { return &___typeofT1_32; }
	inline void set_typeofT1_32(Type_t * value)
	{
		___typeofT1_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeofT1_32), (void*)value);
	}

	inline static int32_t get_offset_of_typeofT2_33() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___typeofT2_33)); }
	inline Type_t * get_typeofT2_33() const { return ___typeofT2_33; }
	inline Type_t ** get_address_of_typeofT2_33() { return &___typeofT2_33; }
	inline void set_typeofT2_33(Type_t * value)
	{
		___typeofT2_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeofT2_33), (void*)value);
	}

	inline static int32_t get_offset_of_typeofTPlugOptions_34() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___typeofTPlugOptions_34)); }
	inline Type_t * get_typeofTPlugOptions_34() const { return ___typeofTPlugOptions_34; }
	inline Type_t ** get_address_of_typeofTPlugOptions_34() { return &___typeofTPlugOptions_34; }
	inline void set_typeofTPlugOptions_34(Type_t * value)
	{
		___typeofTPlugOptions_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeofTPlugOptions_34), (void*)value);
	}

	inline static int32_t get_offset_of_active_35() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___active_35)); }
	inline bool get_active_35() const { return ___active_35; }
	inline bool* get_address_of_active_35() { return &___active_35; }
	inline void set_active_35(bool value)
	{
		___active_35 = value;
	}

	inline static int32_t get_offset_of_isSequenced_36() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isSequenced_36)); }
	inline bool get_isSequenced_36() const { return ___isSequenced_36; }
	inline bool* get_address_of_isSequenced_36() { return &___isSequenced_36; }
	inline void set_isSequenced_36(bool value)
	{
		___isSequenced_36 = value;
	}

	inline static int32_t get_offset_of_sequenceParent_37() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___sequenceParent_37)); }
	inline Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * get_sequenceParent_37() const { return ___sequenceParent_37; }
	inline Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E ** get_address_of_sequenceParent_37() { return &___sequenceParent_37; }
	inline void set_sequenceParent_37(Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * value)
	{
		___sequenceParent_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sequenceParent_37), (void*)value);
	}

	inline static int32_t get_offset_of_activeId_38() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___activeId_38)); }
	inline int32_t get_activeId_38() const { return ___activeId_38; }
	inline int32_t* get_address_of_activeId_38() { return &___activeId_38; }
	inline void set_activeId_38(int32_t value)
	{
		___activeId_38 = value;
	}

	inline static int32_t get_offset_of_specialStartupMode_39() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___specialStartupMode_39)); }
	inline int32_t get_specialStartupMode_39() const { return ___specialStartupMode_39; }
	inline int32_t* get_address_of_specialStartupMode_39() { return &___specialStartupMode_39; }
	inline void set_specialStartupMode_39(int32_t value)
	{
		___specialStartupMode_39 = value;
	}

	inline static int32_t get_offset_of_creationLocked_40() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___creationLocked_40)); }
	inline bool get_creationLocked_40() const { return ___creationLocked_40; }
	inline bool* get_address_of_creationLocked_40() { return &___creationLocked_40; }
	inline void set_creationLocked_40(bool value)
	{
		___creationLocked_40 = value;
	}

	inline static int32_t get_offset_of_startupDone_41() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___startupDone_41)); }
	inline bool get_startupDone_41() const { return ___startupDone_41; }
	inline bool* get_address_of_startupDone_41() { return &___startupDone_41; }
	inline void set_startupDone_41(bool value)
	{
		___startupDone_41 = value;
	}

	inline static int32_t get_offset_of_playedOnce_42() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___playedOnce_42)); }
	inline bool get_playedOnce_42() const { return ___playedOnce_42; }
	inline bool* get_address_of_playedOnce_42() { return &___playedOnce_42; }
	inline void set_playedOnce_42(bool value)
	{
		___playedOnce_42 = value;
	}

	inline static int32_t get_offset_of_position_43() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___position_43)); }
	inline float get_position_43() const { return ___position_43; }
	inline float* get_address_of_position_43() { return &___position_43; }
	inline void set_position_43(float value)
	{
		___position_43 = value;
	}

	inline static int32_t get_offset_of_fullDuration_44() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___fullDuration_44)); }
	inline float get_fullDuration_44() const { return ___fullDuration_44; }
	inline float* get_address_of_fullDuration_44() { return &___fullDuration_44; }
	inline void set_fullDuration_44(float value)
	{
		___fullDuration_44 = value;
	}

	inline static int32_t get_offset_of_completedLoops_45() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___completedLoops_45)); }
	inline int32_t get_completedLoops_45() const { return ___completedLoops_45; }
	inline int32_t* get_address_of_completedLoops_45() { return &___completedLoops_45; }
	inline void set_completedLoops_45(int32_t value)
	{
		___completedLoops_45 = value;
	}

	inline static int32_t get_offset_of_isPlaying_46() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isPlaying_46)); }
	inline bool get_isPlaying_46() const { return ___isPlaying_46; }
	inline bool* get_address_of_isPlaying_46() { return &___isPlaying_46; }
	inline void set_isPlaying_46(bool value)
	{
		___isPlaying_46 = value;
	}

	inline static int32_t get_offset_of_isComplete_47() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isComplete_47)); }
	inline bool get_isComplete_47() const { return ___isComplete_47; }
	inline bool* get_address_of_isComplete_47() { return &___isComplete_47; }
	inline void set_isComplete_47(bool value)
	{
		___isComplete_47 = value;
	}

	inline static int32_t get_offset_of_elapsedDelay_48() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___elapsedDelay_48)); }
	inline float get_elapsedDelay_48() const { return ___elapsedDelay_48; }
	inline float* get_address_of_elapsedDelay_48() { return &___elapsedDelay_48; }
	inline void set_elapsedDelay_48(float value)
	{
		___elapsedDelay_48 = value;
	}

	inline static int32_t get_offset_of_delayComplete_49() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___delayComplete_49)); }
	inline bool get_delayComplete_49() const { return ___delayComplete_49; }
	inline bool* get_address_of_delayComplete_49() { return &___delayComplete_49; }
	inline void set_delayComplete_49(bool value)
	{
		___delayComplete_49 = value;
	}

	inline static int32_t get_offset_of_miscInt_50() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___miscInt_50)); }
	inline int32_t get_miscInt_50() const { return ___miscInt_50; }
	inline int32_t* get_address_of_miscInt_50() { return &___miscInt_50; }
	inline void set_miscInt_50(int32_t value)
	{
		___miscInt_50 = value;
	}
};


// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// DG.Tweening.Tweener
struct  Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8  : public Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941
{
public:
	// System.Boolean DG.Tweening.Tweener::hasManuallySetStartValue
	bool ___hasManuallySetStartValue_51;
	// System.Boolean DG.Tweening.Tweener::isFromAllowed
	bool ___isFromAllowed_52;

public:
	inline static int32_t get_offset_of_hasManuallySetStartValue_51() { return static_cast<int32_t>(offsetof(Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8, ___hasManuallySetStartValue_51)); }
	inline bool get_hasManuallySetStartValue_51() const { return ___hasManuallySetStartValue_51; }
	inline bool* get_address_of_hasManuallySetStartValue_51() { return &___hasManuallySetStartValue_51; }
	inline void set_hasManuallySetStartValue_51(bool value)
	{
		___hasManuallySetStartValue_51 = value;
	}

	inline static int32_t get_offset_of_isFromAllowed_52() { return static_cast<int32_t>(offsetof(Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8, ___isFromAllowed_52)); }
	inline bool get_isFromAllowed_52() const { return ___isFromAllowed_52; }
	inline bool* get_address_of_isFromAllowed_52() { return &___isFromAllowed_52; }
	inline void set_isFromAllowed_52(bool value)
	{
		___isFromAllowed_52 = value;
	}
};


// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct  TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597  : public Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	ColorOptions_t25AF005F398643658A000DBAD00EFF82C944355A  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_t6A1019F3F26DC4DFAFFA4439E8D04997B8A86E75 * ___tweenPlugin_59;

public:
	inline static int32_t get_offset_of_startValue_53() { return static_cast<int32_t>(offsetof(TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597, ___startValue_53)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_startValue_53() const { return ___startValue_53; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_startValue_53() { return &___startValue_53; }
	inline void set_startValue_53(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___startValue_53 = value;
	}

	inline static int32_t get_offset_of_endValue_54() { return static_cast<int32_t>(offsetof(TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597, ___endValue_54)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_endValue_54() const { return ___endValue_54; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_endValue_54() { return &___endValue_54; }
	inline void set_endValue_54(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___endValue_54 = value;
	}

	inline static int32_t get_offset_of_changeValue_55() { return static_cast<int32_t>(offsetof(TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597, ___changeValue_55)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_changeValue_55() const { return ___changeValue_55; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_changeValue_55() { return &___changeValue_55; }
	inline void set_changeValue_55(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___changeValue_55 = value;
	}

	inline static int32_t get_offset_of_plugOptions_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597, ___plugOptions_56)); }
	inline ColorOptions_t25AF005F398643658A000DBAD00EFF82C944355A  get_plugOptions_56() const { return ___plugOptions_56; }
	inline ColorOptions_t25AF005F398643658A000DBAD00EFF82C944355A * get_address_of_plugOptions_56() { return &___plugOptions_56; }
	inline void set_plugOptions_56(ColorOptions_t25AF005F398643658A000DBAD00EFF82C944355A  value)
	{
		___plugOptions_56 = value;
	}

	inline static int32_t get_offset_of_getter_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597, ___getter_57)); }
	inline DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * get_getter_57() const { return ___getter_57; }
	inline DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 ** get_address_of_getter_57() { return &___getter_57; }
	inline void set_getter_57(DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * value)
	{
		___getter_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getter_57), (void*)value);
	}

	inline static int32_t get_offset_of_setter_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597, ___setter_58)); }
	inline DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * get_setter_58() const { return ___setter_58; }
	inline DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF ** get_address_of_setter_58() { return &___setter_58; }
	inline void set_setter_58(DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * value)
	{
		___setter_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___setter_58), (void*)value);
	}

	inline static int32_t get_offset_of_tweenPlugin_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597, ___tweenPlugin_59)); }
	inline ABSTweenPlugin_3_t6A1019F3F26DC4DFAFFA4439E8D04997B8A86E75 * get_tweenPlugin_59() const { return ___tweenPlugin_59; }
	inline ABSTweenPlugin_3_t6A1019F3F26DC4DFAFFA4439E8D04997B8A86E75 ** get_address_of_tweenPlugin_59() { return &___tweenPlugin_59; }
	inline void set_tweenPlugin_59(ABSTweenPlugin_3_t6A1019F3F26DC4DFAFFA4439E8D04997B8A86E75 * value)
	{
		___tweenPlugin_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tweenPlugin_59), (void*)value);
	}
};


// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct  TweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6  : public Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	float ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	float ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	float ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	FloatOptions_t83EDE0C4170937A97158EC0DE5DBBABB89818603  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_tF0D36C857825240D5F2039FD835743757A8410EF * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_tB3077E9EB59C7ADF9A3076747F40B1F3F83296FC * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_tCBAE7B953324A17FF3FA58E62DF7FFF2F5870916 * ___tweenPlugin_59;

public:
	inline static int32_t get_offset_of_startValue_53() { return static_cast<int32_t>(offsetof(TweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6, ___startValue_53)); }
	inline float get_startValue_53() const { return ___startValue_53; }
	inline float* get_address_of_startValue_53() { return &___startValue_53; }
	inline void set_startValue_53(float value)
	{
		___startValue_53 = value;
	}

	inline static int32_t get_offset_of_endValue_54() { return static_cast<int32_t>(offsetof(TweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6, ___endValue_54)); }
	inline float get_endValue_54() const { return ___endValue_54; }
	inline float* get_address_of_endValue_54() { return &___endValue_54; }
	inline void set_endValue_54(float value)
	{
		___endValue_54 = value;
	}

	inline static int32_t get_offset_of_changeValue_55() { return static_cast<int32_t>(offsetof(TweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6, ___changeValue_55)); }
	inline float get_changeValue_55() const { return ___changeValue_55; }
	inline float* get_address_of_changeValue_55() { return &___changeValue_55; }
	inline void set_changeValue_55(float value)
	{
		___changeValue_55 = value;
	}

	inline static int32_t get_offset_of_plugOptions_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6, ___plugOptions_56)); }
	inline FloatOptions_t83EDE0C4170937A97158EC0DE5DBBABB89818603  get_plugOptions_56() const { return ___plugOptions_56; }
	inline FloatOptions_t83EDE0C4170937A97158EC0DE5DBBABB89818603 * get_address_of_plugOptions_56() { return &___plugOptions_56; }
	inline void set_plugOptions_56(FloatOptions_t83EDE0C4170937A97158EC0DE5DBBABB89818603  value)
	{
		___plugOptions_56 = value;
	}

	inline static int32_t get_offset_of_getter_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6, ___getter_57)); }
	inline DOGetter_1_tF0D36C857825240D5F2039FD835743757A8410EF * get_getter_57() const { return ___getter_57; }
	inline DOGetter_1_tF0D36C857825240D5F2039FD835743757A8410EF ** get_address_of_getter_57() { return &___getter_57; }
	inline void set_getter_57(DOGetter_1_tF0D36C857825240D5F2039FD835743757A8410EF * value)
	{
		___getter_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getter_57), (void*)value);
	}

	inline static int32_t get_offset_of_setter_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6, ___setter_58)); }
	inline DOSetter_1_tB3077E9EB59C7ADF9A3076747F40B1F3F83296FC * get_setter_58() const { return ___setter_58; }
	inline DOSetter_1_tB3077E9EB59C7ADF9A3076747F40B1F3F83296FC ** get_address_of_setter_58() { return &___setter_58; }
	inline void set_setter_58(DOSetter_1_tB3077E9EB59C7ADF9A3076747F40B1F3F83296FC * value)
	{
		___setter_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___setter_58), (void*)value);
	}

	inline static int32_t get_offset_of_tweenPlugin_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6, ___tweenPlugin_59)); }
	inline ABSTweenPlugin_3_tCBAE7B953324A17FF3FA58E62DF7FFF2F5870916 * get_tweenPlugin_59() const { return ___tweenPlugin_59; }
	inline ABSTweenPlugin_3_tCBAE7B953324A17FF3FA58E62DF7FFF2F5870916 ** get_address_of_tweenPlugin_59() { return &___tweenPlugin_59; }
	inline void set_tweenPlugin_59(ABSTweenPlugin_3_tCBAE7B953324A17FF3FA58E62DF7FFF2F5870916 * value)
	{
		___tweenPlugin_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tweenPlugin_59), (void*)value);
	}
};


// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct  TweenerCore_3_tC048346C0B552E5B61037FE375271319C0EB9564  : public Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	VectorOptions_t75B437FFE9996394BC720A3F95ABFF81F338B0AB  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_t124A8173423DDFE7DBFE2D18475606BF47E278FF * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_tB633699E0271976DA4B170D01AF6535DE81F46CC * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_tEF220F6242890F93B1000E3697D41B1262D65ED1 * ___tweenPlugin_59;

public:
	inline static int32_t get_offset_of_startValue_53() { return static_cast<int32_t>(offsetof(TweenerCore_3_tC048346C0B552E5B61037FE375271319C0EB9564, ___startValue_53)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_startValue_53() const { return ___startValue_53; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_startValue_53() { return &___startValue_53; }
	inline void set_startValue_53(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___startValue_53 = value;
	}

	inline static int32_t get_offset_of_endValue_54() { return static_cast<int32_t>(offsetof(TweenerCore_3_tC048346C0B552E5B61037FE375271319C0EB9564, ___endValue_54)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_endValue_54() const { return ___endValue_54; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_endValue_54() { return &___endValue_54; }
	inline void set_endValue_54(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___endValue_54 = value;
	}

	inline static int32_t get_offset_of_changeValue_55() { return static_cast<int32_t>(offsetof(TweenerCore_3_tC048346C0B552E5B61037FE375271319C0EB9564, ___changeValue_55)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_changeValue_55() const { return ___changeValue_55; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_changeValue_55() { return &___changeValue_55; }
	inline void set_changeValue_55(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___changeValue_55 = value;
	}

	inline static int32_t get_offset_of_plugOptions_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_tC048346C0B552E5B61037FE375271319C0EB9564, ___plugOptions_56)); }
	inline VectorOptions_t75B437FFE9996394BC720A3F95ABFF81F338B0AB  get_plugOptions_56() const { return ___plugOptions_56; }
	inline VectorOptions_t75B437FFE9996394BC720A3F95ABFF81F338B0AB * get_address_of_plugOptions_56() { return &___plugOptions_56; }
	inline void set_plugOptions_56(VectorOptions_t75B437FFE9996394BC720A3F95ABFF81F338B0AB  value)
	{
		___plugOptions_56 = value;
	}

	inline static int32_t get_offset_of_getter_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_tC048346C0B552E5B61037FE375271319C0EB9564, ___getter_57)); }
	inline DOGetter_1_t124A8173423DDFE7DBFE2D18475606BF47E278FF * get_getter_57() const { return ___getter_57; }
	inline DOGetter_1_t124A8173423DDFE7DBFE2D18475606BF47E278FF ** get_address_of_getter_57() { return &___getter_57; }
	inline void set_getter_57(DOGetter_1_t124A8173423DDFE7DBFE2D18475606BF47E278FF * value)
	{
		___getter_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getter_57), (void*)value);
	}

	inline static int32_t get_offset_of_setter_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_tC048346C0B552E5B61037FE375271319C0EB9564, ___setter_58)); }
	inline DOSetter_1_tB633699E0271976DA4B170D01AF6535DE81F46CC * get_setter_58() const { return ___setter_58; }
	inline DOSetter_1_tB633699E0271976DA4B170D01AF6535DE81F46CC ** get_address_of_setter_58() { return &___setter_58; }
	inline void set_setter_58(DOSetter_1_tB633699E0271976DA4B170D01AF6535DE81F46CC * value)
	{
		___setter_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___setter_58), (void*)value);
	}

	inline static int32_t get_offset_of_tweenPlugin_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_tC048346C0B552E5B61037FE375271319C0EB9564, ___tweenPlugin_59)); }
	inline ABSTweenPlugin_3_tEF220F6242890F93B1000E3697D41B1262D65ED1 * get_tweenPlugin_59() const { return ___tweenPlugin_59; }
	inline ABSTweenPlugin_3_tEF220F6242890F93B1000E3697D41B1262D65ED1 ** get_address_of_tweenPlugin_59() { return &___tweenPlugin_59; }
	inline void set_tweenPlugin_59(ABSTweenPlugin_3_tEF220F6242890F93B1000E3697D41B1262D65ED1 * value)
	{
		___tweenPlugin_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tweenPlugin_59), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif


// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE_gshared (DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352_gshared (DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<System.Object>(!!0,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetTarget_TisRuntimeObject_m34083705F311A02B95287FADFAD21CC55D9545AE_gshared (RuntimeObject * ___t0, RuntimeObject * ___target1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOGetter_1__ctor_mCDCE48F97A38C7D3830E1484A33779D9C16CB99D_gshared (DOGetter_1_t124A8173423DDFE7DBFE2D18475606BF47E278FF * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOSetter_1__ctor_m39F0112BD65B9E72E1A4A7BAD2BCD56AC56CA736_gshared (DOSetter_1_tB633699E0271976DA4B170D01AF6535DE81F46CC * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOGetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOGetter_1__ctor_mCB309EC70189CBE3BF3FE2E6A96AA71716359C08_gshared (DOGetter_1_tF0D36C857825240D5F2039FD835743757A8410EF * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOSetter_1__ctor_m5EA89B43642DA9193568B0D408DC288C2890D76E_gshared (DOSetter_1_tB3077E9EB59C7ADF9A3076747F40B1F3F83296FC * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);

// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m48E05B56036254D49DAEB8F295E9E6C8BDCADC6D (U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96 * __this, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
inline void DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE (DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE_gshared)(__this, ___object0, ___method1, method);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
inline void DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352 (DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352_gshared)(__this, ___object0, ___method1, method);
}
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Color>,DG.Tweening.Core.DOSetter`1<UnityEngine.Color>,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * DOTween_To_mE294CB006DFDA920942DE3D1BD24C4701EBCE90E (DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * ___getter0, DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * ___setter1, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___endValue2, float ___duration3, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>>(!!0,System.Object)
inline TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597_mDB9A07D9A7A3EFAC4B1B27A61BFB1FF1492809C4 (TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * ___t0, RuntimeObject * ___target1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * (*) (TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 *, RuntimeObject *, const RuntimeMethod*))TweenSettingsExtensions_SetTarget_TisRuntimeObject_m34083705F311A02B95287FADFAD21CC55D9545AE_gshared)(___t0, ___target1, method);
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_0__ctor_mB4DF136FEB0BFABE14B7DDE834E0C2FDA685B991 (U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392 * __this, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.DOTween::ToAlpha(DG.Tweening.Core.DOGetter`1<UnityEngine.Color>,DG.Tweening.Core.DOSetter`1<UnityEngine.Color>,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * DOTween_ToAlpha_m3EF34978A74FEA83A950E85150829B53027E521E (DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * ___getter0, DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * ___setter1, float ___endValue2, float ___duration3, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Tweener>(!!0,System.Object)
inline Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * TweenSettingsExtensions_SetTarget_TisTweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_mD9FAB53299A02D8E43256A69277B92879FD3C8AC (Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ___t0, RuntimeObject * ___target1, const RuntimeMethod* method)
{
	return ((  Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * (*) (Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 *, RuntimeObject *, const RuntimeMethod*))TweenSettingsExtensions_SetTarget_TisRuntimeObject_m34083705F311A02B95287FADFAD21CC55D9545AE_gshared)(___t0, ___target1, method);
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass5_0__ctor_m964875B6120212AD8CE2664EC26CA47A926B1740 (U3CU3Ec__DisplayClass5_0_tB8834D1D0E93BA282464F6636FE1B460D73E7AF8 * __this, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
inline void DOGetter_1__ctor_mCDCE48F97A38C7D3830E1484A33779D9C16CB99D (DOGetter_1_t124A8173423DDFE7DBFE2D18475606BF47E278FF * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (DOGetter_1_t124A8173423DDFE7DBFE2D18475606BF47E278FF *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOGetter_1__ctor_mCDCE48F97A38C7D3830E1484A33779D9C16CB99D_gshared)(__this, ___object0, ___method1, method);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
inline void DOSetter_1__ctor_m39F0112BD65B9E72E1A4A7BAD2BCD56AC56CA736 (DOSetter_1_tB633699E0271976DA4B170D01AF6535DE81F46CC * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (DOSetter_1_tB633699E0271976DA4B170D01AF6535DE81F46CC *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOSetter_1__ctor_m39F0112BD65B9E72E1A4A7BAD2BCD56AC56CA736_gshared)(__this, ___object0, ___method1, method);
}
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>,UnityEngine.Vector2,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_tC048346C0B552E5B61037FE375271319C0EB9564 * DOTween_To_m08A8B415E04DF5FD6ED278A7912A8A1167A811A3 (DOGetter_1_t124A8173423DDFE7DBFE2D18475606BF47E278FF * ___getter0, DOSetter_1_tB633699E0271976DA4B170D01AF6535DE81F46CC * ___setter1, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___endValue2, float ___duration3, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * TweenSettingsExtensions_SetOptions_m9C45DDF251D2CD1BC96529E3480E9A91B46AF1E4 (TweenerCore_3_tC048346C0B552E5B61037FE375271319C0EB9564 * ___t0, bool ___snapping1, const RuntimeMethod* method);
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass8_0__ctor_m045AD5EBDDCD3D66F685F1CB51FF83B1AA9557A1 (U3CU3Ec__DisplayClass8_0_tAF5B6371344DA9513471031395C57B5466607F3E * __this, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOGetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
inline void DOGetter_1__ctor_mCB309EC70189CBE3BF3FE2E6A96AA71716359C08 (DOGetter_1_tF0D36C857825240D5F2039FD835743757A8410EF * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (DOGetter_1_tF0D36C857825240D5F2039FD835743757A8410EF *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOGetter_1__ctor_mCB309EC70189CBE3BF3FE2E6A96AA71716359C08_gshared)(__this, ___object0, ___method1, method);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
inline void DOSetter_1__ctor_m5EA89B43642DA9193568B0D408DC288C2890D76E (DOSetter_1_tB3077E9EB59C7ADF9A3076747F40B1F3F83296FC * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (DOSetter_1_tB3077E9EB59C7ADF9A3076747F40B1F3F83296FC *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOSetter_1__ctor_m5EA89B43642DA9193568B0D408DC288C2890D76E_gshared)(__this, ___object0, ___method1, method);
}
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.Single>,DG.Tweening.Core.DOSetter`1<System.Single>,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6 * DOTween_To_mB87E880F73BF460ADEDDDDA0D0E96781E9852BF4 (DOGetter_1_tF0D36C857825240D5F2039FD835743757A8410EF * ___getter0, DOSetter_1_tB3077E9EB59C7ADF9A3076747F40B1F3F83296FC * ___setter1, float ___endValue2, float ___duration3, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>>(!!0,System.Object)
inline TweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6 * TweenSettingsExtensions_SetTarget_TisTweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6_m3ABE5A45167B552A625305CCCFF29BC87F315688 (TweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6 * ___t0, RuntimeObject * ___target1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6 * (*) (TweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6 *, RuntimeObject *, const RuntimeMethod*))TweenSettingsExtensions_SetTarget_TisRuntimeObject_m34083705F311A02B95287FADFAD21CC55D9545AE_gshared)(___t0, ___target1, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.SpriteRenderer::get_color()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  SpriteRenderer_get_color_mAE96B8C6754CBE7820863BD5E97729B5DBF96EAC (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33 (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Rigidbody2D_get_position_mEC7D07E3478BEF5A2A0E22C91CA54935376F84C2 (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rigidbody2D::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rigidbody2D_get_rotation_mD58E62EDB334FCDF7914A94C940F7903E8ADBBFF (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions43_DOColor_mA2543B2AD028B46EA2C799F18C669A00783970E8 (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___target0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___endValue1, float ___duration2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597_mDB9A07D9A7A3EFAC4B1B27A61BFB1FF1492809C4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__0_m1D7DF9711656BBF94B94806F1F8BCEBD054C71E4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__1_m0DFCE7EB3897535D529FA90C2B100ED244F383FC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96 * L_0 = (U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass2_0__ctor_m48E05B56036254D49DAEB8F295E9E6C8BDCADC6D(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96 * L_1 = V_0;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96 * L_3 = V_0;
		DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * L_4 = (DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 *)il2cpp_codegen_object_new(DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE(L_4, L_3, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__0_m1D7DF9711656BBF94B94806F1F8BCEBD054C71E4_RuntimeMethod_var), /*hidden argument*/DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE_RuntimeMethod_var);
		U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96 * L_5 = V_0;
		DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * L_6 = (DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF *)il2cpp_codegen_object_new(DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352(L_6, L_5, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__1_m0DFCE7EB3897535D529FA90C2B100ED244F383FC_RuntimeMethod_var), /*hidden argument*/DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352_RuntimeMethod_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_7 = ___endValue1;
		float L_8 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * L_9;
		L_9 = DOTween_To_mE294CB006DFDA920942DE3D1BD24C4701EBCE90E(L_4, L_6, L_7, L_8, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96 * L_10 = V_0;
		NullCheck(L_10);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_11 = L_10->get_target_0();
		TweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597 * L_12;
		L_12 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597_mDB9A07D9A7A3EFAC4B1B27A61BFB1FF1492809C4(L_9, L_11, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t551A69CD4FF610AE9DF07C97A86B6FE12EBEB597_mDB9A07D9A7A3EFAC4B1B27A61BFB1FF1492809C4_RuntimeMethod_var);
		return L_12;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOFade(UnityEngine.SpriteRenderer,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions43_DOFade_m5F3CD26D926219D7FCBAF07C0EAB1D50F76E5006 (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetTarget_TisTweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_mD9FAB53299A02D8E43256A69277B92879FD3C8AC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__0_m8BD6A8476E7D96B335DCCED905B443EE68DADEF7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__1_mAF96917D873B7CE5960E949FF567DB4C23DC2961_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392 * L_0 = (U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass3_0__ctor_mB4DF136FEB0BFABE14B7DDE834E0C2FDA685B991(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392 * L_1 = V_0;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392 * L_3 = V_0;
		DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 * L_4 = (DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10 *)il2cpp_codegen_object_new(DOGetter_1_tCC9DEA9BB7DA5C2570ED2CE9BD13EF5C154B7B10_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE(L_4, L_3, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__0_m8BD6A8476E7D96B335DCCED905B443EE68DADEF7_RuntimeMethod_var), /*hidden argument*/DOGetter_1__ctor_mAC32C501C09F9F6EC5B0DD2C1A8473BAE88DC8CE_RuntimeMethod_var);
		U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392 * L_5 = V_0;
		DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF * L_6 = (DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF *)il2cpp_codegen_object_new(DOSetter_1_tB62013E4617ECB51822384C8592AAB764E1662BF_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352(L_6, L_5, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__1_mAF96917D873B7CE5960E949FF567DB4C23DC2961_RuntimeMethod_var), /*hidden argument*/DOSetter_1__ctor_m41D0232769B743141B4389F8A4599EC78F310352_RuntimeMethod_var);
		float L_7 = ___endValue1;
		float L_8 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_9;
		L_9 = DOTween_ToAlpha_m3EF34978A74FEA83A950E85150829B53027E521E(L_4, L_6, L_7, L_8, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392 * L_10 = V_0;
		NullCheck(L_10);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_11 = L_10->get_target_0();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_12;
		L_12 = TweenSettingsExtensions_SetTarget_TisTweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_mD9FAB53299A02D8E43256A69277B92879FD3C8AC(L_9, L_11, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_mD9FAB53299A02D8E43256A69277B92879FD3C8AC_RuntimeMethod_var);
		return L_12;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOMove(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions43_DOMove_m2841C4B8C1638272A9C9A23E2FFA00B104C35400 (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___target0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___endValue1, float ___duration2, bool ___snapping3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOGetter_1__ctor_mCDCE48F97A38C7D3830E1484A33779D9C16CB99D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOGetter_1_t124A8173423DDFE7DBFE2D18475606BF47E278FF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOSetter_1__ctor_m39F0112BD65B9E72E1A4A7BAD2BCD56AC56CA736_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOSetter_1_tB633699E0271976DA4B170D01AF6535DE81F46CC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody2D_MovePosition_mB4493BFC30B2FEBB02C7819AAE626871939D5BD9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetTarget_TisTweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_mD9FAB53299A02D8E43256A69277B92879FD3C8AC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass5_0_U3CDOMoveU3Eb__0_m9D8CAC5B7FAEBAACD75489B96693E2066A998218_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass5_0_tB8834D1D0E93BA282464F6636FE1B460D73E7AF8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass5_0_tB8834D1D0E93BA282464F6636FE1B460D73E7AF8 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass5_0_tB8834D1D0E93BA282464F6636FE1B460D73E7AF8 * L_0 = (U3CU3Ec__DisplayClass5_0_tB8834D1D0E93BA282464F6636FE1B460D73E7AF8 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass5_0_tB8834D1D0E93BA282464F6636FE1B460D73E7AF8_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass5_0__ctor_m964875B6120212AD8CE2664EC26CA47A926B1740(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass5_0_tB8834D1D0E93BA282464F6636FE1B460D73E7AF8 * L_1 = V_0;
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass5_0_tB8834D1D0E93BA282464F6636FE1B460D73E7AF8 * L_3 = V_0;
		DOGetter_1_t124A8173423DDFE7DBFE2D18475606BF47E278FF * L_4 = (DOGetter_1_t124A8173423DDFE7DBFE2D18475606BF47E278FF *)il2cpp_codegen_object_new(DOGetter_1_t124A8173423DDFE7DBFE2D18475606BF47E278FF_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_mCDCE48F97A38C7D3830E1484A33779D9C16CB99D(L_4, L_3, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass5_0_U3CDOMoveU3Eb__0_m9D8CAC5B7FAEBAACD75489B96693E2066A998218_RuntimeMethod_var), /*hidden argument*/DOGetter_1__ctor_mCDCE48F97A38C7D3830E1484A33779D9C16CB99D_RuntimeMethod_var);
		U3CU3Ec__DisplayClass5_0_tB8834D1D0E93BA282464F6636FE1B460D73E7AF8 * L_5 = V_0;
		NullCheck(L_5);
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_6 = L_5->get_target_0();
		DOSetter_1_tB633699E0271976DA4B170D01AF6535DE81F46CC * L_7 = (DOSetter_1_tB633699E0271976DA4B170D01AF6535DE81F46CC *)il2cpp_codegen_object_new(DOSetter_1_tB633699E0271976DA4B170D01AF6535DE81F46CC_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m39F0112BD65B9E72E1A4A7BAD2BCD56AC56CA736(L_7, L_6, (intptr_t)((intptr_t)Rigidbody2D_MovePosition_mB4493BFC30B2FEBB02C7819AAE626871939D5BD9_RuntimeMethod_var), /*hidden argument*/DOSetter_1__ctor_m39F0112BD65B9E72E1A4A7BAD2BCD56AC56CA736_RuntimeMethod_var);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8 = ___endValue1;
		float L_9 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		TweenerCore_3_tC048346C0B552E5B61037FE375271319C0EB9564 * L_10;
		L_10 = DOTween_To_m08A8B415E04DF5FD6ED278A7912A8A1167A811A3(L_4, L_7, L_8, L_9, /*hidden argument*/NULL);
		bool L_11 = ___snapping3;
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_12;
		L_12 = TweenSettingsExtensions_SetOptions_m9C45DDF251D2CD1BC96529E3480E9A91B46AF1E4(L_10, L_11, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass5_0_tB8834D1D0E93BA282464F6636FE1B460D73E7AF8 * L_13 = V_0;
		NullCheck(L_13);
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_14 = L_13->get_target_0();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_15;
		L_15 = TweenSettingsExtensions_SetTarget_TisTweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_mD9FAB53299A02D8E43256A69277B92879FD3C8AC(L_12, L_14, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_mD9FAB53299A02D8E43256A69277B92879FD3C8AC_RuntimeMethod_var);
		return L_15;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DORotate(UnityEngine.Rigidbody2D,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions43_DORotate_m868974591A2268B71A3EC37CC6D31CE77979B10C (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOGetter_1__ctor_mCB309EC70189CBE3BF3FE2E6A96AA71716359C08_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOGetter_1_tF0D36C857825240D5F2039FD835743757A8410EF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOSetter_1__ctor_m5EA89B43642DA9193568B0D408DC288C2890D76E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOSetter_1_tB3077E9EB59C7ADF9A3076747F40B1F3F83296FC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody2D_MoveRotation_mB550025C3B09DB909CB895609BF642EF768C5B22_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetTarget_TisTweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6_m3ABE5A45167B552A625305CCCFF29BC87F315688_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass8_0_U3CDORotateU3Eb__0_mD311B368E4BB9122A2784911F2CA4F2846AC1439_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass8_0_tAF5B6371344DA9513471031395C57B5466607F3E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass8_0_tAF5B6371344DA9513471031395C57B5466607F3E * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass8_0_tAF5B6371344DA9513471031395C57B5466607F3E * L_0 = (U3CU3Ec__DisplayClass8_0_tAF5B6371344DA9513471031395C57B5466607F3E *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass8_0_tAF5B6371344DA9513471031395C57B5466607F3E_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass8_0__ctor_m045AD5EBDDCD3D66F685F1CB51FF83B1AA9557A1(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass8_0_tAF5B6371344DA9513471031395C57B5466607F3E * L_1 = V_0;
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass8_0_tAF5B6371344DA9513471031395C57B5466607F3E * L_3 = V_0;
		DOGetter_1_tF0D36C857825240D5F2039FD835743757A8410EF * L_4 = (DOGetter_1_tF0D36C857825240D5F2039FD835743757A8410EF *)il2cpp_codegen_object_new(DOGetter_1_tF0D36C857825240D5F2039FD835743757A8410EF_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_mCB309EC70189CBE3BF3FE2E6A96AA71716359C08(L_4, L_3, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass8_0_U3CDORotateU3Eb__0_mD311B368E4BB9122A2784911F2CA4F2846AC1439_RuntimeMethod_var), /*hidden argument*/DOGetter_1__ctor_mCB309EC70189CBE3BF3FE2E6A96AA71716359C08_RuntimeMethod_var);
		U3CU3Ec__DisplayClass8_0_tAF5B6371344DA9513471031395C57B5466607F3E * L_5 = V_0;
		NullCheck(L_5);
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_6 = L_5->get_target_0();
		DOSetter_1_tB3077E9EB59C7ADF9A3076747F40B1F3F83296FC * L_7 = (DOSetter_1_tB3077E9EB59C7ADF9A3076747F40B1F3F83296FC *)il2cpp_codegen_object_new(DOSetter_1_tB3077E9EB59C7ADF9A3076747F40B1F3F83296FC_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m5EA89B43642DA9193568B0D408DC288C2890D76E(L_7, L_6, (intptr_t)((intptr_t)Rigidbody2D_MoveRotation_mB550025C3B09DB909CB895609BF642EF768C5B22_RuntimeMethod_var), /*hidden argument*/DOSetter_1__ctor_m5EA89B43642DA9193568B0D408DC288C2890D76E_RuntimeMethod_var);
		float L_8 = ___endValue1;
		float L_9 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		TweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6 * L_10;
		L_10 = DOTween_To_mB87E880F73BF460ADEDDDDA0D0E96781E9852BF4(L_4, L_7, L_8, L_9, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass8_0_tAF5B6371344DA9513471031395C57B5466607F3E * L_11 = V_0;
		NullCheck(L_11);
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_12 = L_11->get_target_0();
		TweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6 * L_13;
		L_13 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6_m3ABE5A45167B552A625305CCCFF29BC87F315688(L_10, L_12, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_tB6BE91A77F164BE41E0206B88B735BC28790F5E6_m3ABE5A45167B552A625305CCCFF29BC87F315688_RuntimeMethod_var);
		return L_13;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m48E05B56036254D49DAEB8F295E9E6C8BDCADC6D (U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::<DOColor>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__0_m1D7DF9711656BBF94B94806F1F8BCEBD054C71E4 (U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96 * __this, const RuntimeMethod* method)
{
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1;
		L_1 = SpriteRenderer_get_color_mAE96B8C6754CBE7820863BD5E97729B5DBF96EAC(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::<DOColor>b__1(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__1_m0DFCE7EB3897535D529FA90C2B100ED244F383FC (U3CU3Ec__DisplayClass2_0_tD4EA60A84DE20B55CA8B42D425382B9F9EFA8E96 * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___x0, const RuntimeMethod* method)
{
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0 = __this->get_target_0();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = ___x0;
		NullCheck(L_0);
		SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_0__ctor_mB4DF136FEB0BFABE14B7DDE834E0C2FDA685B991 (U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::<DOFade>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__0_m8BD6A8476E7D96B335DCCED905B443EE68DADEF7 (U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392 * __this, const RuntimeMethod* method)
{
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1;
		L_1 = SpriteRenderer_get_color_mAE96B8C6754CBE7820863BD5E97729B5DBF96EAC(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::<DOFade>b__1(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__1_mAF96917D873B7CE5960E949FF567DB4C23DC2961 (U3CU3Ec__DisplayClass3_0_t18EEE672485A9D6E2DDF26E4FF138DC4E0436392 * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___x0, const RuntimeMethod* method)
{
	{
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0 = __this->get_target_0();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = ___x0;
		NullCheck(L_0);
		SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass5_0__ctor_m964875B6120212AD8CE2664EC26CA47A926B1740 (U3CU3Ec__DisplayClass5_0_tB8834D1D0E93BA282464F6636FE1B460D73E7AF8 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0::<DOMove>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  U3CU3Ec__DisplayClass5_0_U3CDOMoveU3Eb__0_m9D8CAC5B7FAEBAACD75489B96693E2066A998218 (U3CU3Ec__DisplayClass5_0_tB8834D1D0E93BA282464F6636FE1B460D73E7AF8 * __this, const RuntimeMethod* method)
{
	{
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1;
		L_1 = Rigidbody2D_get_position_mEC7D07E3478BEF5A2A0E22C91CA54935376F84C2(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass8_0__ctor_m045AD5EBDDCD3D66F685F1CB51FF83B1AA9557A1 (U3CU3Ec__DisplayClass8_0_tAF5B6371344DA9513471031395C57B5466607F3E * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0::<DORotate>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CU3Ec__DisplayClass8_0_U3CDORotateU3Eb__0_mD311B368E4BB9122A2784911F2CA4F2846AC1439 (U3CU3Ec__DisplayClass8_0_tAF5B6371344DA9513471031395C57B5466607F3E * __this, const RuntimeMethod* method)
{
	{
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		float L_1;
		L_1 = Rigidbody2D_get_rotation_mD58E62EDB334FCDF7914A94C940F7903E8ADBBFF(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
