﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void DG.Tweening.DOTweenAnimation::Awake()
extern void DOTweenAnimation_Awake_m98D3DE4C3FA5DF6CF545091AFDBF5482F57EEA54 (void);
// 0x00000002 System.Void DG.Tweening.DOTweenAnimation::Start()
extern void DOTweenAnimation_Start_m3A051AC580815E0852880030201AD5B6CB33B8AE (void);
// 0x00000003 System.Void DG.Tweening.DOTweenAnimation::OnDestroy()
extern void DOTweenAnimation_OnDestroy_m20C4C502C7104BB0D34E1D20E7968F0047D0AD8D (void);
// 0x00000004 System.Void DG.Tweening.DOTweenAnimation::CreateTween()
extern void DOTweenAnimation_CreateTween_m3A4CD9BFF59D47D95BEA073966ACFAF07738106E (void);
// 0x00000005 System.Void DG.Tweening.DOTweenAnimation::DOPlay()
extern void DOTweenAnimation_DOPlay_mF73F8714C1BC8B1FDF5978BE5A3952F709523794 (void);
// 0x00000006 System.Void DG.Tweening.DOTweenAnimation::DOPlayBackwards()
extern void DOTweenAnimation_DOPlayBackwards_m5C5301E12FCD780FAB2829E78A92E09CEDD0BF12 (void);
// 0x00000007 System.Void DG.Tweening.DOTweenAnimation::DOPlayForward()
extern void DOTweenAnimation_DOPlayForward_m4AEFC44759287AA90497A5FAF28C49DE05D96C42 (void);
// 0x00000008 System.Void DG.Tweening.DOTweenAnimation::DOPause()
extern void DOTweenAnimation_DOPause_mBBA9A1E0C5AC180BD73832D1C9CF7B1251B7F37C (void);
// 0x00000009 System.Void DG.Tweening.DOTweenAnimation::DOTogglePause()
extern void DOTweenAnimation_DOTogglePause_m69E7AEADFDD98EED6AE119DD490BC869FBDD847C (void);
// 0x0000000A System.Void DG.Tweening.DOTweenAnimation::DORewind()
extern void DOTweenAnimation_DORewind_mAD8ECC9275727B676133BCC1CFA7649DE825BADE (void);
// 0x0000000B System.Void DG.Tweening.DOTweenAnimation::DORestart(System.Boolean)
extern void DOTweenAnimation_DORestart_m73EEA95DB0AC3DD26D61A7C72A446E49C5A80E6F (void);
// 0x0000000C System.Void DG.Tweening.DOTweenAnimation::DOComplete()
extern void DOTweenAnimation_DOComplete_mC91160B480D7321E39386945355F03CD3D526CC8 (void);
// 0x0000000D System.Void DG.Tweening.DOTweenAnimation::DOKill()
extern void DOTweenAnimation_DOKill_mE26CAA76E70DD3EB13394C9199CD52F8192608EB (void);
// 0x0000000E System.Void DG.Tweening.DOTweenAnimation::DOPlayById(System.String)
extern void DOTweenAnimation_DOPlayById_mBA0935540BE1F8594C084115F84747C2E7D36C69 (void);
// 0x0000000F System.Void DG.Tweening.DOTweenAnimation::DOPlayAllById(System.String)
extern void DOTweenAnimation_DOPlayAllById_m4C132F9B10DE15F596E967E975DB1F8485C60FEC (void);
// 0x00000010 System.Void DG.Tweening.DOTweenAnimation::DOPauseAllById(System.String)
extern void DOTweenAnimation_DOPauseAllById_m4309C95672F339387B5D2BFF0406B20336C22CC3 (void);
// 0x00000011 System.Void DG.Tweening.DOTweenAnimation::DOPlayBackwardsById(System.String)
extern void DOTweenAnimation_DOPlayBackwardsById_mE65FE6F2E333A54F61B56D7FCE61E423FC127887 (void);
// 0x00000012 System.Void DG.Tweening.DOTweenAnimation::DOPlayBackwardsAllById(System.String)
extern void DOTweenAnimation_DOPlayBackwardsAllById_mE150D1E0B8EDB5A2D0CB6AF3F3D75590050095BF (void);
// 0x00000013 System.Void DG.Tweening.DOTweenAnimation::DOPlayForwardById(System.String)
extern void DOTweenAnimation_DOPlayForwardById_mB21110F44F03657ED596FFF96E203CE7388094B5 (void);
// 0x00000014 System.Void DG.Tweening.DOTweenAnimation::DOPlayForwardAllById(System.String)
extern void DOTweenAnimation_DOPlayForwardAllById_mDCA7519E9E19639FDFC81F1B344CD22C62A1929E (void);
// 0x00000015 System.Void DG.Tweening.DOTweenAnimation::DOPlayNext()
extern void DOTweenAnimation_DOPlayNext_m233EF38D6140BFEF7623EC6E3AD769C184E32544 (void);
// 0x00000016 System.Void DG.Tweening.DOTweenAnimation::DORewindAndPlayNext()
extern void DOTweenAnimation_DORewindAndPlayNext_m9A94A494C50730720A4A0054C91E90D730D0FF1E (void);
// 0x00000017 System.Void DG.Tweening.DOTweenAnimation::DORestartById(System.String)
extern void DOTweenAnimation_DORestartById_mA7F554C81A02C7D05F4CD748843227254479C18A (void);
// 0x00000018 System.Void DG.Tweening.DOTweenAnimation::DORestartAllById(System.String)
extern void DOTweenAnimation_DORestartAllById_mAD1C66ADAC18B95D95C130365AE09D396BE83AB5 (void);
// 0x00000019 System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.DOTweenAnimation::GetTweens()
extern void DOTweenAnimation_GetTweens_mC9E29FBBB58FD0AA8AB588D34CE7D15E25E13E2A (void);
// 0x0000001A DG.Tweening.Core.TargetType DG.Tweening.DOTweenAnimation::TypeToDOTargetType(System.Type)
extern void DOTweenAnimation_TypeToDOTargetType_mAB457ACFE735EC9B05FE0BA52C88E8576EEE304F (void);
// 0x0000001B System.Void DG.Tweening.DOTweenAnimation::ReEvaluateRelativeTween()
extern void DOTweenAnimation_ReEvaluateRelativeTween_mB08DC3F0A42359F5A4B1DA632FFBF4596E365300 (void);
// 0x0000001C System.Void DG.Tweening.DOTweenAnimation::.ctor()
extern void DOTweenAnimation__ctor_m8323282BFF842AEAA8708B0EE7B21D21A19AD0EF (void);
// 0x0000001D System.Void DG.Tweening.DOTweenAnimation::<CreateTween>b__37_0()
extern void DOTweenAnimation_U3CCreateTweenU3Eb__37_0_m7133BCD50DF5E0EDB11799DF7618551D417B05AA (void);
// 0x0000001E System.Boolean DG.Tweening.DOTweenAnimationExtensions::IsSameOrSubclassOf(UnityEngine.Component)
static Il2CppMethodPointer s_methodPointers[30] = 
{
	DOTweenAnimation_Awake_m98D3DE4C3FA5DF6CF545091AFDBF5482F57EEA54,
	DOTweenAnimation_Start_m3A051AC580815E0852880030201AD5B6CB33B8AE,
	DOTweenAnimation_OnDestroy_m20C4C502C7104BB0D34E1D20E7968F0047D0AD8D,
	DOTweenAnimation_CreateTween_m3A4CD9BFF59D47D95BEA073966ACFAF07738106E,
	DOTweenAnimation_DOPlay_mF73F8714C1BC8B1FDF5978BE5A3952F709523794,
	DOTweenAnimation_DOPlayBackwards_m5C5301E12FCD780FAB2829E78A92E09CEDD0BF12,
	DOTweenAnimation_DOPlayForward_m4AEFC44759287AA90497A5FAF28C49DE05D96C42,
	DOTweenAnimation_DOPause_mBBA9A1E0C5AC180BD73832D1C9CF7B1251B7F37C,
	DOTweenAnimation_DOTogglePause_m69E7AEADFDD98EED6AE119DD490BC869FBDD847C,
	DOTweenAnimation_DORewind_mAD8ECC9275727B676133BCC1CFA7649DE825BADE,
	DOTweenAnimation_DORestart_m73EEA95DB0AC3DD26D61A7C72A446E49C5A80E6F,
	DOTweenAnimation_DOComplete_mC91160B480D7321E39386945355F03CD3D526CC8,
	DOTweenAnimation_DOKill_mE26CAA76E70DD3EB13394C9199CD52F8192608EB,
	DOTweenAnimation_DOPlayById_mBA0935540BE1F8594C084115F84747C2E7D36C69,
	DOTweenAnimation_DOPlayAllById_m4C132F9B10DE15F596E967E975DB1F8485C60FEC,
	DOTweenAnimation_DOPauseAllById_m4309C95672F339387B5D2BFF0406B20336C22CC3,
	DOTweenAnimation_DOPlayBackwardsById_mE65FE6F2E333A54F61B56D7FCE61E423FC127887,
	DOTweenAnimation_DOPlayBackwardsAllById_mE150D1E0B8EDB5A2D0CB6AF3F3D75590050095BF,
	DOTweenAnimation_DOPlayForwardById_mB21110F44F03657ED596FFF96E203CE7388094B5,
	DOTweenAnimation_DOPlayForwardAllById_mDCA7519E9E19639FDFC81F1B344CD22C62A1929E,
	DOTweenAnimation_DOPlayNext_m233EF38D6140BFEF7623EC6E3AD769C184E32544,
	DOTweenAnimation_DORewindAndPlayNext_m9A94A494C50730720A4A0054C91E90D730D0FF1E,
	DOTweenAnimation_DORestartById_mA7F554C81A02C7D05F4CD748843227254479C18A,
	DOTweenAnimation_DORestartAllById_mAD1C66ADAC18B95D95C130365AE09D396BE83AB5,
	DOTweenAnimation_GetTweens_mC9E29FBBB58FD0AA8AB588D34CE7D15E25E13E2A,
	DOTweenAnimation_TypeToDOTargetType_mAB457ACFE735EC9B05FE0BA52C88E8576EEE304F,
	DOTweenAnimation_ReEvaluateRelativeTween_mB08DC3F0A42359F5A4B1DA632FFBF4596E365300,
	DOTweenAnimation__ctor_m8323282BFF842AEAA8708B0EE7B21D21A19AD0EF,
	DOTweenAnimation_U3CCreateTweenU3Eb__37_0_m7133BCD50DF5E0EDB11799DF7618551D417B05AA,
	NULL,
};
static const int32_t s_InvokerIndices[30] = 
{
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	2791,
	3418,
	3418,
	2770,
	2770,
	2770,
	2770,
	2770,
	2770,
	2770,
	3418,
	3418,
	2770,
	2770,
	3371,
	5454,
	3418,
	3418,
	3418,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x0600001E, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)2, 121 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	30,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
