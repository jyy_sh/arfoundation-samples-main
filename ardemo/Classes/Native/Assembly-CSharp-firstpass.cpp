﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t815A476B0A21E183042059E705F9E505478CD8AE;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// DG.Tweening.DOTweenAnimation[]
struct DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// DG.Tweening.Tween[]
struct TweenU5BU5D_tFDCF76461360E13F2EF21FD2FB026AB12B4A3847;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// DG.Tweening.Core.ABSAnimationComponent
struct ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasGroup
struct CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// DG.Tweening.DOTweenAnimation
struct DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// DG.Tweening.EaseFunction
struct EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// UnityEngine.Light
struct Light_tA2F349FE839781469A0344CF6039B51512394275;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5;
// DG.Tweening.Sequence
struct Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// DG.Tweening.Tween
struct Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941;
// DG.Tweening.TweenCallback
struct TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB;
// DG.Tweening.Tweener
struct Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8;
// System.Type
struct Type_t;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE;

IL2CPP_EXTERN_C RuntimeClass* AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Enum_t23B90B40F60E677A8025267341651C94AE079CDA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Light_tA2F349FE839781469A0344CF6039B51512394275_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TargetType_t075843C35C2F3BB06953C27ACCC880643AF35310_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral3A9AC714D9B78BE9FD8D8488AC619D7F2556992A;
IL2CPP_EXTERN_C String_t* _stringLiteral6696B5BAE2BDB2FB41C480BCA3BEA55217F8CC87;
IL2CPP_EXTERN_C String_t* _stringLiteral71F308FB57202965E618BD0381AEBFDEEDF31F75;
IL2CPP_EXTERN_C String_t* _stringLiteral8CCF95ED19A7C10C7DD800A4E0C960CACFE7FF1C;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralE2996F43EBFFA914B746B5861EC325951281A3EB;
IL2CPP_EXTERN_C String_t* _stringLiteralF0AD9F8B1A2A2D534505FEA8D4D74C1BFD40753F;
IL2CPP_EXTERN_C String_t* _stringLiteralF3E84B722399601AD7E281754E917478AA9AD48D;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponents_TisDOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9_mD0B291AF74B316EE4EE8444CF959B59235E9A6EA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DOTweenAnimation_U3CCreateTweenU3Eb__37_0_m7133BCD50DF5E0EDB11799DF7618551D417B05AA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponents_TisDOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9_m0EB1FAE4F313EBDE3E2726D1B7B497289CF8A85B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m782F802BE39FD4D3D5758579703B971BC396ABB8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mF8875C86FFAC30CA85251C1B54879C9A33F4DF3F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenExtensions_Pause_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4A0F398B9290C213D5FED5901234BA973AA0C257_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenExtensions_Play_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m1A3AAD3856D80A02DB7F6BB7BBA444FD4C62DA8C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_From_TisTweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_mC83D3CD43EA1E66E8E8B455AA81A4C9B7A75FB4A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_OnComplete_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mE51D62942FE6E2C354ADDB87CE4CDA2206DADCC7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_OnKill_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m8416D52033C9F6508DA669F66198F6477B74EFE7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_OnPlay_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mC17C14807CF262BA39CBF9C55949A053EF127A4C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_OnRewind_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mCDECB675CBFB437D345E0DE1A59F8BC4BA86121E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_OnStart_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mB17597C3125F3F94329BF5C14DE7BCCF05E99EC3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_OnStepComplete_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m5527E140B5D01685DD8041B72C62461EA2A8A1E4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_OnUpdate_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m5F0CCCF9EBBC043CEA09AC3F9AD2CC9A3243EF45_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetAutoKill_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m3B106D91BC3EACE01F526CABBD59D24767E577D9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetDelay_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mD9B865FBEB261A2983D1F5BAB18C7E3F83619480_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetEase_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m30E10D6BA20CCA9FE3099B5BC33D277F48C09880_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetEase_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m423A73833B53B7DF4F822AB1200A8E1150801411_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetId_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4FA57F2F89A8CCEC59A8330199E74221062E929E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetLoops_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4156B77EABC3AA03F624808CDF6472BD0D964EA0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetRelative_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m0DD3CA6BC5B6DFA96EB49AACEB29666342410FDE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetSpeedBased_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m641FDC9A608F06A264C303790E65FA671F3E316A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetTarget_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m0F995FA73A7C3CF6A49200DE6237CB4F250FD5D9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetUpdate_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m3C4F344B80D25DDA04A2C205DD25302E708140DC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* TargetType_t075843C35C2F3BB06953C27ACCC880643AF35310_0_0_0_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8;
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t00DDB26693B7BCCD40544DBD546A67E6CCF39740 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct  List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TweenU5BU5D_tFDCF76461360E13F2EF21FD2FB026AB12B4A3847* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1, ____items_1)); }
	inline TweenU5BU5D_tFDCF76461360E13F2EF21FD2FB026AB12B4A3847* get__items_1() const { return ____items_1; }
	inline TweenU5BU5D_tFDCF76461360E13F2EF21FD2FB026AB12B4A3847** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TweenU5BU5D_tFDCF76461360E13F2EF21FD2FB026AB12B4A3847* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TweenU5BU5D_tFDCF76461360E13F2EF21FD2FB026AB12B4A3847* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1_StaticFields, ____emptyArray_5)); }
	inline TweenU5BU5D_tFDCF76461360E13F2EF21FD2FB026AB12B4A3847* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TweenU5BU5D_tFDCF76461360E13F2EF21FD2FB026AB12B4A3847** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TweenU5BU5D_tFDCF76461360E13F2EF21FD2FB026AB12B4A3847* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// DG.Tweening.DOTweenAnimationExtensions
struct  DOTweenAnimationExtensions_tDDCC25D5D709863A02A73AB3CEC42667DE53FFDF  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.Core.Debugger
struct  Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F  : public RuntimeObject
{
public:

public:
};

struct Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F_StaticFields
{
public:
	// System.Int32 DG.Tweening.Core.Debugger::logPriority
	int32_t ___logPriority_0;

public:
	inline static int32_t get_offset_of_logPriority_0() { return static_cast<int32_t>(offsetof(Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F_StaticFields, ___logPriority_0)); }
	inline int32_t get_logPriority_0() const { return ___logPriority_0; }
	inline int32_t* get_address_of_logPriority_0() { return &___logPriority_0; }
	inline void set_logPriority_0(int32_t value)
	{
		___logPriority_0 = value;
	}
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Keyframe
struct  Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F 
{
public:
	// System.Single UnityEngine.Keyframe::m_Time
	float ___m_Time_0;
	// System.Single UnityEngine.Keyframe::m_Value
	float ___m_Value_1;
	// System.Single UnityEngine.Keyframe::m_InTangent
	float ___m_InTangent_2;
	// System.Single UnityEngine.Keyframe::m_OutTangent
	float ___m_OutTangent_3;
	// System.Int32 UnityEngine.Keyframe::m_WeightedMode
	int32_t ___m_WeightedMode_4;
	// System.Single UnityEngine.Keyframe::m_InWeight
	float ___m_InWeight_5;
	// System.Single UnityEngine.Keyframe::m_OutWeight
	float ___m_OutWeight_6;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_m_InTangent_2() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_InTangent_2)); }
	inline float get_m_InTangent_2() const { return ___m_InTangent_2; }
	inline float* get_address_of_m_InTangent_2() { return &___m_InTangent_2; }
	inline void set_m_InTangent_2(float value)
	{
		___m_InTangent_2 = value;
	}

	inline static int32_t get_offset_of_m_OutTangent_3() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_OutTangent_3)); }
	inline float get_m_OutTangent_3() const { return ___m_OutTangent_3; }
	inline float* get_address_of_m_OutTangent_3() { return &___m_OutTangent_3; }
	inline void set_m_OutTangent_3(float value)
	{
		___m_OutTangent_3 = value;
	}

	inline static int32_t get_offset_of_m_WeightedMode_4() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_WeightedMode_4)); }
	inline int32_t get_m_WeightedMode_4() const { return ___m_WeightedMode_4; }
	inline int32_t* get_address_of_m_WeightedMode_4() { return &___m_WeightedMode_4; }
	inline void set_m_WeightedMode_4(int32_t value)
	{
		___m_WeightedMode_4 = value;
	}

	inline static int32_t get_offset_of_m_InWeight_5() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_InWeight_5)); }
	inline float get_m_InWeight_5() const { return ___m_InWeight_5; }
	inline float* get_address_of_m_InWeight_5() { return &___m_InWeight_5; }
	inline void set_m_InWeight_5(float value)
	{
		___m_InWeight_5 = value;
	}

	inline static int32_t get_offset_of_m_OutWeight_6() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_OutWeight_6)); }
	inline float get_m_OutWeight_6() const { return ___m_OutWeight_6; }
	inline float* get_address_of_m_OutWeight_6() { return &___m_OutWeight_6; }
	inline void set_m_OutWeight_6(float value)
	{
		___m_OutWeight_6 = value;
	}
};


// UnityEngine.Rect
struct  Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Events.UnityEvent
struct  UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Vector2
struct  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct  Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.AnimationCurve
struct  AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.Core.DOTweenAnimationType
struct  DOTweenAnimationType_tCFA1A0B80F60515B002DBDD3996379796F3775D6 
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenAnimationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DOTweenAnimationType_tCFA1A0B80F60515B002DBDD3996379796F3775D6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// DG.Tweening.Ease
struct  Ease_tB04D625230DDC5B40D74E825C8A9DBBE37A146B4 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Ease_tB04D625230DDC5B40D74E825C8A9DBBE37A146B4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.LoopType
struct  LoopType_tF807A5805F6A83F5228BE7D4E633B2572B1B859A 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoopType_tF807A5805F6A83F5228BE7D4E633B2572B1B859A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// DG.Tweening.RotateMode
struct  RotateMode_t16C0F7B9855AE83E1CA407FF541E1060DCE32B1C 
{
public:
	// System.Int32 DG.Tweening.RotateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotateMode_t16C0F7B9855AE83E1CA407FF541E1060DCE32B1C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// DG.Tweening.ScrambleMode
struct  ScrambleMode_tEE3686B6B55694EA9621FDD3679FAE06FF42F866 
{
public:
	// System.Int32 DG.Tweening.ScrambleMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScrambleMode_tEE3686B6B55694EA9621FDD3679FAE06FF42F866, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.Core.Enums.SpecialStartupMode
struct  SpecialStartupMode_t0086D2AE798C217210762DD17C8D3572414ACD92 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.SpecialStartupMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialStartupMode_t0086D2AE798C217210762DD17C8D3572414ACD92, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.Core.TargetType
struct  TargetType_t075843C35C2F3BB06953C27ACCC880643AF35310 
{
public:
	// System.Int32 DG.Tweening.Core.TargetType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TargetType_t075843C35C2F3BB06953C27ACCC880643AF35310, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.TweenType
struct  TweenType_tAB2DEC1268409EA172594368494218E51696EF5D 
{
public:
	// System.Int32 DG.Tweening.TweenType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TweenType_tAB2DEC1268409EA172594368494218E51696EF5D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.UpdateType
struct  UpdateType_t9D838506DD148F29E6183FB298E41921E51CC5ED 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateType_t9D838506DD148F29E6183FB298E41921E51CC5ED, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/FillMethod
struct  FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/Type
struct  Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.Core.ABSSequentiable
struct  ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76  : public RuntimeObject
{
public:
	// DG.Tweening.TweenType DG.Tweening.Core.ABSSequentiable::tweenType
	int32_t ___tweenType_0;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedPosition
	float ___sequencedPosition_1;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedEndPosition
	float ___sequencedEndPosition_2;
	// DG.Tweening.TweenCallback DG.Tweening.Core.ABSSequentiable::onStart
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onStart_3;

public:
	inline static int32_t get_offset_of_tweenType_0() { return static_cast<int32_t>(offsetof(ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76, ___tweenType_0)); }
	inline int32_t get_tweenType_0() const { return ___tweenType_0; }
	inline int32_t* get_address_of_tweenType_0() { return &___tweenType_0; }
	inline void set_tweenType_0(int32_t value)
	{
		___tweenType_0 = value;
	}

	inline static int32_t get_offset_of_sequencedPosition_1() { return static_cast<int32_t>(offsetof(ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76, ___sequencedPosition_1)); }
	inline float get_sequencedPosition_1() const { return ___sequencedPosition_1; }
	inline float* get_address_of_sequencedPosition_1() { return &___sequencedPosition_1; }
	inline void set_sequencedPosition_1(float value)
	{
		___sequencedPosition_1 = value;
	}

	inline static int32_t get_offset_of_sequencedEndPosition_2() { return static_cast<int32_t>(offsetof(ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76, ___sequencedEndPosition_2)); }
	inline float get_sequencedEndPosition_2() const { return ___sequencedEndPosition_2; }
	inline float* get_address_of_sequencedEndPosition_2() { return &___sequencedEndPosition_2; }
	inline void set_sequencedEndPosition_2(float value)
	{
		___sequencedEndPosition_2 = value;
	}

	inline static int32_t get_offset_of_onStart_3() { return static_cast<int32_t>(offsetof(ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76, ___onStart_3)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onStart_3() const { return ___onStart_3; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onStart_3() { return &___onStart_3; }
	inline void set_onStart_3(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onStart_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStart_3), (void*)value);
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Material
struct  Material_t8927C00353A72755313F046D0CE85178AE8218EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Renderer
struct  Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Rigidbody
struct  Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Rigidbody2D
struct  Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// DG.Tweening.Tween
struct  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941  : public ABSSequentiable_t74F7F9AFFD39EE435A16A3E202A7872B46DA6C76
{
public:
	// System.Single DG.Tweening.Tween::timeScale
	float ___timeScale_4;
	// System.Boolean DG.Tweening.Tween::isBackwards
	bool ___isBackwards_5;
	// System.Object DG.Tweening.Tween::id
	RuntimeObject * ___id_6;
	// System.Object DG.Tweening.Tween::target
	RuntimeObject * ___target_7;
	// DG.Tweening.UpdateType DG.Tweening.Tween::updateType
	int32_t ___updateType_8;
	// System.Boolean DG.Tweening.Tween::isIndependentUpdate
	bool ___isIndependentUpdate_9;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPlay
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onPlay_10;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPause
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onPause_11;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onRewind
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onRewind_12;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onUpdate
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onUpdate_13;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onStepComplete
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onStepComplete_14;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onComplete
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onComplete_15;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onKill
	TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___onKill_16;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.Tween::onWaypointChange
	TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B * ___onWaypointChange_17;
	// System.Boolean DG.Tweening.Tween::isFrom
	bool ___isFrom_18;
	// System.Boolean DG.Tweening.Tween::isBlendable
	bool ___isBlendable_19;
	// System.Boolean DG.Tweening.Tween::isRecyclable
	bool ___isRecyclable_20;
	// System.Boolean DG.Tweening.Tween::isSpeedBased
	bool ___isSpeedBased_21;
	// System.Boolean DG.Tweening.Tween::autoKill
	bool ___autoKill_22;
	// System.Single DG.Tweening.Tween::duration
	float ___duration_23;
	// System.Int32 DG.Tweening.Tween::loops
	int32_t ___loops_24;
	// DG.Tweening.LoopType DG.Tweening.Tween::loopType
	int32_t ___loopType_25;
	// System.Single DG.Tweening.Tween::delay
	float ___delay_26;
	// System.Boolean DG.Tweening.Tween::isRelative
	bool ___isRelative_27;
	// DG.Tweening.Ease DG.Tweening.Tween::easeType
	int32_t ___easeType_28;
	// DG.Tweening.EaseFunction DG.Tweening.Tween::customEase
	EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9 * ___customEase_29;
	// System.Single DG.Tweening.Tween::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_30;
	// System.Single DG.Tweening.Tween::easePeriod
	float ___easePeriod_31;
	// System.Type DG.Tweening.Tween::typeofT1
	Type_t * ___typeofT1_32;
	// System.Type DG.Tweening.Tween::typeofT2
	Type_t * ___typeofT2_33;
	// System.Type DG.Tweening.Tween::typeofTPlugOptions
	Type_t * ___typeofTPlugOptions_34;
	// System.Boolean DG.Tweening.Tween::active
	bool ___active_35;
	// System.Boolean DG.Tweening.Tween::isSequenced
	bool ___isSequenced_36;
	// DG.Tweening.Sequence DG.Tweening.Tween::sequenceParent
	Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * ___sequenceParent_37;
	// System.Int32 DG.Tweening.Tween::activeId
	int32_t ___activeId_38;
	// DG.Tweening.Core.Enums.SpecialStartupMode DG.Tweening.Tween::specialStartupMode
	int32_t ___specialStartupMode_39;
	// System.Boolean DG.Tweening.Tween::creationLocked
	bool ___creationLocked_40;
	// System.Boolean DG.Tweening.Tween::startupDone
	bool ___startupDone_41;
	// System.Boolean DG.Tweening.Tween::playedOnce
	bool ___playedOnce_42;
	// System.Single DG.Tweening.Tween::position
	float ___position_43;
	// System.Single DG.Tweening.Tween::fullDuration
	float ___fullDuration_44;
	// System.Int32 DG.Tweening.Tween::completedLoops
	int32_t ___completedLoops_45;
	// System.Boolean DG.Tweening.Tween::isPlaying
	bool ___isPlaying_46;
	// System.Boolean DG.Tweening.Tween::isComplete
	bool ___isComplete_47;
	// System.Single DG.Tweening.Tween::elapsedDelay
	float ___elapsedDelay_48;
	// System.Boolean DG.Tweening.Tween::delayComplete
	bool ___delayComplete_49;
	// System.Int32 DG.Tweening.Tween::miscInt
	int32_t ___miscInt_50;

public:
	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_isBackwards_5() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isBackwards_5)); }
	inline bool get_isBackwards_5() const { return ___isBackwards_5; }
	inline bool* get_address_of_isBackwards_5() { return &___isBackwards_5; }
	inline void set_isBackwards_5(bool value)
	{
		___isBackwards_5 = value;
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___id_6)); }
	inline RuntimeObject * get_id_6() const { return ___id_6; }
	inline RuntimeObject ** get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(RuntimeObject * value)
	{
		___id_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___id_6), (void*)value);
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___target_7)); }
	inline RuntimeObject * get_target_7() const { return ___target_7; }
	inline RuntimeObject ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(RuntimeObject * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_7), (void*)value);
	}

	inline static int32_t get_offset_of_updateType_8() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___updateType_8)); }
	inline int32_t get_updateType_8() const { return ___updateType_8; }
	inline int32_t* get_address_of_updateType_8() { return &___updateType_8; }
	inline void set_updateType_8(int32_t value)
	{
		___updateType_8 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_9() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isIndependentUpdate_9)); }
	inline bool get_isIndependentUpdate_9() const { return ___isIndependentUpdate_9; }
	inline bool* get_address_of_isIndependentUpdate_9() { return &___isIndependentUpdate_9; }
	inline void set_isIndependentUpdate_9(bool value)
	{
		___isIndependentUpdate_9 = value;
	}

	inline static int32_t get_offset_of_onPlay_10() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onPlay_10)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onPlay_10() const { return ___onPlay_10; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onPlay_10() { return &___onPlay_10; }
	inline void set_onPlay_10(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onPlay_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPlay_10), (void*)value);
	}

	inline static int32_t get_offset_of_onPause_11() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onPause_11)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onPause_11() const { return ___onPause_11; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onPause_11() { return &___onPause_11; }
	inline void set_onPause_11(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onPause_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPause_11), (void*)value);
	}

	inline static int32_t get_offset_of_onRewind_12() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onRewind_12)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onRewind_12() const { return ___onRewind_12; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onRewind_12() { return &___onRewind_12; }
	inline void set_onRewind_12(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onRewind_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onRewind_12), (void*)value);
	}

	inline static int32_t get_offset_of_onUpdate_13() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onUpdate_13)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onUpdate_13() const { return ___onUpdate_13; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onUpdate_13() { return &___onUpdate_13; }
	inline void set_onUpdate_13(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onUpdate_13), (void*)value);
	}

	inline static int32_t get_offset_of_onStepComplete_14() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onStepComplete_14)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onStepComplete_14() const { return ___onStepComplete_14; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onStepComplete_14() { return &___onStepComplete_14; }
	inline void set_onStepComplete_14(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onStepComplete_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStepComplete_14), (void*)value);
	}

	inline static int32_t get_offset_of_onComplete_15() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onComplete_15)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onComplete_15() const { return ___onComplete_15; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onComplete_15() { return &___onComplete_15; }
	inline void set_onComplete_15(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onComplete_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onComplete_15), (void*)value);
	}

	inline static int32_t get_offset_of_onKill_16() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onKill_16)); }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * get_onKill_16() const { return ___onKill_16; }
	inline TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB ** get_address_of_onKill_16() { return &___onKill_16; }
	inline void set_onKill_16(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * value)
	{
		___onKill_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onKill_16), (void*)value);
	}

	inline static int32_t get_offset_of_onWaypointChange_17() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___onWaypointChange_17)); }
	inline TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B * get_onWaypointChange_17() const { return ___onWaypointChange_17; }
	inline TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B ** get_address_of_onWaypointChange_17() { return &___onWaypointChange_17; }
	inline void set_onWaypointChange_17(TweenCallback_1_t145CD5D30F08B617B445D2B1B79A7BAADC305B9B * value)
	{
		___onWaypointChange_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onWaypointChange_17), (void*)value);
	}

	inline static int32_t get_offset_of_isFrom_18() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isFrom_18)); }
	inline bool get_isFrom_18() const { return ___isFrom_18; }
	inline bool* get_address_of_isFrom_18() { return &___isFrom_18; }
	inline void set_isFrom_18(bool value)
	{
		___isFrom_18 = value;
	}

	inline static int32_t get_offset_of_isBlendable_19() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isBlendable_19)); }
	inline bool get_isBlendable_19() const { return ___isBlendable_19; }
	inline bool* get_address_of_isBlendable_19() { return &___isBlendable_19; }
	inline void set_isBlendable_19(bool value)
	{
		___isBlendable_19 = value;
	}

	inline static int32_t get_offset_of_isRecyclable_20() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isRecyclable_20)); }
	inline bool get_isRecyclable_20() const { return ___isRecyclable_20; }
	inline bool* get_address_of_isRecyclable_20() { return &___isRecyclable_20; }
	inline void set_isRecyclable_20(bool value)
	{
		___isRecyclable_20 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_21() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isSpeedBased_21)); }
	inline bool get_isSpeedBased_21() const { return ___isSpeedBased_21; }
	inline bool* get_address_of_isSpeedBased_21() { return &___isSpeedBased_21; }
	inline void set_isSpeedBased_21(bool value)
	{
		___isSpeedBased_21 = value;
	}

	inline static int32_t get_offset_of_autoKill_22() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___autoKill_22)); }
	inline bool get_autoKill_22() const { return ___autoKill_22; }
	inline bool* get_address_of_autoKill_22() { return &___autoKill_22; }
	inline void set_autoKill_22(bool value)
	{
		___autoKill_22 = value;
	}

	inline static int32_t get_offset_of_duration_23() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___duration_23)); }
	inline float get_duration_23() const { return ___duration_23; }
	inline float* get_address_of_duration_23() { return &___duration_23; }
	inline void set_duration_23(float value)
	{
		___duration_23 = value;
	}

	inline static int32_t get_offset_of_loops_24() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___loops_24)); }
	inline int32_t get_loops_24() const { return ___loops_24; }
	inline int32_t* get_address_of_loops_24() { return &___loops_24; }
	inline void set_loops_24(int32_t value)
	{
		___loops_24 = value;
	}

	inline static int32_t get_offset_of_loopType_25() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___loopType_25)); }
	inline int32_t get_loopType_25() const { return ___loopType_25; }
	inline int32_t* get_address_of_loopType_25() { return &___loopType_25; }
	inline void set_loopType_25(int32_t value)
	{
		___loopType_25 = value;
	}

	inline static int32_t get_offset_of_delay_26() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___delay_26)); }
	inline float get_delay_26() const { return ___delay_26; }
	inline float* get_address_of_delay_26() { return &___delay_26; }
	inline void set_delay_26(float value)
	{
		___delay_26 = value;
	}

	inline static int32_t get_offset_of_isRelative_27() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isRelative_27)); }
	inline bool get_isRelative_27() const { return ___isRelative_27; }
	inline bool* get_address_of_isRelative_27() { return &___isRelative_27; }
	inline void set_isRelative_27(bool value)
	{
		___isRelative_27 = value;
	}

	inline static int32_t get_offset_of_easeType_28() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___easeType_28)); }
	inline int32_t get_easeType_28() const { return ___easeType_28; }
	inline int32_t* get_address_of_easeType_28() { return &___easeType_28; }
	inline void set_easeType_28(int32_t value)
	{
		___easeType_28 = value;
	}

	inline static int32_t get_offset_of_customEase_29() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___customEase_29)); }
	inline EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9 * get_customEase_29() const { return ___customEase_29; }
	inline EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9 ** get_address_of_customEase_29() { return &___customEase_29; }
	inline void set_customEase_29(EaseFunction_tC7ECEFDCAE4EC041E6FD7AC7C021E7B7680EFEB9 * value)
	{
		___customEase_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___customEase_29), (void*)value);
	}

	inline static int32_t get_offset_of_easeOvershootOrAmplitude_30() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___easeOvershootOrAmplitude_30)); }
	inline float get_easeOvershootOrAmplitude_30() const { return ___easeOvershootOrAmplitude_30; }
	inline float* get_address_of_easeOvershootOrAmplitude_30() { return &___easeOvershootOrAmplitude_30; }
	inline void set_easeOvershootOrAmplitude_30(float value)
	{
		___easeOvershootOrAmplitude_30 = value;
	}

	inline static int32_t get_offset_of_easePeriod_31() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___easePeriod_31)); }
	inline float get_easePeriod_31() const { return ___easePeriod_31; }
	inline float* get_address_of_easePeriod_31() { return &___easePeriod_31; }
	inline void set_easePeriod_31(float value)
	{
		___easePeriod_31 = value;
	}

	inline static int32_t get_offset_of_typeofT1_32() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___typeofT1_32)); }
	inline Type_t * get_typeofT1_32() const { return ___typeofT1_32; }
	inline Type_t ** get_address_of_typeofT1_32() { return &___typeofT1_32; }
	inline void set_typeofT1_32(Type_t * value)
	{
		___typeofT1_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeofT1_32), (void*)value);
	}

	inline static int32_t get_offset_of_typeofT2_33() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___typeofT2_33)); }
	inline Type_t * get_typeofT2_33() const { return ___typeofT2_33; }
	inline Type_t ** get_address_of_typeofT2_33() { return &___typeofT2_33; }
	inline void set_typeofT2_33(Type_t * value)
	{
		___typeofT2_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeofT2_33), (void*)value);
	}

	inline static int32_t get_offset_of_typeofTPlugOptions_34() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___typeofTPlugOptions_34)); }
	inline Type_t * get_typeofTPlugOptions_34() const { return ___typeofTPlugOptions_34; }
	inline Type_t ** get_address_of_typeofTPlugOptions_34() { return &___typeofTPlugOptions_34; }
	inline void set_typeofTPlugOptions_34(Type_t * value)
	{
		___typeofTPlugOptions_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeofTPlugOptions_34), (void*)value);
	}

	inline static int32_t get_offset_of_active_35() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___active_35)); }
	inline bool get_active_35() const { return ___active_35; }
	inline bool* get_address_of_active_35() { return &___active_35; }
	inline void set_active_35(bool value)
	{
		___active_35 = value;
	}

	inline static int32_t get_offset_of_isSequenced_36() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isSequenced_36)); }
	inline bool get_isSequenced_36() const { return ___isSequenced_36; }
	inline bool* get_address_of_isSequenced_36() { return &___isSequenced_36; }
	inline void set_isSequenced_36(bool value)
	{
		___isSequenced_36 = value;
	}

	inline static int32_t get_offset_of_sequenceParent_37() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___sequenceParent_37)); }
	inline Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * get_sequenceParent_37() const { return ___sequenceParent_37; }
	inline Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E ** get_address_of_sequenceParent_37() { return &___sequenceParent_37; }
	inline void set_sequenceParent_37(Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * value)
	{
		___sequenceParent_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sequenceParent_37), (void*)value);
	}

	inline static int32_t get_offset_of_activeId_38() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___activeId_38)); }
	inline int32_t get_activeId_38() const { return ___activeId_38; }
	inline int32_t* get_address_of_activeId_38() { return &___activeId_38; }
	inline void set_activeId_38(int32_t value)
	{
		___activeId_38 = value;
	}

	inline static int32_t get_offset_of_specialStartupMode_39() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___specialStartupMode_39)); }
	inline int32_t get_specialStartupMode_39() const { return ___specialStartupMode_39; }
	inline int32_t* get_address_of_specialStartupMode_39() { return &___specialStartupMode_39; }
	inline void set_specialStartupMode_39(int32_t value)
	{
		___specialStartupMode_39 = value;
	}

	inline static int32_t get_offset_of_creationLocked_40() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___creationLocked_40)); }
	inline bool get_creationLocked_40() const { return ___creationLocked_40; }
	inline bool* get_address_of_creationLocked_40() { return &___creationLocked_40; }
	inline void set_creationLocked_40(bool value)
	{
		___creationLocked_40 = value;
	}

	inline static int32_t get_offset_of_startupDone_41() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___startupDone_41)); }
	inline bool get_startupDone_41() const { return ___startupDone_41; }
	inline bool* get_address_of_startupDone_41() { return &___startupDone_41; }
	inline void set_startupDone_41(bool value)
	{
		___startupDone_41 = value;
	}

	inline static int32_t get_offset_of_playedOnce_42() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___playedOnce_42)); }
	inline bool get_playedOnce_42() const { return ___playedOnce_42; }
	inline bool* get_address_of_playedOnce_42() { return &___playedOnce_42; }
	inline void set_playedOnce_42(bool value)
	{
		___playedOnce_42 = value;
	}

	inline static int32_t get_offset_of_position_43() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___position_43)); }
	inline float get_position_43() const { return ___position_43; }
	inline float* get_address_of_position_43() { return &___position_43; }
	inline void set_position_43(float value)
	{
		___position_43 = value;
	}

	inline static int32_t get_offset_of_fullDuration_44() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___fullDuration_44)); }
	inline float get_fullDuration_44() const { return ___fullDuration_44; }
	inline float* get_address_of_fullDuration_44() { return &___fullDuration_44; }
	inline void set_fullDuration_44(float value)
	{
		___fullDuration_44 = value;
	}

	inline static int32_t get_offset_of_completedLoops_45() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___completedLoops_45)); }
	inline int32_t get_completedLoops_45() const { return ___completedLoops_45; }
	inline int32_t* get_address_of_completedLoops_45() { return &___completedLoops_45; }
	inline void set_completedLoops_45(int32_t value)
	{
		___completedLoops_45 = value;
	}

	inline static int32_t get_offset_of_isPlaying_46() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isPlaying_46)); }
	inline bool get_isPlaying_46() const { return ___isPlaying_46; }
	inline bool* get_address_of_isPlaying_46() { return &___isPlaying_46; }
	inline void set_isPlaying_46(bool value)
	{
		___isPlaying_46 = value;
	}

	inline static int32_t get_offset_of_isComplete_47() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___isComplete_47)); }
	inline bool get_isComplete_47() const { return ___isComplete_47; }
	inline bool* get_address_of_isComplete_47() { return &___isComplete_47; }
	inline void set_isComplete_47(bool value)
	{
		___isComplete_47 = value;
	}

	inline static int32_t get_offset_of_elapsedDelay_48() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___elapsedDelay_48)); }
	inline float get_elapsedDelay_48() const { return ___elapsedDelay_48; }
	inline float* get_address_of_elapsedDelay_48() { return &___elapsedDelay_48; }
	inline void set_elapsedDelay_48(float value)
	{
		___elapsedDelay_48 = value;
	}

	inline static int32_t get_offset_of_delayComplete_49() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___delayComplete_49)); }
	inline bool get_delayComplete_49() const { return ___delayComplete_49; }
	inline bool* get_address_of_delayComplete_49() { return &___delayComplete_49; }
	inline void set_delayComplete_49(bool value)
	{
		___delayComplete_49 = value;
	}

	inline static int32_t get_offset_of_miscInt_50() { return static_cast<int32_t>(offsetof(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941, ___miscInt_50)); }
	inline int32_t get_miscInt_50() const { return ___miscInt_50; }
	inline int32_t* get_address_of_miscInt_50() { return &___miscInt_50; }
	inline void set_miscInt_50(int32_t value)
	{
		___miscInt_50 = value;
	}
};


// DG.Tweening.TweenCallback
struct  TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Camera
struct  Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.CanvasGroup
struct  CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.Light
struct  Light_tA2F349FE839781469A0344CF6039B51512394275  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:
	// System.Int32 UnityEngine.Light::m_BakedIndex
	int32_t ___m_BakedIndex_4;

public:
	inline static int32_t get_offset_of_m_BakedIndex_4() { return static_cast<int32_t>(offsetof(Light_tA2F349FE839781469A0344CF6039B51512394275, ___m_BakedIndex_4)); }
	inline int32_t get_m_BakedIndex_4() const { return ___m_BakedIndex_4; }
	inline int32_t* get_address_of_m_BakedIndex_4() { return &___m_BakedIndex_4; }
	inline void set_m_BakedIndex_4(int32_t value)
	{
		___m_BakedIndex_4 = value;
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.RectTransform
struct  RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072  : public Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1
{
public:

public:
};

struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// DG.Tweening.Tweener
struct  Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8  : public Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941
{
public:
	// System.Boolean DG.Tweening.Tweener::hasManuallySetStartValue
	bool ___hasManuallySetStartValue_51;
	// System.Boolean DG.Tweening.Tweener::isFromAllowed
	bool ___isFromAllowed_52;

public:
	inline static int32_t get_offset_of_hasManuallySetStartValue_51() { return static_cast<int32_t>(offsetof(Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8, ___hasManuallySetStartValue_51)); }
	inline bool get_hasManuallySetStartValue_51() const { return ___hasManuallySetStartValue_51; }
	inline bool* get_address_of_hasManuallySetStartValue_51() { return &___hasManuallySetStartValue_51; }
	inline void set_hasManuallySetStartValue_51(bool value)
	{
		___hasManuallySetStartValue_51 = value;
	}

	inline static int32_t get_offset_of_isFromAllowed_52() { return static_cast<int32_t>(offsetof(Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8, ___isFromAllowed_52)); }
	inline bool get_isFromAllowed_52() const { return ___isFromAllowed_52; }
	inline bool* get_address_of_isFromAllowed_52() { return &___isFromAllowed_52; }
	inline void set_isFromAllowed_52(bool value)
	{
		___isFromAllowed_52 = value;
	}
};


// DG.Tweening.Core.ABSAnimationComponent
struct  ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// DG.Tweening.UpdateType DG.Tweening.Core.ABSAnimationComponent::updateType
	int32_t ___updateType_4;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::isSpeedBased
	bool ___isSpeedBased_5;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnStart
	bool ___hasOnStart_6;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnPlay
	bool ___hasOnPlay_7;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnUpdate
	bool ___hasOnUpdate_8;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnStepComplete
	bool ___hasOnStepComplete_9;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnComplete
	bool ___hasOnComplete_10;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnTweenCreated
	bool ___hasOnTweenCreated_11;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnRewind
	bool ___hasOnRewind_12;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onStart
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onStart_13;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onPlay
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onPlay_14;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onUpdate
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onUpdate_15;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onStepComplete
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onStepComplete_16;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onComplete
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onComplete_17;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onTweenCreated
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onTweenCreated_18;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onRewind
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onRewind_19;
	// DG.Tweening.Tween DG.Tweening.Core.ABSAnimationComponent::tween
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___tween_20;

public:
	inline static int32_t get_offset_of_updateType_4() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___updateType_4)); }
	inline int32_t get_updateType_4() const { return ___updateType_4; }
	inline int32_t* get_address_of_updateType_4() { return &___updateType_4; }
	inline void set_updateType_4(int32_t value)
	{
		___updateType_4 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_5() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___isSpeedBased_5)); }
	inline bool get_isSpeedBased_5() const { return ___isSpeedBased_5; }
	inline bool* get_address_of_isSpeedBased_5() { return &___isSpeedBased_5; }
	inline void set_isSpeedBased_5(bool value)
	{
		___isSpeedBased_5 = value;
	}

	inline static int32_t get_offset_of_hasOnStart_6() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___hasOnStart_6)); }
	inline bool get_hasOnStart_6() const { return ___hasOnStart_6; }
	inline bool* get_address_of_hasOnStart_6() { return &___hasOnStart_6; }
	inline void set_hasOnStart_6(bool value)
	{
		___hasOnStart_6 = value;
	}

	inline static int32_t get_offset_of_hasOnPlay_7() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___hasOnPlay_7)); }
	inline bool get_hasOnPlay_7() const { return ___hasOnPlay_7; }
	inline bool* get_address_of_hasOnPlay_7() { return &___hasOnPlay_7; }
	inline void set_hasOnPlay_7(bool value)
	{
		___hasOnPlay_7 = value;
	}

	inline static int32_t get_offset_of_hasOnUpdate_8() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___hasOnUpdate_8)); }
	inline bool get_hasOnUpdate_8() const { return ___hasOnUpdate_8; }
	inline bool* get_address_of_hasOnUpdate_8() { return &___hasOnUpdate_8; }
	inline void set_hasOnUpdate_8(bool value)
	{
		___hasOnUpdate_8 = value;
	}

	inline static int32_t get_offset_of_hasOnStepComplete_9() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___hasOnStepComplete_9)); }
	inline bool get_hasOnStepComplete_9() const { return ___hasOnStepComplete_9; }
	inline bool* get_address_of_hasOnStepComplete_9() { return &___hasOnStepComplete_9; }
	inline void set_hasOnStepComplete_9(bool value)
	{
		___hasOnStepComplete_9 = value;
	}

	inline static int32_t get_offset_of_hasOnComplete_10() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___hasOnComplete_10)); }
	inline bool get_hasOnComplete_10() const { return ___hasOnComplete_10; }
	inline bool* get_address_of_hasOnComplete_10() { return &___hasOnComplete_10; }
	inline void set_hasOnComplete_10(bool value)
	{
		___hasOnComplete_10 = value;
	}

	inline static int32_t get_offset_of_hasOnTweenCreated_11() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___hasOnTweenCreated_11)); }
	inline bool get_hasOnTweenCreated_11() const { return ___hasOnTweenCreated_11; }
	inline bool* get_address_of_hasOnTweenCreated_11() { return &___hasOnTweenCreated_11; }
	inline void set_hasOnTweenCreated_11(bool value)
	{
		___hasOnTweenCreated_11 = value;
	}

	inline static int32_t get_offset_of_hasOnRewind_12() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___hasOnRewind_12)); }
	inline bool get_hasOnRewind_12() const { return ___hasOnRewind_12; }
	inline bool* get_address_of_hasOnRewind_12() { return &___hasOnRewind_12; }
	inline void set_hasOnRewind_12(bool value)
	{
		___hasOnRewind_12 = value;
	}

	inline static int32_t get_offset_of_onStart_13() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___onStart_13)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onStart_13() const { return ___onStart_13; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onStart_13() { return &___onStart_13; }
	inline void set_onStart_13(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onStart_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStart_13), (void*)value);
	}

	inline static int32_t get_offset_of_onPlay_14() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___onPlay_14)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onPlay_14() const { return ___onPlay_14; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onPlay_14() { return &___onPlay_14; }
	inline void set_onPlay_14(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onPlay_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPlay_14), (void*)value);
	}

	inline static int32_t get_offset_of_onUpdate_15() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___onUpdate_15)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onUpdate_15() const { return ___onUpdate_15; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onUpdate_15() { return &___onUpdate_15; }
	inline void set_onUpdate_15(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onUpdate_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onUpdate_15), (void*)value);
	}

	inline static int32_t get_offset_of_onStepComplete_16() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___onStepComplete_16)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onStepComplete_16() const { return ___onStepComplete_16; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onStepComplete_16() { return &___onStepComplete_16; }
	inline void set_onStepComplete_16(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onStepComplete_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStepComplete_16), (void*)value);
	}

	inline static int32_t get_offset_of_onComplete_17() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___onComplete_17)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onComplete_17() const { return ___onComplete_17; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onComplete_17() { return &___onComplete_17; }
	inline void set_onComplete_17(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onComplete_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onComplete_17), (void*)value);
	}

	inline static int32_t get_offset_of_onTweenCreated_18() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___onTweenCreated_18)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onTweenCreated_18() const { return ___onTweenCreated_18; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onTweenCreated_18() { return &___onTweenCreated_18; }
	inline void set_onTweenCreated_18(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onTweenCreated_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onTweenCreated_18), (void*)value);
	}

	inline static int32_t get_offset_of_onRewind_19() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___onRewind_19)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onRewind_19() const { return ___onRewind_19; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onRewind_19() { return &___onRewind_19; }
	inline void set_onRewind_19(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onRewind_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onRewind_19), (void*)value);
	}

	inline static int32_t get_offset_of_tween_20() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7, ___tween_20)); }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * get_tween_20() const { return ___tween_20; }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 ** get_address_of_tween_20() { return &___tween_20; }
	inline void set_tween_20(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * value)
	{
		___tween_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tween_20), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// DG.Tweening.DOTweenAnimation
struct  DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9  : public ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7
{
public:
	// System.Single DG.Tweening.DOTweenAnimation::delay
	float ___delay_21;
	// System.Single DG.Tweening.DOTweenAnimation::duration
	float ___duration_22;
	// DG.Tweening.Ease DG.Tweening.DOTweenAnimation::easeType
	int32_t ___easeType_23;
	// UnityEngine.AnimationCurve DG.Tweening.DOTweenAnimation::easeCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___easeCurve_24;
	// DG.Tweening.LoopType DG.Tweening.DOTweenAnimation::loopType
	int32_t ___loopType_25;
	// System.Int32 DG.Tweening.DOTweenAnimation::loops
	int32_t ___loops_26;
	// System.String DG.Tweening.DOTweenAnimation::id
	String_t* ___id_27;
	// System.Boolean DG.Tweening.DOTweenAnimation::isRelative
	bool ___isRelative_28;
	// System.Boolean DG.Tweening.DOTweenAnimation::isFrom
	bool ___isFrom_29;
	// System.Boolean DG.Tweening.DOTweenAnimation::isIndependentUpdate
	bool ___isIndependentUpdate_30;
	// System.Boolean DG.Tweening.DOTweenAnimation::autoKill
	bool ___autoKill_31;
	// System.Boolean DG.Tweening.DOTweenAnimation::isActive
	bool ___isActive_32;
	// System.Boolean DG.Tweening.DOTweenAnimation::isValid
	bool ___isValid_33;
	// UnityEngine.Component DG.Tweening.DOTweenAnimation::target
	Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * ___target_34;
	// DG.Tweening.Core.DOTweenAnimationType DG.Tweening.DOTweenAnimation::animationType
	int32_t ___animationType_35;
	// DG.Tweening.Core.TargetType DG.Tweening.DOTweenAnimation::targetType
	int32_t ___targetType_36;
	// DG.Tweening.Core.TargetType DG.Tweening.DOTweenAnimation::forcedTargetType
	int32_t ___forcedTargetType_37;
	// System.Boolean DG.Tweening.DOTweenAnimation::autoPlay
	bool ___autoPlay_38;
	// System.Boolean DG.Tweening.DOTweenAnimation::useTargetAsV3
	bool ___useTargetAsV3_39;
	// System.Single DG.Tweening.DOTweenAnimation::endValueFloat
	float ___endValueFloat_40;
	// UnityEngine.Vector3 DG.Tweening.DOTweenAnimation::endValueV3
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___endValueV3_41;
	// UnityEngine.Vector2 DG.Tweening.DOTweenAnimation::endValueV2
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___endValueV2_42;
	// UnityEngine.Color DG.Tweening.DOTweenAnimation::endValueColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___endValueColor_43;
	// System.String DG.Tweening.DOTweenAnimation::endValueString
	String_t* ___endValueString_44;
	// UnityEngine.Rect DG.Tweening.DOTweenAnimation::endValueRect
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___endValueRect_45;
	// UnityEngine.Transform DG.Tweening.DOTweenAnimation::endValueTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___endValueTransform_46;
	// System.Boolean DG.Tweening.DOTweenAnimation::optionalBool0
	bool ___optionalBool0_47;
	// System.Single DG.Tweening.DOTweenAnimation::optionalFloat0
	float ___optionalFloat0_48;
	// System.Int32 DG.Tweening.DOTweenAnimation::optionalInt0
	int32_t ___optionalInt0_49;
	// DG.Tweening.RotateMode DG.Tweening.DOTweenAnimation::optionalRotationMode
	int32_t ___optionalRotationMode_50;
	// DG.Tweening.ScrambleMode DG.Tweening.DOTweenAnimation::optionalScrambleMode
	int32_t ___optionalScrambleMode_51;
	// System.String DG.Tweening.DOTweenAnimation::optionalString
	String_t* ___optionalString_52;
	// System.Boolean DG.Tweening.DOTweenAnimation::_tweenCreated
	bool ____tweenCreated_53;
	// System.Int32 DG.Tweening.DOTweenAnimation::_playCount
	int32_t ____playCount_54;

public:
	inline static int32_t get_offset_of_delay_21() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___delay_21)); }
	inline float get_delay_21() const { return ___delay_21; }
	inline float* get_address_of_delay_21() { return &___delay_21; }
	inline void set_delay_21(float value)
	{
		___delay_21 = value;
	}

	inline static int32_t get_offset_of_duration_22() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___duration_22)); }
	inline float get_duration_22() const { return ___duration_22; }
	inline float* get_address_of_duration_22() { return &___duration_22; }
	inline void set_duration_22(float value)
	{
		___duration_22 = value;
	}

	inline static int32_t get_offset_of_easeType_23() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___easeType_23)); }
	inline int32_t get_easeType_23() const { return ___easeType_23; }
	inline int32_t* get_address_of_easeType_23() { return &___easeType_23; }
	inline void set_easeType_23(int32_t value)
	{
		___easeType_23 = value;
	}

	inline static int32_t get_offset_of_easeCurve_24() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___easeCurve_24)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_easeCurve_24() const { return ___easeCurve_24; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_easeCurve_24() { return &___easeCurve_24; }
	inline void set_easeCurve_24(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___easeCurve_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___easeCurve_24), (void*)value);
	}

	inline static int32_t get_offset_of_loopType_25() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___loopType_25)); }
	inline int32_t get_loopType_25() const { return ___loopType_25; }
	inline int32_t* get_address_of_loopType_25() { return &___loopType_25; }
	inline void set_loopType_25(int32_t value)
	{
		___loopType_25 = value;
	}

	inline static int32_t get_offset_of_loops_26() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___loops_26)); }
	inline int32_t get_loops_26() const { return ___loops_26; }
	inline int32_t* get_address_of_loops_26() { return &___loops_26; }
	inline void set_loops_26(int32_t value)
	{
		___loops_26 = value;
	}

	inline static int32_t get_offset_of_id_27() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___id_27)); }
	inline String_t* get_id_27() const { return ___id_27; }
	inline String_t** get_address_of_id_27() { return &___id_27; }
	inline void set_id_27(String_t* value)
	{
		___id_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___id_27), (void*)value);
	}

	inline static int32_t get_offset_of_isRelative_28() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___isRelative_28)); }
	inline bool get_isRelative_28() const { return ___isRelative_28; }
	inline bool* get_address_of_isRelative_28() { return &___isRelative_28; }
	inline void set_isRelative_28(bool value)
	{
		___isRelative_28 = value;
	}

	inline static int32_t get_offset_of_isFrom_29() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___isFrom_29)); }
	inline bool get_isFrom_29() const { return ___isFrom_29; }
	inline bool* get_address_of_isFrom_29() { return &___isFrom_29; }
	inline void set_isFrom_29(bool value)
	{
		___isFrom_29 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_30() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___isIndependentUpdate_30)); }
	inline bool get_isIndependentUpdate_30() const { return ___isIndependentUpdate_30; }
	inline bool* get_address_of_isIndependentUpdate_30() { return &___isIndependentUpdate_30; }
	inline void set_isIndependentUpdate_30(bool value)
	{
		___isIndependentUpdate_30 = value;
	}

	inline static int32_t get_offset_of_autoKill_31() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___autoKill_31)); }
	inline bool get_autoKill_31() const { return ___autoKill_31; }
	inline bool* get_address_of_autoKill_31() { return &___autoKill_31; }
	inline void set_autoKill_31(bool value)
	{
		___autoKill_31 = value;
	}

	inline static int32_t get_offset_of_isActive_32() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___isActive_32)); }
	inline bool get_isActive_32() const { return ___isActive_32; }
	inline bool* get_address_of_isActive_32() { return &___isActive_32; }
	inline void set_isActive_32(bool value)
	{
		___isActive_32 = value;
	}

	inline static int32_t get_offset_of_isValid_33() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___isValid_33)); }
	inline bool get_isValid_33() const { return ___isValid_33; }
	inline bool* get_address_of_isValid_33() { return &___isValid_33; }
	inline void set_isValid_33(bool value)
	{
		___isValid_33 = value;
	}

	inline static int32_t get_offset_of_target_34() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___target_34)); }
	inline Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * get_target_34() const { return ___target_34; }
	inline Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 ** get_address_of_target_34() { return &___target_34; }
	inline void set_target_34(Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * value)
	{
		___target_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_34), (void*)value);
	}

	inline static int32_t get_offset_of_animationType_35() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___animationType_35)); }
	inline int32_t get_animationType_35() const { return ___animationType_35; }
	inline int32_t* get_address_of_animationType_35() { return &___animationType_35; }
	inline void set_animationType_35(int32_t value)
	{
		___animationType_35 = value;
	}

	inline static int32_t get_offset_of_targetType_36() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___targetType_36)); }
	inline int32_t get_targetType_36() const { return ___targetType_36; }
	inline int32_t* get_address_of_targetType_36() { return &___targetType_36; }
	inline void set_targetType_36(int32_t value)
	{
		___targetType_36 = value;
	}

	inline static int32_t get_offset_of_forcedTargetType_37() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___forcedTargetType_37)); }
	inline int32_t get_forcedTargetType_37() const { return ___forcedTargetType_37; }
	inline int32_t* get_address_of_forcedTargetType_37() { return &___forcedTargetType_37; }
	inline void set_forcedTargetType_37(int32_t value)
	{
		___forcedTargetType_37 = value;
	}

	inline static int32_t get_offset_of_autoPlay_38() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___autoPlay_38)); }
	inline bool get_autoPlay_38() const { return ___autoPlay_38; }
	inline bool* get_address_of_autoPlay_38() { return &___autoPlay_38; }
	inline void set_autoPlay_38(bool value)
	{
		___autoPlay_38 = value;
	}

	inline static int32_t get_offset_of_useTargetAsV3_39() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___useTargetAsV3_39)); }
	inline bool get_useTargetAsV3_39() const { return ___useTargetAsV3_39; }
	inline bool* get_address_of_useTargetAsV3_39() { return &___useTargetAsV3_39; }
	inline void set_useTargetAsV3_39(bool value)
	{
		___useTargetAsV3_39 = value;
	}

	inline static int32_t get_offset_of_endValueFloat_40() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___endValueFloat_40)); }
	inline float get_endValueFloat_40() const { return ___endValueFloat_40; }
	inline float* get_address_of_endValueFloat_40() { return &___endValueFloat_40; }
	inline void set_endValueFloat_40(float value)
	{
		___endValueFloat_40 = value;
	}

	inline static int32_t get_offset_of_endValueV3_41() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___endValueV3_41)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_endValueV3_41() const { return ___endValueV3_41; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_endValueV3_41() { return &___endValueV3_41; }
	inline void set_endValueV3_41(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___endValueV3_41 = value;
	}

	inline static int32_t get_offset_of_endValueV2_42() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___endValueV2_42)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_endValueV2_42() const { return ___endValueV2_42; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_endValueV2_42() { return &___endValueV2_42; }
	inline void set_endValueV2_42(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___endValueV2_42 = value;
	}

	inline static int32_t get_offset_of_endValueColor_43() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___endValueColor_43)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_endValueColor_43() const { return ___endValueColor_43; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_endValueColor_43() { return &___endValueColor_43; }
	inline void set_endValueColor_43(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___endValueColor_43 = value;
	}

	inline static int32_t get_offset_of_endValueString_44() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___endValueString_44)); }
	inline String_t* get_endValueString_44() const { return ___endValueString_44; }
	inline String_t** get_address_of_endValueString_44() { return &___endValueString_44; }
	inline void set_endValueString_44(String_t* value)
	{
		___endValueString_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___endValueString_44), (void*)value);
	}

	inline static int32_t get_offset_of_endValueRect_45() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___endValueRect_45)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_endValueRect_45() const { return ___endValueRect_45; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_endValueRect_45() { return &___endValueRect_45; }
	inline void set_endValueRect_45(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___endValueRect_45 = value;
	}

	inline static int32_t get_offset_of_endValueTransform_46() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___endValueTransform_46)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_endValueTransform_46() const { return ___endValueTransform_46; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_endValueTransform_46() { return &___endValueTransform_46; }
	inline void set_endValueTransform_46(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___endValueTransform_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___endValueTransform_46), (void*)value);
	}

	inline static int32_t get_offset_of_optionalBool0_47() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___optionalBool0_47)); }
	inline bool get_optionalBool0_47() const { return ___optionalBool0_47; }
	inline bool* get_address_of_optionalBool0_47() { return &___optionalBool0_47; }
	inline void set_optionalBool0_47(bool value)
	{
		___optionalBool0_47 = value;
	}

	inline static int32_t get_offset_of_optionalFloat0_48() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___optionalFloat0_48)); }
	inline float get_optionalFloat0_48() const { return ___optionalFloat0_48; }
	inline float* get_address_of_optionalFloat0_48() { return &___optionalFloat0_48; }
	inline void set_optionalFloat0_48(float value)
	{
		___optionalFloat0_48 = value;
	}

	inline static int32_t get_offset_of_optionalInt0_49() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___optionalInt0_49)); }
	inline int32_t get_optionalInt0_49() const { return ___optionalInt0_49; }
	inline int32_t* get_address_of_optionalInt0_49() { return &___optionalInt0_49; }
	inline void set_optionalInt0_49(int32_t value)
	{
		___optionalInt0_49 = value;
	}

	inline static int32_t get_offset_of_optionalRotationMode_50() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___optionalRotationMode_50)); }
	inline int32_t get_optionalRotationMode_50() const { return ___optionalRotationMode_50; }
	inline int32_t* get_address_of_optionalRotationMode_50() { return &___optionalRotationMode_50; }
	inline void set_optionalRotationMode_50(int32_t value)
	{
		___optionalRotationMode_50 = value;
	}

	inline static int32_t get_offset_of_optionalScrambleMode_51() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___optionalScrambleMode_51)); }
	inline int32_t get_optionalScrambleMode_51() const { return ___optionalScrambleMode_51; }
	inline int32_t* get_address_of_optionalScrambleMode_51() { return &___optionalScrambleMode_51; }
	inline void set_optionalScrambleMode_51(int32_t value)
	{
		___optionalScrambleMode_51 = value;
	}

	inline static int32_t get_offset_of_optionalString_52() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ___optionalString_52)); }
	inline String_t* get_optionalString_52() const { return ___optionalString_52; }
	inline String_t** get_address_of_optionalString_52() { return &___optionalString_52; }
	inline void set_optionalString_52(String_t* value)
	{
		___optionalString_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___optionalString_52), (void*)value);
	}

	inline static int32_t get_offset_of__tweenCreated_53() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ____tweenCreated_53)); }
	inline bool get__tweenCreated_53() const { return ____tweenCreated_53; }
	inline bool* get_address_of__tweenCreated_53() { return &____tweenCreated_53; }
	inline void set__tweenCreated_53(bool value)
	{
		____tweenCreated_53 = value;
	}

	inline static int32_t get_offset_of__playCount_54() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9, ____playCount_54)); }
	inline int32_t get__playCount_54() const { return ____playCount_54; }
	inline int32_t* get_address_of__playCount_54() { return &____playCount_54; }
	inline void set__playCount_54(int32_t value)
	{
		____playCount_54 = value;
	}
};


// UnityEngine.UI.Graphic
struct  Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Image
struct  Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;

public:
	inline static int32_t get_offset_of_m_Sprite_37() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Sprite_37)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_37() const { return ___m_Sprite_37; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_37() { return &___m_Sprite_37; }
	inline void set_m_Sprite_37(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_38() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_OverrideSprite_38)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_OverrideSprite_38() const { return ___m_OverrideSprite_38; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_OverrideSprite_38() { return &___m_OverrideSprite_38; }
	inline void set_m_OverrideSprite_38(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_OverrideSprite_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_39() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Type_39)); }
	inline int32_t get_m_Type_39() const { return ___m_Type_39; }
	inline int32_t* get_address_of_m_Type_39() { return &___m_Type_39; }
	inline void set_m_Type_39(int32_t value)
	{
		___m_Type_39 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_40() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PreserveAspect_40)); }
	inline bool get_m_PreserveAspect_40() const { return ___m_PreserveAspect_40; }
	inline bool* get_address_of_m_PreserveAspect_40() { return &___m_PreserveAspect_40; }
	inline void set_m_PreserveAspect_40(bool value)
	{
		___m_PreserveAspect_40 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_41() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillCenter_41)); }
	inline bool get_m_FillCenter_41() const { return ___m_FillCenter_41; }
	inline bool* get_address_of_m_FillCenter_41() { return &___m_FillCenter_41; }
	inline void set_m_FillCenter_41(bool value)
	{
		___m_FillCenter_41 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_42() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillMethod_42)); }
	inline int32_t get_m_FillMethod_42() const { return ___m_FillMethod_42; }
	inline int32_t* get_address_of_m_FillMethod_42() { return &___m_FillMethod_42; }
	inline void set_m_FillMethod_42(int32_t value)
	{
		___m_FillMethod_42 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_43() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillAmount_43)); }
	inline float get_m_FillAmount_43() const { return ___m_FillAmount_43; }
	inline float* get_address_of_m_FillAmount_43() { return &___m_FillAmount_43; }
	inline void set_m_FillAmount_43(float value)
	{
		___m_FillAmount_43 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_44() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillClockwise_44)); }
	inline bool get_m_FillClockwise_44() const { return ___m_FillClockwise_44; }
	inline bool* get_address_of_m_FillClockwise_44() { return &___m_FillClockwise_44; }
	inline void set_m_FillClockwise_44(bool value)
	{
		___m_FillClockwise_44 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_45() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillOrigin_45)); }
	inline int32_t get_m_FillOrigin_45() const { return ___m_FillOrigin_45; }
	inline int32_t* get_address_of_m_FillOrigin_45() { return &___m_FillOrigin_45; }
	inline void set_m_FillOrigin_45(int32_t value)
	{
		___m_FillOrigin_45 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_46() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_AlphaHitTestMinimumThreshold_46)); }
	inline float get_m_AlphaHitTestMinimumThreshold_46() const { return ___m_AlphaHitTestMinimumThreshold_46; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_46() { return &___m_AlphaHitTestMinimumThreshold_46; }
	inline void set_m_AlphaHitTestMinimumThreshold_46(float value)
	{
		___m_AlphaHitTestMinimumThreshold_46 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_47() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Tracked_47)); }
	inline bool get_m_Tracked_47() const { return ___m_Tracked_47; }
	inline bool* get_address_of_m_Tracked_47() { return &___m_Tracked_47; }
	inline void set_m_Tracked_47(bool value)
	{
		___m_Tracked_47 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_48() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_UseSpriteMesh_48)); }
	inline bool get_m_UseSpriteMesh_48() const { return ___m_UseSpriteMesh_48; }
	inline bool* get_address_of_m_UseSpriteMesh_48() { return &___m_UseSpriteMesh_48; }
	inline void set_m_UseSpriteMesh_48(bool value)
	{
		___m_UseSpriteMesh_48 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_49() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PixelsPerUnitMultiplier_49)); }
	inline float get_m_PixelsPerUnitMultiplier_49() const { return ___m_PixelsPerUnitMultiplier_49; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_49() { return &___m_PixelsPerUnitMultiplier_49; }
	inline void set_m_PixelsPerUnitMultiplier_49(float value)
	{
		___m_PixelsPerUnitMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_50() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_CachedReferencePixelsPerUnit_50)); }
	inline float get_m_CachedReferencePixelsPerUnit_50() const { return ___m_CachedReferencePixelsPerUnit_50; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_50() { return &___m_CachedReferencePixelsPerUnit_50; }
	inline void set_m_CachedReferencePixelsPerUnit_50(float value)
	{
		___m_CachedReferencePixelsPerUnit_50 = value;
	}
};

struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t815A476B0A21E183042059E705F9E505478CD8AE * ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_36() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_ETC1DefaultUI_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_ETC1DefaultUI_36() const { return ___s_ETC1DefaultUI_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_ETC1DefaultUI_36() { return &___s_ETC1DefaultUI_36; }
	inline void set_s_ETC1DefaultUI_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_ETC1DefaultUI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_51() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_VertScratch_51)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_VertScratch_51() const { return ___s_VertScratch_51; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_VertScratch_51() { return &___s_VertScratch_51; }
	inline void set_s_VertScratch_51(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_VertScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_52() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_UVScratch_52)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_UVScratch_52() const { return ___s_UVScratch_52; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_UVScratch_52() { return &___s_UVScratch_52; }
	inline void set_s_UVScratch_52(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_UVScratch_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_53() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Xy_53)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Xy_53() const { return ___s_Xy_53; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Xy_53() { return &___s_Xy_53; }
	inline void set_s_Xy_53(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Xy_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_54() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Uv_54)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Uv_54() const { return ___s_Uv_54; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Uv_54() { return &___s_Uv_54; }
	inline void set_s_Uv_54(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Uv_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_54), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_55() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___m_TrackedTexturelessImages_55)); }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE * get_m_TrackedTexturelessImages_55() const { return ___m_TrackedTexturelessImages_55; }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE ** get_address_of_m_TrackedTexturelessImages_55() { return &___m_TrackedTexturelessImages_55; }
	inline void set_m_TrackedTexturelessImages_55(List_1_t815A476B0A21E183042059E705F9E505478CD8AE * value)
	{
		___m_TrackedTexturelessImages_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_55), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_56() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Initialized_56)); }
	inline bool get_s_Initialized_56() const { return ___s_Initialized_56; }
	inline bool* get_address_of_s_Initialized_56() { return &___s_Initialized_56; }
	inline void set_s_Initialized_56(bool value)
	{
		___s_Initialized_56 = value;
	}
};


// UnityEngine.UI.Text
struct  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// DG.Tweening.DOTweenAnimation[]
struct DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * m_Items[1];

public:
	inline DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  m_Items[1];

public:
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  value)
	{
		m_Items[index] = value;
	}
};


// !!0 DG.Tweening.TweenSettingsExtensions::From<System.Object>(!!0,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_From_TisRuntimeObject_mA83BBEB3C5D5CDBBE0F316186D86645DAC5F336B_gshared (RuntimeObject * ___t0, bool ___isRelative1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<System.Object>(!!0,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetRelative_TisRuntimeObject_mD31A694DCCD7EFAD9BC7B8659C412060D68C3BF1_gshared (RuntimeObject * ___t0, bool ___isRelative1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<System.Object>(!!0,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetTarget_TisRuntimeObject_m34083705F311A02B95287FADFAD21CC55D9545AE_gshared (RuntimeObject * ___t0, RuntimeObject * ___target1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetDelay<System.Object>(!!0,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetDelay_TisRuntimeObject_mAD79DB59BFAAA02166DDBB7C147C9E310E34A257_gshared (RuntimeObject * ___t0, float ___delay1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<System.Object>(!!0,System.Int32,DG.Tweening.LoopType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetLoops_TisRuntimeObject_m65FD37B61EFA696D5A4EB23ABA3C93444E4F9B4B_gshared (RuntimeObject * ___t0, int32_t ___loops1, int32_t ___loopType2, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetAutoKill<System.Object>(!!0,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetAutoKill_TisRuntimeObject_m0398884E97953A082C222383BE97404B2BF254AD_gshared (RuntimeObject * ___t0, bool ___autoKillOnCompletion1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnKill<System.Object>(!!0,DG.Tweening.TweenCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_OnKill_TisRuntimeObject_m3D44E377E810E472132FC1F0F405DCA565E6DCAE_gshared (RuntimeObject * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetSpeedBased<System.Object>(!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetSpeedBased_TisRuntimeObject_m808BBC2133B2235E29198F54D5146FEF34ABB8D7_gshared (RuntimeObject * ___t0, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<System.Object>(!!0,UnityEngine.AnimationCurve)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetEase_TisRuntimeObject_mDFD21B66D89B1600A547B64E3F973F63D9F42B98_gshared (RuntimeObject * ___t0, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___animCurve1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<System.Object>(!!0,DG.Tweening.Ease)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetEase_TisRuntimeObject_m17D546F0CC41676B108187269B5BB10959EFA2A4_gshared (RuntimeObject * ___t0, int32_t ___ease1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetId<System.Object>(!!0,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetId_TisRuntimeObject_m72E09063B9C56E443140E7E01F32618F28131087_gshared (RuntimeObject * ___t0, RuntimeObject * ___id1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetUpdate<System.Object>(!!0,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetUpdate_TisRuntimeObject_mB9C5B68E70354CBB1D7DE55542C2E54571828A74_gshared (RuntimeObject * ___t0, bool ___isIndependentUpdate1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnStart<System.Object>(!!0,DG.Tweening.TweenCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_OnStart_TisRuntimeObject_mE8EF10BC4D36C32FCEFB7DB33732E45775D2A986_gshared (RuntimeObject * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnPlay<System.Object>(!!0,DG.Tweening.TweenCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_OnPlay_TisRuntimeObject_mC8DCD120A7EF0DCBF14EDF1852CF047A6632BBC9_gshared (RuntimeObject * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnUpdate<System.Object>(!!0,DG.Tweening.TweenCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_OnUpdate_TisRuntimeObject_m9833BEBF80F4D5CFBD371351F053310B5758AE99_gshared (RuntimeObject * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnStepComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_OnStepComplete_TisRuntimeObject_mA199BA498C4CD73FBE20E1CA62E6F658AE492D0A_gshared (RuntimeObject * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_OnComplete_TisRuntimeObject_m23E1F90FEE2A0CA4D04C244B7A7A83A541855DC0_gshared (RuntimeObject * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnRewind<System.Object>(!!0,DG.Tweening.TweenCallback)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_OnRewind_TisRuntimeObject_mBFA5258FEF4DA74AC2ABC3C8025506F1EB866AF0_gshared (RuntimeObject * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenExtensions::Play<System.Object>(!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenExtensions_Play_TisRuntimeObject_m3E3C5EA201897EC0A7D33C27152A94B0C6BA54AD_gshared (RuntimeObject * ___t0, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenExtensions::Pause<System.Object>(!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenExtensions_Pause_TisRuntimeObject_mB5DB00EFAF4AD1C64193EBAD6A86680274AD57EA_gshared (RuntimeObject * ___t0, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponents<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* GameObject_GetComponents_TisRuntimeObject_mAB26971A1F37F81EEEF20F7897AA6FAE3B33779E_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponents<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Component_GetComponents_TisRuntimeObject_mEBC8AE0C8843120678A284931632FCCBE15F0C46_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);

// System.Void DG.Tweening.DOTweenAnimation::CreateTween()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_CreateTween_m3A4CD9BFF59D47D95BEA073966ACFAF07738106E (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method);
// System.Boolean DG.Tweening.TweenExtensions::IsActive(DG.Tweening.Tween)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool TweenExtensions_IsActive_m19E1D79E2590A189C95DADED68FFDB43A7F3B2A2_inline (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method);
// System.Void DG.Tweening.TweenExtensions::Kill(DG.Tweening.Tween,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenExtensions_Kill_m8A79B9D5D31C46E9669C2EFEDF26BF4F7EB02D10 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, bool ___complete1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17 (String_t* ___format0, RuntimeObject * ___arg01, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_mE6AF3EFCF84F2296622CD42FBF9EEAF07244C0A8 (RuntimeObject * ___message0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___context1, const RuntimeMethod* method);
// System.Type System.Object::GetType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B (RuntimeObject * __this, const RuntimeMethod* method);
// DG.Tweening.Core.TargetType DG.Tweening.DOTweenAnimation::TypeToDOTargetType(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DOTweenAnimation_TypeToDOTargetType_mAB457ACFE735EC9B05FE0BA52C88E8576EEE304F (Type_t * ___t0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (const RuntimeMethod* method);
// UnityEngine.Vector2 DG.Tweening.DOTweenUtils46::SwitchToRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  DOTweenUtils46_SwitchToRectTransform_m0A393795E7C6FCC971FA44685DBD8C46176BCBB9 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___from0, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___to1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOAnchorPos3D(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions46_DOAnchorPos3D_m104305FCB812004D56F3CDBA2327EC0AC8D2CBFF (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___endValue1, float ___duration2, bool ___snapping3, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOMove_mA83A2C1D4DFC55CB4D2B2763FD1CF8D4ADCD634D (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___endValue1, float ___duration2, bool ___snapping3, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOMove(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions43_DOMove_m2841C4B8C1638272A9C9A23E2FFA00B104C35400 (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___target0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___endValue1, float ___duration2, bool ___snapping3, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMove(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOMove_mD9817CCAE0681D620067D21B4E868428FE2533D8 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___endValue1, float ___duration2, bool ___snapping3, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOLocalMove_m0F6957D951BA19FEA0A48C7CDA52240AC7CD9369 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___endValue1, float ___duration2, bool ___snapping3, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DORotate_mBC72ACEECA2351B7CE6491E5FE0B76469609DEFD (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___endValue1, float ___duration2, int32_t ___mode3, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DORotate(UnityEngine.Rigidbody2D,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions43_DORotate_m868974591A2268B71A3EC37CC6D31CE77979B10C (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORotate(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DORotate_m578E0422C09350DCB3298AA920B6F9684BF09CAD (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___endValue1, float ___duration2, int32_t ___mode3, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalRotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOLocalRotate_m0A3A0561A8DFA471BCA68891686EE51BD6CD2F28 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___endValue1, float ___duration2, int32_t ___mode3, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOScale_m68874CDE5A501CD5EBFD9021D689A8E3029A7797 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___endValue1, float ___duration2, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOSizeDelta(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions46_DOSizeDelta_m0F28C462C6F10CA44C6DDEFBE30838FDB490AB7A (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___endValue1, float ___duration2, bool ___snapping3, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions43_DOColor_mA2543B2AD028B46EA2C799F18C669A00783970E8 (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___target0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___endValue1, float ___duration2, const RuntimeMethod* method);
// UnityEngine.Material UnityEngine.Renderer::get_material()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t8927C00353A72755313F046D0CE85178AE8218EE * Renderer_get_material_mE6B01125502D08EE0D6DFE2EAEC064AD9BB31804 (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Material,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOColor_mE6140CF9E1B3D3B559F6751F9B8C3B1602826710 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___target0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___endValue1, float ___duration2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions46_DOColor_mEFDE35BB24046DCFBAB8DAB12E6B7996C338A719 (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___target0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___endValue1, float ___duration2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions46_DOColor_m80A0A452604641CA11960C491CBC3EBE30450BB6 (Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___target0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___endValue1, float ___duration2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Light,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOColor_mE6D48DDA2B16225D9DBF40D67D8716992016BE5E (Light_tA2F349FE839781469A0344CF6039B51512394275 * ___target0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___endValue1, float ___duration2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOFade(UnityEngine.SpriteRenderer,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions43_DOFade_m5F3CD26D926219D7FCBAF07C0EAB1D50F76E5006 (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFade(UnityEngine.Material,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOFade_mB5B3CAC06223B35C8E666ADC139D93FF17A31C48 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions46_DOFade_mF268968FB793053C5F5230343B2771C9C608C007 (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions46_DOFade_m560566897759A70C4136B961F83EB89E441E0CBC (Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOIntensity(UnityEngine.Light,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOIntensity_m4F6889CB6C25B1B30099075C7AC9F6E3B24FC080 (Light_tA2F349FE839781469A0344CF6039B51512394275 * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.CanvasGroup,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions46_DOFade_m72575C2B2015133EFCC699FE21A2419951AA432A (CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOText(UnityEngine.UI.Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions46_DOText_mE13EDACBB2B161A6E7BB68A4BFAC148DA825EEA0 (Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___target0, String_t* ___endValue1, float ___duration2, bool ___richTextEnabled3, int32_t ___scrambleMode4, String_t* ___scrambleChars5, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOPunchAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions46_DOPunchAnchorPos_m0E4FBB18152C7EF788723DC36A08081CADF3C3CC (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___punch1, float ___duration2, int32_t ___vibrato3, float ___elasticity4, bool ___snapping5, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPunchPosition(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOPunchPosition_m9EEF18AFCE150A50A4F79AA85982B9CE70DAD0DA (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___punch1, float ___duration2, int32_t ___vibrato3, float ___elasticity4, bool ___snapping5, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPunchScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOPunchScale_m351E39CC16A4CC9692955C502F350D56825B17E4 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___punch1, float ___duration2, int32_t ___vibrato3, float ___elasticity4, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPunchRotation(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOPunchRotation_mC1D5DA0D6218A579D04BB373DDF1264E0AC4736A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___punch1, float ___duration2, int32_t ___vibrato3, float ___elasticity4, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,UnityEngine.Vector2,System.Int32,System.Single,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions46_DOShakeAnchorPos_mA8018E67BCA3312E54446B02D184BF7BA4656EEC (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target0, float ___duration1, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___strength2, int32_t ___vibrato3, float ___randomness4, bool ___snapping5, bool ___fadeOut6, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakePosition(UnityEngine.Transform,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOShakePosition_mDEC0E682E9A8D470D6FD1F825161C3E6087DC3B8 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, float ___duration1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___strength2, int32_t ___vibrato3, float ___randomness4, bool ___snapping5, bool ___fadeOut6, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeScale(UnityEngine.Transform,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOShakeScale_m39430B83048A4F601B8B206224E792F7F075D22F (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, float ___duration1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___strength2, int32_t ___vibrato3, float ___randomness4, bool ___fadeOut5, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeRotation(UnityEngine.Transform,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOShakeRotation_m35DC1BCA976A9A2E1C7A6CA4359EB5F30695F1EE (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target0, float ___duration1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___strength2, int32_t ___vibrato3, float ___randomness4, bool ___fadeOut5, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOAspect(UnityEngine.Camera,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOAspect_mCD85FE3D0E67452AC9682825549AD6C973417F70 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Camera,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOColor_m406B84ECF991C880D866C555B3AE4DF41274D203 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___target0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___endValue1, float ___duration2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFieldOfView(UnityEngine.Camera,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOFieldOfView_m2FDB010FDA9B4164709FE0711B349C64FD73B498 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOOrthoSize(UnityEngine.Camera,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOOrthoSize_m6B046F2A9CB65F5C33F7E05E404C81BE5FCC1468 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPixelRect(UnityEngine.Camera,UnityEngine.Rect,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DOPixelRect_m18CCEED227266203D749048A9959857D56EBDEEB (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___target0, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___endValue1, float ___duration2, const RuntimeMethod* method);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORect(UnityEngine.Camera,UnityEngine.Rect,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ShortcutExtensions_DORect_m9D52CABC1C9DACDFE1EF0FB52D22BE293536B39E (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___target0, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___endValue1, float ___duration2, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::From<DG.Tweening.Tweener>(!!0,System.Boolean)
inline Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * TweenSettingsExtensions_From_TisTweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_mC83D3CD43EA1E66E8E8B455AA81A4C9B7A75FB4A (Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * ___t0, bool ___isRelative1, const RuntimeMethod* method)
{
	return ((  Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * (*) (Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 *, bool, const RuntimeMethod*))TweenSettingsExtensions_From_TisRuntimeObject_mA83BBEB3C5D5CDBBE0F316186D86645DAC5F336B_gshared)(___t0, ___isRelative1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<DG.Tweening.Tween>(!!0,System.Boolean)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_SetRelative_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m0DD3CA6BC5B6DFA96EB49AACEB29666342410FDE (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, bool ___isRelative1, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, bool, const RuntimeMethod*))TweenSettingsExtensions_SetRelative_TisRuntimeObject_mD31A694DCCD7EFAD9BC7B8659C412060D68C3BF1_gshared)(___t0, ___isRelative1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Tween>(!!0,System.Object)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_SetTarget_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m0F995FA73A7C3CF6A49200DE6237CB4F250FD5D9 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, RuntimeObject * ___target1, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, RuntimeObject *, const RuntimeMethod*))TweenSettingsExtensions_SetTarget_TisRuntimeObject_m34083705F311A02B95287FADFAD21CC55D9545AE_gshared)(___t0, ___target1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetDelay<DG.Tweening.Tween>(!!0,System.Single)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_SetDelay_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mD9B865FBEB261A2983D1F5BAB18C7E3F83619480 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, float ___delay1, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, float, const RuntimeMethod*))TweenSettingsExtensions_SetDelay_TisRuntimeObject_mAD79DB59BFAAA02166DDBB7C147C9E310E34A257_gshared)(___t0, ___delay1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Tween>(!!0,System.Int32,DG.Tweening.LoopType)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_SetLoops_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4156B77EABC3AA03F624808CDF6472BD0D964EA0 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, int32_t ___loops1, int32_t ___loopType2, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, int32_t, int32_t, const RuntimeMethod*))TweenSettingsExtensions_SetLoops_TisRuntimeObject_m65FD37B61EFA696D5A4EB23ABA3C93444E4F9B4B_gshared)(___t0, ___loops1, ___loopType2, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetAutoKill<DG.Tweening.Tween>(!!0,System.Boolean)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_SetAutoKill_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m3B106D91BC3EACE01F526CABBD59D24767E577D9 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, bool ___autoKillOnCompletion1, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, bool, const RuntimeMethod*))TweenSettingsExtensions_SetAutoKill_TisRuntimeObject_m0398884E97953A082C222383BE97404B2BF254AD_gshared)(___t0, ___autoKillOnCompletion1, method);
}
// System.Void DG.Tweening.TweenCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662 (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnKill<DG.Tweening.Tween>(!!0,DG.Tweening.TweenCallback)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_OnKill_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m8416D52033C9F6508DA669F66198F6477B74EFE7 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *, const RuntimeMethod*))TweenSettingsExtensions_OnKill_TisRuntimeObject_m3D44E377E810E472132FC1F0F405DCA565E6DCAE_gshared)(___t0, ___action1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetSpeedBased<DG.Tweening.Tween>(!!0)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_SetSpeedBased_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m641FDC9A608F06A264C303790E65FA671F3E316A (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, const RuntimeMethod*))TweenSettingsExtensions_SetSpeedBased_TisRuntimeObject_m808BBC2133B2235E29198F54D5146FEF34ABB8D7_gshared)(___t0, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<DG.Tweening.Tween>(!!0,UnityEngine.AnimationCurve)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_SetEase_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m423A73833B53B7DF4F822AB1200A8E1150801411 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___animCurve1, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, const RuntimeMethod*))TweenSettingsExtensions_SetEase_TisRuntimeObject_mDFD21B66D89B1600A547B64E3F973F63D9F42B98_gshared)(___t0, ___animCurve1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<DG.Tweening.Tween>(!!0,DG.Tweening.Ease)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_SetEase_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m30E10D6BA20CCA9FE3099B5BC33D277F48C09880 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, int32_t ___ease1, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, int32_t, const RuntimeMethod*))TweenSettingsExtensions_SetEase_TisRuntimeObject_m17D546F0CC41676B108187269B5BB10959EFA2A4_gshared)(___t0, ___ease1, method);
}
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C (String_t* ___value0, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetId<DG.Tweening.Tween>(!!0,System.Object)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_SetId_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4FA57F2F89A8CCEC59A8330199E74221062E929E (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, RuntimeObject * ___id1, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, RuntimeObject *, const RuntimeMethod*))TweenSettingsExtensions_SetId_TisRuntimeObject_m72E09063B9C56E443140E7E01F32618F28131087_gshared)(___t0, ___id1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetUpdate<DG.Tweening.Tween>(!!0,System.Boolean)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_SetUpdate_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m3C4F344B80D25DDA04A2C205DD25302E708140DC (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, bool ___isIndependentUpdate1, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, bool, const RuntimeMethod*))TweenSettingsExtensions_SetUpdate_TisRuntimeObject_mB9C5B68E70354CBB1D7DE55542C2E54571828A74_gshared)(___t0, ___isIndependentUpdate1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::OnStart<DG.Tweening.Tween>(!!0,DG.Tweening.TweenCallback)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_OnStart_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mB17597C3125F3F94329BF5C14DE7BCCF05E99EC3 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *, const RuntimeMethod*))TweenSettingsExtensions_OnStart_TisRuntimeObject_mE8EF10BC4D36C32FCEFB7DB33732E45775D2A986_gshared)(___t0, ___action1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::OnPlay<DG.Tweening.Tween>(!!0,DG.Tweening.TweenCallback)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_OnPlay_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mC17C14807CF262BA39CBF9C55949A053EF127A4C (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *, const RuntimeMethod*))TweenSettingsExtensions_OnPlay_TisRuntimeObject_mC8DCD120A7EF0DCBF14EDF1852CF047A6632BBC9_gshared)(___t0, ___action1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::OnUpdate<DG.Tweening.Tween>(!!0,DG.Tweening.TweenCallback)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_OnUpdate_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m5F0CCCF9EBBC043CEA09AC3F9AD2CC9A3243EF45 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *, const RuntimeMethod*))TweenSettingsExtensions_OnUpdate_TisRuntimeObject_m9833BEBF80F4D5CFBD371351F053310B5758AE99_gshared)(___t0, ___action1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::OnStepComplete<DG.Tweening.Tween>(!!0,DG.Tweening.TweenCallback)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_OnStepComplete_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m5527E140B5D01685DD8041B72C62461EA2A8A1E4 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *, const RuntimeMethod*))TweenSettingsExtensions_OnStepComplete_TisRuntimeObject_mA199BA498C4CD73FBE20E1CA62E6F658AE492D0A_gshared)(___t0, ___action1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<DG.Tweening.Tween>(!!0,DG.Tweening.TweenCallback)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_OnComplete_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mE51D62942FE6E2C354ADDB87CE4CDA2206DADCC7 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *, const RuntimeMethod*))TweenSettingsExtensions_OnComplete_TisRuntimeObject_m23E1F90FEE2A0CA4D04C244B7A7A83A541855DC0_gshared)(___t0, ___action1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::OnRewind<DG.Tweening.Tween>(!!0,DG.Tweening.TweenCallback)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenSettingsExtensions_OnRewind_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mCDECB675CBFB437D345E0DE1A59F8BC4BA86121E (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * ___action1, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *, const RuntimeMethod*))TweenSettingsExtensions_OnRewind_TisRuntimeObject_mBFA5258FEF4DA74AC2ABC3C8025506F1EB866AF0_gshared)(___t0, ___action1, method);
}
// !!0 DG.Tweening.TweenExtensions::Play<DG.Tweening.Tween>(!!0)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenExtensions_Play_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m1A3AAD3856D80A02DB7F6BB7BBA444FD4C62DA8C (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, const RuntimeMethod*))TweenExtensions_Play_TisRuntimeObject_m3E3C5EA201897EC0A7D33C27152A94B0C6BA54AD_gshared)(___t0, method);
}
// !!0 DG.Tweening.TweenExtensions::Pause<DG.Tweening.Tween>(!!0)
inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * TweenExtensions_Pause_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4A0F398B9290C213D5FED5901234BA973AA0C257 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method)
{
	return ((  Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * (*) (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, const RuntimeMethod*))TweenExtensions_Pause_TisRuntimeObject_mB5DB00EFAF4AD1C64193EBAD6A86680274AD57EA_gshared)(___t0, method);
}
// System.Void UnityEngine.Events.UnityEvent::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5 (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, const RuntimeMethod* method);
// System.Int32 DG.Tweening.DOTween::Play(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DOTween_Play_mC14CD676517FE51D65E52689A7BC9CD40AD3B4A9 (RuntimeObject * ___targetOrId0, const RuntimeMethod* method);
// System.Int32 DG.Tweening.DOTween::PlayBackwards(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DOTween_PlayBackwards_m5246F3D7CA28BDC2ADD46FC100C0C6FF05DBAA88 (RuntimeObject * ___targetOrId0, const RuntimeMethod* method);
// System.Int32 DG.Tweening.DOTween::PlayForward(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DOTween_PlayForward_m7ABD5ED445599090AB74695419E18716C7ADFD84 (RuntimeObject * ___targetOrId0, const RuntimeMethod* method);
// System.Int32 DG.Tweening.DOTween::Pause(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DOTween_Pause_mD2AA03C8EDAAA5154478FA57F81041F5B27B4603 (RuntimeObject * ___targetOrId0, const RuntimeMethod* method);
// System.Int32 DG.Tweening.DOTween::TogglePause(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DOTween_TogglePause_mDD72068F34F6B37697C7DE4AC6D9AC1E3B441EE5 (RuntimeObject * ___targetOrId0, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponents<DG.Tweening.DOTweenAnimation>()
inline DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* GameObject_GetComponents_TisDOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9_m0EB1FAE4F313EBDE3E2726D1B7B497289CF8A85B (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponents_TisRuntimeObject_mAB26971A1F37F81EEEF20F7897AA6FAE3B33779E_gshared)(__this, method);
}
// System.Boolean DG.Tweening.TweenExtensions::IsInitialized(DG.Tweening.Tween)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TweenExtensions_IsInitialized_mE92123AF4FE069B2F175193367CAFF218B552CEB (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method);
// System.Void DG.Tweening.TweenExtensions::Rewind(DG.Tweening.Tween,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenExtensions_Rewind_m911EFDC4DAAEB2DC3C80112FE4C14F01BCF558C8 (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, bool ___includeDelay1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.Debugger::LogNullTween(DG.Tweening.Tween)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debugger_LogNullTween_m6A9B33DC41AC0A765DD49C9AC2DDABECD22F476B (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method);
// System.Void DG.Tweening.DOTweenAnimation::ReEvaluateRelativeTween()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_ReEvaluateRelativeTween_mB08DC3F0A42359F5A4B1DA632FFBF4596E365300 (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method);
// System.Int32 DG.Tweening.DOTween::Restart(System.Object,System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DOTween_Restart_m6881B2FF21311D7A29EB514DFBB6FC85799BB2A6 (RuntimeObject * ___targetOrId0, bool ___includeDelay1, float ___changeDelayTo2, const RuntimeMethod* method);
// System.Int32 DG.Tweening.DOTween::Complete(System.Object,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DOTween_Complete_mF9C035573D7C3F51938F073B38FC3B5EA8F640B9 (RuntimeObject * ___targetOrId0, bool ___withCallbacks1, const RuntimeMethod* method);
// System.Int32 DG.Tweening.DOTween::Kill(System.Object,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DOTween_Kill_mF0F7315CA9F8E32145CA3E3C9249FBF98BF3AD2A (RuntimeObject * ___targetOrId0, bool ___complete1, const RuntimeMethod* method);
// System.Int32 DG.Tweening.DOTween::Play(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DOTween_Play_mAC946600F4FD0CB092D54F037213F5BFBDB1606C (RuntimeObject * ___target0, RuntimeObject * ___id1, const RuntimeMethod* method);
// System.Int32 DG.Tweening.DOTween::PlayBackwards(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DOTween_PlayBackwards_m05450BF2423C577072F2C0879DF3483A626AAB39 (RuntimeObject * ___target0, RuntimeObject * ___id1, const RuntimeMethod* method);
// System.Int32 DG.Tweening.DOTween::PlayForward(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DOTween_PlayForward_m5E6AD8D0E07B823E88E2E64250634A8962415F83 (RuntimeObject * ___target0, RuntimeObject * ___id1, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponents<DG.Tweening.DOTweenAnimation>()
inline DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* Component_GetComponents_TisDOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9_mD0B291AF74B316EE4EE8444CF959B59235E9A6EA (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponents_TisRuntimeObject_mEBC8AE0C8843120678A284931632FCCBE15F0C46_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Boolean DG.Tweening.TweenExtensions::IsPlaying(DG.Tweening.Tween)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TweenExtensions_IsPlaying_m3B9AB4389AA78D1EB5384BB33BF24640E0005F6D (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method);
// System.Boolean DG.Tweening.TweenExtensions::IsComplete(DG.Tweening.Tween)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TweenExtensions_IsComplete_mC87F29FE0746C793F0C11175A9B37087C1C436FD (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method);
// System.Int32 DG.Tweening.DOTween::Rewind(System.Object,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DOTween_Rewind_m5A9A7CD4D2FB1B2F16212A8A4A858B2FCA372C65 (RuntimeObject * ___targetOrId0, bool ___includeDelay1, const RuntimeMethod* method);
// System.Void DG.Tweening.DOTweenAnimation::DOPlayNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DOPlayNext_m233EF38D6140BFEF7623EC6E3AD769C184E32544 (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method);
// System.Int32 DG.Tweening.DOTween::Restart(System.Object,System.Object,System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DOTween_Restart_m26B0C36D9CE55E70C0D97D543F8F95AA7D447445 (RuntimeObject * ___target0, RuntimeObject * ___id1, bool ___includeDelay2, float ___changeDelayTo3, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::.ctor()
inline void List_1__ctor_mF8875C86FFAC30CA85251C1B54879C9A33F4DF3F (List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::Add(!0)
inline void List_1_Add_m782F802BE39FD4D3D5758579703B971BC396ABB8 (List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1 * __this, Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1 *, Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Int32 System.String::LastIndexOf(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t String_LastIndexOf_m80AFBEF2F3857F9D6A67126F4C4D9A9B9CEC5902 (String_t* __this, String_t* ___value0, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Substring_mB6B87FD76552BBF6D4E2B9F07F857FE051DCE190 (String_t* __this, int32_t ___startIndex0, const RuntimeMethod* method);
// System.Int32 System.String::IndexOf(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t String_IndexOf_m90616B2D8ACC645F389750FAE4F9A75BC5D82454 (String_t* __this, String_t* ___value0, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// System.Object System.Enum::Parse(System.Type,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Enum_Parse_m6601224637A9CF40F77358805956C2EE757EAF68 (Type_t * ___enumType0, String_t* ___value1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localPosition_m527B8B5B625DA9A61E551E0FBCD3BE8CA4539FC2 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * __this, float ___time0, float ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0 (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * __this, KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* ___keys0, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m12075526A02B55B680716A34AD5287B223122B70 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.ABSAnimationComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ABSAnimationComponent__ctor_mF453633C79A30A108832D3F331DAB5E4CA24B272 (ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.DOTweenAnimation::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_Awake_m98D3DE4C3FA5DF6CF545091AFDBF5482F57EEA54 (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	{
		// if (!isActive || !isValid) return;
		bool L_0 = __this->get_isActive_32();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = __this->get_isValid_33();
		if (L_1)
		{
			goto IL_0011;
		}
	}

IL_0010:
	{
		// if (!isActive || !isValid) return;
		return;
	}

IL_0011:
	{
		// if (animationType != DOTweenAnimationType.Move || !useTargetAsV3) {
		int32_t L_2 = __this->get_animationType_35();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0022;
		}
	}
	{
		bool L_3 = __this->get_useTargetAsV3_39();
		if (L_3)
		{
			goto IL_002f;
		}
	}

IL_0022:
	{
		// CreateTween();
		DOTweenAnimation_CreateTween_m3A4CD9BFF59D47D95BEA073966ACFAF07738106E(__this, /*hidden argument*/NULL);
		// _tweenCreated = true;
		__this->set__tweenCreated_53((bool)1);
	}

IL_002f:
	{
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_Start_m3A051AC580815E0852880030201AD5B6CB33B8AE (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	{
		// if (_tweenCreated || !isActive || !isValid) return;
		bool L_0 = __this->get__tweenCreated_53();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		bool L_1 = __this->get_isActive_32();
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		bool L_2 = __this->get_isValid_33();
		if (L_2)
		{
			goto IL_0019;
		}
	}

IL_0018:
	{
		// if (_tweenCreated || !isActive || !isValid) return;
		return;
	}

IL_0019:
	{
		// CreateTween();
		DOTweenAnimation_CreateTween_m3A4CD9BFF59D47D95BEA073966ACFAF07738106E(__this, /*hidden argument*/NULL);
		// _tweenCreated = true;
		__this->set__tweenCreated_53((bool)1);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_OnDestroy_m20C4C502C7104BB0D34E1D20E7968F0047D0AD8D (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	{
		// if (tween != null && tween.IsActive()) tween.Kill();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_0 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_1 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		bool L_2;
		L_2 = TweenExtensions_IsActive_m19E1D79E2590A189C95DADED68FFDB43A7F3B2A2_inline(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		// if (tween != null && tween.IsActive()) tween.Kill();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_3 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		TweenExtensions_Kill_m8A79B9D5D31C46E9669C2EFEDF26BF4F7EB02D10(L_3, (bool)0, /*hidden argument*/NULL);
	}

IL_0021:
	{
		// tween = null;
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20((Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *)NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::CreateTween()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_CreateTween_m3A4CD9BFF59D47D95BEA073966ACFAF07738106E (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTweenAnimation_U3CCreateTweenU3Eb__37_0_m7133BCD50DF5E0EDB11799DF7618551D417B05AA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Light_tA2F349FE839781469A0344CF6039B51512394275_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenExtensions_Pause_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4A0F398B9290C213D5FED5901234BA973AA0C257_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenExtensions_Play_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m1A3AAD3856D80A02DB7F6BB7BBA444FD4C62DA8C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_From_TisTweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_mC83D3CD43EA1E66E8E8B455AA81A4C9B7A75FB4A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_OnComplete_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mE51D62942FE6E2C354ADDB87CE4CDA2206DADCC7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_OnKill_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m8416D52033C9F6508DA669F66198F6477B74EFE7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_OnPlay_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mC17C14807CF262BA39CBF9C55949A053EF127A4C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_OnRewind_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mCDECB675CBFB437D345E0DE1A59F8BC4BA86121E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_OnStart_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mB17597C3125F3F94329BF5C14DE7BCCF05E99EC3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_OnStepComplete_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m5527E140B5D01685DD8041B72C62461EA2A8A1E4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_OnUpdate_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m5F0CCCF9EBBC043CEA09AC3F9AD2CC9A3243EF45_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetAutoKill_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m3B106D91BC3EACE01F526CABBD59D24767E577D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetDelay_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mD9B865FBEB261A2983D1F5BAB18C7E3F83619480_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetEase_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m30E10D6BA20CCA9FE3099B5BC33D277F48C09880_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetEase_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m423A73833B53B7DF4F822AB1200A8E1150801411_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetId_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4FA57F2F89A8CCEC59A8330199E74221062E929E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetLoops_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4156B77EABC3AA03F624808CDF6472BD0D964EA0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetRelative_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m0DD3CA6BC5B6DFA96EB49AACEB29666342410FDE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetSpeedBased_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m641FDC9A608F06A264C303790E65FA671F3E316A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetTarget_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m0F995FA73A7C3CF6A49200DE6237CB4F250FD5D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenSettingsExtensions_SetUpdate_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m3C4F344B80D25DDA04A2C205DD25302E708140DC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3A9AC714D9B78BE9FD8D8488AC619D7F2556992A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral71F308FB57202965E618BD0381AEBFDEEDF31F75);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE2996F43EBFFA914B746B5861EC325951281A3EB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF0AD9F8B1A2A2D534505FEA8D4D74C1BFD40753F);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * V_1 = NULL;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * V_2 = NULL;
	int32_t V_3 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * G_B33_0 = NULL;
	DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * G_B33_1 = NULL;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * G_B32_0 = NULL;
	DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * G_B32_1 = NULL;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  G_B34_0;
	memset((&G_B34_0), 0, sizeof(G_B34_0));
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * G_B34_1 = NULL;
	DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * G_B34_2 = NULL;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B37_0 = NULL;
	DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * G_B37_1 = NULL;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B36_0 = NULL;
	DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * G_B36_1 = NULL;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  G_B38_0;
	memset((&G_B38_0), 0, sizeof(G_B38_0));
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B38_1 = NULL;
	DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * G_B38_2 = NULL;
	{
		// if (target == null) {
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_0 = __this->get_target_34();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		// Debug.LogWarning(string.Format("{0} :: This tween's target is NULL, because the animation was created with a DOTween Pro version older than 0.9.255. To fix this, exit Play mode then simply select this object, and it will update automatically", this.gameObject.name), this.gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3;
		L_3 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_2, /*hidden argument*/NULL);
		String_t* L_4;
		L_4 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral3A9AC714D9B78BE9FD8D8488AC619D7F2556992A, L_3, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogWarning_mE6AF3EFCF84F2296622CD42FBF9EEAF07244C0A8(L_4, L_5, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_002f:
	{
		// if (forcedTargetType != TargetType.Unset) targetType = forcedTargetType;
		int32_t L_6 = __this->get_forcedTargetType_37();
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		// if (forcedTargetType != TargetType.Unset) targetType = forcedTargetType;
		int32_t L_7 = __this->get_forcedTargetType_37();
		__this->set_targetType_36(L_7);
	}

IL_0043:
	{
		// if (targetType == TargetType.Unset) {
		int32_t L_8 = __this->get_targetType_36();
		if (L_8)
		{
			goto IL_0061;
		}
	}
	{
		// targetType = TypeToDOTargetType(target.GetType());
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_9 = __this->get_target_34();
		NullCheck(L_9);
		Type_t * L_10;
		L_10 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_9, /*hidden argument*/NULL);
		int32_t L_11;
		L_11 = DOTweenAnimation_TypeToDOTargetType_mAB457ACFE735EC9B05FE0BA52C88E8576EEE304F(L_10, /*hidden argument*/NULL);
		__this->set_targetType_36(L_11);
	}

IL_0061:
	{
		// switch (animationType) {
		int32_t L_12 = __this->get_animationType_35();
		V_0 = L_12;
		int32_t L_13 = V_0;
		switch (L_13)
		{
			case 0:
			{
				goto IL_0957;
			}
			case 1:
			{
				goto IL_00cb;
			}
			case 2:
			{
				goto IL_02ae;
			}
			case 3:
			{
				goto IL_02d6;
			}
			case 4:
			{
				goto IL_037b;
			}
			case 5:
			{
				goto IL_03a3;
			}
			case 6:
			{
				goto IL_0430;
			}
			case 7:
			{
				goto IL_0533;
			}
			case 8:
			{
				goto IL_0661;
			}
			case 9:
			{
				goto IL_06a7;
			}
			case 10:
			{
				goto IL_0761;
			}
			case 11:
			{
				goto IL_0733;
			}
			case 12:
			{
				goto IL_078f;
			}
			case 13:
			{
				goto IL_084c;
			}
			case 14:
			{
				goto IL_081d;
			}
			case 15:
			{
				goto IL_087b;
			}
			case 16:
			{
				goto IL_08a2;
			}
			case 17:
			{
				goto IL_08c9;
			}
			case 18:
			{
				goto IL_08ed;
			}
			case 19:
			{
				goto IL_0911;
			}
			case 20:
			{
				goto IL_0935;
			}
			case 21:
			{
				goto IL_03ed;
			}
		}
	}
	{
		goto IL_0957;
	}

IL_00cb:
	{
		// if (useTargetAsV3) {
		bool L_14 = __this->get_useTargetAsV3_39();
		if (!L_14)
		{
			goto IL_01c5;
		}
	}
	{
		// isRelative = false;
		__this->set_isRelative_28((bool)0);
		// if (endValueTransform == null) {
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_15 = __this->get_endValueTransform_46();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_16;
		L_16 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_15, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_011b;
		}
	}
	{
		// Debug.LogWarning(string.Format("{0} :: This tween's TO target is NULL, a Vector3 of (0,0,0) will be used instead", this.gameObject.name), this.gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17;
		L_17 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		String_t* L_18;
		L_18 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_17, /*hidden argument*/NULL);
		String_t* L_19;
		L_19 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteralE2996F43EBFFA914B746B5861EC325951281A3EB, L_18, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_20;
		L_20 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogWarning_mE6AF3EFCF84F2296622CD42FBF9EEAF07244C0A8(L_19, L_20, /*hidden argument*/NULL);
		// endValueV3 = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		__this->set_endValueV3_41(L_21);
		// } else {
		goto IL_01c5;
	}

IL_011b:
	{
		// if (targetType == TargetType.RectTransform) {
		int32_t L_22 = __this->get_targetType_36();
		if ((!(((uint32_t)L_22) == ((uint32_t)5))))
		{
			goto IL_01b4;
		}
	}
	{
		// RectTransform endValueT = endValueTransform as RectTransform;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_23 = __this->get_endValueTransform_46();
		V_1 = ((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)IsInstSealed((RuntimeObject*)L_23, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var));
		// if (endValueT == null) {
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_25;
		L_25 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_24, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0169;
		}
	}
	{
		// Debug.LogWarning(string.Format("{0} :: This tween's TO target should be a RectTransform, a Vector3 of (0,0,0) will be used instead", this.gameObject.name), this.gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_26;
		L_26 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_27;
		L_27 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_26, /*hidden argument*/NULL);
		String_t* L_28;
		L_28 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral71F308FB57202965E618BD0381AEBFDEEDF31F75, L_27, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_29;
		L_29 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogWarning_mE6AF3EFCF84F2296622CD42FBF9EEAF07244C0A8(L_28, L_29, /*hidden argument*/NULL);
		// endValueV3 = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30;
		L_30 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		__this->set_endValueV3_41(L_30);
		// } else {
		goto IL_01c5;
	}

IL_0169:
	{
		// RectTransform rTarget = target as RectTransform;
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_31 = __this->get_target_34();
		V_2 = ((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)IsInstSealed((RuntimeObject*)L_31, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var));
		// if (rTarget == null) {
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_32 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_33;
		L_33 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_32, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_01a0;
		}
	}
	{
		// Debug.LogWarning(string.Format("{0} :: This tween's target and TO target are not of the same type. Please reassign the values", this.gameObject.name), this.gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_34;
		L_34 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_34);
		String_t* L_35;
		L_35 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_34, /*hidden argument*/NULL);
		String_t* L_36;
		L_36 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteralF0AD9F8B1A2A2D534505FEA8D4D74C1BFD40753F, L_35, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_37;
		L_37 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogWarning_mE6AF3EFCF84F2296622CD42FBF9EEAF07244C0A8(L_36, L_37, /*hidden argument*/NULL);
		// } else {
		goto IL_01c5;
	}

IL_01a0:
	{
		// endValueV3 = DOTweenUtils46.SwitchToRectTransform(endValueT, rTarget);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_38 = V_1;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_39 = V_2;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_40;
		L_40 = DOTweenUtils46_SwitchToRectTransform_m0A393795E7C6FCC971FA44685DBD8C46176BCBB9(L_38, L_39, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_41;
		L_41 = Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline(L_40, /*hidden argument*/NULL);
		__this->set_endValueV3_41(L_41);
		// } else endValueV3 = endValueTransform.position;
		goto IL_01c5;
	}

IL_01b4:
	{
		// } else endValueV3 = endValueTransform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_42 = __this->get_endValueTransform_46();
		NullCheck(L_42);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_43;
		L_43 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_42, /*hidden argument*/NULL);
		__this->set_endValueV3_41(L_43);
	}

IL_01c5:
	{
		// switch (targetType) {
		int32_t L_44 = __this->get_targetType_36();
		V_3 = L_44;
		int32_t L_45 = V_3;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_45, (int32_t)5)))
		{
			case 0:
			{
				goto IL_01f5;
			}
			case 1:
			{
				goto IL_0957;
			}
			case 2:
			{
				goto IL_0957;
			}
			case 3:
			{
				goto IL_0281;
			}
			case 4:
			{
				goto IL_024f;
			}
			case 5:
			{
				goto IL_0957;
			}
			case 6:
			{
				goto IL_0222;
			}
		}
	}
	{
		goto IL_0957;
	}

IL_01f5:
	{
		// tween = ((RectTransform)target).DOAnchorPos3D(endValueV3, duration, optionalBool0);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_46 = __this->get_target_34();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_47 = __this->get_endValueV3_41();
		float L_48 = __this->get_duration_22();
		bool L_49 = __this->get_optionalBool0_47();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_50;
		L_50 = ShortcutExtensions46_DOAnchorPos3D_m104305FCB812004D56F3CDBA2327EC0AC8D2CBFF(((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)CastclassSealed((RuntimeObject*)L_46, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var)), L_47, L_48, L_49, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_50);
		// break;
		goto IL_0957;
	}

IL_0222:
	{
		// tween = ((Transform)target).DOMove(endValueV3, duration, optionalBool0);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_51 = __this->get_target_34();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_52 = __this->get_endValueV3_41();
		float L_53 = __this->get_duration_22();
		bool L_54 = __this->get_optionalBool0_47();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_55;
		L_55 = ShortcutExtensions_DOMove_mA83A2C1D4DFC55CB4D2B2763FD1CF8D4ADCD634D(((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_51, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var)), L_52, L_53, L_54, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_55);
		// break;
		goto IL_0957;
	}

IL_024f:
	{
		// tween = ((Rigidbody2D)target).DOMove(endValueV3, duration, optionalBool0);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_56 = __this->get_target_34();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_57 = __this->get_endValueV3_41();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_58;
		L_58 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_57, /*hidden argument*/NULL);
		float L_59 = __this->get_duration_22();
		bool L_60 = __this->get_optionalBool0_47();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_61;
		L_61 = ShortcutExtensions43_DOMove_m2841C4B8C1638272A9C9A23E2FFA00B104C35400(((Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 *)CastclassSealed((RuntimeObject*)L_56, Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_il2cpp_TypeInfo_var)), L_58, L_59, L_60, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_61);
		// break;
		goto IL_0957;
	}

IL_0281:
	{
		// tween = ((Rigidbody)target).DOMove(endValueV3, duration, optionalBool0);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_62 = __this->get_target_34();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_63 = __this->get_endValueV3_41();
		float L_64 = __this->get_duration_22();
		bool L_65 = __this->get_optionalBool0_47();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_66;
		L_66 = ShortcutExtensions_DOMove_mD9817CCAE0681D620067D21B4E868428FE2533D8(((Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A *)CastclassClass((RuntimeObject*)L_62, Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_il2cpp_TypeInfo_var)), L_63, L_64, L_65, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_66);
		// break;
		goto IL_0957;
	}

IL_02ae:
	{
		// tween = transform.DOLocalMove(endValueV3, duration, optionalBool0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_67;
		L_67 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_68 = __this->get_endValueV3_41();
		float L_69 = __this->get_duration_22();
		bool L_70 = __this->get_optionalBool0_47();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_71;
		L_71 = ShortcutExtensions_DOLocalMove_m0F6957D951BA19FEA0A48C7CDA52240AC7CD9369(L_67, L_68, L_69, L_70, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_71);
		// break;
		goto IL_0957;
	}

IL_02d6:
	{
		// switch (targetType) {
		int32_t L_72 = __this->get_targetType_36();
		V_3 = L_72;
		int32_t L_73 = V_3;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_73, (int32_t)8)))
		{
			case 0:
			{
				goto IL_034e;
			}
			case 1:
			{
				goto IL_0327;
			}
			case 2:
			{
				goto IL_0957;
			}
			case 3:
			{
				goto IL_02fa;
			}
		}
	}
	{
		goto IL_0957;
	}

IL_02fa:
	{
		// tween = ((Transform)target).DORotate(endValueV3, duration, optionalRotationMode);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_74 = __this->get_target_34();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_75 = __this->get_endValueV3_41();
		float L_76 = __this->get_duration_22();
		int32_t L_77 = __this->get_optionalRotationMode_50();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_78;
		L_78 = ShortcutExtensions_DORotate_mBC72ACEECA2351B7CE6491E5FE0B76469609DEFD(((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_74, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var)), L_75, L_76, L_77, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_78);
		// break;
		goto IL_0957;
	}

IL_0327:
	{
		// tween = ((Rigidbody2D)target).DORotate(endValueFloat, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_79 = __this->get_target_34();
		float L_80 = __this->get_endValueFloat_40();
		float L_81 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_82;
		L_82 = ShortcutExtensions43_DORotate_m868974591A2268B71A3EC37CC6D31CE77979B10C(((Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 *)CastclassSealed((RuntimeObject*)L_79, Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_il2cpp_TypeInfo_var)), L_80, L_81, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_82);
		// break;
		goto IL_0957;
	}

IL_034e:
	{
		// tween = ((Rigidbody)target).DORotate(endValueV3, duration, optionalRotationMode);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_83 = __this->get_target_34();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_84 = __this->get_endValueV3_41();
		float L_85 = __this->get_duration_22();
		int32_t L_86 = __this->get_optionalRotationMode_50();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_87;
		L_87 = ShortcutExtensions_DORotate_m578E0422C09350DCB3298AA920B6F9684BF09CAD(((Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A *)CastclassClass((RuntimeObject*)L_83, Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_il2cpp_TypeInfo_var)), L_84, L_85, L_86, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_87);
		// break;
		goto IL_0957;
	}

IL_037b:
	{
		// tween = transform.DOLocalRotate(endValueV3, duration, optionalRotationMode);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_88;
		L_88 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_89 = __this->get_endValueV3_41();
		float L_90 = __this->get_duration_22();
		int32_t L_91 = __this->get_optionalRotationMode_50();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_92;
		L_92 = ShortcutExtensions_DOLocalRotate_m0A3A0561A8DFA471BCA68891686EE51BD6CD2F28(L_88, L_89, L_90, L_91, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_92);
		// break;
		goto IL_0957;
	}

IL_03a3:
	{
		// switch (targetType) {
		int32_t L_93 = __this->get_targetType_36();
		// tween = transform.DOScale(optionalBool0 ? new Vector3(endValueFloat, endValueFloat, endValueFloat) : endValueV3, duration);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_94;
		L_94 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		bool L_95 = __this->get_optionalBool0_47();
		G_B32_0 = L_94;
		G_B32_1 = __this;
		if (L_95)
		{
			G_B33_0 = L_94;
			G_B33_1 = __this;
			goto IL_03c1;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_96 = __this->get_endValueV3_41();
		G_B34_0 = L_96;
		G_B34_1 = G_B32_0;
		G_B34_2 = G_B32_1;
		goto IL_03d8;
	}

IL_03c1:
	{
		float L_97 = __this->get_endValueFloat_40();
		float L_98 = __this->get_endValueFloat_40();
		float L_99 = __this->get_endValueFloat_40();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_100;
		memset((&L_100), 0, sizeof(L_100));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_100), L_97, L_98, L_99, /*hidden argument*/NULL);
		G_B34_0 = L_100;
		G_B34_1 = G_B33_0;
		G_B34_2 = G_B33_1;
	}

IL_03d8:
	{
		float L_101 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_102;
		L_102 = ShortcutExtensions_DOScale_m68874CDE5A501CD5EBFD9021D689A8E3029A7797(G_B34_1, G_B34_0, L_101, /*hidden argument*/NULL);
		NullCheck(G_B34_2);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)G_B34_2)->set_tween_20(L_102);
		// break;
		goto IL_0957;
	}

IL_03ed:
	{
		// tween = ((RectTransform)target).DOSizeDelta(optionalBool0 ? new Vector2(endValueFloat, endValueFloat) : endValueV2, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_103 = __this->get_target_34();
		bool L_104 = __this->get_optionalBool0_47();
		G_B36_0 = ((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)CastclassSealed((RuntimeObject*)L_103, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var));
		G_B36_1 = __this;
		if (L_104)
		{
			G_B37_0 = ((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)CastclassSealed((RuntimeObject*)L_103, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var));
			G_B37_1 = __this;
			goto IL_0409;
		}
	}
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_105 = __this->get_endValueV2_42();
		G_B38_0 = L_105;
		G_B38_1 = G_B36_0;
		G_B38_2 = G_B36_1;
		goto IL_041a;
	}

IL_0409:
	{
		float L_106 = __this->get_endValueFloat_40();
		float L_107 = __this->get_endValueFloat_40();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_108;
		memset((&L_108), 0, sizeof(L_108));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_108), L_106, L_107, /*hidden argument*/NULL);
		G_B38_0 = L_108;
		G_B38_1 = G_B37_0;
		G_B38_2 = G_B37_1;
	}

IL_041a:
	{
		float L_109 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_110;
		L_110 = ShortcutExtensions46_DOSizeDelta_m0F28C462C6F10CA44C6DDEFBE30838FDB490AB7A(G_B38_1, G_B38_0, L_109, (bool)0, /*hidden argument*/NULL);
		NullCheck(G_B38_2);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)G_B38_2)->set_tween_20(L_110);
		// break;
		goto IL_0957;
	}

IL_0430:
	{
		// isRelative = false;
		__this->set_isRelative_28((bool)0);
		// switch (targetType) {
		int32_t L_111 = __this->get_targetType_36();
		V_3 = L_111;
		int32_t L_112 = V_3;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_112, (int32_t)3)))
		{
			case 0:
			{
				goto IL_04be;
			}
			case 1:
			{
				goto IL_050c;
			}
			case 2:
			{
				goto IL_0957;
			}
			case 3:
			{
				goto IL_0492;
			}
			case 4:
			{
				goto IL_046b;
			}
			case 5:
			{
				goto IL_0957;
			}
			case 6:
			{
				goto IL_0957;
			}
			case 7:
			{
				goto IL_04e5;
			}
		}
	}
	{
		goto IL_0957;
	}

IL_046b:
	{
		// tween = ((SpriteRenderer)target).DOColor(endValueColor, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_113 = __this->get_target_34();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_114 = __this->get_endValueColor_43();
		float L_115 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_116;
		L_116 = ShortcutExtensions43_DOColor_mA2543B2AD028B46EA2C799F18C669A00783970E8(((SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF *)CastclassSealed((RuntimeObject*)L_113, SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_il2cpp_TypeInfo_var)), L_114, L_115, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_116);
		// break;
		goto IL_0957;
	}

IL_0492:
	{
		// tween = ((Renderer)target).material.DOColor(endValueColor, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_117 = __this->get_target_34();
		NullCheck(((Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C *)CastclassClass((RuntimeObject*)L_117, Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_il2cpp_TypeInfo_var)));
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_118;
		L_118 = Renderer_get_material_mE6B01125502D08EE0D6DFE2EAEC064AD9BB31804(((Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C *)CastclassClass((RuntimeObject*)L_117, Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_119 = __this->get_endValueColor_43();
		float L_120 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_121;
		L_121 = ShortcutExtensions_DOColor_mE6140CF9E1B3D3B559F6751F9B8C3B1602826710(L_118, L_119, L_120, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_121);
		// break;
		goto IL_0957;
	}

IL_04be:
	{
		// tween = ((Image)target).DOColor(endValueColor, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_122 = __this->get_target_34();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_123 = __this->get_endValueColor_43();
		float L_124 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_125;
		L_125 = ShortcutExtensions46_DOColor_mEFDE35BB24046DCFBAB8DAB12E6B7996C338A719(((Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C *)CastclassClass((RuntimeObject*)L_122, Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_il2cpp_TypeInfo_var)), L_123, L_124, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_125);
		// break;
		goto IL_0957;
	}

IL_04e5:
	{
		// tween = ((Text)target).DOColor(endValueColor, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_126 = __this->get_target_34();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_127 = __this->get_endValueColor_43();
		float L_128 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_129;
		L_129 = ShortcutExtensions46_DOColor_m80A0A452604641CA11960C491CBC3EBE30450BB6(((Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 *)CastclassClass((RuntimeObject*)L_126, Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_il2cpp_TypeInfo_var)), L_127, L_128, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_129);
		// break;
		goto IL_0957;
	}

IL_050c:
	{
		// tween = ((Light)target).DOColor(endValueColor, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_130 = __this->get_target_34();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_131 = __this->get_endValueColor_43();
		float L_132 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_133;
		L_133 = ShortcutExtensions_DOColor_mE6D48DDA2B16225D9DBF40D67D8716992016BE5E(((Light_tA2F349FE839781469A0344CF6039B51512394275 *)CastclassSealed((RuntimeObject*)L_130, Light_tA2F349FE839781469A0344CF6039B51512394275_il2cpp_TypeInfo_var)), L_131, L_132, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_133);
		// break;
		goto IL_0957;
	}

IL_0533:
	{
		// isRelative = false;
		__this->set_isRelative_28((bool)0);
		// switch (targetType) {
		int32_t L_134 = __this->get_targetType_36();
		V_3 = L_134;
		int32_t L_135 = V_3;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_135, (int32_t)2)))
		{
			case 0:
			{
				goto IL_063a;
			}
			case 1:
			{
				goto IL_05c5;
			}
			case 2:
			{
				goto IL_0613;
			}
			case 3:
			{
				goto IL_0957;
			}
			case 4:
			{
				goto IL_0599;
			}
			case 5:
			{
				goto IL_0572;
			}
			case 6:
			{
				goto IL_0957;
			}
			case 7:
			{
				goto IL_0957;
			}
			case 8:
			{
				goto IL_05ec;
			}
		}
	}
	{
		goto IL_0957;
	}

IL_0572:
	{
		// tween = ((SpriteRenderer)target).DOFade(endValueFloat, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_136 = __this->get_target_34();
		float L_137 = __this->get_endValueFloat_40();
		float L_138 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_139;
		L_139 = ShortcutExtensions43_DOFade_m5F3CD26D926219D7FCBAF07C0EAB1D50F76E5006(((SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF *)CastclassSealed((RuntimeObject*)L_136, SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_il2cpp_TypeInfo_var)), L_137, L_138, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_139);
		// break;
		goto IL_0957;
	}

IL_0599:
	{
		// tween = ((Renderer)target).material.DOFade(endValueFloat, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_140 = __this->get_target_34();
		NullCheck(((Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C *)CastclassClass((RuntimeObject*)L_140, Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_il2cpp_TypeInfo_var)));
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_141;
		L_141 = Renderer_get_material_mE6B01125502D08EE0D6DFE2EAEC064AD9BB31804(((Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C *)CastclassClass((RuntimeObject*)L_140, Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		float L_142 = __this->get_endValueFloat_40();
		float L_143 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_144;
		L_144 = ShortcutExtensions_DOFade_mB5B3CAC06223B35C8E666ADC139D93FF17A31C48(L_141, L_142, L_143, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_144);
		// break;
		goto IL_0957;
	}

IL_05c5:
	{
		// tween = ((Image)target).DOFade(endValueFloat, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_145 = __this->get_target_34();
		float L_146 = __this->get_endValueFloat_40();
		float L_147 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_148;
		L_148 = ShortcutExtensions46_DOFade_mF268968FB793053C5F5230343B2771C9C608C007(((Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C *)CastclassClass((RuntimeObject*)L_145, Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_il2cpp_TypeInfo_var)), L_146, L_147, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_148);
		// break;
		goto IL_0957;
	}

IL_05ec:
	{
		// tween = ((Text)target).DOFade(endValueFloat, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_149 = __this->get_target_34();
		float L_150 = __this->get_endValueFloat_40();
		float L_151 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_152;
		L_152 = ShortcutExtensions46_DOFade_m560566897759A70C4136B961F83EB89E441E0CBC(((Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 *)CastclassClass((RuntimeObject*)L_149, Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_il2cpp_TypeInfo_var)), L_150, L_151, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_152);
		// break;
		goto IL_0957;
	}

IL_0613:
	{
		// tween = ((Light)target).DOIntensity(endValueFloat, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_153 = __this->get_target_34();
		float L_154 = __this->get_endValueFloat_40();
		float L_155 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_156;
		L_156 = ShortcutExtensions_DOIntensity_m4F6889CB6C25B1B30099075C7AC9F6E3B24FC080(((Light_tA2F349FE839781469A0344CF6039B51512394275 *)CastclassSealed((RuntimeObject*)L_153, Light_tA2F349FE839781469A0344CF6039B51512394275_il2cpp_TypeInfo_var)), L_154, L_155, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_156);
		// break;
		goto IL_0957;
	}

IL_063a:
	{
		// tween = ((CanvasGroup)target).DOFade(endValueFloat, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_157 = __this->get_target_34();
		float L_158 = __this->get_endValueFloat_40();
		float L_159 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_160;
		L_160 = ShortcutExtensions46_DOFade_m72575C2B2015133EFCC699FE21A2419951AA432A(((CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F *)CastclassSealed((RuntimeObject*)L_157, CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_il2cpp_TypeInfo_var)), L_158, L_159, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_160);
		// break;
		goto IL_0957;
	}

IL_0661:
	{
		// switch (targetType) {
		int32_t L_161 = __this->get_targetType_36();
		if ((!(((uint32_t)L_161) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0957;
		}
	}
	{
		// tween = ((Text)target).DOText(endValueString, duration, optionalBool0, optionalScrambleMode, optionalString);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_162 = __this->get_target_34();
		String_t* L_163 = __this->get_endValueString_44();
		float L_164 = __this->get_duration_22();
		bool L_165 = __this->get_optionalBool0_47();
		int32_t L_166 = __this->get_optionalScrambleMode_51();
		String_t* L_167 = __this->get_optionalString_52();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_168;
		L_168 = ShortcutExtensions46_DOText_mE13EDACBB2B161A6E7BB68A4BFAC148DA825EEA0(((Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 *)CastclassClass((RuntimeObject*)L_162, Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_il2cpp_TypeInfo_var)), L_163, L_164, L_165, L_166, L_167, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_168);
		// break;
		goto IL_0957;
	}

IL_06a7:
	{
		// switch (targetType) {
		int32_t L_169 = __this->get_targetType_36();
		V_3 = L_169;
		int32_t L_170 = V_3;
		if ((((int32_t)L_170) == ((int32_t)5)))
		{
			goto IL_06bc;
		}
	}
	{
		int32_t L_171 = V_3;
		if ((((int32_t)L_171) == ((int32_t)((int32_t)11))))
		{
			goto IL_06fa;
		}
	}
	{
		goto IL_0957;
	}

IL_06bc:
	{
		// tween = ((RectTransform)target).DOPunchAnchorPos(endValueV3, duration, optionalInt0, optionalFloat0, optionalBool0);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_172 = __this->get_target_34();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_173 = __this->get_endValueV3_41();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_174;
		L_174 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_173, /*hidden argument*/NULL);
		float L_175 = __this->get_duration_22();
		int32_t L_176 = __this->get_optionalInt0_49();
		float L_177 = __this->get_optionalFloat0_48();
		bool L_178 = __this->get_optionalBool0_47();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_179;
		L_179 = ShortcutExtensions46_DOPunchAnchorPos_m0E4FBB18152C7EF788723DC36A08081CADF3C3CC(((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)CastclassSealed((RuntimeObject*)L_172, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var)), L_174, L_175, L_176, L_177, L_178, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_179);
		// break;
		goto IL_0957;
	}

IL_06fa:
	{
		// tween = ((Transform)target).DOPunchPosition(endValueV3, duration, optionalInt0, optionalFloat0, optionalBool0);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_180 = __this->get_target_34();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_181 = __this->get_endValueV3_41();
		float L_182 = __this->get_duration_22();
		int32_t L_183 = __this->get_optionalInt0_49();
		float L_184 = __this->get_optionalFloat0_48();
		bool L_185 = __this->get_optionalBool0_47();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_186;
		L_186 = ShortcutExtensions_DOPunchPosition_m9EEF18AFCE150A50A4F79AA85982B9CE70DAD0DA(((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_180, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var)), L_181, L_182, L_183, L_184, L_185, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_186);
		// break;
		goto IL_0957;
	}

IL_0733:
	{
		// tween = transform.DOPunchScale(endValueV3, duration, optionalInt0, optionalFloat0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_187;
		L_187 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_188 = __this->get_endValueV3_41();
		float L_189 = __this->get_duration_22();
		int32_t L_190 = __this->get_optionalInt0_49();
		float L_191 = __this->get_optionalFloat0_48();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_192;
		L_192 = ShortcutExtensions_DOPunchScale_m351E39CC16A4CC9692955C502F350D56825B17E4(L_187, L_188, L_189, L_190, L_191, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_192);
		// break;
		goto IL_0957;
	}

IL_0761:
	{
		// tween = transform.DOPunchRotation(endValueV3, duration, optionalInt0, optionalFloat0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_193;
		L_193 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_194 = __this->get_endValueV3_41();
		float L_195 = __this->get_duration_22();
		int32_t L_196 = __this->get_optionalInt0_49();
		float L_197 = __this->get_optionalFloat0_48();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_198;
		L_198 = ShortcutExtensions_DOPunchRotation_mC1D5DA0D6218A579D04BB373DDF1264E0AC4736A(L_193, L_194, L_195, L_196, L_197, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_198);
		// break;
		goto IL_0957;
	}

IL_078f:
	{
		// switch (targetType) {
		int32_t L_199 = __this->get_targetType_36();
		V_3 = L_199;
		int32_t L_200 = V_3;
		if ((((int32_t)L_200) == ((int32_t)5)))
		{
			goto IL_07a4;
		}
	}
	{
		int32_t L_201 = V_3;
		if ((((int32_t)L_201) == ((int32_t)((int32_t)11))))
		{
			goto IL_07e3;
		}
	}
	{
		goto IL_0957;
	}

IL_07a4:
	{
		// tween = ((RectTransform)target).DOShakeAnchorPos(duration, endValueV3, optionalInt0, optionalFloat0, optionalBool0);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_202 = __this->get_target_34();
		float L_203 = __this->get_duration_22();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_204 = __this->get_endValueV3_41();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_205;
		L_205 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_204, /*hidden argument*/NULL);
		int32_t L_206 = __this->get_optionalInt0_49();
		float L_207 = __this->get_optionalFloat0_48();
		bool L_208 = __this->get_optionalBool0_47();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_209;
		L_209 = ShortcutExtensions46_DOShakeAnchorPos_mA8018E67BCA3312E54446B02D184BF7BA4656EEC(((RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 *)CastclassSealed((RuntimeObject*)L_202, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_il2cpp_TypeInfo_var)), L_203, L_205, L_206, L_207, L_208, (bool)1, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_209);
		// break;
		goto IL_0957;
	}

IL_07e3:
	{
		// tween = ((Transform)target).DOShakePosition(duration, endValueV3, optionalInt0, optionalFloat0, optionalBool0);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_210 = __this->get_target_34();
		float L_211 = __this->get_duration_22();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_212 = __this->get_endValueV3_41();
		int32_t L_213 = __this->get_optionalInt0_49();
		float L_214 = __this->get_optionalFloat0_48();
		bool L_215 = __this->get_optionalBool0_47();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_216;
		L_216 = ShortcutExtensions_DOShakePosition_mDEC0E682E9A8D470D6FD1F825161C3E6087DC3B8(((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_210, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var)), L_211, L_212, L_213, L_214, L_215, (bool)1, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_216);
		// break;
		goto IL_0957;
	}

IL_081d:
	{
		// tween = transform.DOShakeScale(duration, endValueV3, optionalInt0, optionalFloat0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_217;
		L_217 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_218 = __this->get_duration_22();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_219 = __this->get_endValueV3_41();
		int32_t L_220 = __this->get_optionalInt0_49();
		float L_221 = __this->get_optionalFloat0_48();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_222;
		L_222 = ShortcutExtensions_DOShakeScale_m39430B83048A4F601B8B206224E792F7F075D22F(L_217, L_218, L_219, L_220, L_221, (bool)1, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_222);
		// break;
		goto IL_0957;
	}

IL_084c:
	{
		// tween = transform.DOShakeRotation(duration, endValueV3, optionalInt0, optionalFloat0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_223;
		L_223 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_224 = __this->get_duration_22();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_225 = __this->get_endValueV3_41();
		int32_t L_226 = __this->get_optionalInt0_49();
		float L_227 = __this->get_optionalFloat0_48();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_228;
		L_228 = ShortcutExtensions_DOShakeRotation_m35DC1BCA976A9A2E1C7A6CA4359EB5F30695F1EE(L_223, L_224, L_225, L_226, L_227, (bool)1, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_228);
		// break;
		goto IL_0957;
	}

IL_087b:
	{
		// tween = ((Camera)target).DOAspect(endValueFloat, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_229 = __this->get_target_34();
		float L_230 = __this->get_endValueFloat_40();
		float L_231 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_232;
		L_232 = ShortcutExtensions_DOAspect_mCD85FE3D0E67452AC9682825549AD6C973417F70(((Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C *)CastclassSealed((RuntimeObject*)L_229, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_il2cpp_TypeInfo_var)), L_230, L_231, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_232);
		// break;
		goto IL_0957;
	}

IL_08a2:
	{
		// tween = ((Camera)target).DOColor(endValueColor, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_233 = __this->get_target_34();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_234 = __this->get_endValueColor_43();
		float L_235 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_236;
		L_236 = ShortcutExtensions_DOColor_m406B84ECF991C880D866C555B3AE4DF41274D203(((Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C *)CastclassSealed((RuntimeObject*)L_233, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_il2cpp_TypeInfo_var)), L_234, L_235, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_236);
		// break;
		goto IL_0957;
	}

IL_08c9:
	{
		// tween = ((Camera)target).DOFieldOfView(endValueFloat, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_237 = __this->get_target_34();
		float L_238 = __this->get_endValueFloat_40();
		float L_239 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_240;
		L_240 = ShortcutExtensions_DOFieldOfView_m2FDB010FDA9B4164709FE0711B349C64FD73B498(((Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C *)CastclassSealed((RuntimeObject*)L_237, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_il2cpp_TypeInfo_var)), L_238, L_239, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_240);
		// break;
		goto IL_0957;
	}

IL_08ed:
	{
		// tween = ((Camera)target).DOOrthoSize(endValueFloat, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_241 = __this->get_target_34();
		float L_242 = __this->get_endValueFloat_40();
		float L_243 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_244;
		L_244 = ShortcutExtensions_DOOrthoSize_m6B046F2A9CB65F5C33F7E05E404C81BE5FCC1468(((Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C *)CastclassSealed((RuntimeObject*)L_241, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_il2cpp_TypeInfo_var)), L_242, L_243, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_244);
		// break;
		goto IL_0957;
	}

IL_0911:
	{
		// tween = ((Camera)target).DOPixelRect(endValueRect, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_245 = __this->get_target_34();
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_246 = __this->get_endValueRect_45();
		float L_247 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_248;
		L_248 = ShortcutExtensions_DOPixelRect_m18CCEED227266203D749048A9959857D56EBDEEB(((Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C *)CastclassSealed((RuntimeObject*)L_245, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_il2cpp_TypeInfo_var)), L_246, L_247, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_248);
		// break;
		goto IL_0957;
	}

IL_0935:
	{
		// tween = ((Camera)target).DORect(endValueRect, duration);
		Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * L_249 = __this->get_target_34();
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_250 = __this->get_endValueRect_45();
		float L_251 = __this->get_duration_22();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_252;
		L_252 = ShortcutExtensions_DORect_m9D52CABC1C9DACDFE1EF0FB52D22BE293536B39E(((Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C *)CastclassSealed((RuntimeObject*)L_249, Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_il2cpp_TypeInfo_var)), L_250, L_251, /*hidden argument*/NULL);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20(L_252);
	}

IL_0957:
	{
		// if (tween == null) return;
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_253 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		if (L_253)
		{
			goto IL_0960;
		}
	}
	{
		// if (tween == null) return;
		return;
	}

IL_0960:
	{
		// if (isFrom) {
		bool L_254 = __this->get_isFrom_29();
		if (!L_254)
		{
			goto IL_0981;
		}
	}
	{
		// ((Tweener)tween).From(isRelative);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_255 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		bool L_256 = __this->get_isRelative_28();
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_257;
		L_257 = TweenSettingsExtensions_From_TisTweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_mC83D3CD43EA1E66E8E8B455AA81A4C9B7A75FB4A(((Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 *)CastclassClass((RuntimeObject*)L_255, Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_il2cpp_TypeInfo_var)), L_256, /*hidden argument*/TweenSettingsExtensions_From_TisTweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_mC83D3CD43EA1E66E8E8B455AA81A4C9B7A75FB4A_RuntimeMethod_var);
		// } else {
		goto IL_0993;
	}

IL_0981:
	{
		// tween.SetRelative(isRelative);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_258 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		bool L_259 = __this->get_isRelative_28();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_260;
		L_260 = TweenSettingsExtensions_SetRelative_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m0DD3CA6BC5B6DFA96EB49AACEB29666342410FDE(L_258, L_259, /*hidden argument*/TweenSettingsExtensions_SetRelative_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m0DD3CA6BC5B6DFA96EB49AACEB29666342410FDE_RuntimeMethod_var);
	}

IL_0993:
	{
		// tween.SetTarget(this.gameObject).SetDelay(delay).SetLoops(loops, loopType).SetAutoKill(autoKill)
		//     .OnKill(()=> tween = null);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_261 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_262;
		L_262 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_263;
		L_263 = TweenSettingsExtensions_SetTarget_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m0F995FA73A7C3CF6A49200DE6237CB4F250FD5D9(L_261, L_262, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m0F995FA73A7C3CF6A49200DE6237CB4F250FD5D9_RuntimeMethod_var);
		float L_264 = __this->get_delay_21();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_265;
		L_265 = TweenSettingsExtensions_SetDelay_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mD9B865FBEB261A2983D1F5BAB18C7E3F83619480(L_263, L_264, /*hidden argument*/TweenSettingsExtensions_SetDelay_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mD9B865FBEB261A2983D1F5BAB18C7E3F83619480_RuntimeMethod_var);
		int32_t L_266 = __this->get_loops_26();
		int32_t L_267 = __this->get_loopType_25();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_268;
		L_268 = TweenSettingsExtensions_SetLoops_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4156B77EABC3AA03F624808CDF6472BD0D964EA0(L_265, L_266, L_267, /*hidden argument*/TweenSettingsExtensions_SetLoops_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4156B77EABC3AA03F624808CDF6472BD0D964EA0_RuntimeMethod_var);
		bool L_269 = __this->get_autoKill_31();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_270;
		L_270 = TweenSettingsExtensions_SetAutoKill_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m3B106D91BC3EACE01F526CABBD59D24767E577D9(L_268, L_269, /*hidden argument*/TweenSettingsExtensions_SetAutoKill_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m3B106D91BC3EACE01F526CABBD59D24767E577D9_RuntimeMethod_var);
		TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * L_271 = (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *)il2cpp_codegen_object_new(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662(L_271, __this, (intptr_t)((intptr_t)DOTweenAnimation_U3CCreateTweenU3Eb__37_0_m7133BCD50DF5E0EDB11799DF7618551D417B05AA_RuntimeMethod_var), /*hidden argument*/NULL);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_272;
		L_272 = TweenSettingsExtensions_OnKill_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m8416D52033C9F6508DA669F66198F6477B74EFE7(L_270, L_271, /*hidden argument*/TweenSettingsExtensions_OnKill_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m8416D52033C9F6508DA669F66198F6477B74EFE7_RuntimeMethod_var);
		// if (isSpeedBased) tween.SetSpeedBased();
		bool L_273 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_isSpeedBased_5();
		if (!L_273)
		{
			goto IL_09f1;
		}
	}
	{
		// if (isSpeedBased) tween.SetSpeedBased();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_274 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_275;
		L_275 = TweenSettingsExtensions_SetSpeedBased_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m641FDC9A608F06A264C303790E65FA671F3E316A(L_274, /*hidden argument*/TweenSettingsExtensions_SetSpeedBased_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m641FDC9A608F06A264C303790E65FA671F3E316A_RuntimeMethod_var);
	}

IL_09f1:
	{
		// if (easeType == Ease.INTERNAL_Custom) tween.SetEase(easeCurve);
		int32_t L_276 = __this->get_easeType_23();
		if ((!(((uint32_t)L_276) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_0a0f;
		}
	}
	{
		// if (easeType == Ease.INTERNAL_Custom) tween.SetEase(easeCurve);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_277 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_278 = __this->get_easeCurve_24();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_279;
		L_279 = TweenSettingsExtensions_SetEase_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m423A73833B53B7DF4F822AB1200A8E1150801411(L_277, L_278, /*hidden argument*/TweenSettingsExtensions_SetEase_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m423A73833B53B7DF4F822AB1200A8E1150801411_RuntimeMethod_var);
		goto IL_0a21;
	}

IL_0a0f:
	{
		// else tween.SetEase(easeType);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_280 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		int32_t L_281 = __this->get_easeType_23();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_282;
		L_282 = TweenSettingsExtensions_SetEase_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m30E10D6BA20CCA9FE3099B5BC33D277F48C09880(L_280, L_281, /*hidden argument*/TweenSettingsExtensions_SetEase_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m30E10D6BA20CCA9FE3099B5BC33D277F48C09880_RuntimeMethod_var);
	}

IL_0a21:
	{
		// if (!string.IsNullOrEmpty(id)) tween.SetId(id);
		String_t* L_283 = __this->get_id_27();
		bool L_284;
		L_284 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_283, /*hidden argument*/NULL);
		if (L_284)
		{
			goto IL_0a40;
		}
	}
	{
		// if (!string.IsNullOrEmpty(id)) tween.SetId(id);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_285 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		String_t* L_286 = __this->get_id_27();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_287;
		L_287 = TweenSettingsExtensions_SetId_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4FA57F2F89A8CCEC59A8330199E74221062E929E(L_285, L_286, /*hidden argument*/TweenSettingsExtensions_SetId_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4FA57F2F89A8CCEC59A8330199E74221062E929E_RuntimeMethod_var);
	}

IL_0a40:
	{
		// tween.SetUpdate(isIndependentUpdate);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_288 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		bool L_289 = __this->get_isIndependentUpdate_30();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_290;
		L_290 = TweenSettingsExtensions_SetUpdate_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m3C4F344B80D25DDA04A2C205DD25302E708140DC(L_288, L_289, /*hidden argument*/TweenSettingsExtensions_SetUpdate_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m3C4F344B80D25DDA04A2C205DD25302E708140DC_RuntimeMethod_var);
		// if (hasOnStart) {
		bool L_291 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_hasOnStart_6();
		if (!L_291)
		{
			goto IL_0a81;
		}
	}
	{
		// if (onStart != null) tween.OnStart(onStart.Invoke);
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_292 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onStart_13();
		if (!L_292)
		{
			goto IL_0a88;
		}
	}
	{
		// if (onStart != null) tween.OnStart(onStart.Invoke);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_293 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_294 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onStart_13();
		TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * L_295 = (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *)il2cpp_codegen_object_new(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662(L_295, L_294, (intptr_t)((intptr_t)UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5_RuntimeMethod_var), /*hidden argument*/NULL);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_296;
		L_296 = TweenSettingsExtensions_OnStart_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mB17597C3125F3F94329BF5C14DE7BCCF05E99EC3(L_293, L_295, /*hidden argument*/TweenSettingsExtensions_OnStart_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mB17597C3125F3F94329BF5C14DE7BCCF05E99EC3_RuntimeMethod_var);
		// } else onStart = null;
		goto IL_0a88;
	}

IL_0a81:
	{
		// } else onStart = null;
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_onStart_13((UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 *)NULL);
	}

IL_0a88:
	{
		// if (hasOnPlay) {
		bool L_297 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_hasOnPlay_7();
		if (!L_297)
		{
			goto IL_0ab7;
		}
	}
	{
		// if (onPlay != null) tween.OnPlay(onPlay.Invoke);
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_298 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onPlay_14();
		if (!L_298)
		{
			goto IL_0abe;
		}
	}
	{
		// if (onPlay != null) tween.OnPlay(onPlay.Invoke);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_299 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_300 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onPlay_14();
		TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * L_301 = (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *)il2cpp_codegen_object_new(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662(L_301, L_300, (intptr_t)((intptr_t)UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5_RuntimeMethod_var), /*hidden argument*/NULL);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_302;
		L_302 = TweenSettingsExtensions_OnPlay_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mC17C14807CF262BA39CBF9C55949A053EF127A4C(L_299, L_301, /*hidden argument*/TweenSettingsExtensions_OnPlay_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mC17C14807CF262BA39CBF9C55949A053EF127A4C_RuntimeMethod_var);
		// } else onPlay = null;
		goto IL_0abe;
	}

IL_0ab7:
	{
		// } else onPlay = null;
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_onPlay_14((UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 *)NULL);
	}

IL_0abe:
	{
		// if (hasOnUpdate) {
		bool L_303 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_hasOnUpdate_8();
		if (!L_303)
		{
			goto IL_0aed;
		}
	}
	{
		// if (onUpdate != null) tween.OnUpdate(onUpdate.Invoke);
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_304 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onUpdate_15();
		if (!L_304)
		{
			goto IL_0af4;
		}
	}
	{
		// if (onUpdate != null) tween.OnUpdate(onUpdate.Invoke);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_305 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_306 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onUpdate_15();
		TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * L_307 = (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *)il2cpp_codegen_object_new(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662(L_307, L_306, (intptr_t)((intptr_t)UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5_RuntimeMethod_var), /*hidden argument*/NULL);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_308;
		L_308 = TweenSettingsExtensions_OnUpdate_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m5F0CCCF9EBBC043CEA09AC3F9AD2CC9A3243EF45(L_305, L_307, /*hidden argument*/TweenSettingsExtensions_OnUpdate_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m5F0CCCF9EBBC043CEA09AC3F9AD2CC9A3243EF45_RuntimeMethod_var);
		// } else onUpdate = null;
		goto IL_0af4;
	}

IL_0aed:
	{
		// } else onUpdate = null;
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_onUpdate_15((UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 *)NULL);
	}

IL_0af4:
	{
		// if (hasOnStepComplete) {
		bool L_309 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_hasOnStepComplete_9();
		if (!L_309)
		{
			goto IL_0b23;
		}
	}
	{
		// if (onStepComplete != null) tween.OnStepComplete(onStepComplete.Invoke);
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_310 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onStepComplete_16();
		if (!L_310)
		{
			goto IL_0b2a;
		}
	}
	{
		// if (onStepComplete != null) tween.OnStepComplete(onStepComplete.Invoke);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_311 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_312 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onStepComplete_16();
		TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * L_313 = (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *)il2cpp_codegen_object_new(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662(L_313, L_312, (intptr_t)((intptr_t)UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5_RuntimeMethod_var), /*hidden argument*/NULL);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_314;
		L_314 = TweenSettingsExtensions_OnStepComplete_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m5527E140B5D01685DD8041B72C62461EA2A8A1E4(L_311, L_313, /*hidden argument*/TweenSettingsExtensions_OnStepComplete_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m5527E140B5D01685DD8041B72C62461EA2A8A1E4_RuntimeMethod_var);
		// } else onStepComplete = null;
		goto IL_0b2a;
	}

IL_0b23:
	{
		// } else onStepComplete = null;
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_onStepComplete_16((UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 *)NULL);
	}

IL_0b2a:
	{
		// if (hasOnComplete) {
		bool L_315 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_hasOnComplete_10();
		if (!L_315)
		{
			goto IL_0b59;
		}
	}
	{
		// if (onComplete != null) tween.OnComplete(onComplete.Invoke);
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_316 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onComplete_17();
		if (!L_316)
		{
			goto IL_0b60;
		}
	}
	{
		// if (onComplete != null) tween.OnComplete(onComplete.Invoke);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_317 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_318 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onComplete_17();
		TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * L_319 = (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *)il2cpp_codegen_object_new(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662(L_319, L_318, (intptr_t)((intptr_t)UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5_RuntimeMethod_var), /*hidden argument*/NULL);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_320;
		L_320 = TweenSettingsExtensions_OnComplete_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mE51D62942FE6E2C354ADDB87CE4CDA2206DADCC7(L_317, L_319, /*hidden argument*/TweenSettingsExtensions_OnComplete_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mE51D62942FE6E2C354ADDB87CE4CDA2206DADCC7_RuntimeMethod_var);
		// } else onComplete = null;
		goto IL_0b60;
	}

IL_0b59:
	{
		// } else onComplete = null;
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_onComplete_17((UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 *)NULL);
	}

IL_0b60:
	{
		// if (hasOnRewind) {
		bool L_321 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_hasOnRewind_12();
		if (!L_321)
		{
			goto IL_0b8f;
		}
	}
	{
		// if (onRewind != null) tween.OnRewind(onRewind.Invoke);
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_322 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onRewind_19();
		if (!L_322)
		{
			goto IL_0b96;
		}
	}
	{
		// if (onRewind != null) tween.OnRewind(onRewind.Invoke);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_323 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_324 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onRewind_19();
		TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB * L_325 = (TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB *)il2cpp_codegen_object_new(TweenCallback_tCAA7F86252EC47FCDD15C81B4AEE6EEA72DC15CB_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662(L_325, L_324, (intptr_t)((intptr_t)UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5_RuntimeMethod_var), /*hidden argument*/NULL);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_326;
		L_326 = TweenSettingsExtensions_OnRewind_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mCDECB675CBFB437D345E0DE1A59F8BC4BA86121E(L_323, L_325, /*hidden argument*/TweenSettingsExtensions_OnRewind_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_mCDECB675CBFB437D345E0DE1A59F8BC4BA86121E_RuntimeMethod_var);
		// } else onRewind = null;
		goto IL_0b96;
	}

IL_0b8f:
	{
		// } else onRewind = null;
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_onRewind_19((UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 *)NULL);
	}

IL_0b96:
	{
		// if (autoPlay) tween.Play();
		bool L_327 = __this->get_autoPlay_38();
		if (!L_327)
		{
			goto IL_0bac;
		}
	}
	{
		// if (autoPlay) tween.Play();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_328 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_329;
		L_329 = TweenExtensions_Play_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m1A3AAD3856D80A02DB7F6BB7BBA444FD4C62DA8C(L_328, /*hidden argument*/TweenExtensions_Play_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m1A3AAD3856D80A02DB7F6BB7BBA444FD4C62DA8C_RuntimeMethod_var);
		goto IL_0bb8;
	}

IL_0bac:
	{
		// else tween.Pause();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_330 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_331;
		L_331 = TweenExtensions_Pause_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4A0F398B9290C213D5FED5901234BA973AA0C257(L_330, /*hidden argument*/TweenExtensions_Pause_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m4A0F398B9290C213D5FED5901234BA973AA0C257_RuntimeMethod_var);
	}

IL_0bb8:
	{
		// if (hasOnTweenCreated && onTweenCreated != null) onTweenCreated.Invoke();
		bool L_332 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_hasOnTweenCreated_11();
		if (!L_332)
		{
			goto IL_0bd3;
		}
	}
	{
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_333 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onTweenCreated_18();
		if (!L_333)
		{
			goto IL_0bd3;
		}
	}
	{
		// if (hasOnTweenCreated && onTweenCreated != null) onTweenCreated.Invoke();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_334 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_onTweenCreated_18();
		NullCheck(L_334);
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(L_334, /*hidden argument*/NULL);
	}

IL_0bd3:
	{
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlay()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DOPlay_mF73F8714C1BC8B1FDF5978BE5A3952F709523794 (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DOTween.Play(this.gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = DOTween_Play_mC14CD676517FE51D65E52689A7BC9CD40AD3B4A9(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayBackwards()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DOPlayBackwards_m5C5301E12FCD780FAB2829E78A92E09CEDD0BF12 (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DOTween.PlayBackwards(this.gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = DOTween_PlayBackwards_m5246F3D7CA28BDC2ADD46FC100C0C6FF05DBAA88(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayForward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DOPlayForward_m4AEFC44759287AA90497A5FAF28C49DE05D96C42 (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DOTween.PlayForward(this.gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = DOTween_PlayForward_m7ABD5ED445599090AB74695419E18716C7ADFD84(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DOPause_mBBA9A1E0C5AC180BD73832D1C9CF7B1251B7F37C (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DOTween.Pause(this.gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = DOTween_Pause_mD2AA03C8EDAAA5154478FA57F81041F5B27B4603(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOTogglePause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DOTogglePause_m69E7AEADFDD98EED6AE119DD490BC869FBDD847C (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DOTween.TogglePause(this.gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = DOTween_TogglePause_mDD72068F34F6B37697C7DE4AC6D9AC1E3B441EE5(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DORewind()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DORewind_mAD8ECC9275727B676133BCC1CFA7649DE825BADE (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponents_TisDOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9_m0EB1FAE4F313EBDE3E2726D1B7B497289CF8A85B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* V_0 = NULL;
	int32_t V_1 = 0;
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * V_2 = NULL;
	{
		// _playCount = -1;
		__this->set__playCount_54((-1));
		// DOTweenAnimation[] anims = this.gameObject.GetComponents<DOTweenAnimation>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* L_1;
		L_1 = GameObject_GetComponents_TisDOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9_m0EB1FAE4F313EBDE3E2726D1B7B497289CF8A85B(L_0, /*hidden argument*/GameObject_GetComponents_TisDOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9_m0EB1FAE4F313EBDE3E2726D1B7B497289CF8A85B_RuntimeMethod_var);
		V_0 = L_1;
		// for (int i = anims.Length - 1; i > -1; --i) {
		DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* L_2 = V_0;
		NullCheck(L_2);
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length))), (int32_t)1));
		goto IL_0041;
	}

IL_001b:
	{
		// Tween t = anims[i].tween;
		DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* L_3 = V_0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_7 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)L_6)->get_tween_20();
		V_2 = L_7;
		// if (t != null && t.IsInitialized()) anims[i].tween.Rewind();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_8 = V_2;
		if (!L_8)
		{
			goto IL_003d;
		}
	}
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_9 = V_2;
		bool L_10;
		L_10 = TweenExtensions_IsInitialized_mE92123AF4FE069B2F175193367CAFF218B552CEB(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_003d;
		}
	}
	{
		// if (t != null && t.IsInitialized()) anims[i].tween.Rewind();
		DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* L_11 = V_0;
		int32_t L_12 = V_1;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_15 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)L_14)->get_tween_20();
		TweenExtensions_Rewind_m911EFDC4DAAEB2DC3C80112FE4C14F01BCF558C8(L_15, (bool)1, /*hidden argument*/NULL);
	}

IL_003d:
	{
		// for (int i = anims.Length - 1; i > -1; --i) {
		int32_t L_16 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_16, (int32_t)1));
	}

IL_0041:
	{
		// for (int i = anims.Length - 1; i > -1; --i) {
		int32_t L_17 = V_1;
		if ((((int32_t)L_17) > ((int32_t)(-1))))
		{
			goto IL_001b;
		}
	}
	{
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DORestart(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DORestart_m73EEA95DB0AC3DD26D61A7C72A446E49C5A80E6F (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, bool ___fromHere0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _playCount = -1;
		__this->set__playCount_54((-1));
		// if (tween == null) {
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_0 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		// if (Debugger.logPriority > 1) Debugger.LogNullTween(tween); return;
		int32_t L_1 = ((Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F_StaticFields*)il2cpp_codegen_static_fields_for(Debugger_t2D5E755BBD72BE29AB5ECA77979DC4B945BD573F_il2cpp_TypeInfo_var))->get_logPriority_0();
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0022;
		}
	}
	{
		// if (Debugger.logPriority > 1) Debugger.LogNullTween(tween); return;
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_2 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		Debugger_LogNullTween_m6A9B33DC41AC0A765DD49C9AC2DDABECD22F476B(L_2, /*hidden argument*/NULL);
	}

IL_0022:
	{
		// if (Debugger.logPriority > 1) Debugger.LogNullTween(tween); return;
		return;
	}

IL_0023:
	{
		// if (fromHere && isRelative) ReEvaluateRelativeTween();
		bool L_3 = ___fromHere0;
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		bool L_4 = __this->get_isRelative_28();
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		// if (fromHere && isRelative) ReEvaluateRelativeTween();
		DOTweenAnimation_ReEvaluateRelativeTween_mB08DC3F0A42359F5A4B1DA632FFBF4596E365300(__this, /*hidden argument*/NULL);
	}

IL_0034:
	{
		// DOTween.Restart(this.gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_6;
		L_6 = DOTween_Restart_m6881B2FF21311D7A29EB514DFBB6FC85799BB2A6(L_5, (bool)1, (-1.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOComplete()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DOComplete_mC91160B480D7321E39386945355F03CD3D526CC8 (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DOTween.Complete(this.gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = DOTween_Complete_mF9C035573D7C3F51938F073B38FC3B5EA8F640B9(L_0, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOKill()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DOKill_mE26CAA76E70DD3EB13394C9199CD52F8192608EB (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DOTween.Kill(this.gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = DOTween_Kill_mF0F7315CA9F8E32145CA3E3C9249FBF98BF3AD2A(L_0, (bool)0, /*hidden argument*/NULL);
		// tween = null;
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20((Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *)NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayById(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DOPlayById_mBA0935540BE1F8594C084115F84747C2E7D36C69 (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, String_t* ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DOTween.Play(this.gameObject, id);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_2;
		L_2 = DOTween_Play_mAC946600F4FD0CB092D54F037213F5BFBDB1606C(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayAllById(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DOPlayAllById_m4C132F9B10DE15F596E967E975DB1F8485C60FEC (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, String_t* ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DOTween.Play(id);
		String_t* L_0 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = DOTween_Play_mC14CD676517FE51D65E52689A7BC9CD40AD3B4A9(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPauseAllById(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DOPauseAllById_m4309C95672F339387B5D2BFF0406B20336C22CC3 (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, String_t* ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DOTween.Pause(id);
		String_t* L_0 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = DOTween_Pause_mD2AA03C8EDAAA5154478FA57F81041F5B27B4603(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayBackwardsById(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DOPlayBackwardsById_mE65FE6F2E333A54F61B56D7FCE61E423FC127887 (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, String_t* ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DOTween.PlayBackwards(this.gameObject, id);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_2;
		L_2 = DOTween_PlayBackwards_m05450BF2423C577072F2C0879DF3483A626AAB39(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayBackwardsAllById(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DOPlayBackwardsAllById_mE150D1E0B8EDB5A2D0CB6AF3F3D75590050095BF (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, String_t* ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DOTween.PlayBackwards(id);
		String_t* L_0 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = DOTween_PlayBackwards_m5246F3D7CA28BDC2ADD46FC100C0C6FF05DBAA88(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayForwardById(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DOPlayForwardById_mB21110F44F03657ED596FFF96E203CE7388094B5 (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, String_t* ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DOTween.PlayForward(this.gameObject, id);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_2;
		L_2 = DOTween_PlayForward_m5E6AD8D0E07B823E88E2E64250634A8962415F83(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayForwardAllById(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DOPlayForwardAllById_mDCA7519E9E19639FDFC81F1B344CD22C62A1929E (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, String_t* ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DOTween.PlayForward(id);
		String_t* L_0 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = DOTween_PlayForward_m7ABD5ED445599090AB74695419E18716C7ADFD84(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DOPlayNext_m233EF38D6140BFEF7623EC6E3AD769C184E32544 (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponents_TisDOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9_mD0B291AF74B316EE4EE8444CF959B59235E9A6EA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TweenExtensions_Play_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m1A3AAD3856D80A02DB7F6BB7BBA444FD4C62DA8C_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* V_0 = NULL;
	DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * V_1 = NULL;
	{
		// DOTweenAnimation[] anims = this.GetComponents<DOTweenAnimation>();
		DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* L_0;
		L_0 = Component_GetComponents_TisDOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9_mD0B291AF74B316EE4EE8444CF959B59235E9A6EA(__this, /*hidden argument*/Component_GetComponents_TisDOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9_mD0B291AF74B316EE4EE8444CF959B59235E9A6EA_RuntimeMethod_var);
		V_0 = L_0;
		goto IL_0058;
	}

IL_0009:
	{
		// _playCount++;
		int32_t L_1 = __this->get__playCount_54();
		__this->set__playCount_54(((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)1)));
		// DOTweenAnimation anim = anims[_playCount];
		DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* L_2 = V_0;
		int32_t L_3 = __this->get__playCount_54();
		NullCheck(L_2);
		int32_t L_4 = L_3;
		DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		// if (anim != null && anim.tween != null && !anim.tween.IsPlaying() && !anim.tween.IsComplete()) {
		DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_6, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * L_8 = V_1;
		NullCheck(L_8);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_9 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)L_8)->get_tween_20();
		if (!L_9)
		{
			goto IL_0058;
		}
	}
	{
		DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * L_10 = V_1;
		NullCheck(L_10);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_11 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)L_10)->get_tween_20();
		bool L_12;
		L_12 = TweenExtensions_IsPlaying_m3B9AB4389AA78D1EB5384BB33BF24640E0005F6D(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0058;
		}
	}
	{
		DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * L_13 = V_1;
		NullCheck(L_13);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_14 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)L_13)->get_tween_20();
		bool L_15;
		L_15 = TweenExtensions_IsComplete_mC87F29FE0746C793F0C11175A9B37087C1C436FD(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0058;
		}
	}
	{
		// anim.tween.Play();
		DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * L_16 = V_1;
		NullCheck(L_16);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_17 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)L_16)->get_tween_20();
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_18;
		L_18 = TweenExtensions_Play_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m1A3AAD3856D80A02DB7F6BB7BBA444FD4C62DA8C(L_17, /*hidden argument*/TweenExtensions_Play_TisTween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941_m1A3AAD3856D80A02DB7F6BB7BBA444FD4C62DA8C_RuntimeMethod_var);
		// break;
		return;
	}

IL_0058:
	{
		// while (_playCount < anims.Length - 1) {
		int32_t L_19 = __this->get__playCount_54();
		DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* L_20 = V_0;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length))), (int32_t)1)))))
		{
			goto IL_0009;
		}
	}
	{
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DORewindAndPlayNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DORewindAndPlayNext_m9A94A494C50730720A4A0054C91E90D730D0FF1E (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _playCount = -1;
		__this->set__playCount_54((-1));
		// DOTween.Rewind(this.gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = DOTween_Rewind_m5A9A7CD4D2FB1B2F16212A8A4A858B2FCA372C65(L_0, (bool)1, /*hidden argument*/NULL);
		// DOPlayNext();
		DOTweenAnimation_DOPlayNext_m233EF38D6140BFEF7623EC6E3AD769C184E32544(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DORestartById(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DORestartById_mA7F554C81A02C7D05F4CD748843227254479C18A (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, String_t* ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _playCount = -1;
		__this->set__playCount_54((-1));
		// DOTween.Restart(this.gameObject, id);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_2;
		L_2 = DOTween_Restart_m26B0C36D9CE55E70C0D97D543F8F95AA7D447445(L_0, L_1, (bool)1, (-1.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DORestartAllById(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_DORestartAllById_mAD1C66ADAC18B95D95C130365AE09D396BE83AB5 (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, String_t* ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _playCount = -1;
		__this->set__playCount_54((-1));
		// DOTween.Restart(id);
		String_t* L_0 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_il2cpp_TypeInfo_var);
		int32_t L_1;
		L_1 = DOTween_Restart_m6881B2FF21311D7A29EB514DFBB6FC85799BB2A6(L_0, (bool)1, (-1.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.DOTweenAnimation::GetTweens()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1 * DOTweenAnimation_GetTweens_mC9E29FBBB58FD0AA8AB588D34CE7D15E25E13E2A (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponents_TisDOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9_mD0B291AF74B316EE4EE8444CF959B59235E9A6EA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m782F802BE39FD4D3D5758579703B971BC396ABB8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mF8875C86FFAC30CA85251C1B54879C9A33F4DF3F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1 * V_0 = NULL;
	DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* V_1 = NULL;
	int32_t V_2 = 0;
	DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * V_3 = NULL;
	{
		// List<Tween> result = new List<Tween>();
		List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1 * L_0 = (List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1 *)il2cpp_codegen_object_new(List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1_il2cpp_TypeInfo_var);
		List_1__ctor_mF8875C86FFAC30CA85251C1B54879C9A33F4DF3F(L_0, /*hidden argument*/List_1__ctor_mF8875C86FFAC30CA85251C1B54879C9A33F4DF3F_RuntimeMethod_var);
		V_0 = L_0;
		// DOTweenAnimation[] anims = this.GetComponents<DOTweenAnimation>();
		DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* L_1;
		L_1 = Component_GetComponents_TisDOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9_mD0B291AF74B316EE4EE8444CF959B59235E9A6EA(__this, /*hidden argument*/Component_GetComponents_TisDOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9_mD0B291AF74B316EE4EE8444CF959B59235E9A6EA_RuntimeMethod_var);
		// foreach (DOTweenAnimation anim in anims) result.Add(anim.tween);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0025;
	}

IL_0011:
	{
		// foreach (DOTweenAnimation anim in anims) result.Add(anim.tween);
		DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_3 = L_5;
		// foreach (DOTweenAnimation anim in anims) result.Add(anim.tween);
		List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1 * L_6 = V_0;
		DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * L_7 = V_3;
		NullCheck(L_7);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_8 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)L_7)->get_tween_20();
		NullCheck(L_6);
		List_1_Add_m782F802BE39FD4D3D5758579703B971BC396ABB8(L_6, L_8, /*hidden argument*/List_1_Add_m782F802BE39FD4D3D5758579703B971BC396ABB8_RuntimeMethod_var);
		int32_t L_9 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0025:
	{
		// foreach (DOTweenAnimation anim in anims) result.Add(anim.tween);
		int32_t L_10 = V_2;
		DOTweenAnimationU5BU5D_t73C0CC3FF7159BFF54866B412EC72474A81CD3B8* L_11 = V_1;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length))))))
		{
			goto IL_0011;
		}
	}
	{
		// return result;
		List_1_t08B78D4D8AA7967CCB0B188F26199678C0124DF1 * L_12 = V_0;
		return L_12;
	}
}
// DG.Tweening.Core.TargetType DG.Tweening.DOTweenAnimation::TypeToDOTargetType(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DOTweenAnimation_TypeToDOTargetType_mAB457ACFE735EC9B05FE0BA52C88E8576EEE304F (Type_t * ___t0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enum_t23B90B40F60E677A8025267341651C94AE079CDA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TargetType_t075843C35C2F3BB06953C27ACCC880643AF35310_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TargetType_t075843C35C2F3BB06953C27ACCC880643AF35310_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6696B5BAE2BDB2FB41C480BCA3BEA55217F8CC87);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8CCF95ED19A7C10C7DD800A4E0C960CACFE7FF1C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF3E84B722399601AD7E281754E917478AA9AD48D);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// string str = t.ToString();
		Type_t * L_0 = ___t0;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		V_0 = L_1;
		// int dotIndex = str.LastIndexOf(".");
		String_t* L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3;
		L_3 = String_LastIndexOf_m80AFBEF2F3857F9D6A67126F4C4D9A9B9CEC5902(L_2, _stringLiteralF3E84B722399601AD7E281754E917478AA9AD48D, /*hidden argument*/NULL);
		V_1 = L_3;
		// if (dotIndex != -1) str = str.Substring(dotIndex + 1);
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_0021;
		}
	}
	{
		// if (dotIndex != -1) str = str.Substring(dotIndex + 1);
		String_t* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		String_t* L_7;
		L_7 = String_Substring_mB6B87FD76552BBF6D4E2B9F07F857FE051DCE190(L_5, ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_0021:
	{
		// if (str.IndexOf("Renderer") != -1 && (str != "SpriteRenderer")) str = "Renderer";
		String_t* L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9;
		L_9 = String_IndexOf_m90616B2D8ACC645F389750FAE4F9A75BC5D82454(L_8, _stringLiteral8CCF95ED19A7C10C7DD800A4E0C960CACFE7FF1C, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_10 = V_0;
		bool L_11;
		L_11 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_10, _stringLiteral6696B5BAE2BDB2FB41C480BCA3BEA55217F8CC87, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0042;
		}
	}
	{
		// if (str.IndexOf("Renderer") != -1 && (str != "SpriteRenderer")) str = "Renderer";
		V_0 = _stringLiteral8CCF95ED19A7C10C7DD800A4E0C960CACFE7FF1C;
	}

IL_0042:
	{
		// return (TargetType)Enum.Parse(typeof(TargetType), str);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_12 = { reinterpret_cast<intptr_t> (TargetType_t075843C35C2F3BB06953C27ACCC880643AF35310_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13;
		L_13 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_12, /*hidden argument*/NULL);
		String_t* L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_il2cpp_TypeInfo_var);
		RuntimeObject * L_15;
		L_15 = Enum_Parse_m6601224637A9CF40F77358805956C2EE757EAF68(L_13, L_14, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox(L_15, TargetType_t075843C35C2F3BB06953C27ACCC880643AF35310_il2cpp_TypeInfo_var))));
	}
}
// System.Void DG.Tweening.DOTweenAnimation::ReEvaluateRelativeTween()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_ReEvaluateRelativeTween_mB08DC3F0A42359F5A4B1DA632FFBF4596E365300 (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (animationType == DOTweenAnimationType.Move) {
		int32_t L_0 = __this->get_animationType_35();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0037;
		}
	}
	{
		// ((Tweener)tween).ChangeEndValue(transform.position + endValueV3, true);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_1 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = __this->get_endValueV3_41();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_3, L_4, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = L_5;
		RuntimeObject * L_7 = Box(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var, &L_6);
		NullCheck(((Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 *)CastclassClass((RuntimeObject*)L_1, Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_il2cpp_TypeInfo_var)));
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_8;
		L_8 = VirtFuncInvoker2< Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 *, RuntimeObject *, bool >::Invoke(11 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Boolean) */, ((Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 *)CastclassClass((RuntimeObject*)L_1, Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_il2cpp_TypeInfo_var)), L_7, (bool)1);
		// } else if (animationType == DOTweenAnimationType.LocalMove) {
		return;
	}

IL_0037:
	{
		// } else if (animationType == DOTweenAnimationType.LocalMove) {
		int32_t L_9 = __this->get_animationType_35();
		if ((!(((uint32_t)L_9) == ((uint32_t)2))))
		{
			goto IL_006d;
		}
	}
	{
		// ((Tweener)tween).ChangeEndValue(transform.localPosition + endValueV3, true);
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_10 = ((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->get_tween_20();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Transform_get_localPosition_m527B8B5B625DA9A61E551E0FBCD3BE8CA4539FC2(L_11, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = __this->get_endValueV3_41();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_12, L_13, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15 = L_14;
		RuntimeObject * L_16 = Box(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var, &L_15);
		NullCheck(((Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 *)CastclassClass((RuntimeObject*)L_10, Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_il2cpp_TypeInfo_var)));
		Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 * L_17;
		L_17 = VirtFuncInvoker2< Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 *, RuntimeObject *, bool >::Invoke(11 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Boolean) */, ((Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8 *)CastclassClass((RuntimeObject*)L_10, Tweener_tFC8507DF103792DB165B74C4179B772F3B637CA8_il2cpp_TypeInfo_var)), L_16, (bool)1);
	}

IL_006d:
	{
		// }
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation__ctor_m8323282BFF842AEAA8708B0EE7B21D21A19AD0EF (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public float duration = 1;
		__this->set_duration_22((1.0f));
		// public Ease easeType = Ease.OutQuad;
		__this->set_easeType_23(6);
		// public AnimationCurve easeCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_5 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_5, L_3, /*hidden argument*/NULL);
		__this->set_easeCurve_24(L_5);
		// public int loops = 1;
		__this->set_loops_26(1);
		// public string id = "";
		__this->set_id_27(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		// public bool autoKill = true;
		__this->set_autoKill_31((bool)1);
		// public bool isActive = true;
		__this->set_isActive_32((bool)1);
		// public bool autoPlay = true;
		__this->set_autoPlay_38((bool)1);
		// public Color endValueColor = new Color(1, 1, 1, 1);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_6), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_endValueColor_43(L_6);
		// public string endValueString = "";
		__this->set_endValueString_44(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		// public Rect endValueRect = new Rect(0, 0, 0, 0);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_7;
		memset((&L_7), 0, sizeof(L_7));
		Rect__ctor_m12075526A02B55B680716A34AD5287B223122B70((&L_7), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_endValueRect_45(L_7);
		// int _playCount = -1; // Used when calling DOPlayNext
		__this->set__playCount_54((-1));
		ABSAnimationComponent__ctor_mF453633C79A30A108832D3F331DAB5E4CA24B272(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::<CreateTween>b__37_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DOTweenAnimation_U3CCreateTweenU3Eb__37_0_m7133BCD50DF5E0EDB11799DF7618551D417B05AA (DOTweenAnimation_t73501A3B4A0211D21EB2503AEF266AD26D53E9F9 * __this, const RuntimeMethod* method)
{
	{
		// .OnKill(()=> tween = null);
		((ABSAnimationComponent_tA96E6E40900885BB8CC7E8BDA290CE8533CCDFB7 *)__this)->set_tween_20((Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 *)NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool TweenExtensions_IsActive_m19E1D79E2590A189C95DADED68FFDB43A7F3B2A2_inline (Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t0, const RuntimeMethod* method)
{
	{
		Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * L_0 = ___t0;
		NullCheck(L_0);
		bool L_1 = L_0->get_active_35();
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_4), L_1, L_3, (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001a;
	}

IL_001a:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___v0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___v0;
		float L_3 = L_2.get_y_3();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_4), L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
