﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void ShortcutExtensions43_DOColor_mA2543B2AD028B46EA2C799F18C669A00783970E8 (void);
// 0x00000002 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOFade(UnityEngine.SpriteRenderer,System.Single,System.Single)
extern void ShortcutExtensions43_DOFade_m5F3CD26D926219D7FCBAF07C0EAB1D50F76E5006 (void);
// 0x00000003 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOMove(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Boolean)
extern void ShortcutExtensions43_DOMove_m2841C4B8C1638272A9C9A23E2FFA00B104C35400 (void);
// 0x00000004 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DORotate(UnityEngine.Rigidbody2D,System.Single,System.Single)
extern void ShortcutExtensions43_DORotate_m868974591A2268B71A3EC37CC6D31CE77979B10C (void);
// 0x00000005 System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m48E05B56036254D49DAEB8F295E9E6C8BDCADC6D (void);
// 0x00000006 UnityEngine.Color DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__0_m1D7DF9711656BBF94B94806F1F8BCEBD054C71E4 (void);
// 0x00000007 System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__1_m0DFCE7EB3897535D529FA90C2B100ED244F383FC (void);
// 0x00000008 System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mB4DF136FEB0BFABE14B7DDE834E0C2FDA685B991 (void);
// 0x00000009 UnityEngine.Color DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__0_m8BD6A8476E7D96B335DCCED905B443EE68DADEF7 (void);
// 0x0000000A System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__1_mAF96917D873B7CE5960E949FF567DB4C23DC2961 (void);
// 0x0000000B System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m964875B6120212AD8CE2664EC26CA47A926B1740 (void);
// 0x0000000C UnityEngine.Vector2 DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOMoveU3Eb__0_m9D8CAC5B7FAEBAACD75489B96693E2066A998218 (void);
// 0x0000000D System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m045AD5EBDDCD3D66F685F1CB51FF83B1AA9557A1 (void);
// 0x0000000E System.Single DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDORotateU3Eb__0_mD311B368E4BB9122A2784911F2CA4F2846AC1439 (void);
static Il2CppMethodPointer s_methodPointers[14] = 
{
	ShortcutExtensions43_DOColor_mA2543B2AD028B46EA2C799F18C669A00783970E8,
	ShortcutExtensions43_DOFade_m5F3CD26D926219D7FCBAF07C0EAB1D50F76E5006,
	ShortcutExtensions43_DOMove_m2841C4B8C1638272A9C9A23E2FFA00B104C35400,
	ShortcutExtensions43_DORotate_m868974591A2268B71A3EC37CC6D31CE77979B10C,
	U3CU3Ec__DisplayClass2_0__ctor_m48E05B56036254D49DAEB8F295E9E6C8BDCADC6D,
	U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__0_m1D7DF9711656BBF94B94806F1F8BCEBD054C71E4,
	U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__1_m0DFCE7EB3897535D529FA90C2B100ED244F383FC,
	U3CU3Ec__DisplayClass3_0__ctor_mB4DF136FEB0BFABE14B7DDE834E0C2FDA685B991,
	U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__0_m8BD6A8476E7D96B335DCCED905B443EE68DADEF7,
	U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__1_mAF96917D873B7CE5960E949FF567DB4C23DC2961,
	U3CU3Ec__DisplayClass5_0__ctor_m964875B6120212AD8CE2664EC26CA47A926B1740,
	U3CU3Ec__DisplayClass5_0_U3CDOMoveU3Eb__0_m9D8CAC5B7FAEBAACD75489B96693E2066A998218,
	U3CU3Ec__DisplayClass8_0__ctor_m045AD5EBDDCD3D66F685F1CB51FF83B1AA9557A1,
	U3CU3Ec__DisplayClass8_0_U3CDORotateU3Eb__0_mD311B368E4BB9122A2784911F2CA4F2846AC1439,
};
static const int32_t s_InvokerIndices[14] = 
{
	4381,
	4405,
	4136,
	4405,
	3418,
	3300,
	2700,
	3418,
	3300,
	2700,
	3418,
	3414,
	3418,
	3398,
};
extern const CustomAttributesCacheGenerator g_DOTween43_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_DOTween43_CodeGenModule;
const Il2CppCodeGenModule g_DOTween43_CodeGenModule = 
{
	"DOTween43.dll",
	14,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_DOTween43_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
