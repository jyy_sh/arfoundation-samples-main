﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void DG.Tweening.Color2::.ctor(UnityEngine.Color,UnityEngine.Color)
extern void Color2__ctor_mB17DD3F311A7AA9AC3FA318105CF03900034C65B (void);
// 0x00000002 DG.Tweening.Color2 DG.Tweening.Color2::op_Addition(DG.Tweening.Color2,DG.Tweening.Color2)
extern void Color2_op_Addition_m27B3C091014E030F734960C719FEB1AD82B1795A (void);
// 0x00000003 DG.Tweening.Color2 DG.Tweening.Color2::op_Subtraction(DG.Tweening.Color2,DG.Tweening.Color2)
extern void Color2_op_Subtraction_mEFEC3CBDA05EA5309532AF832A903D278546B354 (void);
// 0x00000004 DG.Tweening.Color2 DG.Tweening.Color2::op_Multiply(DG.Tweening.Color2,System.Single)
extern void Color2_op_Multiply_m35AA0399C57ED8164080363F483750D51B912540 (void);
// 0x00000005 System.Void DG.Tweening.TweenCallback::.ctor(System.Object,System.IntPtr)
extern void TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662 (void);
// 0x00000006 System.Void DG.Tweening.TweenCallback::Invoke()
extern void TweenCallback_Invoke_mB38F3AB4456E2A55F93CECFB3C4A529248CBB82E (void);
// 0x00000007 System.IAsyncResult DG.Tweening.TweenCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void TweenCallback_BeginInvoke_m27BCFEAAE71F1A44A7121006B63DB9C0EAC8BD85 (void);
// 0x00000008 System.Void DG.Tweening.TweenCallback::EndInvoke(System.IAsyncResult)
extern void TweenCallback_EndInvoke_m28D9821FD5AEF34270D209860EB15D885FD2E8F0 (void);
// 0x00000009 System.Void DG.Tweening.TweenCallback`1::.ctor(System.Object,System.IntPtr)
// 0x0000000A System.Void DG.Tweening.TweenCallback`1::Invoke(T)
// 0x0000000B System.IAsyncResult DG.Tweening.TweenCallback`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x0000000C System.Void DG.Tweening.TweenCallback`1::EndInvoke(System.IAsyncResult)
// 0x0000000D System.Void DG.Tweening.EaseFunction::.ctor(System.Object,System.IntPtr)
extern void EaseFunction__ctor_m8C63A6FD3BBACF51D464452AC05DA58BC3419466 (void);
// 0x0000000E System.Single DG.Tweening.EaseFunction::Invoke(System.Single,System.Single,System.Single,System.Single)
extern void EaseFunction_Invoke_m21560094511BC4885165BDCD850958673B9B0787 (void);
// 0x0000000F System.IAsyncResult DG.Tweening.EaseFunction::BeginInvoke(System.Single,System.Single,System.Single,System.Single,System.AsyncCallback,System.Object)
extern void EaseFunction_BeginInvoke_mD57FFED181577E1CE4CA2939238E785C861B6BC3 (void);
// 0x00000010 System.Single DG.Tweening.EaseFunction::EndInvoke(System.IAsyncResult)
extern void EaseFunction_EndInvoke_m87B0A07FE2D16908B9721A2CD543AC61D7E8AD78 (void);
// 0x00000011 DG.Tweening.LogBehaviour DG.Tweening.DOTween::get_logBehaviour()
extern void DOTween_get_logBehaviour_mB40FBDD2C7139800D31987C9D6906A53AD62ABF0 (void);
// 0x00000012 System.Void DG.Tweening.DOTween::set_logBehaviour(DG.Tweening.LogBehaviour)
extern void DOTween_set_logBehaviour_m7AE2BAE33CC9A19A5A0A86BB53B2610C12EBF4F6 (void);
// 0x00000013 System.Void DG.Tweening.DOTween::.cctor()
extern void DOTween__cctor_m8AC5173822F9EE1B0DC970CC7644D5A2ED1C8E0A (void);
// 0x00000014 DG.Tweening.IDOTweenInit DG.Tweening.DOTween::Init(System.Nullable`1<System.Boolean>,System.Nullable`1<System.Boolean>,System.Nullable`1<DG.Tweening.LogBehaviour>)
extern void DOTween_Init_m3400C37EBD33639098CC99D4DFB42ED26B588B6A (void);
// 0x00000015 System.Void DG.Tweening.DOTween::AutoInit()
extern void DOTween_AutoInit_m91FB3CB67999828CF2A4C0FF22AB5CDABB47464E (void);
// 0x00000016 DG.Tweening.IDOTweenInit DG.Tweening.DOTween::Init(DG.Tweening.Core.DOTweenSettings,System.Nullable`1<System.Boolean>,System.Nullable`1<System.Boolean>,System.Nullable`1<DG.Tweening.LogBehaviour>)
extern void DOTween_Init_m32C148E25F2292B1DC757AA992EE5EBBCA54721B (void);
// 0x00000017 System.Void DG.Tweening.DOTween::SetTweensCapacity(System.Int32,System.Int32)
extern void DOTween_SetTweensCapacity_mCE1F466A5044C13B82614B3652791832BC1D9934 (void);
// 0x00000018 System.Void DG.Tweening.DOTween::Clear(System.Boolean)
extern void DOTween_Clear_m04B8E98534F91A012F1D157BFAFFC1314FB0E98B (void);
// 0x00000019 System.Void DG.Tweening.DOTween::ClearCachedTweens()
extern void DOTween_ClearCachedTweens_m60BA2402547DA142D3023755407F8A70C96934C0 (void);
// 0x0000001A System.Int32 DG.Tweening.DOTween::Validate()
extern void DOTween_Validate_mCBD23A691CE3D3C02DB79BEFC991005690D5CEDB (void);
// 0x0000001B System.Void DG.Tweening.DOTween::ManualUpdate(System.Single,System.Single)
extern void DOTween_ManualUpdate_m8F589B4EE2E6EC705603FB827A20B79A0A2E6035 (void);
// 0x0000001C DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.Single>,DG.Tweening.Core.DOSetter`1<System.Single>,System.Single,System.Single)
extern void DOTween_To_mB87E880F73BF460ADEDDDDA0D0E96781E9852BF4 (void);
// 0x0000001D DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.Double>,DG.Tweening.Core.DOSetter`1<System.Double>,System.Double,System.Single)
extern void DOTween_To_m951A8CFEF441742F76561A910423B523E153D2C7 (void);
// 0x0000001E DG.Tweening.Tweener DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.Int32>,DG.Tweening.Core.DOSetter`1<System.Int32>,System.Int32,System.Single)
extern void DOTween_To_m7BCA0F692B6E84F0B9215471A8ABB9241014EC03 (void);
// 0x0000001F DG.Tweening.Tweener DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.UInt32>,DG.Tweening.Core.DOSetter`1<System.UInt32>,System.UInt32,System.Single)
extern void DOTween_To_m9435E751D7104AD4B7B9B25B0059E8AFF1FCB654 (void);
// 0x00000020 DG.Tweening.Tweener DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.Int64>,DG.Tweening.Core.DOSetter`1<System.Int64>,System.Int64,System.Single)
extern void DOTween_To_m2A479DD6F7A22C7A8335674E71E4E83AEAAA3C32 (void);
// 0x00000021 DG.Tweening.Tweener DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.UInt64>,DG.Tweening.Core.DOSetter`1<System.UInt64>,System.UInt64,System.Single)
extern void DOTween_To_m71864E7EE752F53C6FAE67B7E4101E459DD8CAAA (void);
// 0x00000022 DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.String>,DG.Tweening.Core.DOSetter`1<System.String>,System.String,System.Single)
extern void DOTween_To_m7F1A7981B718FBBC50C35F426AAFB836207BB2CC (void);
// 0x00000023 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>,UnityEngine.Vector2,System.Single)
extern void DOTween_To_m08A8B415E04DF5FD6ED278A7912A8A1167A811A3 (void);
// 0x00000024 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Single)
extern void DOTween_To_mC70B880FDF6DBD25D94D655E708BF4826A4AAF10 (void);
// 0x00000025 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>,UnityEngine.Vector4,System.Single)
extern void DOTween_To_mB51344CEBA06CEB655A922284A52FDE0DB82F9DE (void);
// 0x00000026 DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>,DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>,UnityEngine.Vector3,System.Single)
extern void DOTween_To_mD5F2402188B7E46E02B9282A000B99C61AAC89C4 (void);
// 0x00000027 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Color>,DG.Tweening.Core.DOSetter`1<UnityEngine.Color>,UnityEngine.Color,System.Single)
extern void DOTween_To_mE294CB006DFDA920942DE3D1BD24C4701EBCE90E (void);
// 0x00000028 DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>,DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>,UnityEngine.Rect,System.Single)
extern void DOTween_To_m787DA25F8DC1481EACC3393F64E9A26C03276178 (void);
// 0x00000029 DG.Tweening.Tweener DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.RectOffset>,DG.Tweening.Core.DOSetter`1<UnityEngine.RectOffset>,UnityEngine.RectOffset,System.Single)
extern void DOTween_To_m383D6D52921F98622D35990E9A3C567FD66AA940 (void);
// 0x0000002A DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions> DG.Tweening.DOTween::To(DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions>,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,T2,System.Single)
// 0x0000002B DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTween::ToAxis(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,System.Single,DG.Tweening.AxisConstraint)
extern void DOTween_ToAxis_m2FFC3E0664FDFF539F457A8A92970A458CE83E73 (void);
// 0x0000002C DG.Tweening.Tweener DG.Tweening.DOTween::ToAlpha(DG.Tweening.Core.DOGetter`1<UnityEngine.Color>,DG.Tweening.Core.DOSetter`1<UnityEngine.Color>,System.Single,System.Single)
extern void DOTween_ToAlpha_m3EF34978A74FEA83A950E85150829B53027E521E (void);
// 0x0000002D DG.Tweening.Tweener DG.Tweening.DOTween::To(DG.Tweening.Core.DOSetter`1<System.Single>,System.Single,System.Single,System.Single)
extern void DOTween_To_m310347ACE0C4FAB0E862ECBD1CEE447F8BE25E81 (void);
// 0x0000002E DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions> DG.Tweening.DOTween::Punch(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern void DOTween_Punch_mE74751F7ED2DC39DCEF7422481F69E9A26FC3D83 (void);
// 0x0000002F DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions> DG.Tweening.DOTween::Shake(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,System.Single,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTween_Shake_mC4CC0305697D6672E0050A3095922402999118CB (void);
// 0x00000030 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions> DG.Tweening.DOTween::Shake(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean)
extern void DOTween_Shake_m82F8898EC94774142E744EEF6282391CCE5942E3 (void);
// 0x00000031 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions> DG.Tweening.DOTween::Shake(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean,System.Boolean,System.Boolean)
extern void DOTween_Shake_m94A185F3C88C55EAEFE89F7BA7756A4A3C9B75FB (void);
// 0x00000032 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions> DG.Tweening.DOTween::ToArray(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,UnityEngine.Vector3[],System.Single[])
extern void DOTween_ToArray_m93EE354BBABCDAA9E304710A8AA1A1E5A2F5076E (void);
// 0x00000033 DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>,DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>,DG.Tweening.Color2,System.Single)
extern void DOTween_To_mC82BC7E459E1DA5E6C74E722A44C1FC8E69B8D9C (void);
// 0x00000034 DG.Tweening.Sequence DG.Tweening.DOTween::Sequence()
extern void DOTween_Sequence_m83221E315CE42CCE7B80D126A549114C86BC388D (void);
// 0x00000035 System.Int32 DG.Tweening.DOTween::CompleteAll(System.Boolean)
extern void DOTween_CompleteAll_mFA01DDBC9F8C0F307E563CECE9346BA247412DED (void);
// 0x00000036 System.Int32 DG.Tweening.DOTween::Complete(System.Object,System.Boolean)
extern void DOTween_Complete_mF9C035573D7C3F51938F073B38FC3B5EA8F640B9 (void);
// 0x00000037 System.Int32 DG.Tweening.DOTween::CompleteAndReturnKilledTot()
extern void DOTween_CompleteAndReturnKilledTot_mF2C24EB8883F5051AD947C081A28DE41FDE127C4 (void);
// 0x00000038 System.Int32 DG.Tweening.DOTween::CompleteAndReturnKilledTot(System.Object)
extern void DOTween_CompleteAndReturnKilledTot_m55B28CC146DF1B721928E206A89A5B1B4F742716 (void);
// 0x00000039 System.Int32 DG.Tweening.DOTween::CompleteAndReturnKilledTotExceptFor(System.Object[])
extern void DOTween_CompleteAndReturnKilledTotExceptFor_m1B34E13253A3590FD3BB5FCAB4091C6F7D84EC5A (void);
// 0x0000003A System.Int32 DG.Tweening.DOTween::FlipAll()
extern void DOTween_FlipAll_mAA6808ADC02BCDD02F3A8C47685145FD998809C0 (void);
// 0x0000003B System.Int32 DG.Tweening.DOTween::Flip(System.Object)
extern void DOTween_Flip_m915AEB3768FB2BB2F503A56F5CFF26A1394FCF5E (void);
// 0x0000003C System.Int32 DG.Tweening.DOTween::GotoAll(System.Single,System.Boolean)
extern void DOTween_GotoAll_m0E77B61840AC491300DC0E2BB393876E5DDDFBAD (void);
// 0x0000003D System.Int32 DG.Tweening.DOTween::Goto(System.Object,System.Single,System.Boolean)
extern void DOTween_Goto_m6B67C3CE020FC056F07743718C9DC9DEDFC7AE96 (void);
// 0x0000003E System.Int32 DG.Tweening.DOTween::KillAll(System.Boolean)
extern void DOTween_KillAll_m9A5541F18DA9DB46CC1C9AA1FE9A848AA31CFA53 (void);
// 0x0000003F System.Int32 DG.Tweening.DOTween::KillAll(System.Boolean,System.Object[])
extern void DOTween_KillAll_mC78D7B22BB32344450F9A3E7BDBCC36E4B3A6E3A (void);
// 0x00000040 System.Int32 DG.Tweening.DOTween::Kill(System.Object,System.Boolean)
extern void DOTween_Kill_mF0F7315CA9F8E32145CA3E3C9249FBF98BF3AD2A (void);
// 0x00000041 System.Int32 DG.Tweening.DOTween::PauseAll()
extern void DOTween_PauseAll_mD2D3214E242AE9309B4B4206768D4B6A451E5641 (void);
// 0x00000042 System.Int32 DG.Tweening.DOTween::Pause(System.Object)
extern void DOTween_Pause_mD2AA03C8EDAAA5154478FA57F81041F5B27B4603 (void);
// 0x00000043 System.Int32 DG.Tweening.DOTween::PlayAll()
extern void DOTween_PlayAll_m060079597102F82BB23673D6392A0120DB20C513 (void);
// 0x00000044 System.Int32 DG.Tweening.DOTween::Play(System.Object)
extern void DOTween_Play_mC14CD676517FE51D65E52689A7BC9CD40AD3B4A9 (void);
// 0x00000045 System.Int32 DG.Tweening.DOTween::Play(System.Object,System.Object)
extern void DOTween_Play_mAC946600F4FD0CB092D54F037213F5BFBDB1606C (void);
// 0x00000046 System.Int32 DG.Tweening.DOTween::PlayBackwardsAll()
extern void DOTween_PlayBackwardsAll_mC6261C8A7D5D7456743D9BA448A8DEFFB302D051 (void);
// 0x00000047 System.Int32 DG.Tweening.DOTween::PlayBackwards(System.Object)
extern void DOTween_PlayBackwards_m5246F3D7CA28BDC2ADD46FC100C0C6FF05DBAA88 (void);
// 0x00000048 System.Int32 DG.Tweening.DOTween::PlayBackwards(System.Object,System.Object)
extern void DOTween_PlayBackwards_m05450BF2423C577072F2C0879DF3483A626AAB39 (void);
// 0x00000049 System.Int32 DG.Tweening.DOTween::PlayForwardAll()
extern void DOTween_PlayForwardAll_m2CF97F8946F463A4D63B63DF7C6992FA37DD8889 (void);
// 0x0000004A System.Int32 DG.Tweening.DOTween::PlayForward(System.Object)
extern void DOTween_PlayForward_m7ABD5ED445599090AB74695419E18716C7ADFD84 (void);
// 0x0000004B System.Int32 DG.Tweening.DOTween::PlayForward(System.Object,System.Object)
extern void DOTween_PlayForward_m5E6AD8D0E07B823E88E2E64250634A8962415F83 (void);
// 0x0000004C System.Int32 DG.Tweening.DOTween::RestartAll(System.Boolean)
extern void DOTween_RestartAll_m391EA6F3ACCF78966E3323AA736924EEE1A79859 (void);
// 0x0000004D System.Int32 DG.Tweening.DOTween::Restart(System.Object,System.Boolean,System.Single)
extern void DOTween_Restart_m6881B2FF21311D7A29EB514DFBB6FC85799BB2A6 (void);
// 0x0000004E System.Int32 DG.Tweening.DOTween::Restart(System.Object,System.Object,System.Boolean,System.Single)
extern void DOTween_Restart_m26B0C36D9CE55E70C0D97D543F8F95AA7D447445 (void);
// 0x0000004F System.Int32 DG.Tweening.DOTween::RewindAll(System.Boolean)
extern void DOTween_RewindAll_mE5E1020426EECBEA6EB90B3F899D3D5A9E51B9ED (void);
// 0x00000050 System.Int32 DG.Tweening.DOTween::Rewind(System.Object,System.Boolean)
extern void DOTween_Rewind_m5A9A7CD4D2FB1B2F16212A8A4A858B2FCA372C65 (void);
// 0x00000051 System.Int32 DG.Tweening.DOTween::SmoothRewindAll()
extern void DOTween_SmoothRewindAll_mB1C2FFE58E0F7B5BD45ABD3EFA0E87B268197D42 (void);
// 0x00000052 System.Int32 DG.Tweening.DOTween::SmoothRewind(System.Object)
extern void DOTween_SmoothRewind_m98B602654BCD6279059A211AE608046019CB612D (void);
// 0x00000053 System.Int32 DG.Tweening.DOTween::TogglePauseAll()
extern void DOTween_TogglePauseAll_mFD9820FAF4F834A4B044EF12CF62B51F47411959 (void);
// 0x00000054 System.Int32 DG.Tweening.DOTween::TogglePause(System.Object)
extern void DOTween_TogglePause_mDD72068F34F6B37697C7DE4AC6D9AC1E3B441EE5 (void);
// 0x00000055 System.Boolean DG.Tweening.DOTween::IsTweening(System.Object,System.Boolean)
extern void DOTween_IsTweening_m9186DE09F26E821FEC3E8971C99E2BA361BB95C6 (void);
// 0x00000056 System.Int32 DG.Tweening.DOTween::TotalPlayingTweens()
extern void DOTween_TotalPlayingTweens_m7432F8641D7754571336B3E68F85955E00FE9D1B (void);
// 0x00000057 System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.DOTween::PlayingTweens()
extern void DOTween_PlayingTweens_mD72E49FE6F2D1012B0839FB51D7EDC88CD466D02 (void);
// 0x00000058 System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.DOTween::PausedTweens()
extern void DOTween_PausedTweens_mCB5A69F922EFC7D0ADD6D3BD34ACC107FF8C5E14 (void);
// 0x00000059 System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.DOTween::TweensById(System.Object,System.Boolean)
extern void DOTween_TweensById_mD50FC14A9ACB3B84317270D839272A43A0243768 (void);
// 0x0000005A System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.DOTween::TweensByTarget(System.Object,System.Boolean)
extern void DOTween_TweensByTarget_mF0BF5D3AB8F269BC8CC3C019635C8A7611A5B226 (void);
// 0x0000005B System.Void DG.Tweening.DOTween::InitCheck()
extern void DOTween_InitCheck_m1EF5626706A24C086ECF79B2504CF387553801DE (void);
// 0x0000005C DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions> DG.Tweening.DOTween::ApplyTo(DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,T2,System.Single,DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions>)
// 0x0000005D System.Void DG.Tweening.DOTween::.ctor()
extern void DOTween__ctor_mBABD7AE24A8163BA65A6C75B183D193B44D6A902 (void);
// 0x0000005E DG.Tweening.Tweener DG.Tweening.DOVirtual::Float(System.Single,System.Single,System.Single,DG.Tweening.TweenCallback`1<System.Single>)
extern void DOVirtual_Float_m0A5BB3095277DD51B929E886B9D5EB64615DF5FA (void);
// 0x0000005F System.Single DG.Tweening.DOVirtual::EasedValue(System.Single,System.Single,System.Single,DG.Tweening.Ease)
extern void DOVirtual_EasedValue_m13B1BCEC7154DBD3C6B63DD66BED84E3ACCB3E92 (void);
// 0x00000060 System.Single DG.Tweening.DOVirtual::EasedValue(System.Single,System.Single,System.Single,DG.Tweening.Ease,System.Single)
extern void DOVirtual_EasedValue_m6A696798DA353122DB77E55A1335ADE48AB7D859 (void);
// 0x00000061 System.Single DG.Tweening.DOVirtual::EasedValue(System.Single,System.Single,System.Single,DG.Tweening.Ease,System.Single,System.Single)
extern void DOVirtual_EasedValue_m1F5EC2D3BB8B8B94802685DE462A870DD1A28A5F (void);
// 0x00000062 System.Single DG.Tweening.DOVirtual::EasedValue(System.Single,System.Single,System.Single,UnityEngine.AnimationCurve)
extern void DOVirtual_EasedValue_m1E515F7D3059B7BA8114C9EBC4429DAEE1CED282 (void);
// 0x00000063 DG.Tweening.Tween DG.Tweening.DOVirtual::DelayedCall(System.Single,DG.Tweening.TweenCallback,System.Boolean)
extern void DOVirtual_DelayedCall_m13A1E292B0CB6E49F2C425ED21DDAB0FC57AC821 (void);
// 0x00000064 DG.Tweening.EaseFunction DG.Tweening.EaseFactory::StopMotion(System.Int32,System.Nullable`1<DG.Tweening.Ease>)
extern void EaseFactory_StopMotion_m5F09AC0389104A282E87BE2A9904E67928E4EECA (void);
// 0x00000065 DG.Tweening.EaseFunction DG.Tweening.EaseFactory::StopMotion(System.Int32,UnityEngine.AnimationCurve)
extern void EaseFactory_StopMotion_m181FC9A0D134C3C1AD008E9CEC9D644E68F77479 (void);
// 0x00000066 DG.Tweening.EaseFunction DG.Tweening.EaseFactory::StopMotion(System.Int32,DG.Tweening.EaseFunction)
extern void EaseFactory_StopMotion_m832C9BB2ED2A253941268BEB89DC0843A888DD12 (void);
// 0x00000067 System.Void DG.Tweening.EaseFactory::.ctor()
extern void EaseFactory__ctor_m555FCC43D592611E808AFE4DA42A0259D182D584 (void);
// 0x00000068 DG.Tweening.IDOTweenInit DG.Tweening.IDOTweenInit::SetCapacity(System.Int32,System.Int32)
// 0x00000069 System.Void DG.Tweening.TweenExtensions::Complete(DG.Tweening.Tween)
extern void TweenExtensions_Complete_m1006012CD6C2AED2F84C6F04C16FF5D6DCEB0188 (void);
// 0x0000006A System.Void DG.Tweening.TweenExtensions::Complete(DG.Tweening.Tween,System.Boolean)
extern void TweenExtensions_Complete_mBB5115AC82EEA28A91F897A767E69D58272EA926 (void);
// 0x0000006B System.Void DG.Tweening.TweenExtensions::Flip(DG.Tweening.Tween)
extern void TweenExtensions_Flip_m53777A3B2C663F559F961A6D90AFBA6910C7EEF1 (void);
// 0x0000006C System.Void DG.Tweening.TweenExtensions::ForceInit(DG.Tweening.Tween)
extern void TweenExtensions_ForceInit_m25AFF7EA6A03406F1080E11292F6C8923E082836 (void);
// 0x0000006D System.Void DG.Tweening.TweenExtensions::Goto(DG.Tweening.Tween,System.Single,System.Boolean)
extern void TweenExtensions_Goto_mFA79DC2B3D9D9B253F4AB7612CFD5E71CA3E41B2 (void);
// 0x0000006E System.Void DG.Tweening.TweenExtensions::Kill(DG.Tweening.Tween,System.Boolean)
extern void TweenExtensions_Kill_m8A79B9D5D31C46E9669C2EFEDF26BF4F7EB02D10 (void);
// 0x0000006F T DG.Tweening.TweenExtensions::Pause(T)
// 0x00000070 T DG.Tweening.TweenExtensions::Play(T)
// 0x00000071 System.Void DG.Tweening.TweenExtensions::PlayBackwards(DG.Tweening.Tween)
extern void TweenExtensions_PlayBackwards_m147A9C8A47E881FA94B137394EF6F4EA53BFD1ED (void);
// 0x00000072 System.Void DG.Tweening.TweenExtensions::PlayForward(DG.Tweening.Tween)
extern void TweenExtensions_PlayForward_m72684DEC2FE49A30A71B00FF442CCBBE3933F547 (void);
// 0x00000073 System.Void DG.Tweening.TweenExtensions::Restart(DG.Tweening.Tween,System.Boolean,System.Single)
extern void TweenExtensions_Restart_mE904E3AB313B9CF8977E8BA3A2A191B821FC95CC (void);
// 0x00000074 System.Void DG.Tweening.TweenExtensions::Rewind(DG.Tweening.Tween,System.Boolean)
extern void TweenExtensions_Rewind_m911EFDC4DAAEB2DC3C80112FE4C14F01BCF558C8 (void);
// 0x00000075 System.Void DG.Tweening.TweenExtensions::SmoothRewind(DG.Tweening.Tween)
extern void TweenExtensions_SmoothRewind_mE58E999B4138952F9B6252DBFC2829E961166922 (void);
// 0x00000076 System.Void DG.Tweening.TweenExtensions::TogglePause(DG.Tweening.Tween)
extern void TweenExtensions_TogglePause_mF6A1C2093B76873D90C8C36DF3E16528B227960B (void);
// 0x00000077 System.Void DG.Tweening.TweenExtensions::GotoWaypoint(DG.Tweening.Tween,System.Int32,System.Boolean)
extern void TweenExtensions_GotoWaypoint_m1D883C58BF3D55F0C88F06E5BD4AFBCFD00C78A4 (void);
// 0x00000078 UnityEngine.YieldInstruction DG.Tweening.TweenExtensions::WaitForCompletion(DG.Tweening.Tween)
extern void TweenExtensions_WaitForCompletion_m19F0941B6FEF13B652521ECE7BCE7742FDC1E925 (void);
// 0x00000079 UnityEngine.YieldInstruction DG.Tweening.TweenExtensions::WaitForRewind(DG.Tweening.Tween)
extern void TweenExtensions_WaitForRewind_m66036030592B041B2CF97D4FEDEED7E3F999E230 (void);
// 0x0000007A UnityEngine.YieldInstruction DG.Tweening.TweenExtensions::WaitForKill(DG.Tweening.Tween)
extern void TweenExtensions_WaitForKill_mA95646AE5DAE73DFEE9D545BCA488AC47D6BDE32 (void);
// 0x0000007B UnityEngine.YieldInstruction DG.Tweening.TweenExtensions::WaitForElapsedLoops(DG.Tweening.Tween,System.Int32)
extern void TweenExtensions_WaitForElapsedLoops_mE72913438B63E226ADCB3A53AD3DF1D91AA2A341 (void);
// 0x0000007C UnityEngine.YieldInstruction DG.Tweening.TweenExtensions::WaitForPosition(DG.Tweening.Tween,System.Single)
extern void TweenExtensions_WaitForPosition_m4B51613BD949DD254307AAA4CD69C63D402833BE (void);
// 0x0000007D UnityEngine.Coroutine DG.Tweening.TweenExtensions::WaitForStart(DG.Tweening.Tween)
extern void TweenExtensions_WaitForStart_m3F13AED0EB2811F6AA57F960ADD807944ADDBBF9 (void);
// 0x0000007E System.Int32 DG.Tweening.TweenExtensions::CompletedLoops(DG.Tweening.Tween)
extern void TweenExtensions_CompletedLoops_m067EA5E0CFEEF70813063DC6336000D02E5C2E87 (void);
// 0x0000007F System.Single DG.Tweening.TweenExtensions::Delay(DG.Tweening.Tween)
extern void TweenExtensions_Delay_m65CC3BC72D7A467348E7873E2DB7600038F0D3B7 (void);
// 0x00000080 System.Single DG.Tweening.TweenExtensions::Duration(DG.Tweening.Tween,System.Boolean)
extern void TweenExtensions_Duration_mB449EF1960C6AC6818CAFAEF32A1E6B1130AC6A0 (void);
// 0x00000081 System.Single DG.Tweening.TweenExtensions::Elapsed(DG.Tweening.Tween,System.Boolean)
extern void TweenExtensions_Elapsed_m63AF61BC4F010D9195A174ED86D42D357753EA80 (void);
// 0x00000082 System.Single DG.Tweening.TweenExtensions::ElapsedPercentage(DG.Tweening.Tween,System.Boolean)
extern void TweenExtensions_ElapsedPercentage_mC12ED1EAF1BEBF089D80B420DC6D987DE9C7C618 (void);
// 0x00000083 System.Single DG.Tweening.TweenExtensions::ElapsedDirectionalPercentage(DG.Tweening.Tween)
extern void TweenExtensions_ElapsedDirectionalPercentage_m478ED527B4F86DCC0240987C317231168587F5DF (void);
// 0x00000084 System.Boolean DG.Tweening.TweenExtensions::IsActive(DG.Tweening.Tween)
extern void TweenExtensions_IsActive_m19E1D79E2590A189C95DADED68FFDB43A7F3B2A2 (void);
// 0x00000085 System.Boolean DG.Tweening.TweenExtensions::IsBackwards(DG.Tweening.Tween)
extern void TweenExtensions_IsBackwards_m8266302C8957C3266BCA3BEF8B5F0A29362BEE0E (void);
// 0x00000086 System.Boolean DG.Tweening.TweenExtensions::IsComplete(DG.Tweening.Tween)
extern void TweenExtensions_IsComplete_mC87F29FE0746C793F0C11175A9B37087C1C436FD (void);
// 0x00000087 System.Boolean DG.Tweening.TweenExtensions::IsInitialized(DG.Tweening.Tween)
extern void TweenExtensions_IsInitialized_mE92123AF4FE069B2F175193367CAFF218B552CEB (void);
// 0x00000088 System.Boolean DG.Tweening.TweenExtensions::IsPlaying(DG.Tweening.Tween)
extern void TweenExtensions_IsPlaying_m3B9AB4389AA78D1EB5384BB33BF24640E0005F6D (void);
// 0x00000089 System.Int32 DG.Tweening.TweenExtensions::Loops(DG.Tweening.Tween)
extern void TweenExtensions_Loops_mF14C87CC213FA4DA5113F6A645BB17577214F1BC (void);
// 0x0000008A UnityEngine.Vector3 DG.Tweening.TweenExtensions::PathGetPoint(DG.Tweening.Tween,System.Single)
extern void TweenExtensions_PathGetPoint_m8DDEC2EABC0BC6B6BC1B0CC6CF396D099C23EDF8 (void);
// 0x0000008B UnityEngine.Vector3[] DG.Tweening.TweenExtensions::PathGetDrawPoints(DG.Tweening.Tween,System.Int32)
extern void TweenExtensions_PathGetDrawPoints_mB97A95B99626040449A5F1B4B4098B4E6D4E6BA9 (void);
// 0x0000008C System.Single DG.Tweening.TweenExtensions::PathLength(DG.Tweening.Tween)
extern void TweenExtensions_PathLength_mCEE0E32D47307DA4C408E9705B8054B540093C82 (void);
// 0x0000008D System.Void DG.Tweening.Sequence::.ctor()
extern void Sequence__ctor_mB97652F05882AFE6EBCB3BE341ED0235B76F6BDF (void);
// 0x0000008E DG.Tweening.Sequence DG.Tweening.Sequence::DoPrepend(DG.Tweening.Sequence,DG.Tweening.Tween)
extern void Sequence_DoPrepend_mA6EC3C2575BFDC705D61EB2C29F0788D047C894C (void);
// 0x0000008F DG.Tweening.Sequence DG.Tweening.Sequence::DoInsert(DG.Tweening.Sequence,DG.Tweening.Tween,System.Single)
extern void Sequence_DoInsert_mA14376970EB381F8BB2F8E2BFA81DAEB2862D388 (void);
// 0x00000090 DG.Tweening.Sequence DG.Tweening.Sequence::DoAppendInterval(DG.Tweening.Sequence,System.Single)
extern void Sequence_DoAppendInterval_mD26D4D1EF50CC0D718586F2CD18852ED5919AAA2 (void);
// 0x00000091 DG.Tweening.Sequence DG.Tweening.Sequence::DoPrependInterval(DG.Tweening.Sequence,System.Single)
extern void Sequence_DoPrependInterval_m20FBC2903E466DD9E7033048FC274DB44D538B7B (void);
// 0x00000092 DG.Tweening.Sequence DG.Tweening.Sequence::DoInsertCallback(DG.Tweening.Sequence,DG.Tweening.TweenCallback,System.Single)
extern void Sequence_DoInsertCallback_m6A7A44765BDED125C4ADB7459F919F37C0825464 (void);
// 0x00000093 System.Void DG.Tweening.Sequence::Reset()
extern void Sequence_Reset_mC4CAA33EF1548745C6D6E784B0BF4D047B3AC188 (void);
// 0x00000094 System.Boolean DG.Tweening.Sequence::Validate()
extern void Sequence_Validate_m6C36B3159599DD8A0B504F65F7494E95396F48B9 (void);
// 0x00000095 System.Boolean DG.Tweening.Sequence::Startup()
extern void Sequence_Startup_m98E75C675CFE20E96A276E828F125F45904DA6AB (void);
// 0x00000096 System.Boolean DG.Tweening.Sequence::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern void Sequence_ApplyTween_m077AFE4C3C99A0108A5893284FD7A4632C354BDC (void);
// 0x00000097 System.Void DG.Tweening.Sequence::Setup(DG.Tweening.Sequence)
extern void Sequence_Setup_m49343758A31BC70721944DD7DCB9E3F38B31705F (void);
// 0x00000098 System.Boolean DG.Tweening.Sequence::DoStartup(DG.Tweening.Sequence)
extern void Sequence_DoStartup_mA896E541099EBE22A115A80C2C6DAB946B110253 (void);
// 0x00000099 System.Boolean DG.Tweening.Sequence::DoApplyTween(DG.Tweening.Sequence,System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode)
extern void Sequence_DoApplyTween_m60CCB3B5AE0771450BC94D8BD8D6B997B563CFC1 (void);
// 0x0000009A System.Boolean DG.Tweening.Sequence::ApplyInternalCycle(DG.Tweening.Sequence,System.Single,System.Single,DG.Tweening.Core.Enums.UpdateMode,System.Boolean,System.Boolean,System.Boolean)
extern void Sequence_ApplyInternalCycle_m0270BA69058C771E554F7FB1FB9EF0EE1684FCC3 (void);
// 0x0000009B System.Int32 DG.Tweening.Sequence::SortSequencedObjs(DG.Tweening.Core.ABSSequentiable,DG.Tweening.Core.ABSSequentiable)
extern void Sequence_SortSequencedObjs_m3AEF11D1B76EC1AA086F2EAD5D71DAFCC7643E46 (void);
// 0x0000009C DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFade(UnityEngine.AudioSource,System.Single,System.Single)
extern void ShortcutExtensions_DOFade_m3AC0CE0E603F8F3A55796F07C2AB3C68A9F480AF (void);
// 0x0000009D DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPitch(UnityEngine.AudioSource,System.Single,System.Single)
extern void ShortcutExtensions_DOPitch_m105F8A473434A2950C30CC3ABDB8FFD1B96AB55F (void);
// 0x0000009E DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOAspect(UnityEngine.Camera,System.Single,System.Single)
extern void ShortcutExtensions_DOAspect_mCD85FE3D0E67452AC9682825549AD6C973417F70 (void);
// 0x0000009F DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Camera,UnityEngine.Color,System.Single)
extern void ShortcutExtensions_DOColor_m406B84ECF991C880D866C555B3AE4DF41274D203 (void);
// 0x000000A0 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFarClipPlane(UnityEngine.Camera,System.Single,System.Single)
extern void ShortcutExtensions_DOFarClipPlane_m0956009131C00C1D3B6ABF39C23EEFB29509A913 (void);
// 0x000000A1 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFieldOfView(UnityEngine.Camera,System.Single,System.Single)
extern void ShortcutExtensions_DOFieldOfView_m2FDB010FDA9B4164709FE0711B349C64FD73B498 (void);
// 0x000000A2 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DONearClipPlane(UnityEngine.Camera,System.Single,System.Single)
extern void ShortcutExtensions_DONearClipPlane_mD6C4FD5A86B7D8E512D412FC687B564F987B8B2E (void);
// 0x000000A3 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOOrthoSize(UnityEngine.Camera,System.Single,System.Single)
extern void ShortcutExtensions_DOOrthoSize_m6B046F2A9CB65F5C33F7E05E404C81BE5FCC1468 (void);
// 0x000000A4 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPixelRect(UnityEngine.Camera,UnityEngine.Rect,System.Single)
extern void ShortcutExtensions_DOPixelRect_m18CCEED227266203D749048A9959857D56EBDEEB (void);
// 0x000000A5 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORect(UnityEngine.Camera,UnityEngine.Rect,System.Single)
extern void ShortcutExtensions_DORect_m9D52CABC1C9DACDFE1EF0FB52D22BE293536B39E (void);
// 0x000000A6 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakePosition(UnityEngine.Camera,System.Single,System.Single,System.Int32,System.Single,System.Boolean)
extern void ShortcutExtensions_DOShakePosition_mE45C809671BA1C7EAFD000988706740A530AA1FA (void);
// 0x000000A7 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakePosition(UnityEngine.Camera,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean)
extern void ShortcutExtensions_DOShakePosition_mD9DF263BD711C7BC12E152F014ED9998A6622FFA (void);
// 0x000000A8 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeRotation(UnityEngine.Camera,System.Single,System.Single,System.Int32,System.Single,System.Boolean)
extern void ShortcutExtensions_DOShakeRotation_m1DE1BCBE8A8E4B236206932EE171B894F9ABDC80 (void);
// 0x000000A9 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeRotation(UnityEngine.Camera,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean)
extern void ShortcutExtensions_DOShakeRotation_m09BA15DFC0073FCB0B0C4DC820B91BC5723C9E51 (void);
// 0x000000AA DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Light,UnityEngine.Color,System.Single)
extern void ShortcutExtensions_DOColor_mE6D48DDA2B16225D9DBF40D67D8716992016BE5E (void);
// 0x000000AB DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOIntensity(UnityEngine.Light,System.Single,System.Single)
extern void ShortcutExtensions_DOIntensity_m4F6889CB6C25B1B30099075C7AC9F6E3B24FC080 (void);
// 0x000000AC DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShadowStrength(UnityEngine.Light,System.Single,System.Single)
extern void ShortcutExtensions_DOShadowStrength_mFA55E1DB84B84DF7DD91BB12E75225630A4BE3C7 (void);
// 0x000000AD DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.LineRenderer,DG.Tweening.Color2,DG.Tweening.Color2,System.Single)
extern void ShortcutExtensions_DOColor_m95F7C9CF81DFA68FDC0048E17587A94E0AF83828 (void);
// 0x000000AE DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Material,UnityEngine.Color,System.Single)
extern void ShortcutExtensions_DOColor_mE6140CF9E1B3D3B559F6751F9B8C3B1602826710 (void);
// 0x000000AF DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Material,UnityEngine.Color,System.String,System.Single)
extern void ShortcutExtensions_DOColor_mDA1E755EC546D05D85BB8AB560DBE5D7F0BB2F03 (void);
// 0x000000B0 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFade(UnityEngine.Material,System.Single,System.Single)
extern void ShortcutExtensions_DOFade_mB5B3CAC06223B35C8E666ADC139D93FF17A31C48 (void);
// 0x000000B1 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFade(UnityEngine.Material,System.Single,System.String,System.Single)
extern void ShortcutExtensions_DOFade_m26A4C55A80B031F02DBE0E88980E528FE5A5D0AF (void);
// 0x000000B2 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFloat(UnityEngine.Material,System.Single,System.String,System.Single)
extern void ShortcutExtensions_DOFloat_m680DEA336497C8D581C407635DFDB4C98E110981 (void);
// 0x000000B3 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOOffset(UnityEngine.Material,UnityEngine.Vector2,System.Single)
extern void ShortcutExtensions_DOOffset_m86FF955A9303A7AA5719B0AC2552A65096FB2071 (void);
// 0x000000B4 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOOffset(UnityEngine.Material,UnityEngine.Vector2,System.String,System.Single)
extern void ShortcutExtensions_DOOffset_mF11F80175BBC7A3F9506A1B926DF993306154C0C (void);
// 0x000000B5 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOTiling(UnityEngine.Material,UnityEngine.Vector2,System.Single)
extern void ShortcutExtensions_DOTiling_m1A4955BC0ED3D1F77EC53FAEF2EBF114CDC38DE8 (void);
// 0x000000B6 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOTiling(UnityEngine.Material,UnityEngine.Vector2,System.String,System.Single)
extern void ShortcutExtensions_DOTiling_m92B0C39EAB8BF112EAF1108D0FFDCCD63DE4734F (void);
// 0x000000B7 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOVector(UnityEngine.Material,UnityEngine.Vector4,System.String,System.Single)
extern void ShortcutExtensions_DOVector_mD77F3BC35D05D7FE67869842889048EE5C33F57D (void);
// 0x000000B8 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMove(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Boolean)
extern void ShortcutExtensions_DOMove_mD9817CCAE0681D620067D21B4E868428FE2533D8 (void);
// 0x000000B9 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveX(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void ShortcutExtensions_DOMoveX_mAFE787202DE6B5E0DDFC2EED98DBA533040C4A10 (void);
// 0x000000BA DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveY(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void ShortcutExtensions_DOMoveY_m5A82F6418B09E4930707934723C4DA82C7D802C3 (void);
// 0x000000BB DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveZ(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void ShortcutExtensions_DOMoveZ_m07BB95E8FFBD4CB8DB710B5771F88FE8F57211B4 (void);
// 0x000000BC DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORotate(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern void ShortcutExtensions_DORotate_m578E0422C09350DCB3298AA920B6F9684BF09CAD (void);
// 0x000000BD DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLookAt(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.AxisConstraint,System.Nullable`1<UnityEngine.Vector3>)
extern void ShortcutExtensions_DOLookAt_m77EF2702C9BC226F7BC85636AE000C661657A114 (void);
// 0x000000BE DG.Tweening.Sequence DG.Tweening.ShortcutExtensions::DOJump(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern void ShortcutExtensions_DOJump_mBFA6A1181AC16A62DC1F587E8593D6AA2C816293 (void);
// 0x000000BF DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void ShortcutExtensions_DOPath_mDDB1604C7FB47EB928B6BC63A1DD7CF9A85D3164 (void);
// 0x000000C0 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOLocalPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void ShortcutExtensions_DOLocalPath_mC7A563E0098ACD6BECF7380308E9507831696FDA (void);
// 0x000000C1 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void ShortcutExtensions_DOPath_m01EDCCA0EE21B7E554FF28BEA409198E50CFD1FC (void);
// 0x000000C2 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOLocalPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void ShortcutExtensions_DOLocalPath_mE0136EA4E054DF88A8322A664D3CCEFAFABEAE4C (void);
// 0x000000C3 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOResize(UnityEngine.TrailRenderer,System.Single,System.Single,System.Single)
extern void ShortcutExtensions_DOResize_mA7C52079F18C8A5344776456730E4C776757B86B (void);
// 0x000000C4 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOTime(UnityEngine.TrailRenderer,System.Single,System.Single)
extern void ShortcutExtensions_DOTime_m69CE5A2BEC68B0F0BF8528BBAAD49B4A68C6A3CB (void);
// 0x000000C5 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern void ShortcutExtensions_DOMove_mA83A2C1D4DFC55CB4D2B2763FD1CF8D4ADCD634D (void);
// 0x000000C6 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveX(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern void ShortcutExtensions_DOMoveX_m5C08EC5E31AB6EE00A812577014611B581402B5F (void);
// 0x000000C7 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveY(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern void ShortcutExtensions_DOMoveY_m27EE0286BBFE2B1586BE88A660B15B55C907A27C (void);
// 0x000000C8 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveZ(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern void ShortcutExtensions_DOMoveZ_m7CAC2050DB7E8E421FC2682C3046A4284ACEA754 (void);
// 0x000000C9 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern void ShortcutExtensions_DOLocalMove_m0F6957D951BA19FEA0A48C7CDA52240AC7CD9369 (void);
// 0x000000CA DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalMoveX(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern void ShortcutExtensions_DOLocalMoveX_m2003324ABEB7FCE822E7A47F9E92F4ABE1CA29CC (void);
// 0x000000CB DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalMoveY(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern void ShortcutExtensions_DOLocalMoveY_m180E4334F1CC2C6E1BE72ECE2DE8B21DD4AA9469 (void);
// 0x000000CC DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalMoveZ(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern void ShortcutExtensions_DOLocalMoveZ_m68386131CD5762AAAF410F045F6AAEF8835A05CB (void);
// 0x000000CD DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern void ShortcutExtensions_DORotate_mBC72ACEECA2351B7CE6491E5FE0B76469609DEFD (void);
// 0x000000CE DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORotateQuaternion(UnityEngine.Transform,UnityEngine.Quaternion,System.Single)
extern void ShortcutExtensions_DORotateQuaternion_m2BDDEBEAC8361BE98535A1A1B67F6C7FA88E1A47 (void);
// 0x000000CF DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalRotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern void ShortcutExtensions_DOLocalRotate_m0A3A0561A8DFA471BCA68891686EE51BD6CD2F28 (void);
// 0x000000D0 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalRotateQuaternion(UnityEngine.Transform,UnityEngine.Quaternion,System.Single)
extern void ShortcutExtensions_DOLocalRotateQuaternion_m287F445C8974F85EE58DF0752443EF0258716F09 (void);
// 0x000000D1 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void ShortcutExtensions_DOScale_m68874CDE5A501CD5EBFD9021D689A8E3029A7797 (void);
// 0x000000D2 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScale(UnityEngine.Transform,System.Single,System.Single)
extern void ShortcutExtensions_DOScale_m295B0D8C9518B001C1B6A9A77FBF7A4A6621488F (void);
// 0x000000D3 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScaleX(UnityEngine.Transform,System.Single,System.Single)
extern void ShortcutExtensions_DOScaleX_m9665F244A1551D37A9B7F089DD5591C45D67D0A9 (void);
// 0x000000D4 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScaleY(UnityEngine.Transform,System.Single,System.Single)
extern void ShortcutExtensions_DOScaleY_mE82DE29207C5EEC076B9CDB1E08001D4336A9923 (void);
// 0x000000D5 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScaleZ(UnityEngine.Transform,System.Single,System.Single)
extern void ShortcutExtensions_DOScaleZ_mC574D66C01E03AE4C8DFBC7381CA6F1A0AE3D2CF (void);
// 0x000000D6 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLookAt(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.AxisConstraint,System.Nullable`1<UnityEngine.Vector3>)
extern void ShortcutExtensions_DOLookAt_mDAA64D1B1CD9AFC1E351C021602C3BE102CC548B (void);
// 0x000000D7 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPunchPosition(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern void ShortcutExtensions_DOPunchPosition_m9EEF18AFCE150A50A4F79AA85982B9CE70DAD0DA (void);
// 0x000000D8 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPunchScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern void ShortcutExtensions_DOPunchScale_m351E39CC16A4CC9692955C502F350D56825B17E4 (void);
// 0x000000D9 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPunchRotation(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern void ShortcutExtensions_DOPunchRotation_mC1D5DA0D6218A579D04BB373DDF1264E0AC4736A (void);
// 0x000000DA DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakePosition(UnityEngine.Transform,System.Single,System.Single,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void ShortcutExtensions_DOShakePosition_mEC6EF5C734CA59D5C7A7808204E380B5E1C25EF5 (void);
// 0x000000DB DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakePosition(UnityEngine.Transform,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void ShortcutExtensions_DOShakePosition_mDEC0E682E9A8D470D6FD1F825161C3E6087DC3B8 (void);
// 0x000000DC DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeRotation(UnityEngine.Transform,System.Single,System.Single,System.Int32,System.Single,System.Boolean)
extern void ShortcutExtensions_DOShakeRotation_m290A50BA6F51EFC713882E1CFCE747B4B932FCB6 (void);
// 0x000000DD DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeRotation(UnityEngine.Transform,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean)
extern void ShortcutExtensions_DOShakeRotation_m35DC1BCA976A9A2E1C7A6CA4359EB5F30695F1EE (void);
// 0x000000DE DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeScale(UnityEngine.Transform,System.Single,System.Single,System.Int32,System.Single,System.Boolean)
extern void ShortcutExtensions_DOShakeScale_mB136215C0FAAD4193DD01A5917384145C0368231 (void);
// 0x000000DF DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeScale(UnityEngine.Transform,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean)
extern void ShortcutExtensions_DOShakeScale_m39430B83048A4F601B8B206224E792F7F075D22F (void);
// 0x000000E0 DG.Tweening.Sequence DG.Tweening.ShortcutExtensions::DOJump(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern void ShortcutExtensions_DOJump_m65381EFDC430C4FA504C2656A4E302D6844CBECB (void);
// 0x000000E1 DG.Tweening.Sequence DG.Tweening.ShortcutExtensions::DOLocalJump(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern void ShortcutExtensions_DOLocalJump_mA83D2B1991F192D59F5953D1D5FC64ADBF728037 (void);
// 0x000000E2 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOPath(UnityEngine.Transform,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void ShortcutExtensions_DOPath_mCEB7849F32E0448CF8ACFEE2871F11796D4A86F2 (void);
// 0x000000E3 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOLocalPath(UnityEngine.Transform,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void ShortcutExtensions_DOLocalPath_m38EEE83BE829F79550424AD767CFCA1862E615BE (void);
// 0x000000E4 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOPath(UnityEngine.Transform,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void ShortcutExtensions_DOPath_m7AB84619455A541B79164E1D270A5AB511533B24 (void);
// 0x000000E5 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOLocalPath(UnityEngine.Transform,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void ShortcutExtensions_DOLocalPath_m28185CD9787640FBB679A0B46F144E1BB0DB43F8 (void);
// 0x000000E6 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOTimeScale(DG.Tweening.Tween,System.Single,System.Single)
extern void ShortcutExtensions_DOTimeScale_m1776BEFED215BFE01F622383F7AF35A99FA972BF (void);
// 0x000000E7 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOBlendableColor(UnityEngine.Light,UnityEngine.Color,System.Single)
extern void ShortcutExtensions_DOBlendableColor_mA127ED765FB1A2BA042A9C91A678AAAC279F493F (void);
// 0x000000E8 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOBlendableColor(UnityEngine.Material,UnityEngine.Color,System.Single)
extern void ShortcutExtensions_DOBlendableColor_mA8441733C425378F3A3D1F4FFAE7A9F940DB0147 (void);
// 0x000000E9 DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOBlendableColor(UnityEngine.Material,UnityEngine.Color,System.String,System.Single)
extern void ShortcutExtensions_DOBlendableColor_m41015F46280C6C7E8649140192B28CD0E8D4DDD3 (void);
// 0x000000EA DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOBlendableMoveBy(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern void ShortcutExtensions_DOBlendableMoveBy_m5EF65D43D7282E6CD69AB3470DDECF36EF85D96E (void);
// 0x000000EB DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOBlendableLocalMoveBy(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern void ShortcutExtensions_DOBlendableLocalMoveBy_m305FC1B99A29FB0B2A114E402906F61DE36DB63D (void);
// 0x000000EC DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOBlendableRotateBy(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern void ShortcutExtensions_DOBlendableRotateBy_m2B26C8D41D59A77FE5315731CF33214DE82A4C59 (void);
// 0x000000ED DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOBlendableLocalRotateBy(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern void ShortcutExtensions_DOBlendableLocalRotateBy_mA0ADE3AD501FE8B31B14B68C78C4BAEEA709342F (void);
// 0x000000EE DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOBlendableScaleBy(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void ShortcutExtensions_DOBlendableScaleBy_m8F7EB3E007F5AC240CC5C483EB44BC0524C9A578 (void);
// 0x000000EF System.Int32 DG.Tweening.ShortcutExtensions::DOComplete(UnityEngine.Component,System.Boolean)
extern void ShortcutExtensions_DOComplete_m1939237DDDE3FA508E979D2AE3DC21523918EE40 (void);
// 0x000000F0 System.Int32 DG.Tweening.ShortcutExtensions::DOComplete(UnityEngine.Material,System.Boolean)
extern void ShortcutExtensions_DOComplete_mB2018D4F1EEACAA7B3F43262CE9F1E3D6BCEC693 (void);
// 0x000000F1 System.Int32 DG.Tweening.ShortcutExtensions::DOKill(UnityEngine.Component,System.Boolean)
extern void ShortcutExtensions_DOKill_m4E7F53B09D2B06D72DE5783E8BD6AB995E31F390 (void);
// 0x000000F2 System.Int32 DG.Tweening.ShortcutExtensions::DOKill(UnityEngine.Material,System.Boolean)
extern void ShortcutExtensions_DOKill_m6C9A4C28424CA62C5EF0A9DF28D1DF8082708BD8 (void);
// 0x000000F3 System.Int32 DG.Tweening.ShortcutExtensions::DOFlip(UnityEngine.Component)
extern void ShortcutExtensions_DOFlip_m88EB2005DB078C4E3BF3D8C6714A3A0F2E529587 (void);
// 0x000000F4 System.Int32 DG.Tweening.ShortcutExtensions::DOFlip(UnityEngine.Material)
extern void ShortcutExtensions_DOFlip_m9F4FA956DDCD67EDBBF5160C2E2718DF55733358 (void);
// 0x000000F5 System.Int32 DG.Tweening.ShortcutExtensions::DOGoto(UnityEngine.Component,System.Single,System.Boolean)
extern void ShortcutExtensions_DOGoto_mB9FEB565E815B21A5C66B3EB71C1EBA36894F46B (void);
// 0x000000F6 System.Int32 DG.Tweening.ShortcutExtensions::DOGoto(UnityEngine.Material,System.Single,System.Boolean)
extern void ShortcutExtensions_DOGoto_mBA4DBCFA8B1BEC3B2D9D5757331843042CABF6CA (void);
// 0x000000F7 System.Int32 DG.Tweening.ShortcutExtensions::DOPause(UnityEngine.Component)
extern void ShortcutExtensions_DOPause_m2A116931A03160BE68769455EB747AF8DA491836 (void);
// 0x000000F8 System.Int32 DG.Tweening.ShortcutExtensions::DOPause(UnityEngine.Material)
extern void ShortcutExtensions_DOPause_mFC159FACC09AF2E17B76F8AF4412F57A84786189 (void);
// 0x000000F9 System.Int32 DG.Tweening.ShortcutExtensions::DOPlay(UnityEngine.Component)
extern void ShortcutExtensions_DOPlay_m5B3FFE8CE9F5A2FCADDA69A3D1DA22D733E126E3 (void);
// 0x000000FA System.Int32 DG.Tweening.ShortcutExtensions::DOPlay(UnityEngine.Material)
extern void ShortcutExtensions_DOPlay_m01B0C62F69368262EDD6E6C03865B3DBBAA80B3B (void);
// 0x000000FB System.Int32 DG.Tweening.ShortcutExtensions::DOPlayBackwards(UnityEngine.Component)
extern void ShortcutExtensions_DOPlayBackwards_mAF952391CB3262C21B8DC73C34084F5788EDC54B (void);
// 0x000000FC System.Int32 DG.Tweening.ShortcutExtensions::DOPlayBackwards(UnityEngine.Material)
extern void ShortcutExtensions_DOPlayBackwards_mC6F2B1B13752B08CCD27DB1B0532207C5482DB85 (void);
// 0x000000FD System.Int32 DG.Tweening.ShortcutExtensions::DOPlayForward(UnityEngine.Component)
extern void ShortcutExtensions_DOPlayForward_m1F20FCE79D40810AE5E361925CE872790D43025A (void);
// 0x000000FE System.Int32 DG.Tweening.ShortcutExtensions::DOPlayForward(UnityEngine.Material)
extern void ShortcutExtensions_DOPlayForward_m5147ABDF037463B1268132EB003F81BE1FDC2B0D (void);
// 0x000000FF System.Int32 DG.Tweening.ShortcutExtensions::DORestart(UnityEngine.Component,System.Boolean)
extern void ShortcutExtensions_DORestart_mC1F7568D09A231DA92F1E997CCC2C4A00DC753C3 (void);
// 0x00000100 System.Int32 DG.Tweening.ShortcutExtensions::DORestart(UnityEngine.Material,System.Boolean)
extern void ShortcutExtensions_DORestart_mBB4474BF78311CF0497452FDECC3A78515ECC28A (void);
// 0x00000101 System.Int32 DG.Tweening.ShortcutExtensions::DORewind(UnityEngine.Component,System.Boolean)
extern void ShortcutExtensions_DORewind_m323E06FBD4B7B6F871A4D2F24D345D126824F6E9 (void);
// 0x00000102 System.Int32 DG.Tweening.ShortcutExtensions::DORewind(UnityEngine.Material,System.Boolean)
extern void ShortcutExtensions_DORewind_m733779A82524CD33F9602588C940048C18A03920 (void);
// 0x00000103 System.Int32 DG.Tweening.ShortcutExtensions::DOSmoothRewind(UnityEngine.Component)
extern void ShortcutExtensions_DOSmoothRewind_m60199BDB37313EFFA85AAB25B5C280AEFA9DA436 (void);
// 0x00000104 System.Int32 DG.Tweening.ShortcutExtensions::DOSmoothRewind(UnityEngine.Material)
extern void ShortcutExtensions_DOSmoothRewind_mA39884D144593E20BB05E04A1FF62E7573A3DFAB (void);
// 0x00000105 System.Int32 DG.Tweening.ShortcutExtensions::DOTogglePause(UnityEngine.Component)
extern void ShortcutExtensions_DOTogglePause_m95176BDF60314FD5D78055896D36C8573F93698D (void);
// 0x00000106 System.Int32 DG.Tweening.ShortcutExtensions::DOTogglePause(UnityEngine.Material)
extern void ShortcutExtensions_DOTogglePause_m6A532FF78795DAB72A338B410F7D024874F526B4 (void);
// 0x00000107 System.Void DG.Tweening.TweenParams::.ctor()
extern void TweenParams__ctor_m589B6753CE877021D50DEBEBA01547FCC034390E (void);
// 0x00000108 DG.Tweening.TweenParams DG.Tweening.TweenParams::Clear()
extern void TweenParams_Clear_m792BD1F41E63AB9B9F7F233587AFB8B72A0DCC96 (void);
// 0x00000109 DG.Tweening.TweenParams DG.Tweening.TweenParams::SetAutoKill(System.Boolean)
extern void TweenParams_SetAutoKill_mE910C0A5AC235EA85C13C57716DEF5F2B127C00A (void);
// 0x0000010A DG.Tweening.TweenParams DG.Tweening.TweenParams::SetId(System.Object)
extern void TweenParams_SetId_mD27AC52C8DC84C10AB00BC4D8F494A6A6201E859 (void);
// 0x0000010B DG.Tweening.TweenParams DG.Tweening.TweenParams::SetTarget(System.Object)
extern void TweenParams_SetTarget_m53C0B155B1AFD8D6210F1B6032AD26D6A75E5C4E (void);
// 0x0000010C DG.Tweening.TweenParams DG.Tweening.TweenParams::SetLoops(System.Int32,System.Nullable`1<DG.Tweening.LoopType>)
extern void TweenParams_SetLoops_mA9379F549A00CE93725E8D338475FCEBE4151363 (void);
// 0x0000010D DG.Tweening.TweenParams DG.Tweening.TweenParams::SetEase(DG.Tweening.Ease,System.Nullable`1<System.Single>,System.Nullable`1<System.Single>)
extern void TweenParams_SetEase_mAC8EDCE42F478585B45FF89776DC8C0A311F2D2C (void);
// 0x0000010E DG.Tweening.TweenParams DG.Tweening.TweenParams::SetEase(UnityEngine.AnimationCurve)
extern void TweenParams_SetEase_mA67B49600782A4A4059DD8CD0706CCC115A51C92 (void);
// 0x0000010F DG.Tweening.TweenParams DG.Tweening.TweenParams::SetEase(DG.Tweening.EaseFunction)
extern void TweenParams_SetEase_m03A3B2C45E9B8A8A2C0F950D0E8BF525A05C96C1 (void);
// 0x00000110 DG.Tweening.TweenParams DG.Tweening.TweenParams::SetRecyclable(System.Boolean)
extern void TweenParams_SetRecyclable_mED85748EA433BCD5FC5145EF7AEA08CA4232B561 (void);
// 0x00000111 DG.Tweening.TweenParams DG.Tweening.TweenParams::SetUpdate(System.Boolean)
extern void TweenParams_SetUpdate_m83AEE08A5DE29BAB8E794745D31BC4D11C85E5D5 (void);
// 0x00000112 DG.Tweening.TweenParams DG.Tweening.TweenParams::SetUpdate(DG.Tweening.UpdateType,System.Boolean)
extern void TweenParams_SetUpdate_m70D5E34B2FA26E77410B6052E1493F2CA9EB4801 (void);
// 0x00000113 DG.Tweening.TweenParams DG.Tweening.TweenParams::OnStart(DG.Tweening.TweenCallback)
extern void TweenParams_OnStart_mA04D5C6DD9DC6F41CEC4A7DD668412E4627B16FD (void);
// 0x00000114 DG.Tweening.TweenParams DG.Tweening.TweenParams::OnPlay(DG.Tweening.TweenCallback)
extern void TweenParams_OnPlay_mC4DAA365F93C267F7C2221D3665173D44277CA09 (void);
// 0x00000115 DG.Tweening.TweenParams DG.Tweening.TweenParams::OnRewind(DG.Tweening.TweenCallback)
extern void TweenParams_OnRewind_mA62E100E3C37E25FAABDB549D10F2D846EF60F4B (void);
// 0x00000116 DG.Tweening.TweenParams DG.Tweening.TweenParams::OnUpdate(DG.Tweening.TweenCallback)
extern void TweenParams_OnUpdate_m0D2D0228CAE9AC0507966A514A82C5E800FFD7BF (void);
// 0x00000117 DG.Tweening.TweenParams DG.Tweening.TweenParams::OnStepComplete(DG.Tweening.TweenCallback)
extern void TweenParams_OnStepComplete_m0E67E4983C0996D255554BED258077F1D030A760 (void);
// 0x00000118 DG.Tweening.TweenParams DG.Tweening.TweenParams::OnComplete(DG.Tweening.TweenCallback)
extern void TweenParams_OnComplete_mE505E8B0CC24806D405CEB9AFF84BBE7CFDDEDCC (void);
// 0x00000119 DG.Tweening.TweenParams DG.Tweening.TweenParams::OnKill(DG.Tweening.TweenCallback)
extern void TweenParams_OnKill_m208E98C37585E85220368BA9D6550FA8D201A047 (void);
// 0x0000011A DG.Tweening.TweenParams DG.Tweening.TweenParams::OnWaypointChange(DG.Tweening.TweenCallback`1<System.Int32>)
extern void TweenParams_OnWaypointChange_m6017D8DDA59369CB4BB8B536E09E423F6770AC73 (void);
// 0x0000011B DG.Tweening.TweenParams DG.Tweening.TweenParams::SetDelay(System.Single)
extern void TweenParams_SetDelay_m2951738A16FDDE2811506667FA903D5E61E9F849 (void);
// 0x0000011C DG.Tweening.TweenParams DG.Tweening.TweenParams::SetRelative(System.Boolean)
extern void TweenParams_SetRelative_m546D5B90ECBC703544C670FE0492E56E4A7B3670 (void);
// 0x0000011D DG.Tweening.TweenParams DG.Tweening.TweenParams::SetSpeedBased(System.Boolean)
extern void TweenParams_SetSpeedBased_m792B2EA71D0B5D0076CFBA38D34D7C2011A12A45 (void);
// 0x0000011E System.Void DG.Tweening.TweenParams::.cctor()
extern void TweenParams__cctor_mF42A94E49D0F826044A137B4825830A1A71AA862 (void);
// 0x0000011F T DG.Tweening.TweenSettingsExtensions::SetAutoKill(T)
// 0x00000120 T DG.Tweening.TweenSettingsExtensions::SetAutoKill(T,System.Boolean)
// 0x00000121 T DG.Tweening.TweenSettingsExtensions::SetId(T,System.Object)
// 0x00000122 T DG.Tweening.TweenSettingsExtensions::SetTarget(T,System.Object)
// 0x00000123 T DG.Tweening.TweenSettingsExtensions::SetLoops(T,System.Int32)
// 0x00000124 T DG.Tweening.TweenSettingsExtensions::SetLoops(T,System.Int32,DG.Tweening.LoopType)
// 0x00000125 T DG.Tweening.TweenSettingsExtensions::SetEase(T,DG.Tweening.Ease)
// 0x00000126 T DG.Tweening.TweenSettingsExtensions::SetEase(T,DG.Tweening.Ease,System.Single)
// 0x00000127 T DG.Tweening.TweenSettingsExtensions::SetEase(T,DG.Tweening.Ease,System.Single,System.Single)
// 0x00000128 T DG.Tweening.TweenSettingsExtensions::SetEase(T,UnityEngine.AnimationCurve)
// 0x00000129 T DG.Tweening.TweenSettingsExtensions::SetEase(T,DG.Tweening.EaseFunction)
// 0x0000012A T DG.Tweening.TweenSettingsExtensions::SetRecyclable(T)
// 0x0000012B T DG.Tweening.TweenSettingsExtensions::SetRecyclable(T,System.Boolean)
// 0x0000012C T DG.Tweening.TweenSettingsExtensions::SetUpdate(T,System.Boolean)
// 0x0000012D T DG.Tweening.TweenSettingsExtensions::SetUpdate(T,DG.Tweening.UpdateType)
// 0x0000012E T DG.Tweening.TweenSettingsExtensions::SetUpdate(T,DG.Tweening.UpdateType,System.Boolean)
// 0x0000012F T DG.Tweening.TweenSettingsExtensions::OnStart(T,DG.Tweening.TweenCallback)
// 0x00000130 T DG.Tweening.TweenSettingsExtensions::OnPlay(T,DG.Tweening.TweenCallback)
// 0x00000131 T DG.Tweening.TweenSettingsExtensions::OnPause(T,DG.Tweening.TweenCallback)
// 0x00000132 T DG.Tweening.TweenSettingsExtensions::OnRewind(T,DG.Tweening.TweenCallback)
// 0x00000133 T DG.Tweening.TweenSettingsExtensions::OnUpdate(T,DG.Tweening.TweenCallback)
// 0x00000134 T DG.Tweening.TweenSettingsExtensions::OnStepComplete(T,DG.Tweening.TweenCallback)
// 0x00000135 T DG.Tweening.TweenSettingsExtensions::OnComplete(T,DG.Tweening.TweenCallback)
// 0x00000136 T DG.Tweening.TweenSettingsExtensions::OnKill(T,DG.Tweening.TweenCallback)
// 0x00000137 T DG.Tweening.TweenSettingsExtensions::OnWaypointChange(T,DG.Tweening.TweenCallback`1<System.Int32>)
// 0x00000138 T DG.Tweening.TweenSettingsExtensions::SetAs(T,DG.Tweening.Tween)
// 0x00000139 T DG.Tweening.TweenSettingsExtensions::SetAs(T,DG.Tweening.TweenParams)
// 0x0000013A DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Append(DG.Tweening.Sequence,DG.Tweening.Tween)
extern void TweenSettingsExtensions_Append_m045B3A5C557D2007A05A55F1D5B86A26AA5F13D5 (void);
// 0x0000013B DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Prepend(DG.Tweening.Sequence,DG.Tweening.Tween)
extern void TweenSettingsExtensions_Prepend_mA5ACFDDF27607E1D6A1556A18C4467099A25ABDD (void);
// 0x0000013C DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Join(DG.Tweening.Sequence,DG.Tweening.Tween)
extern void TweenSettingsExtensions_Join_m33E665745C1F091AAD10AD2C3FDD91017861D89F (void);
// 0x0000013D DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Insert(DG.Tweening.Sequence,System.Single,DG.Tweening.Tween)
extern void TweenSettingsExtensions_Insert_m6C8C4CAFC8CC4C5F2120879AE79BD01E3903422A (void);
// 0x0000013E DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::AppendInterval(DG.Tweening.Sequence,System.Single)
extern void TweenSettingsExtensions_AppendInterval_m20E76B7FD0B9E0FD85D5E55444177E412D2A39FE (void);
// 0x0000013F DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::PrependInterval(DG.Tweening.Sequence,System.Single)
extern void TweenSettingsExtensions_PrependInterval_mF91C39268775A123FCCE526262C391CB826B8B9B (void);
// 0x00000140 DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::AppendCallback(DG.Tweening.Sequence,DG.Tweening.TweenCallback)
extern void TweenSettingsExtensions_AppendCallback_m056A9F025C67DB07A403E70BED916DCDDF8671E6 (void);
// 0x00000141 DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::PrependCallback(DG.Tweening.Sequence,DG.Tweening.TweenCallback)
extern void TweenSettingsExtensions_PrependCallback_m430A6A2B38D293E34439857D757394BAF37D9EEA (void);
// 0x00000142 DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::InsertCallback(DG.Tweening.Sequence,System.Single,DG.Tweening.TweenCallback)
extern void TweenSettingsExtensions_InsertCallback_mCEC5B490A95472ED358A6A930F69941843B21826 (void);
// 0x00000143 T DG.Tweening.TweenSettingsExtensions::From(T)
// 0x00000144 T DG.Tweening.TweenSettingsExtensions::From(T,System.Boolean)
// 0x00000145 T DG.Tweening.TweenSettingsExtensions::SetDelay(T,System.Single)
// 0x00000146 T DG.Tweening.TweenSettingsExtensions::SetRelative(T)
// 0x00000147 T DG.Tweening.TweenSettingsExtensions::SetRelative(T,System.Boolean)
// 0x00000148 T DG.Tweening.TweenSettingsExtensions::SetSpeedBased(T)
// 0x00000149 T DG.Tweening.TweenSettingsExtensions::SetSpeedBased(T,System.Boolean)
// 0x0000014A DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>,System.Boolean)
extern void TweenSettingsExtensions_SetOptions_m619178CD390C9F59543EBF056E54CECFF4BA95CD (void);
// 0x0000014B DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>,System.Boolean)
extern void TweenSettingsExtensions_SetOptions_m9C45DDF251D2CD1BC96529E3480E9A91B46AF1E4 (void);
// 0x0000014C DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>,DG.Tweening.AxisConstraint,System.Boolean)
extern void TweenSettingsExtensions_SetOptions_m9B154343AFE2B086C2BE39C9E913F51186A10BAF (void);
// 0x0000014D DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,System.Boolean)
extern void TweenSettingsExtensions_SetOptions_mA202FF383BEAAE03292420F1163032B5D0566742 (void);
// 0x0000014E DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,DG.Tweening.AxisConstraint,System.Boolean)
extern void TweenSettingsExtensions_SetOptions_mE2CF0CEA2DE42858A86D1C736E162AC6814256AE (void);
// 0x0000014F DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>,System.Boolean)
extern void TweenSettingsExtensions_SetOptions_m527D580A508B8297888953E59601C95C399E98F4 (void);
// 0x00000150 DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>,DG.Tweening.AxisConstraint,System.Boolean)
extern void TweenSettingsExtensions_SetOptions_mAF32D7903868EB663666CB91E101BF2477234647 (void);
// 0x00000151 DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>,System.Boolean)
extern void TweenSettingsExtensions_SetOptions_mF1E28DA2E05779F1E161A62F103038250446302D (void);
// 0x00000152 DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>,System.Boolean)
extern void TweenSettingsExtensions_SetOptions_mDA3551543550914D61FBDAF7F749A5FC69889AA7 (void);
// 0x00000153 DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>,System.Boolean)
extern void TweenSettingsExtensions_SetOptions_mF4CB8F4A815A8CF310CB76FA85D22E854A65319A (void);
// 0x00000154 DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern void TweenSettingsExtensions_SetOptions_mB198DDCA53C32A43F7EA9213244369D8B55F9A40 (void);
// 0x00000155 DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>,System.Boolean)
extern void TweenSettingsExtensions_SetOptions_mF4577F3599CEDC3C1AF2F31CD109481C229C4994 (void);
// 0x00000156 DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>,DG.Tweening.AxisConstraint,System.Boolean)
extern void TweenSettingsExtensions_SetOptions_mD2E5E89120A9CD6676C15DB603DC72BFE7EC126F (void);
// 0x00000157 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,DG.Tweening.AxisConstraint,DG.Tweening.AxisConstraint)
extern void TweenSettingsExtensions_SetOptions_mC7FC2851A87231470309BC2F740335EC663BD21F (void);
// 0x00000158 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,System.Boolean,DG.Tweening.AxisConstraint,DG.Tweening.AxisConstraint)
extern void TweenSettingsExtensions_SetOptions_m232500C0CAFAD89AFE0A437A6015F2E59ED3D8FF (void);
// 0x00000159 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetLookAt(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,UnityEngine.Vector3,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Vector3>)
extern void TweenSettingsExtensions_SetLookAt_m31B2E7ECAADA53B8C6EDA9BC760AFA2F6155B382 (void);
// 0x0000015A DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetLookAt(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,UnityEngine.Transform,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Vector3>)
extern void TweenSettingsExtensions_SetLookAt_m3BFF34B7765FCB4B3DDE2B3B90869FE9086DDD01 (void);
// 0x0000015B DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetLookAt(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,System.Single,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Vector3>)
extern void TweenSettingsExtensions_SetLookAt_mAD3CC7329671C9A6D0FA41A9AB2D3B236769C2AF (void);
// 0x0000015C System.Void DG.Tweening.TweenSettingsExtensions::SetPathForwardDirection(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Vector3>)
extern void TweenSettingsExtensions_SetPathForwardDirection_m8EF4B32280222570CCC96CE57E9D0C4936F1F732 (void);
// 0x0000015D System.Single DG.Tweening.Tween::get_fullPosition()
extern void Tween_get_fullPosition_m21DC9222B4A2B17909B7E4C3CD7FE6925CCD97AD (void);
// 0x0000015E System.Void DG.Tweening.Tween::set_fullPosition(System.Single)
extern void Tween_set_fullPosition_m589CFD59274B5028A21265F8CA754A173BC180A3 (void);
// 0x0000015F System.Void DG.Tweening.Tween::Reset()
extern void Tween_Reset_mAADD5CF8D940FB674B8E3914E16230599FB7A957 (void);
// 0x00000160 System.Boolean DG.Tweening.Tween::Validate()
// 0x00000161 System.Single DG.Tweening.Tween::UpdateDelay(System.Single)
extern void Tween_UpdateDelay_mA8151BABE455A62407CEFCFE342F7B5730378701 (void);
// 0x00000162 System.Boolean DG.Tweening.Tween::Startup()
// 0x00000163 System.Boolean DG.Tweening.Tween::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
// 0x00000164 System.Boolean DG.Tweening.Tween::DoGoto(DG.Tweening.Tween,System.Single,System.Int32,DG.Tweening.Core.Enums.UpdateMode)
extern void Tween_DoGoto_mBB5ECAA333D861AD3491AFAB5E7626D2F1BC310F (void);
// 0x00000165 System.Boolean DG.Tweening.Tween::OnTweenCallback(DG.Tweening.TweenCallback)
extern void Tween_OnTweenCallback_m9973B444F1D39598A217F58AE7757E0633A3F5F4 (void);
// 0x00000166 System.Boolean DG.Tweening.Tween::OnTweenCallback(DG.Tweening.TweenCallback`1<T>,T)
// 0x00000167 System.Void DG.Tweening.Tween::.ctor()
extern void Tween__ctor_mB8C032C20B3FECEC4AF6F9C0FB7FB90F91CC3E23 (void);
// 0x00000168 System.Void DG.Tweening.Tweener::.ctor()
extern void Tweener__ctor_m48B4AF3E51DFEAED312F4875F09EE9B7E41248D7 (void);
// 0x00000169 DG.Tweening.Tweener DG.Tweening.Tweener::ChangeStartValue(System.Object,System.Single)
// 0x0000016A DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean)
// 0x0000016B DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Boolean)
// 0x0000016C DG.Tweening.Tweener DG.Tweening.Tweener::ChangeValues(System.Object,System.Object,System.Single)
// 0x0000016D DG.Tweening.Tweener DG.Tweening.Tweener::SetFrom(System.Boolean)
// 0x0000016E System.Boolean DG.Tweening.Tweener::Setup(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,T2,System.Single,DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions>)
// 0x0000016F System.Single DG.Tweening.Tweener::DoUpdateDelay(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
// 0x00000170 System.Boolean DG.Tweening.Tweener::DoStartup(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// 0x00000171 DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
// 0x00000172 DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
// 0x00000173 DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
// 0x00000174 System.Boolean DG.Tweening.Tweener::DOStartupSpecials(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// 0x00000175 System.Void DG.Tweening.Tweener::DOStartupDurationBased(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// 0x00000176 System.Void DG.Tweening.Plugins.Color2Plugin::Reset(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>)
extern void Color2Plugin_Reset_mB904D4F6B9443C72072B1B4E1B2DFD2C3E27B09C (void);
// 0x00000177 System.Void DG.Tweening.Plugins.Color2Plugin::SetFrom(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>,System.Boolean)
extern void Color2Plugin_SetFrom_mD7D3786F8C6D8B8A2FB0FC2BFA7CB699357E2530 (void);
// 0x00000178 DG.Tweening.Color2 DG.Tweening.Plugins.Color2Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>,DG.Tweening.Color2)
extern void Color2Plugin_ConvertToStartValue_m60900134C3EBEDD78DABC35181D601AE6B0F90D2 (void);
// 0x00000179 System.Void DG.Tweening.Plugins.Color2Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>)
extern void Color2Plugin_SetRelativeEndValue_m6E4E6804691BFE37D2534D787C7194B832414910 (void);
// 0x0000017A System.Void DG.Tweening.Plugins.Color2Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>)
extern void Color2Plugin_SetChangeValue_mDBF5284A8859118DBF874CC37B8E2175A0798CE1 (void);
// 0x0000017B System.Single DG.Tweening.Plugins.Color2Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.ColorOptions,System.Single,DG.Tweening.Color2)
extern void Color2Plugin_GetSpeedBasedDuration_mE76CF42CB177706504203B0249C64A780EFC702F (void);
// 0x0000017C System.Void DG.Tweening.Plugins.Color2Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.ColorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>,DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>,System.Single,DG.Tweening.Color2,DG.Tweening.Color2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void Color2Plugin_EvaluateAndApply_mCE44814101CFBA3394C9D435B3306CB2D4A82AAA (void);
// 0x0000017D System.Void DG.Tweening.Plugins.Color2Plugin::.ctor()
extern void Color2Plugin__ctor_m2C523642D184C931C339A1CDA79F0E1463A2CD97 (void);
// 0x0000017E System.Void DG.Tweening.Plugins.DoublePlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>)
extern void DoublePlugin_Reset_m9F6A83E41CCFCFE1F062E954C1785A4A0B965042 (void);
// 0x0000017F System.Void DG.Tweening.Plugins.DoublePlugin::SetFrom(DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>,System.Boolean)
extern void DoublePlugin_SetFrom_mCA576F2678F7AB9269CA7F2328AD5B1750F2F8DB (void);
// 0x00000180 System.Double DG.Tweening.Plugins.DoublePlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>,System.Double)
extern void DoublePlugin_ConvertToStartValue_m6547AEF7C82F685DDB8A5D130D4979BD42392DB0 (void);
// 0x00000181 System.Void DG.Tweening.Plugins.DoublePlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>)
extern void DoublePlugin_SetRelativeEndValue_m1E855855D71C51E3EEB72CEE9511925EF00418D3 (void);
// 0x00000182 System.Void DG.Tweening.Plugins.DoublePlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>)
extern void DoublePlugin_SetChangeValue_m2811678E89ADAC207834AAB86CBC7BE1B97E1104 (void);
// 0x00000183 System.Single DG.Tweening.Plugins.DoublePlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.Double)
extern void DoublePlugin_GetSpeedBasedDuration_mC171C3419FF3C035F7D1BCBC56D504C024CC309D (void);
// 0x00000184 System.Void DG.Tweening.Plugins.DoublePlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Double>,DG.Tweening.Core.DOSetter`1<System.Double>,System.Single,System.Double,System.Double,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void DoublePlugin_EvaluateAndApply_m4E1BF2EAAFEB4CD33C5E900C8786B512E95DBB34 (void);
// 0x00000185 System.Void DG.Tweening.Plugins.DoublePlugin::.ctor()
extern void DoublePlugin__ctor_mF420BC11DE2E0BF3B79D044A1F90C5F0C0708410 (void);
// 0x00000186 System.Void DG.Tweening.Plugins.LongPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>)
extern void LongPlugin_Reset_m0912A633F347939A612327703A0CBD43AE6A0F2E (void);
// 0x00000187 System.Void DG.Tweening.Plugins.LongPlugin::SetFrom(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>,System.Boolean)
extern void LongPlugin_SetFrom_m3DD81DA2853FE682A379A65F1E5C77DE2CB94C56 (void);
// 0x00000188 System.Int64 DG.Tweening.Plugins.LongPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>,System.Int64)
extern void LongPlugin_ConvertToStartValue_mF09FBA1064E303D54C92060F24625CD0F6027BAD (void);
// 0x00000189 System.Void DG.Tweening.Plugins.LongPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>)
extern void LongPlugin_SetRelativeEndValue_m80BEA044DE2D487C35FB9061F11DD841BBE09A23 (void);
// 0x0000018A System.Void DG.Tweening.Plugins.LongPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>)
extern void LongPlugin_SetChangeValue_m3267961A22BEFCB43A4F3000D6A85000F9FF8C1C (void);
// 0x0000018B System.Single DG.Tweening.Plugins.LongPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.Int64)
extern void LongPlugin_GetSpeedBasedDuration_mED09F7E9C3B44844DCEC8B63F8322F338C77EABE (void);
// 0x0000018C System.Void DG.Tweening.Plugins.LongPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Int64>,DG.Tweening.Core.DOSetter`1<System.Int64>,System.Single,System.Int64,System.Int64,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void LongPlugin_EvaluateAndApply_mF9D9E0DDB9C98FECB344BFBBFCE99B28CFCA0D62 (void);
// 0x0000018D System.Void DG.Tweening.Plugins.LongPlugin::.ctor()
extern void LongPlugin__ctor_m04FE82F230F471A46F15AED810FC56B0FFF90EC8 (void);
// 0x0000018E System.Void DG.Tweening.Plugins.UlongPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>)
extern void UlongPlugin_Reset_mFC20C0746C0A48A5C6117205BF52B38FC07CF2B6 (void);
// 0x0000018F System.Void DG.Tweening.Plugins.UlongPlugin::SetFrom(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>,System.Boolean)
extern void UlongPlugin_SetFrom_mB79BAA3526BF3C9CA78609C9E09A0545512ED5B1 (void);
// 0x00000190 System.UInt64 DG.Tweening.Plugins.UlongPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>,System.UInt64)
extern void UlongPlugin_ConvertToStartValue_mD8298148B2DCC204ED57E96C7D1225CF62C533B0 (void);
// 0x00000191 System.Void DG.Tweening.Plugins.UlongPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>)
extern void UlongPlugin_SetRelativeEndValue_m6D17D07A8243D97C9DB0522450BCBFCA1506D24D (void);
// 0x00000192 System.Void DG.Tweening.Plugins.UlongPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>)
extern void UlongPlugin_SetChangeValue_m21FF1EF586E81FC9801520D596208570E05C5318 (void);
// 0x00000193 System.Single DG.Tweening.Plugins.UlongPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.UInt64)
extern void UlongPlugin_GetSpeedBasedDuration_mAE01A1A0519E6AE9602054B7F7EAD15A26657C7C (void);
// 0x00000194 System.Void DG.Tweening.Plugins.UlongPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.UInt64>,DG.Tweening.Core.DOSetter`1<System.UInt64>,System.Single,System.UInt64,System.UInt64,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void UlongPlugin_EvaluateAndApply_mBD4E50796B9AA1736ECE14E41A964AD55D5A5AE4 (void);
// 0x00000195 System.Void DG.Tweening.Plugins.UlongPlugin::.ctor()
extern void UlongPlugin__ctor_mC75488AFF3AA9497592260C860D16FD386482E16 (void);
// 0x00000196 System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern void Vector3ArrayPlugin_Reset_m95EB255B91F630BF02581FACB9EB1C73C610ACC4 (void);
// 0x00000197 System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::SetFrom(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>,System.Boolean)
extern void Vector3ArrayPlugin_SetFrom_m5B9C114C460461C0FCEF0F9AC3A61CED75322B13 (void);
// 0x00000198 UnityEngine.Vector3[] DG.Tweening.Plugins.Vector3ArrayPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>,UnityEngine.Vector3)
extern void Vector3ArrayPlugin_ConvertToStartValue_mDA38159B0CEC8F3FBDDF1ABF080590ED52C3C3F2 (void);
// 0x00000199 System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern void Vector3ArrayPlugin_SetRelativeEndValue_mAC05D397B4304FF7807898F0069F31AA16F5E72B (void);
// 0x0000019A System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern void Vector3ArrayPlugin_SetChangeValue_mA10FC73A97D1C547E173F489A5E996A2601D7DD1 (void);
// 0x0000019B System.Single DG.Tweening.Plugins.Vector3ArrayPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.Vector3ArrayOptions,System.Single,UnityEngine.Vector3[])
extern void Vector3ArrayPlugin_GetSpeedBasedDuration_mDA722C1F881E3C5E01919CEFCD5DDF4A3BD4C9EA (void);
// 0x0000019C System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.Vector3ArrayOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,UnityEngine.Vector3[],UnityEngine.Vector3[],System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void Vector3ArrayPlugin_EvaluateAndApply_m337B4D71A1A41CEFCAA4848EC6C0731A87879A4A (void);
// 0x0000019D System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::.ctor()
extern void Vector3ArrayPlugin__ctor_m66DC60F55463C1A2F01CB9A576F3E79F56EC5BC8 (void);
// 0x0000019E System.Void DG.Tweening.Plugins.PathPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>)
extern void PathPlugin_Reset_m0D22977A473DDF724AF8D93E904D0262F55D5F8A (void);
// 0x0000019F System.Void DG.Tweening.Plugins.PathPlugin::SetFrom(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,System.Boolean)
extern void PathPlugin_SetFrom_m331C0217AADFD4EAA491E920BFBEF3708AF1F55A (void);
// 0x000001A0 DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.Plugins.PathPlugin::Get()
extern void PathPlugin_Get_m1D5B2D927828EF5D7543475191CAE0B620A0A5A3 (void);
// 0x000001A1 DG.Tweening.Plugins.Core.PathCore.Path DG.Tweening.Plugins.PathPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,UnityEngine.Vector3)
extern void PathPlugin_ConvertToStartValue_mC43D885FAEA0DF9F75EE90CC88E38F6D7CC82588 (void);
// 0x000001A2 System.Void DG.Tweening.Plugins.PathPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>)
extern void PathPlugin_SetRelativeEndValue_m5ADDA3AB3CAE201E25864F924D98311E5751D94B (void);
// 0x000001A3 System.Void DG.Tweening.Plugins.PathPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>)
extern void PathPlugin_SetChangeValue_m309C4BB9A503831A66BC927FA0769905C219558E (void);
// 0x000001A4 System.Single DG.Tweening.Plugins.PathPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.PathOptions,System.Single,DG.Tweening.Plugins.Core.PathCore.Path)
extern void PathPlugin_GetSpeedBasedDuration_m11FC2E670501D6B8EDAFB668092D110CCC87DA40 (void);
// 0x000001A5 System.Void DG.Tweening.Plugins.PathPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void PathPlugin_EvaluateAndApply_mA5F082E78C7183946B80532BBCAE7AADB413BFB9 (void);
// 0x000001A6 System.Void DG.Tweening.Plugins.PathPlugin::SetOrientation(DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,UnityEngine.Vector3,DG.Tweening.Core.Enums.UpdateNotice)
extern void PathPlugin_SetOrientation_mA723C7B1ECA013738CFD9789423FF3C91F6FEF49 (void);
// 0x000001A7 System.Void DG.Tweening.Plugins.PathPlugin::.ctor()
extern void PathPlugin__ctor_mEBC9E19D350BA9C3980BF3B9AFA32CA0BCC459FD (void);
// 0x000001A8 System.Void DG.Tweening.Plugins.ColorPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>)
extern void ColorPlugin_Reset_mF8E1AE9221E8A74523FAB45070A4FD26B394BD35 (void);
// 0x000001A9 System.Void DG.Tweening.Plugins.ColorPlugin::SetFrom(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>,System.Boolean)
extern void ColorPlugin_SetFrom_mDE788269C22DC64D6B935023A45870E4F027E4A4 (void);
// 0x000001AA UnityEngine.Color DG.Tweening.Plugins.ColorPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>,UnityEngine.Color)
extern void ColorPlugin_ConvertToStartValue_mF254AE0A1E4B73BD2FC4E3EF7EE0053FBA226713 (void);
// 0x000001AB System.Void DG.Tweening.Plugins.ColorPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>)
extern void ColorPlugin_SetRelativeEndValue_m8DD740D391AB1D0EC8DC57341A94347B604ECE61 (void);
// 0x000001AC System.Void DG.Tweening.Plugins.ColorPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>)
extern void ColorPlugin_SetChangeValue_m71B84592A9C77FFE3EE0A68AE35DE8E240C04D3F (void);
// 0x000001AD System.Single DG.Tweening.Plugins.ColorPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.ColorOptions,System.Single,UnityEngine.Color)
extern void ColorPlugin_GetSpeedBasedDuration_m428E21C053B02DD46B6B5FF63C6C6E6B86A551A5 (void);
// 0x000001AE System.Void DG.Tweening.Plugins.ColorPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.ColorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Color>,DG.Tweening.Core.DOSetter`1<UnityEngine.Color>,System.Single,UnityEngine.Color,UnityEngine.Color,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void ColorPlugin_EvaluateAndApply_m6B3E18787E83E45DAD99AC1AA7C0DA9DF7714BCE (void);
// 0x000001AF System.Void DG.Tweening.Plugins.ColorPlugin::.ctor()
extern void ColorPlugin__ctor_mB3437144ABC10E3733E87D0D1BA2E8F9AE6FCBFA (void);
// 0x000001B0 System.Void DG.Tweening.Plugins.IntPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern void IntPlugin_Reset_m95F23E6C48BDD7D1650A4EABEAD440F054117C9D (void);
// 0x000001B1 System.Void DG.Tweening.Plugins.IntPlugin::SetFrom(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>,System.Boolean)
extern void IntPlugin_SetFrom_m2B07A75CD24015DAA4D77103488767D0926553C8 (void);
// 0x000001B2 System.Int32 DG.Tweening.Plugins.IntPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>,System.Int32)
extern void IntPlugin_ConvertToStartValue_mB090390FFE1EFB459952C19416BE4E3327D2BFB9 (void);
// 0x000001B3 System.Void DG.Tweening.Plugins.IntPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern void IntPlugin_SetRelativeEndValue_m12E3972FD374B94E187A903F150E092503D930BB (void);
// 0x000001B4 System.Void DG.Tweening.Plugins.IntPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern void IntPlugin_SetChangeValue_mDED66936B27B41BAD55EC9305016F6AB95C96016 (void);
// 0x000001B5 System.Single DG.Tweening.Plugins.IntPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.Int32)
extern void IntPlugin_GetSpeedBasedDuration_m958E04DCA7F2F5FE9892FBC908A79A017E17EC97 (void);
// 0x000001B6 System.Void DG.Tweening.Plugins.IntPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Int32>,DG.Tweening.Core.DOSetter`1<System.Int32>,System.Single,System.Int32,System.Int32,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void IntPlugin_EvaluateAndApply_m94DE1FA97B2C4D9DDA83EE633FDB08D6F63FD722 (void);
// 0x000001B7 System.Void DG.Tweening.Plugins.IntPlugin::.ctor()
extern void IntPlugin__ctor_m208428BA7A040F6F902499122457980952044432 (void);
// 0x000001B8 System.Void DG.Tweening.Plugins.QuaternionPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>)
extern void QuaternionPlugin_Reset_m0649C8AC12C023BCDEC214DFA0F973EDC6C66024 (void);
// 0x000001B9 System.Void DG.Tweening.Plugins.QuaternionPlugin::SetFrom(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>,System.Boolean)
extern void QuaternionPlugin_SetFrom_mAF5BD3EB5C4152F98A95FB80A13CE79D3AD558AF (void);
// 0x000001BA UnityEngine.Vector3 DG.Tweening.Plugins.QuaternionPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>,UnityEngine.Quaternion)
extern void QuaternionPlugin_ConvertToStartValue_m9C871C5F50AFA3043B0C0044A38F4C883919FA26 (void);
// 0x000001BB System.Void DG.Tweening.Plugins.QuaternionPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>)
extern void QuaternionPlugin_SetRelativeEndValue_m0750B967437A22407ED0CB6836A4C38D9E8AF3F7 (void);
// 0x000001BC System.Void DG.Tweening.Plugins.QuaternionPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>)
extern void QuaternionPlugin_SetChangeValue_mE01B2DA751F39BC5B404D2509648B393F18FFE6C (void);
// 0x000001BD System.Single DG.Tweening.Plugins.QuaternionPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.QuaternionOptions,System.Single,UnityEngine.Vector3)
extern void QuaternionPlugin_GetSpeedBasedDuration_mD7D92DBEAD656FBD1130CBF8A8067476638450B9 (void);
// 0x000001BE System.Void DG.Tweening.Plugins.QuaternionPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.QuaternionOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>,DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>,System.Single,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void QuaternionPlugin_EvaluateAndApply_m293AC3183649C55894B0AC156757DF87D4272788 (void);
// 0x000001BF System.Void DG.Tweening.Plugins.QuaternionPlugin::.ctor()
extern void QuaternionPlugin__ctor_m0E8810278B6DC9B6A25FD63303A581D3D98B33EA (void);
// 0x000001C0 System.Void DG.Tweening.Plugins.RectOffsetPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>)
extern void RectOffsetPlugin_Reset_m010F59F59EE4187D49E420FAA4A37B3595258E4E (void);
// 0x000001C1 System.Void DG.Tweening.Plugins.RectOffsetPlugin::SetFrom(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>,System.Boolean)
extern void RectOffsetPlugin_SetFrom_m92FED32CCBC3EA2DF032C524B5B90DAB2DCBC009 (void);
// 0x000001C2 UnityEngine.RectOffset DG.Tweening.Plugins.RectOffsetPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>,UnityEngine.RectOffset)
extern void RectOffsetPlugin_ConvertToStartValue_mAD997FE953F6F3D9DC3EE016A2E92FA6A0831DB7 (void);
// 0x000001C3 System.Void DG.Tweening.Plugins.RectOffsetPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>)
extern void RectOffsetPlugin_SetRelativeEndValue_m87D884B1E4E8D270D0AB3A7E1A5D85FB8C4D14AA (void);
// 0x000001C4 System.Void DG.Tweening.Plugins.RectOffsetPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>)
extern void RectOffsetPlugin_SetChangeValue_mE72CB478071BD93E3A4DDED919943794D214205B (void);
// 0x000001C5 System.Single DG.Tweening.Plugins.RectOffsetPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,UnityEngine.RectOffset)
extern void RectOffsetPlugin_GetSpeedBasedDuration_mAA0F08BE2AC1D0C4AD06CC09621EF90CFAB91B8C (void);
// 0x000001C6 System.Void DG.Tweening.Plugins.RectOffsetPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.RectOffset>,DG.Tweening.Core.DOSetter`1<UnityEngine.RectOffset>,System.Single,UnityEngine.RectOffset,UnityEngine.RectOffset,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void RectOffsetPlugin_EvaluateAndApply_m06CB41CD7ACC9186EA878819597AE05064A5BC8E (void);
// 0x000001C7 System.Void DG.Tweening.Plugins.RectOffsetPlugin::.ctor()
extern void RectOffsetPlugin__ctor_mFF822DB0D7B655C76D46D0CE9A94ADD4835A5F9B (void);
// 0x000001C8 System.Void DG.Tweening.Plugins.RectOffsetPlugin::.cctor()
extern void RectOffsetPlugin__cctor_m996743433053049BD8F606C9B32A64205159D3B7 (void);
// 0x000001C9 System.Void DG.Tweening.Plugins.RectPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern void RectPlugin_Reset_mB1D0DC4F3DE841A1B33F05A1B13A0E248A093B26 (void);
// 0x000001CA System.Void DG.Tweening.Plugins.RectPlugin::SetFrom(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>,System.Boolean)
extern void RectPlugin_SetFrom_m1B267E9B0414A5A0DDB50CDA4BB129F57AC5E276 (void);
// 0x000001CB UnityEngine.Rect DG.Tweening.Plugins.RectPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>,UnityEngine.Rect)
extern void RectPlugin_ConvertToStartValue_m8EAA87E45893560FEDC16AEDE463EEAAE3033290 (void);
// 0x000001CC System.Void DG.Tweening.Plugins.RectPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern void RectPlugin_SetRelativeEndValue_m0427090C7C597720D8A9BA2CCDE7822E050A257B (void);
// 0x000001CD System.Void DG.Tweening.Plugins.RectPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern void RectPlugin_SetChangeValue_m6464D92DD679FC99A6616158A6CEFE56D4B4A29E (void);
// 0x000001CE System.Single DG.Tweening.Plugins.RectPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.RectOptions,System.Single,UnityEngine.Rect)
extern void RectPlugin_GetSpeedBasedDuration_m053D877CC923DED96BDD7E48DD9DC2FF18C5DD03 (void);
// 0x000001CF System.Void DG.Tweening.Plugins.RectPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.RectOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>,DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>,System.Single,UnityEngine.Rect,UnityEngine.Rect,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void RectPlugin_EvaluateAndApply_m13D0D7CEBBB34B9D31A7596CD6DC0C31E68872E9 (void);
// 0x000001D0 System.Void DG.Tweening.Plugins.RectPlugin::.ctor()
extern void RectPlugin__ctor_m010C901CECBCF5999DD2A28D54EC20845FF452FF (void);
// 0x000001D1 System.Void DG.Tweening.Plugins.UintPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>)
extern void UintPlugin_Reset_m193AE4F691F9BBDE696FBC5B0B5436BAF73A76AA (void);
// 0x000001D2 System.Void DG.Tweening.Plugins.UintPlugin::SetFrom(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>,System.Boolean)
extern void UintPlugin_SetFrom_m91620454D164FC43858A46CE6F3B79686F0D30F4 (void);
// 0x000001D3 System.UInt32 DG.Tweening.Plugins.UintPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>,System.UInt32)
extern void UintPlugin_ConvertToStartValue_mFDB6D84340ACF5440D64D685E9A1C1174087FD30 (void);
// 0x000001D4 System.Void DG.Tweening.Plugins.UintPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>)
extern void UintPlugin_SetRelativeEndValue_m76EAD21D81FE646B4EBD89F43D598FC102F48AC1 (void);
// 0x000001D5 System.Void DG.Tweening.Plugins.UintPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>)
extern void UintPlugin_SetChangeValue_m3C0697D6C48F6BC6933403517DB461AA055E9B06 (void);
// 0x000001D6 System.Single DG.Tweening.Plugins.UintPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.UintOptions,System.Single,System.UInt32)
extern void UintPlugin_GetSpeedBasedDuration_m8D8A316C45479265320C430AFC6475C1443DEF9B (void);
// 0x000001D7 System.Void DG.Tweening.Plugins.UintPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.UintOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.UInt32>,DG.Tweening.Core.DOSetter`1<System.UInt32>,System.Single,System.UInt32,System.UInt32,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void UintPlugin_EvaluateAndApply_m0424424673DB615C6E81618710EE244DDCB5316D (void);
// 0x000001D8 System.Void DG.Tweening.Plugins.UintPlugin::.ctor()
extern void UintPlugin__ctor_mC1590E3DD4EFE6686AD587D371E1C4BA5556B960 (void);
// 0x000001D9 System.Void DG.Tweening.Plugins.Vector2Plugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>)
extern void Vector2Plugin_Reset_m1379B8670AFF4A6D6172CEB00E3A3B332BEFBD00 (void);
// 0x000001DA System.Void DG.Tweening.Plugins.Vector2Plugin::SetFrom(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>,System.Boolean)
extern void Vector2Plugin_SetFrom_mB9EC8AEF790F8A4220AB40E76CF6FF4467DEBC1A (void);
// 0x000001DB UnityEngine.Vector2 DG.Tweening.Plugins.Vector2Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>,UnityEngine.Vector2)
extern void Vector2Plugin_ConvertToStartValue_m13086D3524387C3EE47DE4388FAD6E5DBEABBD4A (void);
// 0x000001DC System.Void DG.Tweening.Plugins.Vector2Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>)
extern void Vector2Plugin_SetRelativeEndValue_mD2D0163D27EAE6EBADDF89B888E24FF3C7F21BB8 (void);
// 0x000001DD System.Void DG.Tweening.Plugins.Vector2Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>)
extern void Vector2Plugin_SetChangeValue_mBF8B99B3D35FC2F7E6BDA8D489306871CB79B97E (void);
// 0x000001DE System.Single DG.Tweening.Plugins.Vector2Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.VectorOptions,System.Single,UnityEngine.Vector2)
extern void Vector2Plugin_GetSpeedBasedDuration_mC696FAAA0B2CEACCEE809986ECA4CA97A646905D (void);
// 0x000001DF System.Void DG.Tweening.Plugins.Vector2Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.VectorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>,System.Single,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void Vector2Plugin_EvaluateAndApply_mA734062CBF02456CB02D0932332EE24312C50B0D (void);
// 0x000001E0 System.Void DG.Tweening.Plugins.Vector2Plugin::.ctor()
extern void Vector2Plugin__ctor_m8FFC85977ACE0836F6A6B53EEF78A1007AE08C8B (void);
// 0x000001E1 System.Void DG.Tweening.Plugins.Vector4Plugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>)
extern void Vector4Plugin_Reset_m1EB49BA31CBAD0980F998EEEF904C763E983FD1C (void);
// 0x000001E2 System.Void DG.Tweening.Plugins.Vector4Plugin::SetFrom(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>,System.Boolean)
extern void Vector4Plugin_SetFrom_m31B1C50D23D7AADCB265E5D3267BC98905FBA6B9 (void);
// 0x000001E3 UnityEngine.Vector4 DG.Tweening.Plugins.Vector4Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>,UnityEngine.Vector4)
extern void Vector4Plugin_ConvertToStartValue_mA11F01B5789F1FB0B159B2B2541E7541E8096638 (void);
// 0x000001E4 System.Void DG.Tweening.Plugins.Vector4Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>)
extern void Vector4Plugin_SetRelativeEndValue_m77FFB11D318A380D5DB4F95CD46C43EFD7BE2583 (void);
// 0x000001E5 System.Void DG.Tweening.Plugins.Vector4Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>)
extern void Vector4Plugin_SetChangeValue_mC427A3E178CB5296D3AA667EC47B837804D2380B (void);
// 0x000001E6 System.Single DG.Tweening.Plugins.Vector4Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.VectorOptions,System.Single,UnityEngine.Vector4)
extern void Vector4Plugin_GetSpeedBasedDuration_m152B2C3CF428132E78DC78EB6B2D4CF0D78D03AB (void);
// 0x000001E7 System.Void DG.Tweening.Plugins.Vector4Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.VectorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>,System.Single,UnityEngine.Vector4,UnityEngine.Vector4,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void Vector4Plugin_EvaluateAndApply_m74A4BA39C9A6A5E7FCA486AB3CC703F44D0F2D7F (void);
// 0x000001E8 System.Void DG.Tweening.Plugins.Vector4Plugin::.ctor()
extern void Vector4Plugin__ctor_m21D86C0C7A38430563F307EE7F4B0A43B2EA8DAF (void);
// 0x000001E9 System.Void DG.Tweening.Plugins.StringPlugin::SetFrom(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>,System.Boolean)
extern void StringPlugin_SetFrom_mD2926BE763AFB806389A055112C54E71A0E4FC0A (void);
// 0x000001EA System.Void DG.Tweening.Plugins.StringPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>)
extern void StringPlugin_Reset_m8D68BCE13C66172E0502C28C4373AD28C5E1AAE0 (void);
// 0x000001EB System.String DG.Tweening.Plugins.StringPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>,System.String)
extern void StringPlugin_ConvertToStartValue_m76106D9534182E72BB37194829CEDEB4476FAEE7 (void);
// 0x000001EC System.Void DG.Tweening.Plugins.StringPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>)
extern void StringPlugin_SetRelativeEndValue_m9D67ACC4F555285AB48CC3AD7C250DBD6323DD76 (void);
// 0x000001ED System.Void DG.Tweening.Plugins.StringPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>)
extern void StringPlugin_SetChangeValue_mCDA147C7DD517507A4F9BB269A75BBF175EA0083 (void);
// 0x000001EE System.Single DG.Tweening.Plugins.StringPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.StringOptions,System.Single,System.String)
extern void StringPlugin_GetSpeedBasedDuration_mFA690E2C3CB46920940A48C31BB960CB9793273A (void);
// 0x000001EF System.Void DG.Tweening.Plugins.StringPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.StringOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.String>,DG.Tweening.Core.DOSetter`1<System.String>,System.Single,System.String,System.String,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void StringPlugin_EvaluateAndApply_m3F590DBE11ADDFE530966D609318117270844AF6 (void);
// 0x000001F0 System.Text.StringBuilder DG.Tweening.Plugins.StringPlugin::Append(System.String,System.Int32,System.Int32,System.Boolean)
extern void StringPlugin_Append_mA1230766A1463D1E3ED55BFF00E00C1512C7304C (void);
// 0x000001F1 System.Char[] DG.Tweening.Plugins.StringPlugin::ScrambledCharsToUse(DG.Tweening.Plugins.Options.StringOptions)
extern void StringPlugin_ScrambledCharsToUse_m080754A4C844369B7554C1A13EFC1485537FB5B7 (void);
// 0x000001F2 System.Void DG.Tweening.Plugins.StringPlugin::.ctor()
extern void StringPlugin__ctor_m1E0AFE2A04FAABD74E1C4245F8EB078A91E23F2C (void);
// 0x000001F3 System.Void DG.Tweening.Plugins.StringPlugin::.cctor()
extern void StringPlugin__cctor_mDFC93819107CC7B3441F4B678856A0D7A1E7FBD0 (void);
// 0x000001F4 System.Void DG.Tweening.Plugins.StringPluginExtensions::.cctor()
extern void StringPluginExtensions__cctor_m1D257C1DE8259FD7FFA3BDBB35E731A2B3889495 (void);
// 0x000001F5 System.Void DG.Tweening.Plugins.StringPluginExtensions::ScrambleChars(System.Char[])
extern void StringPluginExtensions_ScrambleChars_m3177B26808D6532A66A40AA9292B134A216D4C90 (void);
// 0x000001F6 System.Text.StringBuilder DG.Tweening.Plugins.StringPluginExtensions::AppendScrambledChars(System.Text.StringBuilder,System.Int32,System.Char[])
extern void StringPluginExtensions_AppendScrambledChars_m8A75B95AF635D8479043492E1DF7084C4A9E4870 (void);
// 0x000001F7 System.Void DG.Tweening.Plugins.FloatPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>)
extern void FloatPlugin_Reset_m5754EBAB59D3C20EC406B9183C5DE5BB983F231C (void);
// 0x000001F8 System.Void DG.Tweening.Plugins.FloatPlugin::SetFrom(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>,System.Boolean)
extern void FloatPlugin_SetFrom_mF394F333160C04A02EC0163AA574EE4AD02872FC (void);
// 0x000001F9 System.Single DG.Tweening.Plugins.FloatPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>,System.Single)
extern void FloatPlugin_ConvertToStartValue_m7685B6918C4AAB675F05B8991D7D595994ADEC1F (void);
// 0x000001FA System.Void DG.Tweening.Plugins.FloatPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>)
extern void FloatPlugin_SetRelativeEndValue_m029A2E4325B3F14BF8BCD9941D7D98D9E2E8F2F9 (void);
// 0x000001FB System.Void DG.Tweening.Plugins.FloatPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>)
extern void FloatPlugin_SetChangeValue_mD7B09FC3BDE63D0945635DA20D4EDFE3554ABDB3 (void);
// 0x000001FC System.Single DG.Tweening.Plugins.FloatPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.FloatOptions,System.Single,System.Single)
extern void FloatPlugin_GetSpeedBasedDuration_m275FC86B86DAB6D7B5599F69E037CC5058010045 (void);
// 0x000001FD System.Void DG.Tweening.Plugins.FloatPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.FloatOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Single>,DG.Tweening.Core.DOSetter`1<System.Single>,System.Single,System.Single,System.Single,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void FloatPlugin_EvaluateAndApply_mDCF5EED6F4A206CF05B20F4F10EBF981B989B338 (void);
// 0x000001FE System.Void DG.Tweening.Plugins.FloatPlugin::.ctor()
extern void FloatPlugin__ctor_mD5B21EB10468A8F69FA7A70F533958E16F86D559 (void);
// 0x000001FF System.Void DG.Tweening.Plugins.Vector3Plugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>)
extern void Vector3Plugin_Reset_mAAC0F0D61D28AFC74881AAE32A8FBEA47C5ACBAE (void);
// 0x00000200 System.Void DG.Tweening.Plugins.Vector3Plugin::SetFrom(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,System.Boolean)
extern void Vector3Plugin_SetFrom_m8EDBA8B2F9D1E701E3A0292C864D6F4D5E150091 (void);
// 0x00000201 UnityEngine.Vector3 DG.Tweening.Plugins.Vector3Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,UnityEngine.Vector3)
extern void Vector3Plugin_ConvertToStartValue_m4260440F59538DB1A9C6E2CC6423D46D206340ED (void);
// 0x00000202 System.Void DG.Tweening.Plugins.Vector3Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>)
extern void Vector3Plugin_SetRelativeEndValue_m57A736F284D8651D2D6C9302F7B5AD7D2E0E4D82 (void);
// 0x00000203 System.Void DG.Tweening.Plugins.Vector3Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>)
extern void Vector3Plugin_SetChangeValue_mA78E49154DA5961CDF539D4716BBA2A301399B14 (void);
// 0x00000204 System.Single DG.Tweening.Plugins.Vector3Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.VectorOptions,System.Single,UnityEngine.Vector3)
extern void Vector3Plugin_GetSpeedBasedDuration_m1C863665F5EF19902D8972CDC30EBC787B7B580B (void);
// 0x00000205 System.Void DG.Tweening.Plugins.Vector3Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.VectorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void Vector3Plugin_EvaluateAndApply_m188A9A4A49A36E63623EB08A6E0F83BEDC76786A (void);
// 0x00000206 System.Void DG.Tweening.Plugins.Vector3Plugin::.ctor()
extern void Vector3Plugin__ctor_m79ED6C92B67FBD1D76E58A2D71B1AE65A662876C (void);
// 0x00000207 System.Void DG.Tweening.Plugins.Options.IPlugOptions::Reset()
// 0x00000208 System.Void DG.Tweening.Plugins.Options.PathOptions::Reset()
extern void PathOptions_Reset_mAE569F0A485C767B02D5A39FE73FBB99F499181E (void);
// 0x00000209 System.Void DG.Tweening.Plugins.Options.QuaternionOptions::Reset()
extern void QuaternionOptions_Reset_m9F3925AC48AC22071B77E36A944E061E0C46A134 (void);
// 0x0000020A System.Void DG.Tweening.Plugins.Options.UintOptions::Reset()
extern void UintOptions_Reset_mC2110A83EF78588DC8E220C98D2ABE7971110602 (void);
// 0x0000020B System.Void DG.Tweening.Plugins.Options.Vector3ArrayOptions::Reset()
extern void Vector3ArrayOptions_Reset_m3B9A2BB478088F79B5B9BF318FC8BBAE1799453F (void);
// 0x0000020C System.Void DG.Tweening.Plugins.Options.NoOptions::Reset()
extern void NoOptions_Reset_m27971DF59C8A9C56F0DB40B6ED3C01F5816F8BE6 (void);
// 0x0000020D System.Void DG.Tweening.Plugins.Options.ColorOptions::Reset()
extern void ColorOptions_Reset_mBFFB7CD03418E47DEE6B7CD3EDB3BF38D2FE3714 (void);
// 0x0000020E System.Void DG.Tweening.Plugins.Options.FloatOptions::Reset()
extern void FloatOptions_Reset_mA56EA5E336B757B6DFEF7C512FAB46E7D491BAA4 (void);
// 0x0000020F System.Void DG.Tweening.Plugins.Options.RectOptions::Reset()
extern void RectOptions_Reset_m76EC7D6D0CA41EAAE53344226F46FDFF5FB2578A (void);
// 0x00000210 System.Void DG.Tweening.Plugins.Options.StringOptions::Reset()
extern void StringOptions_Reset_m2ECE10EBDC0E574ED2D50230BB2D49ED57C230F0 (void);
// 0x00000211 System.Void DG.Tweening.Plugins.Options.VectorOptions::Reset()
extern void VectorOptions_Reset_mF46B9B9771CBDA05DD32C4396C611D9D8E86BCD4 (void);
// 0x00000212 System.Boolean DG.Tweening.Plugins.Core.SpecialPluginsUtils::SetLookAt(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>)
extern void SpecialPluginsUtils_SetLookAt_m62B3D82E9521FEBC1E22763881D8CB5F7C699100 (void);
// 0x00000213 System.Boolean DG.Tweening.Plugins.Core.SpecialPluginsUtils::SetPunch(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern void SpecialPluginsUtils_SetPunch_m09C64EF4F2FAA213FA5615AF546F3EEC6C00FDA7 (void);
// 0x00000214 System.Boolean DG.Tweening.Plugins.Core.SpecialPluginsUtils::SetShake(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern void SpecialPluginsUtils_SetShake_m094DAE5237FB92330103BB2CCFF36586A14B52F8 (void);
// 0x00000215 System.Boolean DG.Tweening.Plugins.Core.SpecialPluginsUtils::SetCameraShakePosition(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern void SpecialPluginsUtils_SetCameraShakePosition_mEC71E3A0D2885F4C62D6E0E5B6AD1553866D29AB (void);
// 0x00000216 DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Plugins.Core.IPlugSetter`4::Getter()
// 0x00000217 DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Plugins.Core.IPlugSetter`4::Setter()
// 0x00000218 T2 DG.Tweening.Plugins.Core.IPlugSetter`4::EndValue()
// 0x00000219 TPlugOptions DG.Tweening.Plugins.Core.IPlugSetter`4::GetOptions()
// 0x0000021A System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// 0x0000021B System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean)
// 0x0000021C T2 DG.Tweening.Plugins.Core.ABSTweenPlugin`3::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T1)
// 0x0000021D System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// 0x0000021E System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3::SetChangeValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// 0x0000021F System.Single DG.Tweening.Plugins.Core.ABSTweenPlugin`3::GetSpeedBasedDuration(TPlugOptions,System.Single,T2)
// 0x00000220 System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
// 0x00000221 System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3::.ctor()
// 0x00000222 DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Plugins.Core.PluginsManager::GetDefaultPlugin()
// 0x00000223 DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Plugins.Core.PluginsManager::GetCustomPlugin()
// 0x00000224 System.Void DG.Tweening.Plugins.Core.PluginsManager::PurgeAll()
extern void PluginsManager_PurgeAll_m207FAF2D3D6AF0C311D19210FB236DA4B271B25B (void);
// 0x00000225 System.Void DG.Tweening.Plugins.Core.PathCore.ControlPoint::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern void ControlPoint__ctor_m3E47DB0F5677F7DF493D707670BC3A853B24E708 (void);
// 0x00000226 DG.Tweening.Plugins.Core.PathCore.ControlPoint DG.Tweening.Plugins.Core.PathCore.ControlPoint::op_Addition(DG.Tweening.Plugins.Core.PathCore.ControlPoint,UnityEngine.Vector3)
extern void ControlPoint_op_Addition_mE6AE6C0925859DE939AEC4EF6151FEA4C4571077 (void);
// 0x00000227 System.Void DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder::FinalizePath(DG.Tweening.Plugins.Core.PathCore.Path,UnityEngine.Vector3[],System.Boolean)
// 0x00000228 UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder::GetPoint(System.Single,UnityEngine.Vector3[],DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Core.PathCore.ControlPoint[])
// 0x00000229 System.Void DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder::.ctor()
extern void ABSPathDecoder__ctor_m75687A62F6DEFD390499137CB7739B22EAA58848 (void);
// 0x0000022A System.Void DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder::FinalizePath(DG.Tweening.Plugins.Core.PathCore.Path,UnityEngine.Vector3[],System.Boolean)
extern void CatmullRomDecoder_FinalizePath_mA7BD1A338A217B3BAE3D85153E1178E785BB38CA (void);
// 0x0000022B UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder::GetPoint(System.Single,UnityEngine.Vector3[],DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Core.PathCore.ControlPoint[])
extern void CatmullRomDecoder_GetPoint_mE5A2FFBD1F1A50A5B288F92A1E048D00A8806BFF (void);
// 0x0000022C System.Void DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder::SetTimeToLengthTables(DG.Tweening.Plugins.Core.PathCore.Path,System.Int32)
extern void CatmullRomDecoder_SetTimeToLengthTables_m555F7D01F28CF4A32AB30B48974EC287956CCC0E (void);
// 0x0000022D System.Void DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder::SetWaypointsLengths(DG.Tweening.Plugins.Core.PathCore.Path,System.Int32)
extern void CatmullRomDecoder_SetWaypointsLengths_m4AD97B4AAA3E22B86A3432193DD42D66E66679D6 (void);
// 0x0000022E System.Void DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder::.ctor()
extern void CatmullRomDecoder__ctor_m58D647013D71654962E29F4044F14D78F0D14BEB (void);
// 0x0000022F System.Void DG.Tweening.Plugins.Core.PathCore.LinearDecoder::FinalizePath(DG.Tweening.Plugins.Core.PathCore.Path,UnityEngine.Vector3[],System.Boolean)
extern void LinearDecoder_FinalizePath_m31AEF72BC02ED1CA1936247EA9FAC2DC63D8EEF5 (void);
// 0x00000230 UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.LinearDecoder::GetPoint(System.Single,UnityEngine.Vector3[],DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Core.PathCore.ControlPoint[])
extern void LinearDecoder_GetPoint_m3A090FB590C263B01B1A8E3206B0306D1B9FAF78 (void);
// 0x00000231 System.Void DG.Tweening.Plugins.Core.PathCore.LinearDecoder::SetTimeToLengthTables(DG.Tweening.Plugins.Core.PathCore.Path,System.Int32)
extern void LinearDecoder_SetTimeToLengthTables_m1FA7914138A816D616569924313EC5BDF55CEEEE (void);
// 0x00000232 System.Void DG.Tweening.Plugins.Core.PathCore.LinearDecoder::SetWaypointsLengths(DG.Tweening.Plugins.Core.PathCore.Path,System.Int32)
extern void LinearDecoder_SetWaypointsLengths_m47E85BF06A3469EEB0D35D4DC33158D4B1F5F919 (void);
// 0x00000233 System.Void DG.Tweening.Plugins.Core.PathCore.LinearDecoder::.ctor()
extern void LinearDecoder__ctor_mA4ECBA6C86C7F9B90A0D65002C0540E173B02DDF (void);
// 0x00000234 System.Void DG.Tweening.Plugins.Core.PathCore.Path::.ctor(DG.Tweening.PathType,UnityEngine.Vector3[],System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void Path__ctor_mFC92041030E41315A55CFE293F4500B2D36DBE20 (void);
// 0x00000235 System.Void DG.Tweening.Plugins.Core.PathCore.Path::.ctor()
extern void Path__ctor_mA1C6976D933FA2CF6995BDD01445034E80F9850D (void);
// 0x00000236 System.Void DG.Tweening.Plugins.Core.PathCore.Path::FinalizePath(System.Boolean,DG.Tweening.AxisConstraint,UnityEngine.Vector3)
extern void Path_FinalizePath_mE2F8FBB45A0C5EDC7C87513502D4559FE329D32B (void);
// 0x00000237 UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.Path::GetPoint(System.Single,System.Boolean)
extern void Path_GetPoint_m23698672B67AE58436EAF7E41E4E07E48206C149 (void);
// 0x00000238 System.Single DG.Tweening.Plugins.Core.PathCore.Path::ConvertToConstantPathPerc(System.Single)
extern void Path_ConvertToConstantPathPerc_mEB0C3D4CD0CAB697EEA10C5B02AE69AE93E11AD6 (void);
// 0x00000239 System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::GetWaypointIndexFromPerc(System.Single,System.Boolean)
extern void Path_GetWaypointIndexFromPerc_m3A1A77D53813B6ADCCAB8C74EF3A0AF41EC5F959 (void);
// 0x0000023A UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.Path::GetDrawPoints(DG.Tweening.Plugins.Core.PathCore.Path,System.Int32)
extern void Path_GetDrawPoints_m208AE495E1F916DDDF566EA18658581CB98E29E6 (void);
// 0x0000023B System.Void DG.Tweening.Plugins.Core.PathCore.Path::RefreshNonLinearDrawWps(DG.Tweening.Plugins.Core.PathCore.Path)
extern void Path_RefreshNonLinearDrawWps_mCD80F5FC7567C45D26BB9348244EBCEB1C46B018 (void);
// 0x0000023C System.Void DG.Tweening.Plugins.Core.PathCore.Path::Destroy()
extern void Path_Destroy_mB032169AEE079328BDF5D9BC2AEFDC2D5605A066 (void);
// 0x0000023D DG.Tweening.Plugins.Core.PathCore.Path DG.Tweening.Plugins.Core.PathCore.Path::CloneIncremental(System.Int32)
extern void Path_CloneIncremental_m0A63F28B7D4015A7A07916CEBD97176F8DBBE2C7 (void);
// 0x0000023E System.Void DG.Tweening.Plugins.Core.PathCore.Path::AssignWaypoints(UnityEngine.Vector3[],System.Boolean)
extern void Path_AssignWaypoints_m8E804D1A254E946D1241C3E856FA956DF1B08FBB (void);
// 0x0000023F System.Void DG.Tweening.Plugins.Core.PathCore.Path::AssignDecoder(DG.Tweening.PathType)
extern void Path_AssignDecoder_mD999EA9D655C222C11AF74D1F24EBD51AFC37097 (void);
// 0x00000240 System.Void DG.Tweening.Plugins.Core.PathCore.Path::Draw()
extern void Path_Draw_m98F7E4F7EAD1D65843AFCA4C097E0BEF2E5CC783 (void);
// 0x00000241 System.Void DG.Tweening.Plugins.Core.PathCore.Path::Draw(DG.Tweening.Plugins.Core.PathCore.Path)
extern void Path_Draw_m78E0D953AE61D04E6651424ED36492F36C68A381 (void);
// 0x00000242 DG.Tweening.CustomPlugins.PureQuaternionPlugin DG.Tweening.CustomPlugins.PureQuaternionPlugin::Plug()
extern void PureQuaternionPlugin_Plug_m57BF0C3C1901BAE1794CC7892837A3EC43B22046 (void);
// 0x00000243 System.Void DG.Tweening.CustomPlugins.PureQuaternionPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>)
extern void PureQuaternionPlugin_Reset_m1FC197191A292BC0CCA48CA726309A3FC828BEE7 (void);
// 0x00000244 System.Void DG.Tweening.CustomPlugins.PureQuaternionPlugin::SetFrom(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>,System.Boolean)
extern void PureQuaternionPlugin_SetFrom_m0B8EBB771FDA733E7DAA793576A443F2C372CAA7 (void);
// 0x00000245 UnityEngine.Quaternion DG.Tweening.CustomPlugins.PureQuaternionPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>,UnityEngine.Quaternion)
extern void PureQuaternionPlugin_ConvertToStartValue_mAF286A6EB9D689C7B7C31970532B158600C2722A (void);
// 0x00000246 System.Void DG.Tweening.CustomPlugins.PureQuaternionPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>)
extern void PureQuaternionPlugin_SetRelativeEndValue_m6B2ACA2251800F47BC54FE4427BC6D289B25F11D (void);
// 0x00000247 System.Void DG.Tweening.CustomPlugins.PureQuaternionPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>)
extern void PureQuaternionPlugin_SetChangeValue_m53B40012A11DE3C3383A0A7D4CC2F8BA6137D401 (void);
// 0x00000248 System.Single DG.Tweening.CustomPlugins.PureQuaternionPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,UnityEngine.Quaternion)
extern void PureQuaternionPlugin_GetSpeedBasedDuration_m52D8A12F1316286D25F0736EFF7DC53FB351273D (void);
// 0x00000249 System.Void DG.Tweening.CustomPlugins.PureQuaternionPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>,DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>,System.Single,UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern void PureQuaternionPlugin_EvaluateAndApply_m6E8EA043EDE1B15A43052E8AA0897AA0CF70FEE2 (void);
// 0x0000024A System.Void DG.Tweening.CustomPlugins.PureQuaternionPlugin::.ctor()
extern void PureQuaternionPlugin__ctor_mB5D4C12941AF07DD6FC0395EF9544323E9A1C906 (void);
// 0x0000024B System.Void DG.Tweening.Core.ABSSequentiable::.ctor()
extern void ABSSequentiable__ctor_mC6848561E17C86E0350B84A1056AEC74EC07847D (void);
// 0x0000024C System.Void DG.Tweening.Core.DOGetter`1::.ctor(System.Object,System.IntPtr)
// 0x0000024D T DG.Tweening.Core.DOGetter`1::Invoke()
// 0x0000024E System.IAsyncResult DG.Tweening.Core.DOGetter`1::BeginInvoke(System.AsyncCallback,System.Object)
// 0x0000024F T DG.Tweening.Core.DOGetter`1::EndInvoke(System.IAsyncResult)
// 0x00000250 System.Void DG.Tweening.Core.DOSetter`1::.ctor(System.Object,System.IntPtr)
// 0x00000251 System.Void DG.Tweening.Core.DOSetter`1::Invoke(T)
// 0x00000252 System.IAsyncResult DG.Tweening.Core.DOSetter`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x00000253 System.Void DG.Tweening.Core.DOSetter`1::EndInvoke(System.IAsyncResult)
// 0x00000254 System.Void DG.Tweening.Core.Debugger::Log(System.Object)
extern void Debugger_Log_m7FFD88A31284EA5D424ECCDD3CC3969EFA888A60 (void);
// 0x00000255 System.Void DG.Tweening.Core.Debugger::LogWarning(System.Object)
extern void Debugger_LogWarning_m0B6001D682A2F01BB835AE4AD56405EE3FB146ED (void);
// 0x00000256 System.Void DG.Tweening.Core.Debugger::LogError(System.Object)
extern void Debugger_LogError_m6371F09308CBAF417D23ED479DCD6179FB6CD501 (void);
// 0x00000257 System.Void DG.Tweening.Core.Debugger::LogReport(System.Object)
extern void Debugger_LogReport_mE5EACE910A9EEFF1AED12993313A9EFC754983E3 (void);
// 0x00000258 System.Void DG.Tweening.Core.Debugger::LogInvalidTween(DG.Tweening.Tween)
extern void Debugger_LogInvalidTween_m89B8B151DC18203462D28B7A6FC9E5382BE7B5B2 (void);
// 0x00000259 System.Void DG.Tweening.Core.Debugger::LogNestedTween(DG.Tweening.Tween)
extern void Debugger_LogNestedTween_m20636A24F330E1A40E8BC38E90FA51CC0B7903A7 (void);
// 0x0000025A System.Void DG.Tweening.Core.Debugger::LogNullTween(DG.Tweening.Tween)
extern void Debugger_LogNullTween_m6A9B33DC41AC0A765DD49C9AC2DDABECD22F476B (void);
// 0x0000025B System.Void DG.Tweening.Core.Debugger::LogNonPathTween(DG.Tweening.Tween)
extern void Debugger_LogNonPathTween_m2073C7F72A26E7C2AD1321C130871946EE146FF1 (void);
// 0x0000025C System.Void DG.Tweening.Core.Debugger::LogMissingMaterialProperty(System.String)
extern void Debugger_LogMissingMaterialProperty_mF21C74A880DF58531233076CAD09891E04815E61 (void);
// 0x0000025D System.Void DG.Tweening.Core.Debugger::LogRemoveActiveTweenError(System.String)
extern void Debugger_LogRemoveActiveTweenError_m2D9BAAC1679552B0310CFD4978367FC232D817C6 (void);
// 0x0000025E System.Void DG.Tweening.Core.Debugger::SetLogPriority(DG.Tweening.LogBehaviour)
extern void Debugger_SetLogPriority_mD079823A6237D25090E0B91E094E484253BEBE17 (void);
// 0x0000025F System.Void DG.Tweening.Core.DOTweenComponent::Awake()
extern void DOTweenComponent_Awake_mDDB8FA2556C96A64275BB0193FD380ABCAEAF6E4 (void);
// 0x00000260 System.Void DG.Tweening.Core.DOTweenComponent::Start()
extern void DOTweenComponent_Start_mD16AA301BD8BF8EED3D66DDBBEC0E5A701E91B8D (void);
// 0x00000261 System.Void DG.Tweening.Core.DOTweenComponent::Update()
extern void DOTweenComponent_Update_m03F0653C0840CAF1927B496231127D33018C4094 (void);
// 0x00000262 System.Void DG.Tweening.Core.DOTweenComponent::LateUpdate()
extern void DOTweenComponent_LateUpdate_m32DB6CA5FE7B99631071E5D1895FE21C71C52547 (void);
// 0x00000263 System.Void DG.Tweening.Core.DOTweenComponent::FixedUpdate()
extern void DOTweenComponent_FixedUpdate_m9980784AE79BE2BA8D4F0D2378D77735F3979B70 (void);
// 0x00000264 System.Void DG.Tweening.Core.DOTweenComponent::ManualUpdate(System.Single,System.Single)
extern void DOTweenComponent_ManualUpdate_m3081BB828DA87A14AE435172B4B56B2F62863A60 (void);
// 0x00000265 System.Void DG.Tweening.Core.DOTweenComponent::OnDrawGizmos()
extern void DOTweenComponent_OnDrawGizmos_m986451D47907251DD3341C0082C46E5B393B4323 (void);
// 0x00000266 System.Void DG.Tweening.Core.DOTweenComponent::OnDestroy()
extern void DOTweenComponent_OnDestroy_m245C2AEE9CA2E63676F948E4AC8F5B79FADA8E71 (void);
// 0x00000267 System.Void DG.Tweening.Core.DOTweenComponent::OnApplicationQuit()
extern void DOTweenComponent_OnApplicationQuit_m97821B800F750E177A16FDD6E3D146483F85BED3 (void);
// 0x00000268 DG.Tweening.IDOTweenInit DG.Tweening.Core.DOTweenComponent::SetCapacity(System.Int32,System.Int32)
extern void DOTweenComponent_SetCapacity_m0E73677D5F156B6D624478E62D8D14DE0505F01D (void);
// 0x00000269 System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForCompletion(DG.Tweening.Tween)
extern void DOTweenComponent_WaitForCompletion_m7BA68341D27B9B492A65777AFD379E71F47B102B (void);
// 0x0000026A System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForRewind(DG.Tweening.Tween)
extern void DOTweenComponent_WaitForRewind_m7F96B4437D370D38E0B2463D096FD646387776B2 (void);
// 0x0000026B System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForKill(DG.Tweening.Tween)
extern void DOTweenComponent_WaitForKill_mB7C038F6E4B1363478CF373324CB2FBF235D0CA2 (void);
// 0x0000026C System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForElapsedLoops(DG.Tweening.Tween,System.Int32)
extern void DOTweenComponent_WaitForElapsedLoops_m73B338BCE98498A6B39A4D497C7DC37DFDAAA568 (void);
// 0x0000026D System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForPosition(DG.Tweening.Tween,System.Single)
extern void DOTweenComponent_WaitForPosition_m1B69080F19D45EB93A65B70B08A96FF5C13DE2EA (void);
// 0x0000026E System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForStart(DG.Tweening.Tween)
extern void DOTweenComponent_WaitForStart_m41699A9E4D9FB04610EE336ED66DEBF3196209BC (void);
// 0x0000026F System.Void DG.Tweening.Core.DOTweenComponent::Create()
extern void DOTweenComponent_Create_m0F1760B8E1CBE8F3CE2CEF9636F5978261E65DCB (void);
// 0x00000270 System.Void DG.Tweening.Core.DOTweenComponent::DestroyInstance()
extern void DOTweenComponent_DestroyInstance_m6236F832D1B1F34C22EEA55A6E6379C440E6B803 (void);
// 0x00000271 System.Void DG.Tweening.Core.DOTweenComponent::.ctor()
extern void DOTweenComponent__ctor_m5A2FBA4B31230F1BE2CCA174778B3DF5040933C6 (void);
// 0x00000272 System.Void DG.Tweening.Core.DOTweenSettings::.ctor()
extern void DOTweenSettings__ctor_m1C8B5E51F778CCB971735DC68318E4CD56FFF5AE (void);
// 0x00000273 T DG.Tweening.Core.Extensions::SetSpecialStartupMode(T,DG.Tweening.Core.Enums.SpecialStartupMode)
// 0x00000274 DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions> DG.Tweening.Core.Extensions::NoFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// 0x00000275 DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions> DG.Tweening.Core.Extensions::Blendable(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// 0x00000276 System.Void DG.Tweening.Core.SequenceCallback::.ctor(System.Single,DG.Tweening.TweenCallback)
extern void SequenceCallback__ctor_mCA4C0F5DFBF60FB024A67CAAFB968710301AED94 (void);
// 0x00000277 DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenManager::GetTweener()
// 0x00000278 DG.Tweening.Sequence DG.Tweening.Core.TweenManager::GetSequence()
extern void TweenManager_GetSequence_mB4DDC8BB253854799E302739AAA0A4005CD1D04C (void);
// 0x00000279 System.Void DG.Tweening.Core.TweenManager::SetUpdateType(DG.Tweening.Tween,DG.Tweening.UpdateType,System.Boolean)
extern void TweenManager_SetUpdateType_m7319E156C04BC3E82A3299B6322D952AEB123F0C (void);
// 0x0000027A System.Void DG.Tweening.Core.TweenManager::AddActiveTweenToSequence(DG.Tweening.Tween)
extern void TweenManager_AddActiveTweenToSequence_m95D2E6AF58BD892FBC697DD7D626068BC480ECE1 (void);
// 0x0000027B System.Int32 DG.Tweening.Core.TweenManager::DespawnAll()
extern void TweenManager_DespawnAll_m4FDE391EB0D266421A0D7A777888393D28C6E0C2 (void);
// 0x0000027C System.Void DG.Tweening.Core.TweenManager::Despawn(DG.Tweening.Tween,System.Boolean)
extern void TweenManager_Despawn_mBA89672AB846AA136C04EBBED03570775D5C2373 (void);
// 0x0000027D System.Void DG.Tweening.Core.TweenManager::PurgeAll()
extern void TweenManager_PurgeAll_m99F2511C27195D790293F94EA03C0ED66E6EBF15 (void);
// 0x0000027E System.Void DG.Tweening.Core.TweenManager::PurgePools()
extern void TweenManager_PurgePools_m145D170E5541DD5F33B204F77218271BA7E88B2C (void);
// 0x0000027F System.Void DG.Tweening.Core.TweenManager::ResetCapacities()
extern void TweenManager_ResetCapacities_m837C0EC04113FC56064B56BE3A2B60D3B6045E16 (void);
// 0x00000280 System.Void DG.Tweening.Core.TweenManager::SetCapacities(System.Int32,System.Int32)
extern void TweenManager_SetCapacities_m16552F59E957F1B0B495B670573CF4B1CECF5615 (void);
// 0x00000281 System.Int32 DG.Tweening.Core.TweenManager::Validate()
extern void TweenManager_Validate_m9FD1D8CADC09857CF17C70C1CB920ADC10C8BF5F (void);
// 0x00000282 System.Void DG.Tweening.Core.TweenManager::Update(DG.Tweening.UpdateType,System.Single,System.Single)
extern void TweenManager_Update_m6CE9360D1C820BA6DBBA10DEDC131DC748581B55 (void);
// 0x00000283 System.Int32 DG.Tweening.Core.TweenManager::FilteredOperation(DG.Tweening.Core.Enums.OperationType,DG.Tweening.Core.Enums.FilterType,System.Object,System.Boolean,System.Single,System.Object,System.Object[])
extern void TweenManager_FilteredOperation_mE0B8019A163D005B02B2A0E4A1506407181ECB77 (void);
// 0x00000284 System.Boolean DG.Tweening.Core.TweenManager::Complete(DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.Enums.UpdateMode)
extern void TweenManager_Complete_m23CB7E180527065E2E41A75D37261061D37C7547 (void);
// 0x00000285 System.Boolean DG.Tweening.Core.TweenManager::Flip(DG.Tweening.Tween)
extern void TweenManager_Flip_mF843250C1A4F0DB21721377E0F49593140F84675 (void);
// 0x00000286 System.Void DG.Tweening.Core.TweenManager::ForceInit(DG.Tweening.Tween,System.Boolean)
extern void TweenManager_ForceInit_m23B6EA289D16DAC62AB504E102594B55A2DE9708 (void);
// 0x00000287 System.Boolean DG.Tweening.Core.TweenManager::Goto(DG.Tweening.Tween,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateMode)
extern void TweenManager_Goto_m916DA36B3DEA6A298885C27143D8F78FB6BB40DF (void);
// 0x00000288 System.Boolean DG.Tweening.Core.TweenManager::Pause(DG.Tweening.Tween)
extern void TweenManager_Pause_m14AA579FB84B8AD6FD698ADAB30EC7B94517DE0F (void);
// 0x00000289 System.Boolean DG.Tweening.Core.TweenManager::Play(DG.Tweening.Tween)
extern void TweenManager_Play_m12AFE3745A43C09F3662DF359F634947E5EC95A6 (void);
// 0x0000028A System.Boolean DG.Tweening.Core.TweenManager::PlayBackwards(DG.Tweening.Tween)
extern void TweenManager_PlayBackwards_mB50289428BDF97AE57DB7321833089E51F80F59C (void);
// 0x0000028B System.Boolean DG.Tweening.Core.TweenManager::PlayForward(DG.Tweening.Tween)
extern void TweenManager_PlayForward_m4B2622EEA7E1D1E87765DB23A5F1D686FFE2271A (void);
// 0x0000028C System.Boolean DG.Tweening.Core.TweenManager::Restart(DG.Tweening.Tween,System.Boolean,System.Single)
extern void TweenManager_Restart_m9A1BDA041F5514A8F67AE4318F8CE7F05065D151 (void);
// 0x0000028D System.Boolean DG.Tweening.Core.TweenManager::Rewind(DG.Tweening.Tween,System.Boolean)
extern void TweenManager_Rewind_m9A5B531B2B9F36AE022EE76458757FC6A78DA89D (void);
// 0x0000028E System.Boolean DG.Tweening.Core.TweenManager::SmoothRewind(DG.Tweening.Tween)
extern void TweenManager_SmoothRewind_m48F722FD7FCED29D070F5E901CDD571172CC5C60 (void);
// 0x0000028F System.Boolean DG.Tweening.Core.TweenManager::TogglePause(DG.Tweening.Tween)
extern void TweenManager_TogglePause_m290FEBDD4F1F491CE9FF9D2B0A2FF03AAC583F6B (void);
// 0x00000290 System.Int32 DG.Tweening.Core.TweenManager::TotalPooledTweens()
extern void TweenManager_TotalPooledTweens_m4BF409197E4F2681A780F921EDE7AC3B01CC6C0D (void);
// 0x00000291 System.Int32 DG.Tweening.Core.TweenManager::TotalPlayingTweens()
extern void TweenManager_TotalPlayingTweens_mFD0E1A2EB28A8E7C28BE6A082C82AFB85B64D6C0 (void);
// 0x00000292 System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.Core.TweenManager::GetActiveTweens(System.Boolean)
extern void TweenManager_GetActiveTweens_m445B7873862CF7B6C880DCF15B542860E9575BBC (void);
// 0x00000293 System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.Core.TweenManager::GetTweensById(System.Object,System.Boolean)
extern void TweenManager_GetTweensById_m11BF0BE966B567C8487BC4D72D925ED10206B825 (void);
// 0x00000294 System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.Core.TweenManager::GetTweensByTarget(System.Object,System.Boolean)
extern void TweenManager_GetTweensByTarget_m06BCD088FA9D2BBF00DFA5078E9AA714582D7F14 (void);
// 0x00000295 System.Void DG.Tweening.Core.TweenManager::MarkForKilling(DG.Tweening.Tween)
extern void TweenManager_MarkForKilling_m9D7122C1A52CA72709D03BAC9F98343A8F27A9F6 (void);
// 0x00000296 System.Void DG.Tweening.Core.TweenManager::AddActiveTween(DG.Tweening.Tween)
extern void TweenManager_AddActiveTween_m72F1CD5193ED0B41AA5FC01F9D2A667718F107DE (void);
// 0x00000297 System.Void DG.Tweening.Core.TweenManager::ReorganizeActiveTweens()
extern void TweenManager_ReorganizeActiveTweens_mC03A4ED3E74E94FB604C65B7D8F27D8DC9506426 (void);
// 0x00000298 System.Void DG.Tweening.Core.TweenManager::DespawnActiveTweens(System.Collections.Generic.List`1<DG.Tweening.Tween>)
extern void TweenManager_DespawnActiveTweens_m37508B7398B79E00BB77A6FCD8A67E1BF053F244 (void);
// 0x00000299 System.Void DG.Tweening.Core.TweenManager::RemoveActiveTween(DG.Tweening.Tween)
extern void TweenManager_RemoveActiveTween_m7667979DE3FF9443EE543D369C0E04DF4C66F940 (void);
// 0x0000029A System.Void DG.Tweening.Core.TweenManager::ClearTweenArray(DG.Tweening.Tween[])
extern void TweenManager_ClearTweenArray_m0964AADC1D6A57832A8EDEEEEFDB7B72E90A1074 (void);
// 0x0000029B System.Void DG.Tweening.Core.TweenManager::IncreaseCapacities(DG.Tweening.Core.TweenManager/CapacityIncreaseMode)
extern void TweenManager_IncreaseCapacities_m289AF4539DABF6FEA349C0449B7EF2ED31B2B3A9 (void);
// 0x0000029C System.Void DG.Tweening.Core.TweenManager::.cctor()
extern void TweenManager__cctor_m85FD5609EB964C27ACF5AF6AAD99FE747D7CB470 (void);
// 0x0000029D UnityEngine.Vector3 DG.Tweening.Core.Utils::Vector3FromAngle(System.Single,System.Single)
extern void Utils_Vector3FromAngle_m073D50B4279D401AD2A89D74262CED7A74C72151 (void);
// 0x0000029E System.Single DG.Tweening.Core.Utils::Angle2D(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Utils_Angle2D_mA18DF43B096D1DFE85ED6C46CBD5A44D6A48B1EA (void);
// 0x0000029F System.Boolean DG.Tweening.Core.Utils::Vector3AreApproximatelyEqual(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Utils_Vector3AreApproximatelyEqual_m3DDED7828048F20AEB6A05E1938BED8AD5E321FA (void);
// 0x000002A0 System.Void DG.Tweening.Core.TweenerCore`3::.ctor()
// 0x000002A1 DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3::ChangeStartValue(System.Object,System.Single)
// 0x000002A2 DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3::ChangeEndValue(System.Object,System.Boolean)
// 0x000002A3 DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3::ChangeEndValue(System.Object,System.Single,System.Boolean)
// 0x000002A4 DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3::ChangeValues(System.Object,System.Object,System.Single)
// 0x000002A5 DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3::SetFrom(System.Boolean)
// 0x000002A6 System.Void DG.Tweening.Core.TweenerCore`3::Reset()
// 0x000002A7 System.Boolean DG.Tweening.Core.TweenerCore`3::Validate()
// 0x000002A8 System.Single DG.Tweening.Core.TweenerCore`3::UpdateDelay(System.Single)
// 0x000002A9 System.Boolean DG.Tweening.Core.TweenerCore`3::Startup()
// 0x000002AA System.Boolean DG.Tweening.Core.TweenerCore`3::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
// 0x000002AB System.Single DG.Tweening.Core.Easing.Bounce::EaseIn(System.Single,System.Single,System.Single,System.Single)
extern void Bounce_EaseIn_m7ED4AFA856CE5E8A4ADDDE63F560F8CD3C933EF3 (void);
// 0x000002AC System.Single DG.Tweening.Core.Easing.Bounce::EaseOut(System.Single,System.Single,System.Single,System.Single)
extern void Bounce_EaseOut_m88B5E1E41115135F049BE05E2A6A755E98FAFE1B (void);
// 0x000002AD System.Single DG.Tweening.Core.Easing.Bounce::EaseInOut(System.Single,System.Single,System.Single,System.Single)
extern void Bounce_EaseInOut_mF1279096941029D9D4EBA2CD862858B66575BC3D (void);
// 0x000002AE System.Single DG.Tweening.Core.Easing.EaseManager::Evaluate(DG.Tweening.Tween,System.Single,System.Single,System.Single,System.Single)
extern void EaseManager_Evaluate_mFFCCC216976B86EE1A69D485690E963CF8B61D40 (void);
// 0x000002AF System.Single DG.Tweening.Core.Easing.EaseManager::Evaluate(DG.Tweening.Ease,DG.Tweening.EaseFunction,System.Single,System.Single,System.Single,System.Single)
extern void EaseManager_Evaluate_m6C16E8E2792775BD48D6CB4DB02FE72FC2E555FA (void);
// 0x000002B0 DG.Tweening.EaseFunction DG.Tweening.Core.Easing.EaseManager::ToEaseFunction(DG.Tweening.Ease)
extern void EaseManager_ToEaseFunction_m89A308AA580E9F3F44675154110726939C6F1B66 (void);
// 0x000002B1 System.Boolean DG.Tweening.Core.Easing.EaseManager::IsFlashEase(DG.Tweening.Ease)
extern void EaseManager_IsFlashEase_m23FEBA92B8438BCE000A3F3A4B6D9C35213DE9C1 (void);
// 0x000002B2 System.Void DG.Tweening.Core.Easing.EaseCurve::.ctor(UnityEngine.AnimationCurve)
extern void EaseCurve__ctor_m9ECCBAB3097CEFFA5D910D8312B67E97FB3DE6D1 (void);
// 0x000002B3 System.Single DG.Tweening.Core.Easing.EaseCurve::Evaluate(System.Single,System.Single,System.Single,System.Single)
extern void EaseCurve_Evaluate_m4194413CCB04078BCF1EB7FF553A45D9AD76508E (void);
// 0x000002B4 System.Single DG.Tweening.Core.Easing.Flash::Ease(System.Single,System.Single,System.Single,System.Single)
extern void Flash_Ease_m19545C2333831E29B3135F6CA76FAF3D783DC57C (void);
// 0x000002B5 System.Single DG.Tweening.Core.Easing.Flash::EaseIn(System.Single,System.Single,System.Single,System.Single)
extern void Flash_EaseIn_mCC41C58C4297D3FA9C9D889516989CF2CA11A95C (void);
// 0x000002B6 System.Single DG.Tweening.Core.Easing.Flash::EaseOut(System.Single,System.Single,System.Single,System.Single)
extern void Flash_EaseOut_mB897CF26BE9FC1436E9676CF46D36589713C5A53 (void);
// 0x000002B7 System.Single DG.Tweening.Core.Easing.Flash::EaseInOut(System.Single,System.Single,System.Single,System.Single)
extern void Flash_EaseInOut_m55E092635346F3DD417FA21C1BAD24A359D023A8 (void);
// 0x000002B8 System.Single DG.Tweening.Core.Easing.Flash::WeightedEase(System.Single,System.Single,System.Int32,System.Single,System.Single,System.Single)
extern void Flash_WeightedEase_m8251F2A44FEA37052A977A58CBAAB0A15BA148B3 (void);
// 0x000002B9 System.Void DG.Tweening.DOTween/<>c__DisplayClass54_0::.ctor()
extern void U3CU3Ec__DisplayClass54_0__ctor_mF76503C17593526AF9D00FCB60074261461A0A36 (void);
// 0x000002BA System.Single DG.Tweening.DOTween/<>c__DisplayClass54_0::<To>b__0()
extern void U3CU3Ec__DisplayClass54_0_U3CToU3Eb__0_mBE433706DAB16866BFC3584B0D3E98D29932FD19 (void);
// 0x000002BB System.Void DG.Tweening.DOTween/<>c__DisplayClass54_0::<To>b__1(System.Single)
extern void U3CU3Ec__DisplayClass54_0_U3CToU3Eb__1_m7C939C63D34767418CE5BB7C7129380945630BA4 (void);
// 0x000002BC System.Void DG.Tweening.DOVirtual/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m460734B3A8EEEFB285BF5CB2B106CCEED410F872 (void);
// 0x000002BD System.Single DG.Tweening.DOVirtual/<>c__DisplayClass0_0::<Float>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CFloatU3Eb__0_m386A6857F7D208FDBFCE788383F2C8DE1AF9C516 (void);
// 0x000002BE System.Void DG.Tweening.DOVirtual/<>c__DisplayClass0_0::<Float>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CFloatU3Eb__1_m0E6F8890CB27D5DC25CA4FEE8DD54BA314FC32C5 (void);
// 0x000002BF System.Void DG.Tweening.DOVirtual/<>c__DisplayClass0_0::<Float>b__2()
extern void U3CU3Ec__DisplayClass0_0_U3CFloatU3Eb__2_m871516C18DBA820CF68F881A2EE127EC364A5013 (void);
// 0x000002C0 System.Void DG.Tweening.EaseFactory/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mCCCF6E2BBECA7C07FFF31A70C8CAC315B2C45C8A (void);
// 0x000002C1 System.Single DG.Tweening.EaseFactory/<>c__DisplayClass2_0::<StopMotion>b__0(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec__DisplayClass2_0_U3CStopMotionU3Eb__0_m51E68455AD7ECC3CDCE386C30AB9C0069D41ADDA (void);
// 0x000002C2 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m9ECE4AD670FD3ACA531CB4877350603C7A66B94E (void);
// 0x000002C3 System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m99F4938B1C5F9BD5A80EB15D29B854C120F221E9 (void);
// 0x000002C4 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mE3B4BAA7C7136C1E0131799C59D72592AA55B281 (void);
// 0x000002C5 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mDB012068F8814E0863F53559200CF95239208A06 (void);
// 0x000002C6 System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass1_0::<DOPitch>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__0_m2E2662CD37D1D2E0AB328AC4CE36FDCD5CEDA569 (void);
// 0x000002C7 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass1_0::<DOPitch>b__1(System.Single)
extern void U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__1_m8EB80C8975BF3BCE8A5134DDAF3F070AB3549D46 (void);
// 0x000002C8 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mAC36CD0F4CAC1F362B1FE54ED3E2EBD58F019B68 (void);
// 0x000002C9 System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass2_0::<DOAspect>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOAspectU3Eb__0_mAEB20AF1D085C22E17F9FBB0C0C14C3BDB454BC2 (void);
// 0x000002CA System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass2_0::<DOAspect>b__1(System.Single)
extern void U3CU3Ec__DisplayClass2_0_U3CDOAspectU3Eb__1_mE6552DAB6BD0811E748AF67F8C20E823103772E0 (void);
// 0x000002CB System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mE07E29CBFDCAD09EC69C4C003EC82EE797904B3E (void);
// 0x000002CC UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass3_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m856529C7793193878ED4D0E2811F0FCBA47819D4 (void);
// 0x000002CD System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass3_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_m2F4B3463C3680773DED7A5947721062E31821415 (void);
// 0x000002CE System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m963C2102B6D39D8687B6983F5201D2F930A26ED5 (void);
// 0x000002CF System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass4_0::<DOFarClipPlane>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOFarClipPlaneU3Eb__0_m9BF808F709BEC49AF6275550CED29968CF65F276 (void);
// 0x000002D0 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass4_0::<DOFarClipPlane>b__1(System.Single)
extern void U3CU3Ec__DisplayClass4_0_U3CDOFarClipPlaneU3Eb__1_m2A94F8D7B14C8E40FEE514AA85E373744A84473E (void);
// 0x000002D1 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m8C722A93B75A23D0BCB562EBE20F58E8B6DD04A9 (void);
// 0x000002D2 System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass5_0::<DOFieldOfView>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOFieldOfViewU3Eb__0_mDD5A733E43A625DFA63132895B671F8ECB6250CD (void);
// 0x000002D3 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass5_0::<DOFieldOfView>b__1(System.Single)
extern void U3CU3Ec__DisplayClass5_0_U3CDOFieldOfViewU3Eb__1_mEF2ACB1FF7409863EE0E72F057D72F7513C9C2B8 (void);
// 0x000002D4 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_mEE55DE39F1FF8478DDCCD500CAA220C6E8A5E827 (void);
// 0x000002D5 System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass6_0::<DONearClipPlane>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CDONearClipPlaneU3Eb__0_m313071A353F52AC89BFCC677B86AB6C1E37C42BF (void);
// 0x000002D6 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass6_0::<DONearClipPlane>b__1(System.Single)
extern void U3CU3Ec__DisplayClass6_0_U3CDONearClipPlaneU3Eb__1_mA4DEB9BBD55C5274FA0B9B22537EDFD41F3677D8 (void);
// 0x000002D7 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mF0A11C80DD3A0418573858693DA2AF52F8FDB964 (void);
// 0x000002D8 System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass7_0::<DOOrthoSize>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOOrthoSizeU3Eb__0_m487FBE25061B86BBBDA1CD6DEA934C9C0189DE9F (void);
// 0x000002D9 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass7_0::<DOOrthoSize>b__1(System.Single)
extern void U3CU3Ec__DisplayClass7_0_U3CDOOrthoSizeU3Eb__1_m48A57F3A85E601A7DAA7B9AE9924C8A47152B403 (void);
// 0x000002DA System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m9C3F9C15120E46EE54B6D5E3C2792DE7A6FE8DD0 (void);
// 0x000002DB UnityEngine.Rect DG.Tweening.ShortcutExtensions/<>c__DisplayClass8_0::<DOPixelRect>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOPixelRectU3Eb__0_mBCE0B56710CDB1E989F49D0B989E0CD1C788B637 (void);
// 0x000002DC System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass8_0::<DOPixelRect>b__1(UnityEngine.Rect)
extern void U3CU3Ec__DisplayClass8_0_U3CDOPixelRectU3Eb__1_m71539FCE99D11D4ED651CFCFA0CAE7C06D946DB6 (void);
// 0x000002DD System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m4BE4835CF89A3DB5D09B8B11B69DCEFA3D2AF352 (void);
// 0x000002DE UnityEngine.Rect DG.Tweening.ShortcutExtensions/<>c__DisplayClass9_0::<DORect>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDORectU3Eb__0_mDA158912889EC70C55E4E17B369161AAB72080BE (void);
// 0x000002DF System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass9_0::<DORect>b__1(UnityEngine.Rect)
extern void U3CU3Ec__DisplayClass9_0_U3CDORectU3Eb__1_m9E60367B2A9D9F9984F41F01880F5EB366B7CFCD (void);
// 0x000002E0 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m7E1CB370ABD439676854113E6D4E0F297B4D4965 (void);
// 0x000002E1 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass10_0::<DOShakePosition>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOShakePositionU3Eb__0_mB2D828A9F539F4246475DEE44A881CBD958CE429 (void);
// 0x000002E2 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass10_0::<DOShakePosition>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass10_0_U3CDOShakePositionU3Eb__1_m88194783A205B5142F04AAD2469BC2ED8F96A554 (void);
// 0x000002E3 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_mA1788740F370A16A08411900B408D7F0930A54D0 (void);
// 0x000002E4 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass11_0::<DOShakePosition>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CDOShakePositionU3Eb__0_m85BCE4227BDE6CF0A2234C6AFAD1046D65B4FF80 (void);
// 0x000002E5 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass11_0::<DOShakePosition>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass11_0_U3CDOShakePositionU3Eb__1_mE57BC0F6775AF37F06B9F21A43D3389C66D90080 (void);
// 0x000002E6 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mE606C680B1CBA55E547580130862D84B29E9F3B1 (void);
// 0x000002E7 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass12_0::<DOShakeRotation>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CDOShakeRotationU3Eb__0_m1FB86C3985B5CDD89095BB7FD62504D40781B53C (void);
// 0x000002E8 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass12_0::<DOShakeRotation>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass12_0_U3CDOShakeRotationU3Eb__1_mED8B09C58683856F429C145CFD0D4A640BE2F452 (void);
// 0x000002E9 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m8EB6CBD11BA0AA6BF04F64914D4AF92CDE0446C9 (void);
// 0x000002EA UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass13_0::<DOShakeRotation>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CDOShakeRotationU3Eb__0_mD2CD3CF51252C5E126627D2E21C19E6B5472B10C (void);
// 0x000002EB System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass13_0::<DOShakeRotation>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass13_0_U3CDOShakeRotationU3Eb__1_mE75DCD31E11574BE539EEB98C53F60BB5C812FAE (void);
// 0x000002EC System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_mB398AE4CFAE33F76C5158CD09F27890593BA677A (void);
// 0x000002ED UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass14_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass14_0_U3CDOColorU3Eb__0_mF32FF3E3299F0271867C6F256735BFD646279222 (void);
// 0x000002EE System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass14_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass14_0_U3CDOColorU3Eb__1_m0EB9EB4B06BFFC13EECDB01DFC1F13AF8D8DECED (void);
// 0x000002EF System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_mB754754F91F38FD7CDB4DE49EA1117DE804FA44A (void);
// 0x000002F0 System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass15_0::<DOIntensity>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CDOIntensityU3Eb__0_mBA3B54906DC9A96DA27D63EEC74A1596DCB77FFE (void);
// 0x000002F1 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass15_0::<DOIntensity>b__1(System.Single)
extern void U3CU3Ec__DisplayClass15_0_U3CDOIntensityU3Eb__1_mF6CD8062864F9FF02A47237C5BC407801F3A4C56 (void);
// 0x000002F2 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m8C4AD7E5A27A938FBE1FFF399D9856E1F70B686F (void);
// 0x000002F3 System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass16_0::<DOShadowStrength>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CDOShadowStrengthU3Eb__0_m040BF1588DB18DCEB51D6B85B6F5A4BE61AA618C (void);
// 0x000002F4 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass16_0::<DOShadowStrength>b__1(System.Single)
extern void U3CU3Ec__DisplayClass16_0_U3CDOShadowStrengthU3Eb__1_mB8E5409C50C751D2C46781038CE0CED964FE3FE6 (void);
// 0x000002F5 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_m88C223C17347C554FE5C0DC8B21B282C230DA9E9 (void);
// 0x000002F6 DG.Tweening.Color2 DG.Tweening.ShortcutExtensions/<>c__DisplayClass17_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CDOColorU3Eb__0_mFA4A452D27E1DA131356FF322CA1A0973F3285B6 (void);
// 0x000002F7 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass17_0::<DOColor>b__1(DG.Tweening.Color2)
extern void U3CU3Ec__DisplayClass17_0_U3CDOColorU3Eb__1_m45099F313C593379EA8CBDBD5BF9A5C8B2E65737 (void);
// 0x000002F8 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m8E1C09B3484478E1EAB563E857CA0EF71D6EDDA1 (void);
// 0x000002F9 UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass18_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CDOColorU3Eb__0_m3D180482F1BD66D1AAA4284C531D909D0C00D744 (void);
// 0x000002FA System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass18_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass18_0_U3CDOColorU3Eb__1_mBBC0089428F3A9C8187E3A7F828F23E1088DF885 (void);
// 0x000002FB System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_m2A670B0E82C4AED6934BE4C3CE5300965A6ABCFC (void);
// 0x000002FC UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass19_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CDOColorU3Eb__0_mEE514D6E989E5D0F0C0B77F2ED9E16C4A13F863C (void);
// 0x000002FD System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass19_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass19_0_U3CDOColorU3Eb__1_m80042F78DB607C56416F5BC5E425A0B994E5CE76 (void);
// 0x000002FE System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m55A4ABB994BC5C802D7E728EB3BE384C0101579C (void);
// 0x000002FF UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass20_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3CDOFadeU3Eb__0_mE41D0EC1CB35FE35D493977A0578F8E7418F4C8D (void);
// 0x00000300 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass20_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass20_0_U3CDOFadeU3Eb__1_mB6CB8EC51BC054284E08BA64531C2C9A4127A6A5 (void);
// 0x00000301 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mF47F23D94A30DECD1C4BF2C7CB57FEAC0B78CAF8 (void);
// 0x00000302 UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass21_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass21_0_U3CDOFadeU3Eb__0_m772703E02845ED92E977F02CF9DA7535A7C1A077 (void);
// 0x00000303 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass21_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass21_0_U3CDOFadeU3Eb__1_m429B3D8C806B05AF89BCAF3B48FAB2C0C7481669 (void);
// 0x00000304 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m55929F023175FABA6C1479BE42AA987BE28D0364 (void);
// 0x00000305 System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass22_0::<DOFloat>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CDOFloatU3Eb__0_mB928BE369B1D25CCD549D8F66EEBD141670AA57C (void);
// 0x00000306 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass22_0::<DOFloat>b__1(System.Single)
extern void U3CU3Ec__DisplayClass22_0_U3CDOFloatU3Eb__1_mF39934108EB8BA7C4562FFA735B8C3A5719CAF47 (void);
// 0x00000307 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_m80BB7A10B09AC55F0DCFF3DC4176250C0C2ACBBD (void);
// 0x00000308 UnityEngine.Vector2 DG.Tweening.ShortcutExtensions/<>c__DisplayClass23_0::<DOOffset>b__0()
extern void U3CU3Ec__DisplayClass23_0_U3CDOOffsetU3Eb__0_mDCAF44A5CE750A55C8B3036FFE5840DA517F7187 (void);
// 0x00000309 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass23_0::<DOOffset>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass23_0_U3CDOOffsetU3Eb__1_mB0D58FA46EC38462421C4CF778D0385B93C7E4C0 (void);
// 0x0000030A System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m22D05B57D4DC7ECBBFC9C89CD090F01BC5681718 (void);
// 0x0000030B UnityEngine.Vector2 DG.Tweening.ShortcutExtensions/<>c__DisplayClass24_0::<DOOffset>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CDOOffsetU3Eb__0_m3CD2D18FB45199EB95923126EFB3CAEE5D516624 (void);
// 0x0000030C System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass24_0::<DOOffset>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass24_0_U3CDOOffsetU3Eb__1_m27BB1B04A6CEE67B271E355AD7CEBEB7F23E11B7 (void);
// 0x0000030D System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_mEFA552EA3C088F7C20B12C7067495D2975C327BA (void);
// 0x0000030E UnityEngine.Vector2 DG.Tweening.ShortcutExtensions/<>c__DisplayClass25_0::<DOTiling>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CDOTilingU3Eb__0_mFBFE59634B5642736929B22E5E301F7379DF88AA (void);
// 0x0000030F System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass25_0::<DOTiling>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass25_0_U3CDOTilingU3Eb__1_m85ED680808DCD2E7615F2B5A94887D757D8B44FF (void);
// 0x00000310 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m7F47E1F7EC24B6EF21EF5E6D31A46BA5E4E11F98 (void);
// 0x00000311 UnityEngine.Vector2 DG.Tweening.ShortcutExtensions/<>c__DisplayClass26_0::<DOTiling>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CDOTilingU3Eb__0_mEEAB79D1D1A659252E96B2C8F1CCE2C5C3341E07 (void);
// 0x00000312 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass26_0::<DOTiling>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass26_0_U3CDOTilingU3Eb__1_m7C8B83804672258A8D59F587A7ED0561B94DB643 (void);
// 0x00000313 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_m6CF10B7FA9063C154564292CBA53952991737A1A (void);
// 0x00000314 UnityEngine.Vector4 DG.Tweening.ShortcutExtensions/<>c__DisplayClass27_0::<DOVector>b__0()
extern void U3CU3Ec__DisplayClass27_0_U3CDOVectorU3Eb__0_m8377D5B9A8B02DE5B5348E50756D14DE913D0303 (void);
// 0x00000315 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass27_0::<DOVector>b__1(UnityEngine.Vector4)
extern void U3CU3Ec__DisplayClass27_0_U3CDOVectorU3Eb__1_mAF61404889DF199C5192394624ADFB1E2518624E (void);
// 0x00000316 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_m90A534D792092C098AC98F431D3AD3CF5C2E827C (void);
// 0x00000317 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass28_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass28_0_U3CDOMoveU3Eb__0_m03B19B10C49540DED743F03EE4558ECCFA615E95 (void);
// 0x00000318 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_mCDF281D408DA52B7C76F075088B67D5A03034C9F (void);
// 0x00000319 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass29_0::<DOMoveX>b__0()
extern void U3CU3Ec__DisplayClass29_0_U3CDOMoveXU3Eb__0_m6DED4DC130935B09B23EC98A17F052A519615911 (void);
// 0x0000031A System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_mBE3EB40629AF2DD8E24480A6D3D4209A7DA4FC45 (void);
// 0x0000031B UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass30_0::<DOMoveY>b__0()
extern void U3CU3Ec__DisplayClass30_0_U3CDOMoveYU3Eb__0_m20FCFCCB2B3B2164568452FAFB019A22451A4675 (void);
// 0x0000031C System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_m3E8471A32AFDA8B9A8AF3268FB5612B3CF0132CD (void);
// 0x0000031D UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass31_0::<DOMoveZ>b__0()
extern void U3CU3Ec__DisplayClass31_0_U3CDOMoveZU3Eb__0_m1CABD72CD35F76BC8CC04F4B2FEBA1EB87C7C819 (void);
// 0x0000031E System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_mE023046BD5A6D330DAAE4135185540A905CD713C (void);
// 0x0000031F UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass32_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CDORotateU3Eb__0_m671F3CF957422B3BEB125C0ED6883FF39CF5F564 (void);
// 0x00000320 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_mE182EFDA4B21E2565475EA75B711210E1FC6ADBD (void);
// 0x00000321 UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass33_0::<DOLookAt>b__0()
extern void U3CU3Ec__DisplayClass33_0_U3CDOLookAtU3Eb__0_m5E92533756830B20661BAB402472DCAF93B0F757 (void);
// 0x00000322 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m87947EDBE476EB119E99E2BE328DED6A0BB008DE (void);
// 0x00000323 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass34_0::<DOJump>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CDOJumpU3Eb__0_m6A69B42B7E23D4EE27749E23CA88429CDABCA82C (void);
// 0x00000324 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass34_0::<DOJump>b__1()
extern void U3CU3Ec__DisplayClass34_0_U3CDOJumpU3Eb__1_m5075456A1FF9C6AE85D98C033A3788F8FC277BBD (void);
// 0x00000325 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass34_0::<DOJump>b__2()
extern void U3CU3Ec__DisplayClass34_0_U3CDOJumpU3Eb__2_mB56ECAF435FC0811D32DC87D69644B95846BF37C (void);
// 0x00000326 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass34_0::<DOJump>b__3()
extern void U3CU3Ec__DisplayClass34_0_U3CDOJumpU3Eb__3_m3A8B537660EFDE07AD9029E226C9844E88C11F41 (void);
// 0x00000327 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_mD01D581412BA53D097FA2768561DA4BBB711C3E1 (void);
// 0x00000328 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass35_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass35_0_U3CDOPathU3Eb__0_mAC5CB1AAF81C8BD908107AA603AF78B6EE7C93AD (void);
// 0x00000329 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_mB269FDF5927C88DF46FFFD0247C99D03291E8B32 (void);
// 0x0000032A UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass36_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CDOLocalPathU3Eb__0_mACB424383CABC4A3AECC6F59581A51F533840C76 (void);
// 0x0000032B System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass36_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass36_0_U3CDOLocalPathU3Eb__1_mAC32E7674A9FEC018D839654C1C1B452831857E0 (void);
// 0x0000032C System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_m1A1FC7FDC72AB2D61EA947D07507BE7BE6415D7A (void);
// 0x0000032D UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass37_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass37_0_U3CDOPathU3Eb__0_m2D020D59442E575F70DE9F8CE2D73E4DC7433265 (void);
// 0x0000032E System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_mA37B06178CB3001538BB5FB3720BC916DC8A3CA5 (void);
// 0x0000032F UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass38_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass38_0_U3CDOLocalPathU3Eb__0_m43AC6EFB5F6CC305225A7D39465A6735D2BF842F (void);
// 0x00000330 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass38_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass38_0_U3CDOLocalPathU3Eb__1_mECA11BC4C6356B85D38D6493DFB92EC20AF383A5 (void);
// 0x00000331 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m61F78B5D0D38DD49E8DFC7136A45A76E5514F1F5 (void);
// 0x00000332 UnityEngine.Vector2 DG.Tweening.ShortcutExtensions/<>c__DisplayClass39_0::<DOResize>b__0()
extern void U3CU3Ec__DisplayClass39_0_U3CDOResizeU3Eb__0_m4B15595A7CBB995D0E0FA337A70F4012BF1E5C5A (void);
// 0x00000333 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass39_0::<DOResize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass39_0_U3CDOResizeU3Eb__1_mE328EED94BD32D2E8DC98AC6DA5077FC3D6F7C3D (void);
// 0x00000334 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_m7FEF1C22F05CBE0DC9AFE20937F1E9F9DAD0FD47 (void);
// 0x00000335 System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass40_0::<DOTime>b__0()
extern void U3CU3Ec__DisplayClass40_0_U3CDOTimeU3Eb__0_mB5FF71296F11C254CAF01B96108BC7BA70A860E4 (void);
// 0x00000336 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass40_0::<DOTime>b__1(System.Single)
extern void U3CU3Ec__DisplayClass40_0_U3CDOTimeU3Eb__1_m1BA3BBB4989B1FD5441C5FCB743EDBC5D382CD06 (void);
// 0x00000337 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_mD71DAF48B5C0BDD08D77E1E0A86CFB3D690D8CE7 (void);
// 0x00000338 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass41_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass41_0_U3CDOMoveU3Eb__0_m62C57A489D46DF0624110F9F8AF38C940827CD9A (void);
// 0x00000339 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass41_0::<DOMove>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass41_0_U3CDOMoveU3Eb__1_m21FC6CBB1B56045861BDCDC4644F41D462715151 (void);
// 0x0000033A System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass42_0::.ctor()
extern void U3CU3Ec__DisplayClass42_0__ctor_m5DD5B409BA6AD7353C13BA706CAD84F1ED9141B1 (void);
// 0x0000033B UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass42_0::<DOMoveX>b__0()
extern void U3CU3Ec__DisplayClass42_0_U3CDOMoveXU3Eb__0_m69551D55F7FD01A37B60C2AB469B15C49E425E5B (void);
// 0x0000033C System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass42_0::<DOMoveX>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass42_0_U3CDOMoveXU3Eb__1_m727D19EC1CD2D4E60A573063ACD167340AECA556 (void);
// 0x0000033D System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass43_0::.ctor()
extern void U3CU3Ec__DisplayClass43_0__ctor_mA2661B2DBF8E1CF340B1C860E1260BFCF82A5AB4 (void);
// 0x0000033E UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass43_0::<DOMoveY>b__0()
extern void U3CU3Ec__DisplayClass43_0_U3CDOMoveYU3Eb__0_mA65876046B07353BB36FE0020D00E0E6D2F0F65C (void);
// 0x0000033F System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass43_0::<DOMoveY>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass43_0_U3CDOMoveYU3Eb__1_mFCE7900F15A63DEC781791B2DEBB79396EFF595A (void);
// 0x00000340 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_m561007304BF22CD59E5FA30E69B514E603ECA46D (void);
// 0x00000341 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass44_0::<DOMoveZ>b__0()
extern void U3CU3Ec__DisplayClass44_0_U3CDOMoveZU3Eb__0_m748CB8A802995E5DB49F286B8857F9A9B5436767 (void);
// 0x00000342 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass44_0::<DOMoveZ>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass44_0_U3CDOMoveZU3Eb__1_mAAD584B94AD32709950B6FC3389F8744826A4E5E (void);
// 0x00000343 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_mDCA06E2C93223BDAF428170FBDF87029F4618D66 (void);
// 0x00000344 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass45_0::<DOLocalMove>b__0()
extern void U3CU3Ec__DisplayClass45_0_U3CDOLocalMoveU3Eb__0_m2457A3ECE4412BE02D37D269867D76DE446F4EFF (void);
// 0x00000345 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass45_0::<DOLocalMove>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass45_0_U3CDOLocalMoveU3Eb__1_m1D80C2BDA35BF4EDF80C029768E86213C2245C5F (void);
// 0x00000346 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass46_0::.ctor()
extern void U3CU3Ec__DisplayClass46_0__ctor_m28C791D4CFCA8E98EE753EEE233A233E4D077AEF (void);
// 0x00000347 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass46_0::<DOLocalMoveX>b__0()
extern void U3CU3Ec__DisplayClass46_0_U3CDOLocalMoveXU3Eb__0_m4E8CAE10E0A9B31C090AEFAD12B649AB319BD7F6 (void);
// 0x00000348 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass46_0::<DOLocalMoveX>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass46_0_U3CDOLocalMoveXU3Eb__1_m3C14E3D5B7194ED766ABB158F6A83524FB7B0C73 (void);
// 0x00000349 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass47_0::.ctor()
extern void U3CU3Ec__DisplayClass47_0__ctor_mAE5DA37BC6914CD1A7A5839B9DB3CE0D5DC590A0 (void);
// 0x0000034A UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass47_0::<DOLocalMoveY>b__0()
extern void U3CU3Ec__DisplayClass47_0_U3CDOLocalMoveYU3Eb__0_m4CB694090CF0C2CB804B36B7F1E98BA14C751FE1 (void);
// 0x0000034B System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass47_0::<DOLocalMoveY>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass47_0_U3CDOLocalMoveYU3Eb__1_m73B703EC4295353CBBCC58C74A011775AF43CF2B (void);
// 0x0000034C System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass48_0::.ctor()
extern void U3CU3Ec__DisplayClass48_0__ctor_m8FEB64841ECF0DE83AD8256E8B7842ADF0A6C28C (void);
// 0x0000034D UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass48_0::<DOLocalMoveZ>b__0()
extern void U3CU3Ec__DisplayClass48_0_U3CDOLocalMoveZU3Eb__0_mA28BCFFE9F0A4BF59E3FD5FA08BEA69DB9AB94D9 (void);
// 0x0000034E System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass48_0::<DOLocalMoveZ>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass48_0_U3CDOLocalMoveZU3Eb__1_m03009B09B9C01B4EEC01717742AF2E2F7A8081FE (void);
// 0x0000034F System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass49_0::.ctor()
extern void U3CU3Ec__DisplayClass49_0__ctor_m8441E58EFFA3A7A22ACE18A36BADFCBB9FFDA121 (void);
// 0x00000350 UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass49_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass49_0_U3CDORotateU3Eb__0_m0C28D7A399552C203977CFFD04D0476F1A5F53CF (void);
// 0x00000351 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass49_0::<DORotate>b__1(UnityEngine.Quaternion)
extern void U3CU3Ec__DisplayClass49_0_U3CDORotateU3Eb__1_m3D15872923950681AB4AC36CE040BA6F51BBDB54 (void);
// 0x00000352 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass50_0::.ctor()
extern void U3CU3Ec__DisplayClass50_0__ctor_mE7A05719EB9FF09713486D350D640371AE8C8A12 (void);
// 0x00000353 UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass50_0::<DORotateQuaternion>b__0()
extern void U3CU3Ec__DisplayClass50_0_U3CDORotateQuaternionU3Eb__0_mBB5410D8340A681E2F5F2021DC83506088065040 (void);
// 0x00000354 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass50_0::<DORotateQuaternion>b__1(UnityEngine.Quaternion)
extern void U3CU3Ec__DisplayClass50_0_U3CDORotateQuaternionU3Eb__1_mE3A3A35B3B211DE430751CBBCB88A823779A2E60 (void);
// 0x00000355 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass51_0::.ctor()
extern void U3CU3Ec__DisplayClass51_0__ctor_m88B12570D3FA8DA4AB6FEA6C9C459B9D6FBEF02E (void);
// 0x00000356 UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass51_0::<DOLocalRotate>b__0()
extern void U3CU3Ec__DisplayClass51_0_U3CDOLocalRotateU3Eb__0_mBDBC3F6AECA66EDFF7D7E0E4DE1C030919172006 (void);
// 0x00000357 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass51_0::<DOLocalRotate>b__1(UnityEngine.Quaternion)
extern void U3CU3Ec__DisplayClass51_0_U3CDOLocalRotateU3Eb__1_m8601D9DB188B2BD6BBA5988744B7A732309F223D (void);
// 0x00000358 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass52_0::.ctor()
extern void U3CU3Ec__DisplayClass52_0__ctor_mDDAEA1236B676F6073AE29B64A1403AB26ACE6DB (void);
// 0x00000359 UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass52_0::<DOLocalRotateQuaternion>b__0()
extern void U3CU3Ec__DisplayClass52_0_U3CDOLocalRotateQuaternionU3Eb__0_m0ED78CF5D8886A592F6802A51A22218A2ED92280 (void);
// 0x0000035A System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass52_0::<DOLocalRotateQuaternion>b__1(UnityEngine.Quaternion)
extern void U3CU3Ec__DisplayClass52_0_U3CDOLocalRotateQuaternionU3Eb__1_mB53D41BC4DB551A85FB23A8D93D8E84EABF5D9A5 (void);
// 0x0000035B System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass53_0::.ctor()
extern void U3CU3Ec__DisplayClass53_0__ctor_m1C514889BDCD73E4A3A811A9C58465B7805456A8 (void);
// 0x0000035C UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass53_0::<DOScale>b__0()
extern void U3CU3Ec__DisplayClass53_0_U3CDOScaleU3Eb__0_mCC8D7AC358069E730FD972ADF1CFA9CC5C2BED6F (void);
// 0x0000035D System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass53_0::<DOScale>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass53_0_U3CDOScaleU3Eb__1_m270B1F6D3CD2992C7421A85EE2685D8536B155F9 (void);
// 0x0000035E System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass54_0::.ctor()
extern void U3CU3Ec__DisplayClass54_0__ctor_mB3C0B1483FE3E60BA677904B8ADF8B62E907969B (void);
// 0x0000035F UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass54_0::<DOScale>b__0()
extern void U3CU3Ec__DisplayClass54_0_U3CDOScaleU3Eb__0_mD5D6708251F75D9FC6B94B133BE437F98266D942 (void);
// 0x00000360 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass54_0::<DOScale>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass54_0_U3CDOScaleU3Eb__1_m660620CCE701A1AA2FD58871D52238F645041146 (void);
// 0x00000361 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass55_0::.ctor()
extern void U3CU3Ec__DisplayClass55_0__ctor_mF5E9434DE1248F815C8D7EF032AA1ABB5E90C16E (void);
// 0x00000362 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass55_0::<DOScaleX>b__0()
extern void U3CU3Ec__DisplayClass55_0_U3CDOScaleXU3Eb__0_mBC916D4554B9A2194AF6B59CEB88A66C2121AC37 (void);
// 0x00000363 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass55_0::<DOScaleX>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass55_0_U3CDOScaleXU3Eb__1_m7AC8820060FE91213F073BF1533D7870E2ADA8E6 (void);
// 0x00000364 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass56_0::.ctor()
extern void U3CU3Ec__DisplayClass56_0__ctor_m0A4482699B086B67BDF70929CAEB6F007F8D17E6 (void);
// 0x00000365 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass56_0::<DOScaleY>b__0()
extern void U3CU3Ec__DisplayClass56_0_U3CDOScaleYU3Eb__0_mA87DF9618A52B6AB69DE684873CF0B4094185B85 (void);
// 0x00000366 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass56_0::<DOScaleY>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass56_0_U3CDOScaleYU3Eb__1_mDA025DBA85781F57F5BB388C2C0336D4BF092CBC (void);
// 0x00000367 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass57_0::.ctor()
extern void U3CU3Ec__DisplayClass57_0__ctor_m1506C65FA353A0180FDC50712E034200632DCB6B (void);
// 0x00000368 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass57_0::<DOScaleZ>b__0()
extern void U3CU3Ec__DisplayClass57_0_U3CDOScaleZU3Eb__0_m1F774ED5B93D0EE161CCBD92F3848659882AAA1B (void);
// 0x00000369 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass57_0::<DOScaleZ>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass57_0_U3CDOScaleZU3Eb__1_m1765257B1EAC7707BF59A1D1161F6EC582330EDE (void);
// 0x0000036A System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass58_0::.ctor()
extern void U3CU3Ec__DisplayClass58_0__ctor_mED96E830E7A4E8CE92146A98904772D41E084636 (void);
// 0x0000036B UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass58_0::<DOLookAt>b__0()
extern void U3CU3Ec__DisplayClass58_0_U3CDOLookAtU3Eb__0_mEDFDB547323BBF70F23947C1E7338A3A8AE7771F (void);
// 0x0000036C System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass58_0::<DOLookAt>b__1(UnityEngine.Quaternion)
extern void U3CU3Ec__DisplayClass58_0_U3CDOLookAtU3Eb__1_m8EDBD1ECB1362EECCB8C48F307BF8AA9D3665589 (void);
// 0x0000036D System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass59_0::.ctor()
extern void U3CU3Ec__DisplayClass59_0__ctor_mF3B3C7DFA8827D5BCDF897D0B161C5CE19814554 (void);
// 0x0000036E UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass59_0::<DOPunchPosition>b__0()
extern void U3CU3Ec__DisplayClass59_0_U3CDOPunchPositionU3Eb__0_mC3226C94783A791C7391793FBC469C15BC1A4894 (void);
// 0x0000036F System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass59_0::<DOPunchPosition>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass59_0_U3CDOPunchPositionU3Eb__1_m93B4693EE386918AACCCC4CFDD7359AF7E2C2AAD (void);
// 0x00000370 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0::.ctor()
extern void U3CU3Ec__DisplayClass60_0__ctor_m28DEA3D1E5A20A170AC0824BBF3AC5E8579BA3DC (void);
// 0x00000371 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0::<DOPunchScale>b__0()
extern void U3CU3Ec__DisplayClass60_0_U3CDOPunchScaleU3Eb__0_m00A612D7AD8561E87A479BCA800D40E3D074B706 (void);
// 0x00000372 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0::<DOPunchScale>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass60_0_U3CDOPunchScaleU3Eb__1_mBFC473AA6F7BB9D390AEAE259F4E85DFFF9AC608 (void);
// 0x00000373 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0::.ctor()
extern void U3CU3Ec__DisplayClass61_0__ctor_mC3F96960F74D9ADADC62E2B2A78309E847E36084 (void);
// 0x00000374 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0::<DOPunchRotation>b__0()
extern void U3CU3Ec__DisplayClass61_0_U3CDOPunchRotationU3Eb__0_m651AA8503EEEDB24A7A99CABA333D986F5B19490 (void);
// 0x00000375 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0::<DOPunchRotation>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass61_0_U3CDOPunchRotationU3Eb__1_m898D2C59027CFD120D2F3401CAD3DA541D42AC09 (void);
// 0x00000376 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0::.ctor()
extern void U3CU3Ec__DisplayClass62_0__ctor_mE7575A65CFB8E2A6CD8D303EC360759D6C49C29A (void);
// 0x00000377 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0::<DOShakePosition>b__0()
extern void U3CU3Ec__DisplayClass62_0_U3CDOShakePositionU3Eb__0_m556A87CE6DB7638B9257ED9657716BBA8B630317 (void);
// 0x00000378 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0::<DOShakePosition>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass62_0_U3CDOShakePositionU3Eb__1_mDA035DF61C90285E91BBE309137E1CC8A0B39B76 (void);
// 0x00000379 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass63_0::.ctor()
extern void U3CU3Ec__DisplayClass63_0__ctor_m751095F9FBF992B99188F0DD2A59B05C59F45FC6 (void);
// 0x0000037A UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass63_0::<DOShakePosition>b__0()
extern void U3CU3Ec__DisplayClass63_0_U3CDOShakePositionU3Eb__0_m9BA11BB61DA5F013C2242EC75E31394F5DDD1B35 (void);
// 0x0000037B System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass63_0::<DOShakePosition>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass63_0_U3CDOShakePositionU3Eb__1_mC42437BCBF7B7E8A5791EABFA940FC23D2D904C5 (void);
// 0x0000037C System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass64_0::.ctor()
extern void U3CU3Ec__DisplayClass64_0__ctor_m93FA757C2F100A9B98DF73369A358E2B80BC0247 (void);
// 0x0000037D UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass64_0::<DOShakeRotation>b__0()
extern void U3CU3Ec__DisplayClass64_0_U3CDOShakeRotationU3Eb__0_m75D4F7FB68D23D9B16494147086BD25511519323 (void);
// 0x0000037E System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass64_0::<DOShakeRotation>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass64_0_U3CDOShakeRotationU3Eb__1_mF39C403B79DF9FEA3EF47E6AEE2CAF3222D98843 (void);
// 0x0000037F System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass65_0::.ctor()
extern void U3CU3Ec__DisplayClass65_0__ctor_mB5D4F70938E7752219827AB7C3E0F4A88DBB5300 (void);
// 0x00000380 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass65_0::<DOShakeRotation>b__0()
extern void U3CU3Ec__DisplayClass65_0_U3CDOShakeRotationU3Eb__0_mA6C8154BFB8700E4F91DB116F6E752C1D693F1A4 (void);
// 0x00000381 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass65_0::<DOShakeRotation>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass65_0_U3CDOShakeRotationU3Eb__1_m5DF0A97E2EB382969BECD80858328D56F4641BA5 (void);
// 0x00000382 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass66_0::.ctor()
extern void U3CU3Ec__DisplayClass66_0__ctor_mB3E82C541A24DA7A41932576C1FA9DB1E54A6D0E (void);
// 0x00000383 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass66_0::<DOShakeScale>b__0()
extern void U3CU3Ec__DisplayClass66_0_U3CDOShakeScaleU3Eb__0_mEBC36ACEBE6CE4B1E8A81D470928593DA2A4361E (void);
// 0x00000384 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass66_0::<DOShakeScale>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass66_0_U3CDOShakeScaleU3Eb__1_m96237B5163EB288592FC551A6BF3E4B669537A92 (void);
// 0x00000385 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass67_0::.ctor()
extern void U3CU3Ec__DisplayClass67_0__ctor_m9452D35C9DCEA16B9778C855E5FA8B4D3A2401DA (void);
// 0x00000386 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass67_0::<DOShakeScale>b__0()
extern void U3CU3Ec__DisplayClass67_0_U3CDOShakeScaleU3Eb__0_mAA98EAA2A062CCC8A4FC4DC9565B28B7E6973034 (void);
// 0x00000387 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass67_0::<DOShakeScale>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass67_0_U3CDOShakeScaleU3Eb__1_mD696AC0C6F961F707AC1EAB2397A1C0B42419C71 (void);
// 0x00000388 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::.ctor()
extern void U3CU3Ec__DisplayClass68_0__ctor_mE195658976E1930732C2B2F3D0CC128E49CAFF1C (void);
// 0x00000389 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::<DOJump>b__0()
extern void U3CU3Ec__DisplayClass68_0_U3CDOJumpU3Eb__0_mA1F36D5344D3D488C71733ED54C0B6B54152FFD2 (void);
// 0x0000038A System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::<DOJump>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass68_0_U3CDOJumpU3Eb__1_m50605D00E3CD9268DE5D79AE29D0869AE6E45161 (void);
// 0x0000038B System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::<DOJump>b__2()
extern void U3CU3Ec__DisplayClass68_0_U3CDOJumpU3Eb__2_mFA8683E8AE0FA11EEC7A95F5EDB0F13C17A8608C (void);
// 0x0000038C UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::<DOJump>b__3()
extern void U3CU3Ec__DisplayClass68_0_U3CDOJumpU3Eb__3_m61E575832ECE4F9649A978B0D5739B6392698E9F (void);
// 0x0000038D System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::<DOJump>b__4(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass68_0_U3CDOJumpU3Eb__4_mD562A20F84AFB44CCF566557B47961A403C6A6AE (void);
// 0x0000038E UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::<DOJump>b__5()
extern void U3CU3Ec__DisplayClass68_0_U3CDOJumpU3Eb__5_mB76CBD86CBB3CAEBD87CE85F8EBDF6D8E34A8B2E (void);
// 0x0000038F System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::<DOJump>b__6(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass68_0_U3CDOJumpU3Eb__6_m23BE9D64DC96D25D9CF432C5321A219D7D1985B6 (void);
// 0x00000390 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::.ctor()
extern void U3CU3Ec__DisplayClass69_0__ctor_m168BC1CFB89D50382CF943A5485D5E42862021EF (void);
// 0x00000391 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::<DOLocalJump>b__0()
extern void U3CU3Ec__DisplayClass69_0_U3CDOLocalJumpU3Eb__0_m9ACFEFEC7CB8E1F5E58296479FC252A213E76054 (void);
// 0x00000392 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::<DOLocalJump>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass69_0_U3CDOLocalJumpU3Eb__1_m6F3DFDC993896028642050DD559167D530C7A0B4 (void);
// 0x00000393 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::<DOLocalJump>b__2()
extern void U3CU3Ec__DisplayClass69_0_U3CDOLocalJumpU3Eb__2_m26DFFD6A4599ECEA1E9861B8D77AFEB635C09B1D (void);
// 0x00000394 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::<DOLocalJump>b__3()
extern void U3CU3Ec__DisplayClass69_0_U3CDOLocalJumpU3Eb__3_mBF5ABF833F09FAA2A4F60541FCC437D4BE88A457 (void);
// 0x00000395 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::<DOLocalJump>b__4(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass69_0_U3CDOLocalJumpU3Eb__4_mB1A8C8DD403AE1830C71CC24ACC5DBBB87F865AE (void);
// 0x00000396 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::<DOLocalJump>b__5()
extern void U3CU3Ec__DisplayClass69_0_U3CDOLocalJumpU3Eb__5_m7A12906348DA7ED8E82519DCD9E86A23617825C4 (void);
// 0x00000397 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::<DOLocalJump>b__6(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass69_0_U3CDOLocalJumpU3Eb__6_mE534589F1F34065D31ACD68C537D896751DE5BC1 (void);
// 0x00000398 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0::.ctor()
extern void U3CU3Ec__DisplayClass70_0__ctor_m3FFF8D0AAEC236077E9CF42D4E14CA273987EAD4 (void);
// 0x00000399 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass70_0_U3CDOPathU3Eb__0_m9A0C0A67AD6839BB579ED12F821092B8DB23ECEA (void);
// 0x0000039A System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0::<DOPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass70_0_U3CDOPathU3Eb__1_m3880CA477A405C9010D0A70F0EA14B1623A6E1FC (void);
// 0x0000039B System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass71_0::.ctor()
extern void U3CU3Ec__DisplayClass71_0__ctor_m8984100650DE98B17C87FB26DE5E6E7EF4CA30A5 (void);
// 0x0000039C UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass71_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass71_0_U3CDOLocalPathU3Eb__0_mE1D5F3EB1B500B51BE3A56D3336F03EA2F9D936F (void);
// 0x0000039D System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass71_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass71_0_U3CDOLocalPathU3Eb__1_m82CD11B88E1148540B1C87E156DA78CFADC54182 (void);
// 0x0000039E System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0::.ctor()
extern void U3CU3Ec__DisplayClass72_0__ctor_m8EA9B1DDB9F19E37B8EA9234BDEC50CD76096CFE (void);
// 0x0000039F UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass72_0_U3CDOPathU3Eb__0_mD354490B9D2FCCEFA61AA07CE969A25E7BC744B3 (void);
// 0x000003A0 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0::<DOPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass72_0_U3CDOPathU3Eb__1_m9FC9CC3F1BC6A9825C6AAAACC009A80B9002601E (void);
// 0x000003A1 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass73_0::.ctor()
extern void U3CU3Ec__DisplayClass73_0__ctor_mBA657FF3A1A66BAD348402D55C0CBF907EF04500 (void);
// 0x000003A2 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass73_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass73_0_U3CDOLocalPathU3Eb__0_m6F62BCA3F8EA1C0B289DAE5D29BB98B91637182D (void);
// 0x000003A3 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass73_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass73_0_U3CDOLocalPathU3Eb__1_mB5E0EB45470B749C89A1AFB16B94A5A4DD70C8A7 (void);
// 0x000003A4 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass74_0::.ctor()
extern void U3CU3Ec__DisplayClass74_0__ctor_mB149BC27C91715D1390070AC1611EC0B5E407DA4 (void);
// 0x000003A5 System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass74_0::<DOTimeScale>b__0()
extern void U3CU3Ec__DisplayClass74_0_U3CDOTimeScaleU3Eb__0_m3E6D393D63F227A1FE0344D251FE6062A7733798 (void);
// 0x000003A6 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass74_0::<DOTimeScale>b__1(System.Single)
extern void U3CU3Ec__DisplayClass74_0_U3CDOTimeScaleU3Eb__1_m23DE9C348FA50B978BCBE8F82F0AEB0685D2EB85 (void);
// 0x000003A7 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0::.ctor()
extern void U3CU3Ec__DisplayClass75_0__ctor_m3E8EEFA5EFC6F9B932892AF9DEF001B386A742C4 (void);
// 0x000003A8 UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass75_0_U3CDOBlendableColorU3Eb__0_m95C20EC251CB95704A7CF56D5C44AF4452A31C40 (void);
// 0x000003A9 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass75_0_U3CDOBlendableColorU3Eb__1_m8C706B706D7E3413BDB9E78A29B3E12D21C7F405 (void);
// 0x000003AA System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0::.ctor()
extern void U3CU3Ec__DisplayClass76_0__ctor_m21AC20B4D851EC2183AACE433C2FC734EBF9DE3E (void);
// 0x000003AB UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass76_0_U3CDOBlendableColorU3Eb__0_m4700D2A464D3BA139AB08AE4E8B7C371A9C9DFA3 (void);
// 0x000003AC System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass76_0_U3CDOBlendableColorU3Eb__1_mBD3E852C2C7DE134BDD399079EC8AD6D414F73F3 (void);
// 0x000003AD System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0::.ctor()
extern void U3CU3Ec__DisplayClass77_0__ctor_mE3F1213264B3DAB7BDB42F3B7AD72F0B454EF3CC (void);
// 0x000003AE UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass77_0_U3CDOBlendableColorU3Eb__0_mCE3589B0887759940AEB1BE9487320F338E92BFB (void);
// 0x000003AF System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass77_0_U3CDOBlendableColorU3Eb__1_mB9076AE89488405260928B8AFB8298C2E29898CD (void);
// 0x000003B0 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass78_0::.ctor()
extern void U3CU3Ec__DisplayClass78_0__ctor_mC945331E7A543C50DB1C06B2DDE915BDA17445E3 (void);
// 0x000003B1 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass78_0::<DOBlendableMoveBy>b__0()
extern void U3CU3Ec__DisplayClass78_0_U3CDOBlendableMoveByU3Eb__0_m34327B601092B8EC0063433ACF4B675B1916F262 (void);
// 0x000003B2 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass78_0::<DOBlendableMoveBy>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass78_0_U3CDOBlendableMoveByU3Eb__1_mBA2C42167D25EA510E5F45D475F6069D3BE07FB0 (void);
// 0x000003B3 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass79_0::.ctor()
extern void U3CU3Ec__DisplayClass79_0__ctor_m9ACAC8B9F78745771E6F5D79135A2F48D4E45FF3 (void);
// 0x000003B4 UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass79_0::<DOBlendableLocalMoveBy>b__0()
extern void U3CU3Ec__DisplayClass79_0_U3CDOBlendableLocalMoveByU3Eb__0_mA7417C35BC4493F589D21ED50D2DB534B89C0720 (void);
// 0x000003B5 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass79_0::<DOBlendableLocalMoveBy>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass79_0_U3CDOBlendableLocalMoveByU3Eb__1_m3F206A1C3000246E24EF27FA9AD2A6A40236FE6B (void);
// 0x000003B6 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass80_0::.ctor()
extern void U3CU3Ec__DisplayClass80_0__ctor_m206F5342AD73218D71E40A75225296C8283248A1 (void);
// 0x000003B7 UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass80_0::<DOBlendableRotateBy>b__0()
extern void U3CU3Ec__DisplayClass80_0_U3CDOBlendableRotateByU3Eb__0_m27F0458B5FA3804D85B104E6A04BE1110F9D8429 (void);
// 0x000003B8 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass80_0::<DOBlendableRotateBy>b__1(UnityEngine.Quaternion)
extern void U3CU3Ec__DisplayClass80_0_U3CDOBlendableRotateByU3Eb__1_mD2AA7CB2FF9EBE3C2B0327F7C166BFD268CB049B (void);
// 0x000003B9 System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass81_0::.ctor()
extern void U3CU3Ec__DisplayClass81_0__ctor_m42E5D2CB7CCD8208A59348F69263245C461C9D93 (void);
// 0x000003BA UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass81_0::<DOBlendableLocalRotateBy>b__0()
extern void U3CU3Ec__DisplayClass81_0_U3CDOBlendableLocalRotateByU3Eb__0_m161F5C4A89F61BEE9C07983B244A0C9CB9A0C0E2 (void);
// 0x000003BB System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass81_0::<DOBlendableLocalRotateBy>b__1(UnityEngine.Quaternion)
extern void U3CU3Ec__DisplayClass81_0_U3CDOBlendableLocalRotateByU3Eb__1_m0B659E64749A5E5E4B4851CEC3558108DE8944B7 (void);
// 0x000003BC System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass82_0::.ctor()
extern void U3CU3Ec__DisplayClass82_0__ctor_m392015F9097A9F33F12F6B392F6C3E4C7C47BD91 (void);
// 0x000003BD UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass82_0::<DOBlendableScaleBy>b__0()
extern void U3CU3Ec__DisplayClass82_0_U3CDOBlendableScaleByU3Eb__0_m73E4E201C3B816C8BD38045B19B630CD56FB0B41 (void);
// 0x000003BE System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass82_0::<DOBlendableScaleBy>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass82_0_U3CDOBlendableScaleByU3Eb__1_m9342366ED6735575E70FA58CFE905D2978E32D26 (void);
// 0x000003BF System.Void DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__14::.ctor(System.Int32)
extern void U3CWaitForCompletionU3Ed__14__ctor_mE7533C100A0B5404B3FC93CEA6A0C37827870A87 (void);
// 0x000003C0 System.Void DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__14::System.IDisposable.Dispose()
extern void U3CWaitForCompletionU3Ed__14_System_IDisposable_Dispose_m303A8F433405425BDC09AAE0F023A63D315B1EF9 (void);
// 0x000003C1 System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__14::MoveNext()
extern void U3CWaitForCompletionU3Ed__14_MoveNext_mFEF5F6183204655EDB9653AB2411907380008298 (void);
// 0x000003C2 System.Object DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForCompletionU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE19223304A68A35794CA6F24216AAFD892EA5AC1 (void);
// 0x000003C3 System.Void DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__14::System.Collections.IEnumerator.Reset()
extern void U3CWaitForCompletionU3Ed__14_System_Collections_IEnumerator_Reset_mF6C6790F450FE739E25B336C52E64226C575BD95 (void);
// 0x000003C4 System.Object DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForCompletionU3Ed__14_System_Collections_IEnumerator_get_Current_mC04EEB82100E6C38A307534B0B9C28060021DB16 (void);
// 0x000003C5 System.Void DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__15::.ctor(System.Int32)
extern void U3CWaitForRewindU3Ed__15__ctor_m37784760A70E04AC02EC1D582B83F4668111A5A0 (void);
// 0x000003C6 System.Void DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__15::System.IDisposable.Dispose()
extern void U3CWaitForRewindU3Ed__15_System_IDisposable_Dispose_m3B4A63F07D1DD69D384626E147834AD7F9149A26 (void);
// 0x000003C7 System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__15::MoveNext()
extern void U3CWaitForRewindU3Ed__15_MoveNext_m1E84ABD217A5D6C6C72C96771EE8F8C9B2F76D8A (void);
// 0x000003C8 System.Object DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForRewindU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD0CB3F77C35B9E9981BA568AF23A35F479B91C53 (void);
// 0x000003C9 System.Void DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__15::System.Collections.IEnumerator.Reset()
extern void U3CWaitForRewindU3Ed__15_System_Collections_IEnumerator_Reset_m9C0EC279DBA63D45FEB58C3E702A8BB4858BA619 (void);
// 0x000003CA System.Object DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForRewindU3Ed__15_System_Collections_IEnumerator_get_Current_m750B3811AE0DB4A6F9805BECB8DF2596A2C54F3B (void);
// 0x000003CB System.Void DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__16::.ctor(System.Int32)
extern void U3CWaitForKillU3Ed__16__ctor_m6EE0C594B82600E5E99AD05056367F5325ABD2FB (void);
// 0x000003CC System.Void DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__16::System.IDisposable.Dispose()
extern void U3CWaitForKillU3Ed__16_System_IDisposable_Dispose_m3732D9849EC07BDD486028486DEF03885C650C09 (void);
// 0x000003CD System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__16::MoveNext()
extern void U3CWaitForKillU3Ed__16_MoveNext_m2159DC2ED05D95253A737C47F71D27C147E85441 (void);
// 0x000003CE System.Object DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForKillU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD22099F2EB9F7275F4C9664AD9657CF571A45DC6 (void);
// 0x000003CF System.Void DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__16::System.Collections.IEnumerator.Reset()
extern void U3CWaitForKillU3Ed__16_System_Collections_IEnumerator_Reset_mDA1D8BF979E23C98EEA6DAF7B2C2230ABB1B10F4 (void);
// 0x000003D0 System.Object DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForKillU3Ed__16_System_Collections_IEnumerator_get_Current_mBDACB19F06D62D737C514DAC0CCBC697F44DAA9B (void);
// 0x000003D1 System.Void DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__17::.ctor(System.Int32)
extern void U3CWaitForElapsedLoopsU3Ed__17__ctor_m1AEA52887D651620F95BF3075BFB69D4D91747CC (void);
// 0x000003D2 System.Void DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__17::System.IDisposable.Dispose()
extern void U3CWaitForElapsedLoopsU3Ed__17_System_IDisposable_Dispose_m5B505953396B4222005CF5E9C20CC9D4B6C5C041 (void);
// 0x000003D3 System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__17::MoveNext()
extern void U3CWaitForElapsedLoopsU3Ed__17_MoveNext_m2FE7359E892D86FB3A1C828A321EFA8C94B7DE29 (void);
// 0x000003D4 System.Object DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForElapsedLoopsU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB6B87B9867FA04AA0FC215032805FEC243A46FDC (void);
// 0x000003D5 System.Void DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__17::System.Collections.IEnumerator.Reset()
extern void U3CWaitForElapsedLoopsU3Ed__17_System_Collections_IEnumerator_Reset_m67224BDA06DBD0C7B2A8465BF9B0B732C2A4B394 (void);
// 0x000003D6 System.Object DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForElapsedLoopsU3Ed__17_System_Collections_IEnumerator_get_Current_m8801CA279BCD694AC804BA6B9702C486A2567CCC (void);
// 0x000003D7 System.Void DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__18::.ctor(System.Int32)
extern void U3CWaitForPositionU3Ed__18__ctor_m0265088734E4649A7C2DA707563A5BB4C5D1ED2E (void);
// 0x000003D8 System.Void DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__18::System.IDisposable.Dispose()
extern void U3CWaitForPositionU3Ed__18_System_IDisposable_Dispose_m1D8512DD5734B0F334C7D86B5189495981BB9067 (void);
// 0x000003D9 System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__18::MoveNext()
extern void U3CWaitForPositionU3Ed__18_MoveNext_m248E3EAEE5300311FF2BE2AE6AADB8B567CE1AA8 (void);
// 0x000003DA System.Object DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForPositionU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF944285E4CEC654F6F2A4ED68C5BD7C37BAF360 (void);
// 0x000003DB System.Void DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__18::System.Collections.IEnumerator.Reset()
extern void U3CWaitForPositionU3Ed__18_System_Collections_IEnumerator_Reset_m5199C8176C24AB72FCAC16AA9CA6997EEC5C36B5 (void);
// 0x000003DC System.Object DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForPositionU3Ed__18_System_Collections_IEnumerator_get_Current_m1230933A96DB7238797F1C0B3E1592669D367581 (void);
// 0x000003DD System.Void DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__19::.ctor(System.Int32)
extern void U3CWaitForStartU3Ed__19__ctor_mC5BB3CB2C97AA15519D6CB641CAE394F1DB11C2E (void);
// 0x000003DE System.Void DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__19::System.IDisposable.Dispose()
extern void U3CWaitForStartU3Ed__19_System_IDisposable_Dispose_mDC8918991DDE6C8EBA355B49557FEB6EDC331B31 (void);
// 0x000003DF System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__19::MoveNext()
extern void U3CWaitForStartU3Ed__19_MoveNext_m7273BB46ADA2EEB3D45328F92A20677718E8F3E4 (void);
// 0x000003E0 System.Object DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForStartU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51827FF406E046E75CB7BB1FBCAD94D5CFD4E6A4 (void);
// 0x000003E1 System.Void DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__19::System.Collections.IEnumerator.Reset()
extern void U3CWaitForStartU3Ed__19_System_Collections_IEnumerator_Reset_mE24D88E9B004C8AB98E432B4963736DC69468813 (void);
// 0x000003E2 System.Object DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForStartU3Ed__19_System_Collections_IEnumerator_get_Current_mE1637A2CE0B2145054A0E512DE310A31C8FB4A57 (void);
// 0x000003E3 System.Void DG.Tweening.Core.Easing.EaseManager/<>c::.cctor()
extern void U3CU3Ec__cctor_mB7D6B119DEABF2D27B0E2DF78C19745EAE02C6B5 (void);
// 0x000003E4 System.Void DG.Tweening.Core.Easing.EaseManager/<>c::.ctor()
extern void U3CU3Ec__ctor_mCEBEFF6B6FECAAFB0E1D7744298A0C816DCC42BB (void);
// 0x000003E5 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_0(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_0_m8422CB8EE877F7AC7975590F4430CD16467FD2D5 (void);
// 0x000003E6 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_1(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_1_mA4FA951639F14C84A46BCF3DEF064594BED04FF0 (void);
// 0x000003E7 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_2(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_2_mC63DDF643E5EE7DA17E58A8E0ABD2881DA51623C (void);
// 0x000003E8 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_3(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_3_m92C37170BA2074145756259E716DD6CBDAA58928 (void);
// 0x000003E9 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_4(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_4_m79B14CBC25F458C1C144E7CA6D4E00D0078FFEF9 (void);
// 0x000003EA System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_5(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_5_mF42AEF55B3EAEBB5F69155345B050A57732F13CE (void);
// 0x000003EB System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_6(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_6_m6F5CCA6C15982F075F52ACE4D1DD88118DF2FB3B (void);
// 0x000003EC System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_7(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_7_m7C0A9ABB7AB2FA66626A3C422EA0A0734F192F3C (void);
// 0x000003ED System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_8(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_8_mAE66D99FB7EDBC75B1EB6B52BF220931C078007E (void);
// 0x000003EE System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_9(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_9_mD12823F20B4DC939D2E6537779DA5B820A6BECF8 (void);
// 0x000003EF System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_10(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_10_mEB2A499C6F094747DB9C46ECFF0EEB6405A831F6 (void);
// 0x000003F0 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_11(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_11_m189CD77E0724EFC30E23444610543605B004B37F (void);
// 0x000003F1 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_12(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_12_mA9B325A277839DED4276CDC80B7E234C576E3099 (void);
// 0x000003F2 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_13(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_13_m996454852D07FED1E1D94880957E26CFD30D0315 (void);
// 0x000003F3 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_14(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_14_m42C5B6ECB50310C06836FE9D91205D91906E38F8 (void);
// 0x000003F4 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_15(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_15_mEF5B627D4123E82693BF46ACA2AE47E1299A4265 (void);
// 0x000003F5 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_16(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_16_m2A5850539FDC19F19F1B8E6F4310574B8C4BDB17 (void);
// 0x000003F6 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_17(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_17_m7AB0A1C30ACEB82AAEBF34F22272FCD2B1A959E9 (void);
// 0x000003F7 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_18(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_18_m554E547B8438F3336855FCBA3236A0B08BFCA6FF (void);
// 0x000003F8 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_19(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_19_m05EB95C055B75FFDEAA77F159B4DBB1FFAEAE01E (void);
// 0x000003F9 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_20(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_20_m6BE84CDF139FB97FCCC64EE13D69B8CD99662F77 (void);
// 0x000003FA System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_21(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_21_m3CEBD0982B0561CB39EA0F295F7E253669F9984B (void);
// 0x000003FB System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_22(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_22_m6D23875F0A658C5DFE6BFF8D3E37825D93105522 (void);
// 0x000003FC System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_23(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_23_mA4141804D1FFA686B4DA2F128E8B1C6AD271D69A (void);
// 0x000003FD System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_24(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_24_m952E9771551239F0DBA53B7ADA44E7DA0F70A36F (void);
// 0x000003FE System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_25(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_25_m5A35078F32346ECF7C0125AAF8F2295E5A58B128 (void);
// 0x000003FF System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_26(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_26_m2349F372B4B11CDD4512EB2616553C553D81CC77 (void);
// 0x00000400 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_27(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_27_mC2192BD27C605C82942AC991125E69E62557E51E (void);
// 0x00000401 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_28(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_28_mD8A7A5269E41AEA94F2008884A31D7E76447C204 (void);
// 0x00000402 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_29(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_29_m951B17D9BE05EFF08958ED094B075CD6DDF6CB68 (void);
// 0x00000403 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_30(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_30_mC14074E2AFB650112DFB01AFAA1E626E344B8A60 (void);
// 0x00000404 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_31(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_31_m4B774FB374D0C7CF5024F7F311794E0FC4CF38FA (void);
// 0x00000405 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_32(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_32_m92DC339E14ACB0D97A8E37F518919DDA2D5AE081 (void);
// 0x00000406 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_33(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_33_m4A09D0C05A6647EF50C04183DB7C7ABB468F89A8 (void);
// 0x00000407 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_34(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_34_m958048AD5E79C4FE563F0738EECABD4AC9383ADC (void);
// 0x00000408 System.Single DG.Tweening.Core.Easing.EaseManager/<>c::<ToEaseFunction>b__4_35(System.Single,System.Single,System.Single,System.Single)
extern void U3CU3Ec_U3CToEaseFunctionU3Eb__4_35_m26D1751E54BD8E843A0647A57379070FD1822ABF (void);
static Il2CppMethodPointer s_methodPointers[1032] = 
{
	Color2__ctor_mB17DD3F311A7AA9AC3FA318105CF03900034C65B,
	Color2_op_Addition_m27B3C091014E030F734960C719FEB1AD82B1795A,
	Color2_op_Subtraction_mEFEC3CBDA05EA5309532AF832A903D278546B354,
	Color2_op_Multiply_m35AA0399C57ED8164080363F483750D51B912540,
	TweenCallback__ctor_m595231CFB0B8CD35F6377EA2A88CB98A8E905662,
	TweenCallback_Invoke_mB38F3AB4456E2A55F93CECFB3C4A529248CBB82E,
	TweenCallback_BeginInvoke_m27BCFEAAE71F1A44A7121006B63DB9C0EAC8BD85,
	TweenCallback_EndInvoke_m28D9821FD5AEF34270D209860EB15D885FD2E8F0,
	NULL,
	NULL,
	NULL,
	NULL,
	EaseFunction__ctor_m8C63A6FD3BBACF51D464452AC05DA58BC3419466,
	EaseFunction_Invoke_m21560094511BC4885165BDCD850958673B9B0787,
	EaseFunction_BeginInvoke_mD57FFED181577E1CE4CA2939238E785C861B6BC3,
	EaseFunction_EndInvoke_m87B0A07FE2D16908B9721A2CD543AC61D7E8AD78,
	DOTween_get_logBehaviour_mB40FBDD2C7139800D31987C9D6906A53AD62ABF0,
	DOTween_set_logBehaviour_m7AE2BAE33CC9A19A5A0A86BB53B2610C12EBF4F6,
	DOTween__cctor_m8AC5173822F9EE1B0DC970CC7644D5A2ED1C8E0A,
	DOTween_Init_m3400C37EBD33639098CC99D4DFB42ED26B588B6A,
	DOTween_AutoInit_m91FB3CB67999828CF2A4C0FF22AB5CDABB47464E,
	DOTween_Init_m32C148E25F2292B1DC757AA992EE5EBBCA54721B,
	DOTween_SetTweensCapacity_mCE1F466A5044C13B82614B3652791832BC1D9934,
	DOTween_Clear_m04B8E98534F91A012F1D157BFAFFC1314FB0E98B,
	DOTween_ClearCachedTweens_m60BA2402547DA142D3023755407F8A70C96934C0,
	DOTween_Validate_mCBD23A691CE3D3C02DB79BEFC991005690D5CEDB,
	DOTween_ManualUpdate_m8F589B4EE2E6EC705603FB827A20B79A0A2E6035,
	DOTween_To_mB87E880F73BF460ADEDDDDA0D0E96781E9852BF4,
	DOTween_To_m951A8CFEF441742F76561A910423B523E153D2C7,
	DOTween_To_m7BCA0F692B6E84F0B9215471A8ABB9241014EC03,
	DOTween_To_m9435E751D7104AD4B7B9B25B0059E8AFF1FCB654,
	DOTween_To_m2A479DD6F7A22C7A8335674E71E4E83AEAAA3C32,
	DOTween_To_m71864E7EE752F53C6FAE67B7E4101E459DD8CAAA,
	DOTween_To_m7F1A7981B718FBBC50C35F426AAFB836207BB2CC,
	DOTween_To_m08A8B415E04DF5FD6ED278A7912A8A1167A811A3,
	DOTween_To_mC70B880FDF6DBD25D94D655E708BF4826A4AAF10,
	DOTween_To_mB51344CEBA06CEB655A922284A52FDE0DB82F9DE,
	DOTween_To_mD5F2402188B7E46E02B9282A000B99C61AAC89C4,
	DOTween_To_mE294CB006DFDA920942DE3D1BD24C4701EBCE90E,
	DOTween_To_m787DA25F8DC1481EACC3393F64E9A26C03276178,
	DOTween_To_m383D6D52921F98622D35990E9A3C567FD66AA940,
	NULL,
	DOTween_ToAxis_m2FFC3E0664FDFF539F457A8A92970A458CE83E73,
	DOTween_ToAlpha_m3EF34978A74FEA83A950E85150829B53027E521E,
	DOTween_To_m310347ACE0C4FAB0E862ECBD1CEE447F8BE25E81,
	DOTween_Punch_mE74751F7ED2DC39DCEF7422481F69E9A26FC3D83,
	DOTween_Shake_mC4CC0305697D6672E0050A3095922402999118CB,
	DOTween_Shake_m82F8898EC94774142E744EEF6282391CCE5942E3,
	DOTween_Shake_m94A185F3C88C55EAEFE89F7BA7756A4A3C9B75FB,
	DOTween_ToArray_m93EE354BBABCDAA9E304710A8AA1A1E5A2F5076E,
	DOTween_To_mC82BC7E459E1DA5E6C74E722A44C1FC8E69B8D9C,
	DOTween_Sequence_m83221E315CE42CCE7B80D126A549114C86BC388D,
	DOTween_CompleteAll_mFA01DDBC9F8C0F307E563CECE9346BA247412DED,
	DOTween_Complete_mF9C035573D7C3F51938F073B38FC3B5EA8F640B9,
	DOTween_CompleteAndReturnKilledTot_mF2C24EB8883F5051AD947C081A28DE41FDE127C4,
	DOTween_CompleteAndReturnKilledTot_m55B28CC146DF1B721928E206A89A5B1B4F742716,
	DOTween_CompleteAndReturnKilledTotExceptFor_m1B34E13253A3590FD3BB5FCAB4091C6F7D84EC5A,
	DOTween_FlipAll_mAA6808ADC02BCDD02F3A8C47685145FD998809C0,
	DOTween_Flip_m915AEB3768FB2BB2F503A56F5CFF26A1394FCF5E,
	DOTween_GotoAll_m0E77B61840AC491300DC0E2BB393876E5DDDFBAD,
	DOTween_Goto_m6B67C3CE020FC056F07743718C9DC9DEDFC7AE96,
	DOTween_KillAll_m9A5541F18DA9DB46CC1C9AA1FE9A848AA31CFA53,
	DOTween_KillAll_mC78D7B22BB32344450F9A3E7BDBCC36E4B3A6E3A,
	DOTween_Kill_mF0F7315CA9F8E32145CA3E3C9249FBF98BF3AD2A,
	DOTween_PauseAll_mD2D3214E242AE9309B4B4206768D4B6A451E5641,
	DOTween_Pause_mD2AA03C8EDAAA5154478FA57F81041F5B27B4603,
	DOTween_PlayAll_m060079597102F82BB23673D6392A0120DB20C513,
	DOTween_Play_mC14CD676517FE51D65E52689A7BC9CD40AD3B4A9,
	DOTween_Play_mAC946600F4FD0CB092D54F037213F5BFBDB1606C,
	DOTween_PlayBackwardsAll_mC6261C8A7D5D7456743D9BA448A8DEFFB302D051,
	DOTween_PlayBackwards_m5246F3D7CA28BDC2ADD46FC100C0C6FF05DBAA88,
	DOTween_PlayBackwards_m05450BF2423C577072F2C0879DF3483A626AAB39,
	DOTween_PlayForwardAll_m2CF97F8946F463A4D63B63DF7C6992FA37DD8889,
	DOTween_PlayForward_m7ABD5ED445599090AB74695419E18716C7ADFD84,
	DOTween_PlayForward_m5E6AD8D0E07B823E88E2E64250634A8962415F83,
	DOTween_RestartAll_m391EA6F3ACCF78966E3323AA736924EEE1A79859,
	DOTween_Restart_m6881B2FF21311D7A29EB514DFBB6FC85799BB2A6,
	DOTween_Restart_m26B0C36D9CE55E70C0D97D543F8F95AA7D447445,
	DOTween_RewindAll_mE5E1020426EECBEA6EB90B3F899D3D5A9E51B9ED,
	DOTween_Rewind_m5A9A7CD4D2FB1B2F16212A8A4A858B2FCA372C65,
	DOTween_SmoothRewindAll_mB1C2FFE58E0F7B5BD45ABD3EFA0E87B268197D42,
	DOTween_SmoothRewind_m98B602654BCD6279059A211AE608046019CB612D,
	DOTween_TogglePauseAll_mFD9820FAF4F834A4B044EF12CF62B51F47411959,
	DOTween_TogglePause_mDD72068F34F6B37697C7DE4AC6D9AC1E3B441EE5,
	DOTween_IsTweening_m9186DE09F26E821FEC3E8971C99E2BA361BB95C6,
	DOTween_TotalPlayingTweens_m7432F8641D7754571336B3E68F85955E00FE9D1B,
	DOTween_PlayingTweens_mD72E49FE6F2D1012B0839FB51D7EDC88CD466D02,
	DOTween_PausedTweens_mCB5A69F922EFC7D0ADD6D3BD34ACC107FF8C5E14,
	DOTween_TweensById_mD50FC14A9ACB3B84317270D839272A43A0243768,
	DOTween_TweensByTarget_mF0BF5D3AB8F269BC8CC3C019635C8A7611A5B226,
	DOTween_InitCheck_m1EF5626706A24C086ECF79B2504CF387553801DE,
	NULL,
	DOTween__ctor_mBABD7AE24A8163BA65A6C75B183D193B44D6A902,
	DOVirtual_Float_m0A5BB3095277DD51B929E886B9D5EB64615DF5FA,
	DOVirtual_EasedValue_m13B1BCEC7154DBD3C6B63DD66BED84E3ACCB3E92,
	DOVirtual_EasedValue_m6A696798DA353122DB77E55A1335ADE48AB7D859,
	DOVirtual_EasedValue_m1F5EC2D3BB8B8B94802685DE462A870DD1A28A5F,
	DOVirtual_EasedValue_m1E515F7D3059B7BA8114C9EBC4429DAEE1CED282,
	DOVirtual_DelayedCall_m13A1E292B0CB6E49F2C425ED21DDAB0FC57AC821,
	EaseFactory_StopMotion_m5F09AC0389104A282E87BE2A9904E67928E4EECA,
	EaseFactory_StopMotion_m181FC9A0D134C3C1AD008E9CEC9D644E68F77479,
	EaseFactory_StopMotion_m832C9BB2ED2A253941268BEB89DC0843A888DD12,
	EaseFactory__ctor_m555FCC43D592611E808AFE4DA42A0259D182D584,
	NULL,
	TweenExtensions_Complete_m1006012CD6C2AED2F84C6F04C16FF5D6DCEB0188,
	TweenExtensions_Complete_mBB5115AC82EEA28A91F897A767E69D58272EA926,
	TweenExtensions_Flip_m53777A3B2C663F559F961A6D90AFBA6910C7EEF1,
	TweenExtensions_ForceInit_m25AFF7EA6A03406F1080E11292F6C8923E082836,
	TweenExtensions_Goto_mFA79DC2B3D9D9B253F4AB7612CFD5E71CA3E41B2,
	TweenExtensions_Kill_m8A79B9D5D31C46E9669C2EFEDF26BF4F7EB02D10,
	NULL,
	NULL,
	TweenExtensions_PlayBackwards_m147A9C8A47E881FA94B137394EF6F4EA53BFD1ED,
	TweenExtensions_PlayForward_m72684DEC2FE49A30A71B00FF442CCBBE3933F547,
	TweenExtensions_Restart_mE904E3AB313B9CF8977E8BA3A2A191B821FC95CC,
	TweenExtensions_Rewind_m911EFDC4DAAEB2DC3C80112FE4C14F01BCF558C8,
	TweenExtensions_SmoothRewind_mE58E999B4138952F9B6252DBFC2829E961166922,
	TweenExtensions_TogglePause_mF6A1C2093B76873D90C8C36DF3E16528B227960B,
	TweenExtensions_GotoWaypoint_m1D883C58BF3D55F0C88F06E5BD4AFBCFD00C78A4,
	TweenExtensions_WaitForCompletion_m19F0941B6FEF13B652521ECE7BCE7742FDC1E925,
	TweenExtensions_WaitForRewind_m66036030592B041B2CF97D4FEDEED7E3F999E230,
	TweenExtensions_WaitForKill_mA95646AE5DAE73DFEE9D545BCA488AC47D6BDE32,
	TweenExtensions_WaitForElapsedLoops_mE72913438B63E226ADCB3A53AD3DF1D91AA2A341,
	TweenExtensions_WaitForPosition_m4B51613BD949DD254307AAA4CD69C63D402833BE,
	TweenExtensions_WaitForStart_m3F13AED0EB2811F6AA57F960ADD807944ADDBBF9,
	TweenExtensions_CompletedLoops_m067EA5E0CFEEF70813063DC6336000D02E5C2E87,
	TweenExtensions_Delay_m65CC3BC72D7A467348E7873E2DB7600038F0D3B7,
	TweenExtensions_Duration_mB449EF1960C6AC6818CAFAEF32A1E6B1130AC6A0,
	TweenExtensions_Elapsed_m63AF61BC4F010D9195A174ED86D42D357753EA80,
	TweenExtensions_ElapsedPercentage_mC12ED1EAF1BEBF089D80B420DC6D987DE9C7C618,
	TweenExtensions_ElapsedDirectionalPercentage_m478ED527B4F86DCC0240987C317231168587F5DF,
	TweenExtensions_IsActive_m19E1D79E2590A189C95DADED68FFDB43A7F3B2A2,
	TweenExtensions_IsBackwards_m8266302C8957C3266BCA3BEF8B5F0A29362BEE0E,
	TweenExtensions_IsComplete_mC87F29FE0746C793F0C11175A9B37087C1C436FD,
	TweenExtensions_IsInitialized_mE92123AF4FE069B2F175193367CAFF218B552CEB,
	TweenExtensions_IsPlaying_m3B9AB4389AA78D1EB5384BB33BF24640E0005F6D,
	TweenExtensions_Loops_mF14C87CC213FA4DA5113F6A645BB17577214F1BC,
	TweenExtensions_PathGetPoint_m8DDEC2EABC0BC6B6BC1B0CC6CF396D099C23EDF8,
	TweenExtensions_PathGetDrawPoints_mB97A95B99626040449A5F1B4B4098B4E6D4E6BA9,
	TweenExtensions_PathLength_mCEE0E32D47307DA4C408E9705B8054B540093C82,
	Sequence__ctor_mB97652F05882AFE6EBCB3BE341ED0235B76F6BDF,
	Sequence_DoPrepend_mA6EC3C2575BFDC705D61EB2C29F0788D047C894C,
	Sequence_DoInsert_mA14376970EB381F8BB2F8E2BFA81DAEB2862D388,
	Sequence_DoAppendInterval_mD26D4D1EF50CC0D718586F2CD18852ED5919AAA2,
	Sequence_DoPrependInterval_m20FBC2903E466DD9E7033048FC274DB44D538B7B,
	Sequence_DoInsertCallback_m6A7A44765BDED125C4ADB7459F919F37C0825464,
	Sequence_Reset_mC4CAA33EF1548745C6D6E784B0BF4D047B3AC188,
	Sequence_Validate_m6C36B3159599DD8A0B504F65F7494E95396F48B9,
	Sequence_Startup_m98E75C675CFE20E96A276E828F125F45904DA6AB,
	Sequence_ApplyTween_m077AFE4C3C99A0108A5893284FD7A4632C354BDC,
	Sequence_Setup_m49343758A31BC70721944DD7DCB9E3F38B31705F,
	Sequence_DoStartup_mA896E541099EBE22A115A80C2C6DAB946B110253,
	Sequence_DoApplyTween_m60CCB3B5AE0771450BC94D8BD8D6B997B563CFC1,
	Sequence_ApplyInternalCycle_m0270BA69058C771E554F7FB1FB9EF0EE1684FCC3,
	Sequence_SortSequencedObjs_m3AEF11D1B76EC1AA086F2EAD5D71DAFCC7643E46,
	ShortcutExtensions_DOFade_m3AC0CE0E603F8F3A55796F07C2AB3C68A9F480AF,
	ShortcutExtensions_DOPitch_m105F8A473434A2950C30CC3ABDB8FFD1B96AB55F,
	ShortcutExtensions_DOAspect_mCD85FE3D0E67452AC9682825549AD6C973417F70,
	ShortcutExtensions_DOColor_m406B84ECF991C880D866C555B3AE4DF41274D203,
	ShortcutExtensions_DOFarClipPlane_m0956009131C00C1D3B6ABF39C23EEFB29509A913,
	ShortcutExtensions_DOFieldOfView_m2FDB010FDA9B4164709FE0711B349C64FD73B498,
	ShortcutExtensions_DONearClipPlane_mD6C4FD5A86B7D8E512D412FC687B564F987B8B2E,
	ShortcutExtensions_DOOrthoSize_m6B046F2A9CB65F5C33F7E05E404C81BE5FCC1468,
	ShortcutExtensions_DOPixelRect_m18CCEED227266203D749048A9959857D56EBDEEB,
	ShortcutExtensions_DORect_m9D52CABC1C9DACDFE1EF0FB52D22BE293536B39E,
	ShortcutExtensions_DOShakePosition_mE45C809671BA1C7EAFD000988706740A530AA1FA,
	ShortcutExtensions_DOShakePosition_mD9DF263BD711C7BC12E152F014ED9998A6622FFA,
	ShortcutExtensions_DOShakeRotation_m1DE1BCBE8A8E4B236206932EE171B894F9ABDC80,
	ShortcutExtensions_DOShakeRotation_m09BA15DFC0073FCB0B0C4DC820B91BC5723C9E51,
	ShortcutExtensions_DOColor_mE6D48DDA2B16225D9DBF40D67D8716992016BE5E,
	ShortcutExtensions_DOIntensity_m4F6889CB6C25B1B30099075C7AC9F6E3B24FC080,
	ShortcutExtensions_DOShadowStrength_mFA55E1DB84B84DF7DD91BB12E75225630A4BE3C7,
	ShortcutExtensions_DOColor_m95F7C9CF81DFA68FDC0048E17587A94E0AF83828,
	ShortcutExtensions_DOColor_mE6140CF9E1B3D3B559F6751F9B8C3B1602826710,
	ShortcutExtensions_DOColor_mDA1E755EC546D05D85BB8AB560DBE5D7F0BB2F03,
	ShortcutExtensions_DOFade_mB5B3CAC06223B35C8E666ADC139D93FF17A31C48,
	ShortcutExtensions_DOFade_m26A4C55A80B031F02DBE0E88980E528FE5A5D0AF,
	ShortcutExtensions_DOFloat_m680DEA336497C8D581C407635DFDB4C98E110981,
	ShortcutExtensions_DOOffset_m86FF955A9303A7AA5719B0AC2552A65096FB2071,
	ShortcutExtensions_DOOffset_mF11F80175BBC7A3F9506A1B926DF993306154C0C,
	ShortcutExtensions_DOTiling_m1A4955BC0ED3D1F77EC53FAEF2EBF114CDC38DE8,
	ShortcutExtensions_DOTiling_m92B0C39EAB8BF112EAF1108D0FFDCCD63DE4734F,
	ShortcutExtensions_DOVector_mD77F3BC35D05D7FE67869842889048EE5C33F57D,
	ShortcutExtensions_DOMove_mD9817CCAE0681D620067D21B4E868428FE2533D8,
	ShortcutExtensions_DOMoveX_mAFE787202DE6B5E0DDFC2EED98DBA533040C4A10,
	ShortcutExtensions_DOMoveY_m5A82F6418B09E4930707934723C4DA82C7D802C3,
	ShortcutExtensions_DOMoveZ_m07BB95E8FFBD4CB8DB710B5771F88FE8F57211B4,
	ShortcutExtensions_DORotate_m578E0422C09350DCB3298AA920B6F9684BF09CAD,
	ShortcutExtensions_DOLookAt_m77EF2702C9BC226F7BC85636AE000C661657A114,
	ShortcutExtensions_DOJump_mBFA6A1181AC16A62DC1F587E8593D6AA2C816293,
	ShortcutExtensions_DOPath_mDDB1604C7FB47EB928B6BC63A1DD7CF9A85D3164,
	ShortcutExtensions_DOLocalPath_mC7A563E0098ACD6BECF7380308E9507831696FDA,
	ShortcutExtensions_DOPath_m01EDCCA0EE21B7E554FF28BEA409198E50CFD1FC,
	ShortcutExtensions_DOLocalPath_mE0136EA4E054DF88A8322A664D3CCEFAFABEAE4C,
	ShortcutExtensions_DOResize_mA7C52079F18C8A5344776456730E4C776757B86B,
	ShortcutExtensions_DOTime_m69CE5A2BEC68B0F0BF8528BBAAD49B4A68C6A3CB,
	ShortcutExtensions_DOMove_mA83A2C1D4DFC55CB4D2B2763FD1CF8D4ADCD634D,
	ShortcutExtensions_DOMoveX_m5C08EC5E31AB6EE00A812577014611B581402B5F,
	ShortcutExtensions_DOMoveY_m27EE0286BBFE2B1586BE88A660B15B55C907A27C,
	ShortcutExtensions_DOMoveZ_m7CAC2050DB7E8E421FC2682C3046A4284ACEA754,
	ShortcutExtensions_DOLocalMove_m0F6957D951BA19FEA0A48C7CDA52240AC7CD9369,
	ShortcutExtensions_DOLocalMoveX_m2003324ABEB7FCE822E7A47F9E92F4ABE1CA29CC,
	ShortcutExtensions_DOLocalMoveY_m180E4334F1CC2C6E1BE72ECE2DE8B21DD4AA9469,
	ShortcutExtensions_DOLocalMoveZ_m68386131CD5762AAAF410F045F6AAEF8835A05CB,
	ShortcutExtensions_DORotate_mBC72ACEECA2351B7CE6491E5FE0B76469609DEFD,
	ShortcutExtensions_DORotateQuaternion_m2BDDEBEAC8361BE98535A1A1B67F6C7FA88E1A47,
	ShortcutExtensions_DOLocalRotate_m0A3A0561A8DFA471BCA68891686EE51BD6CD2F28,
	ShortcutExtensions_DOLocalRotateQuaternion_m287F445C8974F85EE58DF0752443EF0258716F09,
	ShortcutExtensions_DOScale_m68874CDE5A501CD5EBFD9021D689A8E3029A7797,
	ShortcutExtensions_DOScale_m295B0D8C9518B001C1B6A9A77FBF7A4A6621488F,
	ShortcutExtensions_DOScaleX_m9665F244A1551D37A9B7F089DD5591C45D67D0A9,
	ShortcutExtensions_DOScaleY_mE82DE29207C5EEC076B9CDB1E08001D4336A9923,
	ShortcutExtensions_DOScaleZ_mC574D66C01E03AE4C8DFBC7381CA6F1A0AE3D2CF,
	ShortcutExtensions_DOLookAt_mDAA64D1B1CD9AFC1E351C021602C3BE102CC548B,
	ShortcutExtensions_DOPunchPosition_m9EEF18AFCE150A50A4F79AA85982B9CE70DAD0DA,
	ShortcutExtensions_DOPunchScale_m351E39CC16A4CC9692955C502F350D56825B17E4,
	ShortcutExtensions_DOPunchRotation_mC1D5DA0D6218A579D04BB373DDF1264E0AC4736A,
	ShortcutExtensions_DOShakePosition_mEC6EF5C734CA59D5C7A7808204E380B5E1C25EF5,
	ShortcutExtensions_DOShakePosition_mDEC0E682E9A8D470D6FD1F825161C3E6087DC3B8,
	ShortcutExtensions_DOShakeRotation_m290A50BA6F51EFC713882E1CFCE747B4B932FCB6,
	ShortcutExtensions_DOShakeRotation_m35DC1BCA976A9A2E1C7A6CA4359EB5F30695F1EE,
	ShortcutExtensions_DOShakeScale_mB136215C0FAAD4193DD01A5917384145C0368231,
	ShortcutExtensions_DOShakeScale_m39430B83048A4F601B8B206224E792F7F075D22F,
	ShortcutExtensions_DOJump_m65381EFDC430C4FA504C2656A4E302D6844CBECB,
	ShortcutExtensions_DOLocalJump_mA83D2B1991F192D59F5953D1D5FC64ADBF728037,
	ShortcutExtensions_DOPath_mCEB7849F32E0448CF8ACFEE2871F11796D4A86F2,
	ShortcutExtensions_DOLocalPath_m38EEE83BE829F79550424AD767CFCA1862E615BE,
	ShortcutExtensions_DOPath_m7AB84619455A541B79164E1D270A5AB511533B24,
	ShortcutExtensions_DOLocalPath_m28185CD9787640FBB679A0B46F144E1BB0DB43F8,
	ShortcutExtensions_DOTimeScale_m1776BEFED215BFE01F622383F7AF35A99FA972BF,
	ShortcutExtensions_DOBlendableColor_mA127ED765FB1A2BA042A9C91A678AAAC279F493F,
	ShortcutExtensions_DOBlendableColor_mA8441733C425378F3A3D1F4FFAE7A9F940DB0147,
	ShortcutExtensions_DOBlendableColor_m41015F46280C6C7E8649140192B28CD0E8D4DDD3,
	ShortcutExtensions_DOBlendableMoveBy_m5EF65D43D7282E6CD69AB3470DDECF36EF85D96E,
	ShortcutExtensions_DOBlendableLocalMoveBy_m305FC1B99A29FB0B2A114E402906F61DE36DB63D,
	ShortcutExtensions_DOBlendableRotateBy_m2B26C8D41D59A77FE5315731CF33214DE82A4C59,
	ShortcutExtensions_DOBlendableLocalRotateBy_mA0ADE3AD501FE8B31B14B68C78C4BAEEA709342F,
	ShortcutExtensions_DOBlendableScaleBy_m8F7EB3E007F5AC240CC5C483EB44BC0524C9A578,
	ShortcutExtensions_DOComplete_m1939237DDDE3FA508E979D2AE3DC21523918EE40,
	ShortcutExtensions_DOComplete_mB2018D4F1EEACAA7B3F43262CE9F1E3D6BCEC693,
	ShortcutExtensions_DOKill_m4E7F53B09D2B06D72DE5783E8BD6AB995E31F390,
	ShortcutExtensions_DOKill_m6C9A4C28424CA62C5EF0A9DF28D1DF8082708BD8,
	ShortcutExtensions_DOFlip_m88EB2005DB078C4E3BF3D8C6714A3A0F2E529587,
	ShortcutExtensions_DOFlip_m9F4FA956DDCD67EDBBF5160C2E2718DF55733358,
	ShortcutExtensions_DOGoto_mB9FEB565E815B21A5C66B3EB71C1EBA36894F46B,
	ShortcutExtensions_DOGoto_mBA4DBCFA8B1BEC3B2D9D5757331843042CABF6CA,
	ShortcutExtensions_DOPause_m2A116931A03160BE68769455EB747AF8DA491836,
	ShortcutExtensions_DOPause_mFC159FACC09AF2E17B76F8AF4412F57A84786189,
	ShortcutExtensions_DOPlay_m5B3FFE8CE9F5A2FCADDA69A3D1DA22D733E126E3,
	ShortcutExtensions_DOPlay_m01B0C62F69368262EDD6E6C03865B3DBBAA80B3B,
	ShortcutExtensions_DOPlayBackwards_mAF952391CB3262C21B8DC73C34084F5788EDC54B,
	ShortcutExtensions_DOPlayBackwards_mC6F2B1B13752B08CCD27DB1B0532207C5482DB85,
	ShortcutExtensions_DOPlayForward_m1F20FCE79D40810AE5E361925CE872790D43025A,
	ShortcutExtensions_DOPlayForward_m5147ABDF037463B1268132EB003F81BE1FDC2B0D,
	ShortcutExtensions_DORestart_mC1F7568D09A231DA92F1E997CCC2C4A00DC753C3,
	ShortcutExtensions_DORestart_mBB4474BF78311CF0497452FDECC3A78515ECC28A,
	ShortcutExtensions_DORewind_m323E06FBD4B7B6F871A4D2F24D345D126824F6E9,
	ShortcutExtensions_DORewind_m733779A82524CD33F9602588C940048C18A03920,
	ShortcutExtensions_DOSmoothRewind_m60199BDB37313EFFA85AAB25B5C280AEFA9DA436,
	ShortcutExtensions_DOSmoothRewind_mA39884D144593E20BB05E04A1FF62E7573A3DFAB,
	ShortcutExtensions_DOTogglePause_m95176BDF60314FD5D78055896D36C8573F93698D,
	ShortcutExtensions_DOTogglePause_m6A532FF78795DAB72A338B410F7D024874F526B4,
	TweenParams__ctor_m589B6753CE877021D50DEBEBA01547FCC034390E,
	TweenParams_Clear_m792BD1F41E63AB9B9F7F233587AFB8B72A0DCC96,
	TweenParams_SetAutoKill_mE910C0A5AC235EA85C13C57716DEF5F2B127C00A,
	TweenParams_SetId_mD27AC52C8DC84C10AB00BC4D8F494A6A6201E859,
	TweenParams_SetTarget_m53C0B155B1AFD8D6210F1B6032AD26D6A75E5C4E,
	TweenParams_SetLoops_mA9379F549A00CE93725E8D338475FCEBE4151363,
	TweenParams_SetEase_mAC8EDCE42F478585B45FF89776DC8C0A311F2D2C,
	TweenParams_SetEase_mA67B49600782A4A4059DD8CD0706CCC115A51C92,
	TweenParams_SetEase_m03A3B2C45E9B8A8A2C0F950D0E8BF525A05C96C1,
	TweenParams_SetRecyclable_mED85748EA433BCD5FC5145EF7AEA08CA4232B561,
	TweenParams_SetUpdate_m83AEE08A5DE29BAB8E794745D31BC4D11C85E5D5,
	TweenParams_SetUpdate_m70D5E34B2FA26E77410B6052E1493F2CA9EB4801,
	TweenParams_OnStart_mA04D5C6DD9DC6F41CEC4A7DD668412E4627B16FD,
	TweenParams_OnPlay_mC4DAA365F93C267F7C2221D3665173D44277CA09,
	TweenParams_OnRewind_mA62E100E3C37E25FAABDB549D10F2D846EF60F4B,
	TweenParams_OnUpdate_m0D2D0228CAE9AC0507966A514A82C5E800FFD7BF,
	TweenParams_OnStepComplete_m0E67E4983C0996D255554BED258077F1D030A760,
	TweenParams_OnComplete_mE505E8B0CC24806D405CEB9AFF84BBE7CFDDEDCC,
	TweenParams_OnKill_m208E98C37585E85220368BA9D6550FA8D201A047,
	TweenParams_OnWaypointChange_m6017D8DDA59369CB4BB8B536E09E423F6770AC73,
	TweenParams_SetDelay_m2951738A16FDDE2811506667FA903D5E61E9F849,
	TweenParams_SetRelative_m546D5B90ECBC703544C670FE0492E56E4A7B3670,
	TweenParams_SetSpeedBased_m792B2EA71D0B5D0076CFBA38D34D7C2011A12A45,
	TweenParams__cctor_mF42A94E49D0F826044A137B4825830A1A71AA862,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TweenSettingsExtensions_Append_m045B3A5C557D2007A05A55F1D5B86A26AA5F13D5,
	TweenSettingsExtensions_Prepend_mA5ACFDDF27607E1D6A1556A18C4467099A25ABDD,
	TweenSettingsExtensions_Join_m33E665745C1F091AAD10AD2C3FDD91017861D89F,
	TweenSettingsExtensions_Insert_m6C8C4CAFC8CC4C5F2120879AE79BD01E3903422A,
	TweenSettingsExtensions_AppendInterval_m20E76B7FD0B9E0FD85D5E55444177E412D2A39FE,
	TweenSettingsExtensions_PrependInterval_mF91C39268775A123FCCE526262C391CB826B8B9B,
	TweenSettingsExtensions_AppendCallback_m056A9F025C67DB07A403E70BED916DCDDF8671E6,
	TweenSettingsExtensions_PrependCallback_m430A6A2B38D293E34439857D757394BAF37D9EEA,
	TweenSettingsExtensions_InsertCallback_mCEC5B490A95472ED358A6A930F69941843B21826,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TweenSettingsExtensions_SetOptions_m619178CD390C9F59543EBF056E54CECFF4BA95CD,
	TweenSettingsExtensions_SetOptions_m9C45DDF251D2CD1BC96529E3480E9A91B46AF1E4,
	TweenSettingsExtensions_SetOptions_m9B154343AFE2B086C2BE39C9E913F51186A10BAF,
	TweenSettingsExtensions_SetOptions_mA202FF383BEAAE03292420F1163032B5D0566742,
	TweenSettingsExtensions_SetOptions_mE2CF0CEA2DE42858A86D1C736E162AC6814256AE,
	TweenSettingsExtensions_SetOptions_m527D580A508B8297888953E59601C95C399E98F4,
	TweenSettingsExtensions_SetOptions_mAF32D7903868EB663666CB91E101BF2477234647,
	TweenSettingsExtensions_SetOptions_mF1E28DA2E05779F1E161A62F103038250446302D,
	TweenSettingsExtensions_SetOptions_mDA3551543550914D61FBDAF7F749A5FC69889AA7,
	TweenSettingsExtensions_SetOptions_mF4CB8F4A815A8CF310CB76FA85D22E854A65319A,
	TweenSettingsExtensions_SetOptions_mB198DDCA53C32A43F7EA9213244369D8B55F9A40,
	TweenSettingsExtensions_SetOptions_mF4577F3599CEDC3C1AF2F31CD109481C229C4994,
	TweenSettingsExtensions_SetOptions_mD2E5E89120A9CD6676C15DB603DC72BFE7EC126F,
	TweenSettingsExtensions_SetOptions_mC7FC2851A87231470309BC2F740335EC663BD21F,
	TweenSettingsExtensions_SetOptions_m232500C0CAFAD89AFE0A437A6015F2E59ED3D8FF,
	TweenSettingsExtensions_SetLookAt_m31B2E7ECAADA53B8C6EDA9BC760AFA2F6155B382,
	TweenSettingsExtensions_SetLookAt_m3BFF34B7765FCB4B3DDE2B3B90869FE9086DDD01,
	TweenSettingsExtensions_SetLookAt_mAD3CC7329671C9A6D0FA41A9AB2D3B236769C2AF,
	TweenSettingsExtensions_SetPathForwardDirection_m8EF4B32280222570CCC96CE57E9D0C4936F1F732,
	Tween_get_fullPosition_m21DC9222B4A2B17909B7E4C3CD7FE6925CCD97AD,
	Tween_set_fullPosition_m589CFD59274B5028A21265F8CA754A173BC180A3,
	Tween_Reset_mAADD5CF8D940FB674B8E3914E16230599FB7A957,
	NULL,
	Tween_UpdateDelay_mA8151BABE455A62407CEFCFE342F7B5730378701,
	NULL,
	NULL,
	Tween_DoGoto_mBB5ECAA333D861AD3491AFAB5E7626D2F1BC310F,
	Tween_OnTweenCallback_m9973B444F1D39598A217F58AE7757E0633A3F5F4,
	NULL,
	Tween__ctor_mB8C032C20B3FECEC4AF6F9C0FB7FB90F91CC3E23,
	Tweener__ctor_m48B4AF3E51DFEAED312F4875F09EE9B7E41248D7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Color2Plugin_Reset_mB904D4F6B9443C72072B1B4E1B2DFD2C3E27B09C,
	Color2Plugin_SetFrom_mD7D3786F8C6D8B8A2FB0FC2BFA7CB699357E2530,
	Color2Plugin_ConvertToStartValue_m60900134C3EBEDD78DABC35181D601AE6B0F90D2,
	Color2Plugin_SetRelativeEndValue_m6E4E6804691BFE37D2534D787C7194B832414910,
	Color2Plugin_SetChangeValue_mDBF5284A8859118DBF874CC37B8E2175A0798CE1,
	Color2Plugin_GetSpeedBasedDuration_mE76CF42CB177706504203B0249C64A780EFC702F,
	Color2Plugin_EvaluateAndApply_mCE44814101CFBA3394C9D435B3306CB2D4A82AAA,
	Color2Plugin__ctor_m2C523642D184C931C339A1CDA79F0E1463A2CD97,
	DoublePlugin_Reset_m9F6A83E41CCFCFE1F062E954C1785A4A0B965042,
	DoublePlugin_SetFrom_mCA576F2678F7AB9269CA7F2328AD5B1750F2F8DB,
	DoublePlugin_ConvertToStartValue_m6547AEF7C82F685DDB8A5D130D4979BD42392DB0,
	DoublePlugin_SetRelativeEndValue_m1E855855D71C51E3EEB72CEE9511925EF00418D3,
	DoublePlugin_SetChangeValue_m2811678E89ADAC207834AAB86CBC7BE1B97E1104,
	DoublePlugin_GetSpeedBasedDuration_mC171C3419FF3C035F7D1BCBC56D504C024CC309D,
	DoublePlugin_EvaluateAndApply_m4E1BF2EAAFEB4CD33C5E900C8786B512E95DBB34,
	DoublePlugin__ctor_mF420BC11DE2E0BF3B79D044A1F90C5F0C0708410,
	LongPlugin_Reset_m0912A633F347939A612327703A0CBD43AE6A0F2E,
	LongPlugin_SetFrom_m3DD81DA2853FE682A379A65F1E5C77DE2CB94C56,
	LongPlugin_ConvertToStartValue_mF09FBA1064E303D54C92060F24625CD0F6027BAD,
	LongPlugin_SetRelativeEndValue_m80BEA044DE2D487C35FB9061F11DD841BBE09A23,
	LongPlugin_SetChangeValue_m3267961A22BEFCB43A4F3000D6A85000F9FF8C1C,
	LongPlugin_GetSpeedBasedDuration_mED09F7E9C3B44844DCEC8B63F8322F338C77EABE,
	LongPlugin_EvaluateAndApply_mF9D9E0DDB9C98FECB344BFBBFCE99B28CFCA0D62,
	LongPlugin__ctor_m04FE82F230F471A46F15AED810FC56B0FFF90EC8,
	UlongPlugin_Reset_mFC20C0746C0A48A5C6117205BF52B38FC07CF2B6,
	UlongPlugin_SetFrom_mB79BAA3526BF3C9CA78609C9E09A0545512ED5B1,
	UlongPlugin_ConvertToStartValue_mD8298148B2DCC204ED57E96C7D1225CF62C533B0,
	UlongPlugin_SetRelativeEndValue_m6D17D07A8243D97C9DB0522450BCBFCA1506D24D,
	UlongPlugin_SetChangeValue_m21FF1EF586E81FC9801520D596208570E05C5318,
	UlongPlugin_GetSpeedBasedDuration_mAE01A1A0519E6AE9602054B7F7EAD15A26657C7C,
	UlongPlugin_EvaluateAndApply_mBD4E50796B9AA1736ECE14E41A964AD55D5A5AE4,
	UlongPlugin__ctor_mC75488AFF3AA9497592260C860D16FD386482E16,
	Vector3ArrayPlugin_Reset_m95EB255B91F630BF02581FACB9EB1C73C610ACC4,
	Vector3ArrayPlugin_SetFrom_m5B9C114C460461C0FCEF0F9AC3A61CED75322B13,
	Vector3ArrayPlugin_ConvertToStartValue_mDA38159B0CEC8F3FBDDF1ABF080590ED52C3C3F2,
	Vector3ArrayPlugin_SetRelativeEndValue_mAC05D397B4304FF7807898F0069F31AA16F5E72B,
	Vector3ArrayPlugin_SetChangeValue_mA10FC73A97D1C547E173F489A5E996A2601D7DD1,
	Vector3ArrayPlugin_GetSpeedBasedDuration_mDA722C1F881E3C5E01919CEFCD5DDF4A3BD4C9EA,
	Vector3ArrayPlugin_EvaluateAndApply_m337B4D71A1A41CEFCAA4848EC6C0731A87879A4A,
	Vector3ArrayPlugin__ctor_m66DC60F55463C1A2F01CB9A576F3E79F56EC5BC8,
	PathPlugin_Reset_m0D22977A473DDF724AF8D93E904D0262F55D5F8A,
	PathPlugin_SetFrom_m331C0217AADFD4EAA491E920BFBEF3708AF1F55A,
	PathPlugin_Get_m1D5B2D927828EF5D7543475191CAE0B620A0A5A3,
	PathPlugin_ConvertToStartValue_mC43D885FAEA0DF9F75EE90CC88E38F6D7CC82588,
	PathPlugin_SetRelativeEndValue_m5ADDA3AB3CAE201E25864F924D98311E5751D94B,
	PathPlugin_SetChangeValue_m309C4BB9A503831A66BC927FA0769905C219558E,
	PathPlugin_GetSpeedBasedDuration_m11FC2E670501D6B8EDAFB668092D110CCC87DA40,
	PathPlugin_EvaluateAndApply_mA5F082E78C7183946B80532BBCAE7AADB413BFB9,
	PathPlugin_SetOrientation_mA723C7B1ECA013738CFD9789423FF3C91F6FEF49,
	PathPlugin__ctor_mEBC9E19D350BA9C3980BF3B9AFA32CA0BCC459FD,
	ColorPlugin_Reset_mF8E1AE9221E8A74523FAB45070A4FD26B394BD35,
	ColorPlugin_SetFrom_mDE788269C22DC64D6B935023A45870E4F027E4A4,
	ColorPlugin_ConvertToStartValue_mF254AE0A1E4B73BD2FC4E3EF7EE0053FBA226713,
	ColorPlugin_SetRelativeEndValue_m8DD740D391AB1D0EC8DC57341A94347B604ECE61,
	ColorPlugin_SetChangeValue_m71B84592A9C77FFE3EE0A68AE35DE8E240C04D3F,
	ColorPlugin_GetSpeedBasedDuration_m428E21C053B02DD46B6B5FF63C6C6E6B86A551A5,
	ColorPlugin_EvaluateAndApply_m6B3E18787E83E45DAD99AC1AA7C0DA9DF7714BCE,
	ColorPlugin__ctor_mB3437144ABC10E3733E87D0D1BA2E8F9AE6FCBFA,
	IntPlugin_Reset_m95F23E6C48BDD7D1650A4EABEAD440F054117C9D,
	IntPlugin_SetFrom_m2B07A75CD24015DAA4D77103488767D0926553C8,
	IntPlugin_ConvertToStartValue_mB090390FFE1EFB459952C19416BE4E3327D2BFB9,
	IntPlugin_SetRelativeEndValue_m12E3972FD374B94E187A903F150E092503D930BB,
	IntPlugin_SetChangeValue_mDED66936B27B41BAD55EC9305016F6AB95C96016,
	IntPlugin_GetSpeedBasedDuration_m958E04DCA7F2F5FE9892FBC908A79A017E17EC97,
	IntPlugin_EvaluateAndApply_m94DE1FA97B2C4D9DDA83EE633FDB08D6F63FD722,
	IntPlugin__ctor_m208428BA7A040F6F902499122457980952044432,
	QuaternionPlugin_Reset_m0649C8AC12C023BCDEC214DFA0F973EDC6C66024,
	QuaternionPlugin_SetFrom_mAF5BD3EB5C4152F98A95FB80A13CE79D3AD558AF,
	QuaternionPlugin_ConvertToStartValue_m9C871C5F50AFA3043B0C0044A38F4C883919FA26,
	QuaternionPlugin_SetRelativeEndValue_m0750B967437A22407ED0CB6836A4C38D9E8AF3F7,
	QuaternionPlugin_SetChangeValue_mE01B2DA751F39BC5B404D2509648B393F18FFE6C,
	QuaternionPlugin_GetSpeedBasedDuration_mD7D92DBEAD656FBD1130CBF8A8067476638450B9,
	QuaternionPlugin_EvaluateAndApply_m293AC3183649C55894B0AC156757DF87D4272788,
	QuaternionPlugin__ctor_m0E8810278B6DC9B6A25FD63303A581D3D98B33EA,
	RectOffsetPlugin_Reset_m010F59F59EE4187D49E420FAA4A37B3595258E4E,
	RectOffsetPlugin_SetFrom_m92FED32CCBC3EA2DF032C524B5B90DAB2DCBC009,
	RectOffsetPlugin_ConvertToStartValue_mAD997FE953F6F3D9DC3EE016A2E92FA6A0831DB7,
	RectOffsetPlugin_SetRelativeEndValue_m87D884B1E4E8D270D0AB3A7E1A5D85FB8C4D14AA,
	RectOffsetPlugin_SetChangeValue_mE72CB478071BD93E3A4DDED919943794D214205B,
	RectOffsetPlugin_GetSpeedBasedDuration_mAA0F08BE2AC1D0C4AD06CC09621EF90CFAB91B8C,
	RectOffsetPlugin_EvaluateAndApply_m06CB41CD7ACC9186EA878819597AE05064A5BC8E,
	RectOffsetPlugin__ctor_mFF822DB0D7B655C76D46D0CE9A94ADD4835A5F9B,
	RectOffsetPlugin__cctor_m996743433053049BD8F606C9B32A64205159D3B7,
	RectPlugin_Reset_mB1D0DC4F3DE841A1B33F05A1B13A0E248A093B26,
	RectPlugin_SetFrom_m1B267E9B0414A5A0DDB50CDA4BB129F57AC5E276,
	RectPlugin_ConvertToStartValue_m8EAA87E45893560FEDC16AEDE463EEAAE3033290,
	RectPlugin_SetRelativeEndValue_m0427090C7C597720D8A9BA2CCDE7822E050A257B,
	RectPlugin_SetChangeValue_m6464D92DD679FC99A6616158A6CEFE56D4B4A29E,
	RectPlugin_GetSpeedBasedDuration_m053D877CC923DED96BDD7E48DD9DC2FF18C5DD03,
	RectPlugin_EvaluateAndApply_m13D0D7CEBBB34B9D31A7596CD6DC0C31E68872E9,
	RectPlugin__ctor_m010C901CECBCF5999DD2A28D54EC20845FF452FF,
	UintPlugin_Reset_m193AE4F691F9BBDE696FBC5B0B5436BAF73A76AA,
	UintPlugin_SetFrom_m91620454D164FC43858A46CE6F3B79686F0D30F4,
	UintPlugin_ConvertToStartValue_mFDB6D84340ACF5440D64D685E9A1C1174087FD30,
	UintPlugin_SetRelativeEndValue_m76EAD21D81FE646B4EBD89F43D598FC102F48AC1,
	UintPlugin_SetChangeValue_m3C0697D6C48F6BC6933403517DB461AA055E9B06,
	UintPlugin_GetSpeedBasedDuration_m8D8A316C45479265320C430AFC6475C1443DEF9B,
	UintPlugin_EvaluateAndApply_m0424424673DB615C6E81618710EE244DDCB5316D,
	UintPlugin__ctor_mC1590E3DD4EFE6686AD587D371E1C4BA5556B960,
	Vector2Plugin_Reset_m1379B8670AFF4A6D6172CEB00E3A3B332BEFBD00,
	Vector2Plugin_SetFrom_mB9EC8AEF790F8A4220AB40E76CF6FF4467DEBC1A,
	Vector2Plugin_ConvertToStartValue_m13086D3524387C3EE47DE4388FAD6E5DBEABBD4A,
	Vector2Plugin_SetRelativeEndValue_mD2D0163D27EAE6EBADDF89B888E24FF3C7F21BB8,
	Vector2Plugin_SetChangeValue_mBF8B99B3D35FC2F7E6BDA8D489306871CB79B97E,
	Vector2Plugin_GetSpeedBasedDuration_mC696FAAA0B2CEACCEE809986ECA4CA97A646905D,
	Vector2Plugin_EvaluateAndApply_mA734062CBF02456CB02D0932332EE24312C50B0D,
	Vector2Plugin__ctor_m8FFC85977ACE0836F6A6B53EEF78A1007AE08C8B,
	Vector4Plugin_Reset_m1EB49BA31CBAD0980F998EEEF904C763E983FD1C,
	Vector4Plugin_SetFrom_m31B1C50D23D7AADCB265E5D3267BC98905FBA6B9,
	Vector4Plugin_ConvertToStartValue_mA11F01B5789F1FB0B159B2B2541E7541E8096638,
	Vector4Plugin_SetRelativeEndValue_m77FFB11D318A380D5DB4F95CD46C43EFD7BE2583,
	Vector4Plugin_SetChangeValue_mC427A3E178CB5296D3AA667EC47B837804D2380B,
	Vector4Plugin_GetSpeedBasedDuration_m152B2C3CF428132E78DC78EB6B2D4CF0D78D03AB,
	Vector4Plugin_EvaluateAndApply_m74A4BA39C9A6A5E7FCA486AB3CC703F44D0F2D7F,
	Vector4Plugin__ctor_m21D86C0C7A38430563F307EE7F4B0A43B2EA8DAF,
	StringPlugin_SetFrom_mD2926BE763AFB806389A055112C54E71A0E4FC0A,
	StringPlugin_Reset_m8D68BCE13C66172E0502C28C4373AD28C5E1AAE0,
	StringPlugin_ConvertToStartValue_m76106D9534182E72BB37194829CEDEB4476FAEE7,
	StringPlugin_SetRelativeEndValue_m9D67ACC4F555285AB48CC3AD7C250DBD6323DD76,
	StringPlugin_SetChangeValue_mCDA147C7DD517507A4F9BB269A75BBF175EA0083,
	StringPlugin_GetSpeedBasedDuration_mFA690E2C3CB46920940A48C31BB960CB9793273A,
	StringPlugin_EvaluateAndApply_m3F590DBE11ADDFE530966D609318117270844AF6,
	StringPlugin_Append_mA1230766A1463D1E3ED55BFF00E00C1512C7304C,
	StringPlugin_ScrambledCharsToUse_m080754A4C844369B7554C1A13EFC1485537FB5B7,
	StringPlugin__ctor_m1E0AFE2A04FAABD74E1C4245F8EB078A91E23F2C,
	StringPlugin__cctor_mDFC93819107CC7B3441F4B678856A0D7A1E7FBD0,
	StringPluginExtensions__cctor_m1D257C1DE8259FD7FFA3BDBB35E731A2B3889495,
	StringPluginExtensions_ScrambleChars_m3177B26808D6532A66A40AA9292B134A216D4C90,
	StringPluginExtensions_AppendScrambledChars_m8A75B95AF635D8479043492E1DF7084C4A9E4870,
	FloatPlugin_Reset_m5754EBAB59D3C20EC406B9183C5DE5BB983F231C,
	FloatPlugin_SetFrom_mF394F333160C04A02EC0163AA574EE4AD02872FC,
	FloatPlugin_ConvertToStartValue_m7685B6918C4AAB675F05B8991D7D595994ADEC1F,
	FloatPlugin_SetRelativeEndValue_m029A2E4325B3F14BF8BCD9941D7D98D9E2E8F2F9,
	FloatPlugin_SetChangeValue_mD7B09FC3BDE63D0945635DA20D4EDFE3554ABDB3,
	FloatPlugin_GetSpeedBasedDuration_m275FC86B86DAB6D7B5599F69E037CC5058010045,
	FloatPlugin_EvaluateAndApply_mDCF5EED6F4A206CF05B20F4F10EBF981B989B338,
	FloatPlugin__ctor_mD5B21EB10468A8F69FA7A70F533958E16F86D559,
	Vector3Plugin_Reset_mAAC0F0D61D28AFC74881AAE32A8FBEA47C5ACBAE,
	Vector3Plugin_SetFrom_m8EDBA8B2F9D1E701E3A0292C864D6F4D5E150091,
	Vector3Plugin_ConvertToStartValue_m4260440F59538DB1A9C6E2CC6423D46D206340ED,
	Vector3Plugin_SetRelativeEndValue_m57A736F284D8651D2D6C9302F7B5AD7D2E0E4D82,
	Vector3Plugin_SetChangeValue_mA78E49154DA5961CDF539D4716BBA2A301399B14,
	Vector3Plugin_GetSpeedBasedDuration_m1C863665F5EF19902D8972CDC30EBC787B7B580B,
	Vector3Plugin_EvaluateAndApply_m188A9A4A49A36E63623EB08A6E0F83BEDC76786A,
	Vector3Plugin__ctor_m79ED6C92B67FBD1D76E58A2D71B1AE65A662876C,
	NULL,
	PathOptions_Reset_mAE569F0A485C767B02D5A39FE73FBB99F499181E,
	QuaternionOptions_Reset_m9F3925AC48AC22071B77E36A944E061E0C46A134,
	UintOptions_Reset_mC2110A83EF78588DC8E220C98D2ABE7971110602,
	Vector3ArrayOptions_Reset_m3B9A2BB478088F79B5B9BF318FC8BBAE1799453F,
	NoOptions_Reset_m27971DF59C8A9C56F0DB40B6ED3C01F5816F8BE6,
	ColorOptions_Reset_mBFFB7CD03418E47DEE6B7CD3EDB3BF38D2FE3714,
	FloatOptions_Reset_mA56EA5E336B757B6DFEF7C512FAB46E7D491BAA4,
	RectOptions_Reset_m76EC7D6D0CA41EAAE53344226F46FDFF5FB2578A,
	StringOptions_Reset_m2ECE10EBDC0E574ED2D50230BB2D49ED57C230F0,
	VectorOptions_Reset_mF46B9B9771CBDA05DD32C4396C611D9D8E86BCD4,
	SpecialPluginsUtils_SetLookAt_m62B3D82E9521FEBC1E22763881D8CB5F7C699100,
	SpecialPluginsUtils_SetPunch_m09C64EF4F2FAA213FA5615AF546F3EEC6C00FDA7,
	SpecialPluginsUtils_SetShake_m094DAE5237FB92330103BB2CCFF36586A14B52F8,
	SpecialPluginsUtils_SetCameraShakePosition_mEC71E3A0D2885F4C62D6E0E5B6AD1553866D29AB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PluginsManager_PurgeAll_m207FAF2D3D6AF0C311D19210FB236DA4B271B25B,
	ControlPoint__ctor_m3E47DB0F5677F7DF493D707670BC3A853B24E708,
	ControlPoint_op_Addition_mE6AE6C0925859DE939AEC4EF6151FEA4C4571077,
	NULL,
	NULL,
	ABSPathDecoder__ctor_m75687A62F6DEFD390499137CB7739B22EAA58848,
	CatmullRomDecoder_FinalizePath_mA7BD1A338A217B3BAE3D85153E1178E785BB38CA,
	CatmullRomDecoder_GetPoint_mE5A2FFBD1F1A50A5B288F92A1E048D00A8806BFF,
	CatmullRomDecoder_SetTimeToLengthTables_m555F7D01F28CF4A32AB30B48974EC287956CCC0E,
	CatmullRomDecoder_SetWaypointsLengths_m4AD97B4AAA3E22B86A3432193DD42D66E66679D6,
	CatmullRomDecoder__ctor_m58D647013D71654962E29F4044F14D78F0D14BEB,
	LinearDecoder_FinalizePath_m31AEF72BC02ED1CA1936247EA9FAC2DC63D8EEF5,
	LinearDecoder_GetPoint_m3A090FB590C263B01B1A8E3206B0306D1B9FAF78,
	LinearDecoder_SetTimeToLengthTables_m1FA7914138A816D616569924313EC5BDF55CEEEE,
	LinearDecoder_SetWaypointsLengths_m47E85BF06A3469EEB0D35D4DC33158D4B1F5F919,
	LinearDecoder__ctor_mA4ECBA6C86C7F9B90A0D65002C0540E173B02DDF,
	Path__ctor_mFC92041030E41315A55CFE293F4500B2D36DBE20,
	Path__ctor_mA1C6976D933FA2CF6995BDD01445034E80F9850D,
	Path_FinalizePath_mE2F8FBB45A0C5EDC7C87513502D4559FE329D32B,
	Path_GetPoint_m23698672B67AE58436EAF7E41E4E07E48206C149,
	Path_ConvertToConstantPathPerc_mEB0C3D4CD0CAB697EEA10C5B02AE69AE93E11AD6,
	Path_GetWaypointIndexFromPerc_m3A1A77D53813B6ADCCAB8C74EF3A0AF41EC5F959,
	Path_GetDrawPoints_m208AE495E1F916DDDF566EA18658581CB98E29E6,
	Path_RefreshNonLinearDrawWps_mCD80F5FC7567C45D26BB9348244EBCEB1C46B018,
	Path_Destroy_mB032169AEE079328BDF5D9BC2AEFDC2D5605A066,
	Path_CloneIncremental_m0A63F28B7D4015A7A07916CEBD97176F8DBBE2C7,
	Path_AssignWaypoints_m8E804D1A254E946D1241C3E856FA956DF1B08FBB,
	Path_AssignDecoder_mD999EA9D655C222C11AF74D1F24EBD51AFC37097,
	Path_Draw_m98F7E4F7EAD1D65843AFCA4C097E0BEF2E5CC783,
	Path_Draw_m78E0D953AE61D04E6651424ED36492F36C68A381,
	PureQuaternionPlugin_Plug_m57BF0C3C1901BAE1794CC7892837A3EC43B22046,
	PureQuaternionPlugin_Reset_m1FC197191A292BC0CCA48CA726309A3FC828BEE7,
	PureQuaternionPlugin_SetFrom_m0B8EBB771FDA733E7DAA793576A443F2C372CAA7,
	PureQuaternionPlugin_ConvertToStartValue_mAF286A6EB9D689C7B7C31970532B158600C2722A,
	PureQuaternionPlugin_SetRelativeEndValue_m6B2ACA2251800F47BC54FE4427BC6D289B25F11D,
	PureQuaternionPlugin_SetChangeValue_m53B40012A11DE3C3383A0A7D4CC2F8BA6137D401,
	PureQuaternionPlugin_GetSpeedBasedDuration_m52D8A12F1316286D25F0736EFF7DC53FB351273D,
	PureQuaternionPlugin_EvaluateAndApply_m6E8EA043EDE1B15A43052E8AA0897AA0CF70FEE2,
	PureQuaternionPlugin__ctor_mB5D4C12941AF07DD6FC0395EF9544323E9A1C906,
	ABSSequentiable__ctor_mC6848561E17C86E0350B84A1056AEC74EC07847D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Debugger_Log_m7FFD88A31284EA5D424ECCDD3CC3969EFA888A60,
	Debugger_LogWarning_m0B6001D682A2F01BB835AE4AD56405EE3FB146ED,
	Debugger_LogError_m6371F09308CBAF417D23ED479DCD6179FB6CD501,
	Debugger_LogReport_mE5EACE910A9EEFF1AED12993313A9EFC754983E3,
	Debugger_LogInvalidTween_m89B8B151DC18203462D28B7A6FC9E5382BE7B5B2,
	Debugger_LogNestedTween_m20636A24F330E1A40E8BC38E90FA51CC0B7903A7,
	Debugger_LogNullTween_m6A9B33DC41AC0A765DD49C9AC2DDABECD22F476B,
	Debugger_LogNonPathTween_m2073C7F72A26E7C2AD1321C130871946EE146FF1,
	Debugger_LogMissingMaterialProperty_mF21C74A880DF58531233076CAD09891E04815E61,
	Debugger_LogRemoveActiveTweenError_m2D9BAAC1679552B0310CFD4978367FC232D817C6,
	Debugger_SetLogPriority_mD079823A6237D25090E0B91E094E484253BEBE17,
	DOTweenComponent_Awake_mDDB8FA2556C96A64275BB0193FD380ABCAEAF6E4,
	DOTweenComponent_Start_mD16AA301BD8BF8EED3D66DDBBEC0E5A701E91B8D,
	DOTweenComponent_Update_m03F0653C0840CAF1927B496231127D33018C4094,
	DOTweenComponent_LateUpdate_m32DB6CA5FE7B99631071E5D1895FE21C71C52547,
	DOTweenComponent_FixedUpdate_m9980784AE79BE2BA8D4F0D2378D77735F3979B70,
	DOTweenComponent_ManualUpdate_m3081BB828DA87A14AE435172B4B56B2F62863A60,
	DOTweenComponent_OnDrawGizmos_m986451D47907251DD3341C0082C46E5B393B4323,
	DOTweenComponent_OnDestroy_m245C2AEE9CA2E63676F948E4AC8F5B79FADA8E71,
	DOTweenComponent_OnApplicationQuit_m97821B800F750E177A16FDD6E3D146483F85BED3,
	DOTweenComponent_SetCapacity_m0E73677D5F156B6D624478E62D8D14DE0505F01D,
	DOTweenComponent_WaitForCompletion_m7BA68341D27B9B492A65777AFD379E71F47B102B,
	DOTweenComponent_WaitForRewind_m7F96B4437D370D38E0B2463D096FD646387776B2,
	DOTweenComponent_WaitForKill_mB7C038F6E4B1363478CF373324CB2FBF235D0CA2,
	DOTweenComponent_WaitForElapsedLoops_m73B338BCE98498A6B39A4D497C7DC37DFDAAA568,
	DOTweenComponent_WaitForPosition_m1B69080F19D45EB93A65B70B08A96FF5C13DE2EA,
	DOTweenComponent_WaitForStart_m41699A9E4D9FB04610EE336ED66DEBF3196209BC,
	DOTweenComponent_Create_m0F1760B8E1CBE8F3CE2CEF9636F5978261E65DCB,
	DOTweenComponent_DestroyInstance_m6236F832D1B1F34C22EEA55A6E6379C440E6B803,
	DOTweenComponent__ctor_m5A2FBA4B31230F1BE2CCA174778B3DF5040933C6,
	DOTweenSettings__ctor_m1C8B5E51F778CCB971735DC68318E4CD56FFF5AE,
	NULL,
	NULL,
	NULL,
	SequenceCallback__ctor_mCA4C0F5DFBF60FB024A67CAAFB968710301AED94,
	NULL,
	TweenManager_GetSequence_mB4DDC8BB253854799E302739AAA0A4005CD1D04C,
	TweenManager_SetUpdateType_m7319E156C04BC3E82A3299B6322D952AEB123F0C,
	TweenManager_AddActiveTweenToSequence_m95D2E6AF58BD892FBC697DD7D626068BC480ECE1,
	TweenManager_DespawnAll_m4FDE391EB0D266421A0D7A777888393D28C6E0C2,
	TweenManager_Despawn_mBA89672AB846AA136C04EBBED03570775D5C2373,
	TweenManager_PurgeAll_m99F2511C27195D790293F94EA03C0ED66E6EBF15,
	TweenManager_PurgePools_m145D170E5541DD5F33B204F77218271BA7E88B2C,
	TweenManager_ResetCapacities_m837C0EC04113FC56064B56BE3A2B60D3B6045E16,
	TweenManager_SetCapacities_m16552F59E957F1B0B495B670573CF4B1CECF5615,
	TweenManager_Validate_m9FD1D8CADC09857CF17C70C1CB920ADC10C8BF5F,
	TweenManager_Update_m6CE9360D1C820BA6DBBA10DEDC131DC748581B55,
	TweenManager_FilteredOperation_mE0B8019A163D005B02B2A0E4A1506407181ECB77,
	TweenManager_Complete_m23CB7E180527065E2E41A75D37261061D37C7547,
	TweenManager_Flip_mF843250C1A4F0DB21721377E0F49593140F84675,
	TweenManager_ForceInit_m23B6EA289D16DAC62AB504E102594B55A2DE9708,
	TweenManager_Goto_m916DA36B3DEA6A298885C27143D8F78FB6BB40DF,
	TweenManager_Pause_m14AA579FB84B8AD6FD698ADAB30EC7B94517DE0F,
	TweenManager_Play_m12AFE3745A43C09F3662DF359F634947E5EC95A6,
	TweenManager_PlayBackwards_mB50289428BDF97AE57DB7321833089E51F80F59C,
	TweenManager_PlayForward_m4B2622EEA7E1D1E87765DB23A5F1D686FFE2271A,
	TweenManager_Restart_m9A1BDA041F5514A8F67AE4318F8CE7F05065D151,
	TweenManager_Rewind_m9A5B531B2B9F36AE022EE76458757FC6A78DA89D,
	TweenManager_SmoothRewind_m48F722FD7FCED29D070F5E901CDD571172CC5C60,
	TweenManager_TogglePause_m290FEBDD4F1F491CE9FF9D2B0A2FF03AAC583F6B,
	TweenManager_TotalPooledTweens_m4BF409197E4F2681A780F921EDE7AC3B01CC6C0D,
	TweenManager_TotalPlayingTweens_mFD0E1A2EB28A8E7C28BE6A082C82AFB85B64D6C0,
	TweenManager_GetActiveTweens_m445B7873862CF7B6C880DCF15B542860E9575BBC,
	TweenManager_GetTweensById_m11BF0BE966B567C8487BC4D72D925ED10206B825,
	TweenManager_GetTweensByTarget_m06BCD088FA9D2BBF00DFA5078E9AA714582D7F14,
	TweenManager_MarkForKilling_m9D7122C1A52CA72709D03BAC9F98343A8F27A9F6,
	TweenManager_AddActiveTween_m72F1CD5193ED0B41AA5FC01F9D2A667718F107DE,
	TweenManager_ReorganizeActiveTweens_mC03A4ED3E74E94FB604C65B7D8F27D8DC9506426,
	TweenManager_DespawnActiveTweens_m37508B7398B79E00BB77A6FCD8A67E1BF053F244,
	TweenManager_RemoveActiveTween_m7667979DE3FF9443EE543D369C0E04DF4C66F940,
	TweenManager_ClearTweenArray_m0964AADC1D6A57832A8EDEEEEFDB7B72E90A1074,
	TweenManager_IncreaseCapacities_m289AF4539DABF6FEA349C0449B7EF2ED31B2B3A9,
	TweenManager__cctor_m85FD5609EB964C27ACF5AF6AAD99FE747D7CB470,
	Utils_Vector3FromAngle_m073D50B4279D401AD2A89D74262CED7A74C72151,
	Utils_Angle2D_mA18DF43B096D1DFE85ED6C46CBD5A44D6A48B1EA,
	Utils_Vector3AreApproximatelyEqual_m3DDED7828048F20AEB6A05E1938BED8AD5E321FA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Bounce_EaseIn_m7ED4AFA856CE5E8A4ADDDE63F560F8CD3C933EF3,
	Bounce_EaseOut_m88B5E1E41115135F049BE05E2A6A755E98FAFE1B,
	Bounce_EaseInOut_mF1279096941029D9D4EBA2CD862858B66575BC3D,
	EaseManager_Evaluate_mFFCCC216976B86EE1A69D485690E963CF8B61D40,
	EaseManager_Evaluate_m6C16E8E2792775BD48D6CB4DB02FE72FC2E555FA,
	EaseManager_ToEaseFunction_m89A308AA580E9F3F44675154110726939C6F1B66,
	EaseManager_IsFlashEase_m23FEBA92B8438BCE000A3F3A4B6D9C35213DE9C1,
	EaseCurve__ctor_m9ECCBAB3097CEFFA5D910D8312B67E97FB3DE6D1,
	EaseCurve_Evaluate_m4194413CCB04078BCF1EB7FF553A45D9AD76508E,
	Flash_Ease_m19545C2333831E29B3135F6CA76FAF3D783DC57C,
	Flash_EaseIn_mCC41C58C4297D3FA9C9D889516989CF2CA11A95C,
	Flash_EaseOut_mB897CF26BE9FC1436E9676CF46D36589713C5A53,
	Flash_EaseInOut_m55E092635346F3DD417FA21C1BAD24A359D023A8,
	Flash_WeightedEase_m8251F2A44FEA37052A977A58CBAAB0A15BA148B3,
	U3CU3Ec__DisplayClass54_0__ctor_mF76503C17593526AF9D00FCB60074261461A0A36,
	U3CU3Ec__DisplayClass54_0_U3CToU3Eb__0_mBE433706DAB16866BFC3584B0D3E98D29932FD19,
	U3CU3Ec__DisplayClass54_0_U3CToU3Eb__1_m7C939C63D34767418CE5BB7C7129380945630BA4,
	U3CU3Ec__DisplayClass0_0__ctor_m460734B3A8EEEFB285BF5CB2B106CCEED410F872,
	U3CU3Ec__DisplayClass0_0_U3CFloatU3Eb__0_m386A6857F7D208FDBFCE788383F2C8DE1AF9C516,
	U3CU3Ec__DisplayClass0_0_U3CFloatU3Eb__1_m0E6F8890CB27D5DC25CA4FEE8DD54BA314FC32C5,
	U3CU3Ec__DisplayClass0_0_U3CFloatU3Eb__2_m871516C18DBA820CF68F881A2EE127EC364A5013,
	U3CU3Ec__DisplayClass2_0__ctor_mCCCF6E2BBECA7C07FFF31A70C8CAC315B2C45C8A,
	U3CU3Ec__DisplayClass2_0_U3CStopMotionU3Eb__0_m51E68455AD7ECC3CDCE386C30AB9C0069D41ADDA,
	U3CU3Ec__DisplayClass0_0__ctor_m9ECE4AD670FD3ACA531CB4877350603C7A66B94E,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m99F4938B1C5F9BD5A80EB15D29B854C120F221E9,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mE3B4BAA7C7136C1E0131799C59D72592AA55B281,
	U3CU3Ec__DisplayClass1_0__ctor_mDB012068F8814E0863F53559200CF95239208A06,
	U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__0_m2E2662CD37D1D2E0AB328AC4CE36FDCD5CEDA569,
	U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__1_m8EB80C8975BF3BCE8A5134DDAF3F070AB3549D46,
	U3CU3Ec__DisplayClass2_0__ctor_mAC36CD0F4CAC1F362B1FE54ED3E2EBD58F019B68,
	U3CU3Ec__DisplayClass2_0_U3CDOAspectU3Eb__0_mAEB20AF1D085C22E17F9FBB0C0C14C3BDB454BC2,
	U3CU3Ec__DisplayClass2_0_U3CDOAspectU3Eb__1_mE6552DAB6BD0811E748AF67F8C20E823103772E0,
	U3CU3Ec__DisplayClass3_0__ctor_mE07E29CBFDCAD09EC69C4C003EC82EE797904B3E,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m856529C7793193878ED4D0E2811F0FCBA47819D4,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_m2F4B3463C3680773DED7A5947721062E31821415,
	U3CU3Ec__DisplayClass4_0__ctor_m963C2102B6D39D8687B6983F5201D2F930A26ED5,
	U3CU3Ec__DisplayClass4_0_U3CDOFarClipPlaneU3Eb__0_m9BF808F709BEC49AF6275550CED29968CF65F276,
	U3CU3Ec__DisplayClass4_0_U3CDOFarClipPlaneU3Eb__1_m2A94F8D7B14C8E40FEE514AA85E373744A84473E,
	U3CU3Ec__DisplayClass5_0__ctor_m8C722A93B75A23D0BCB562EBE20F58E8B6DD04A9,
	U3CU3Ec__DisplayClass5_0_U3CDOFieldOfViewU3Eb__0_mDD5A733E43A625DFA63132895B671F8ECB6250CD,
	U3CU3Ec__DisplayClass5_0_U3CDOFieldOfViewU3Eb__1_mEF2ACB1FF7409863EE0E72F057D72F7513C9C2B8,
	U3CU3Ec__DisplayClass6_0__ctor_mEE55DE39F1FF8478DDCCD500CAA220C6E8A5E827,
	U3CU3Ec__DisplayClass6_0_U3CDONearClipPlaneU3Eb__0_m313071A353F52AC89BFCC677B86AB6C1E37C42BF,
	U3CU3Ec__DisplayClass6_0_U3CDONearClipPlaneU3Eb__1_mA4DEB9BBD55C5274FA0B9B22537EDFD41F3677D8,
	U3CU3Ec__DisplayClass7_0__ctor_mF0A11C80DD3A0418573858693DA2AF52F8FDB964,
	U3CU3Ec__DisplayClass7_0_U3CDOOrthoSizeU3Eb__0_m487FBE25061B86BBBDA1CD6DEA934C9C0189DE9F,
	U3CU3Ec__DisplayClass7_0_U3CDOOrthoSizeU3Eb__1_m48A57F3A85E601A7DAA7B9AE9924C8A47152B403,
	U3CU3Ec__DisplayClass8_0__ctor_m9C3F9C15120E46EE54B6D5E3C2792DE7A6FE8DD0,
	U3CU3Ec__DisplayClass8_0_U3CDOPixelRectU3Eb__0_mBCE0B56710CDB1E989F49D0B989E0CD1C788B637,
	U3CU3Ec__DisplayClass8_0_U3CDOPixelRectU3Eb__1_m71539FCE99D11D4ED651CFCFA0CAE7C06D946DB6,
	U3CU3Ec__DisplayClass9_0__ctor_m4BE4835CF89A3DB5D09B8B11B69DCEFA3D2AF352,
	U3CU3Ec__DisplayClass9_0_U3CDORectU3Eb__0_mDA158912889EC70C55E4E17B369161AAB72080BE,
	U3CU3Ec__DisplayClass9_0_U3CDORectU3Eb__1_m9E60367B2A9D9F9984F41F01880F5EB366B7CFCD,
	U3CU3Ec__DisplayClass10_0__ctor_m7E1CB370ABD439676854113E6D4E0F297B4D4965,
	U3CU3Ec__DisplayClass10_0_U3CDOShakePositionU3Eb__0_mB2D828A9F539F4246475DEE44A881CBD958CE429,
	U3CU3Ec__DisplayClass10_0_U3CDOShakePositionU3Eb__1_m88194783A205B5142F04AAD2469BC2ED8F96A554,
	U3CU3Ec__DisplayClass11_0__ctor_mA1788740F370A16A08411900B408D7F0930A54D0,
	U3CU3Ec__DisplayClass11_0_U3CDOShakePositionU3Eb__0_m85BCE4227BDE6CF0A2234C6AFAD1046D65B4FF80,
	U3CU3Ec__DisplayClass11_0_U3CDOShakePositionU3Eb__1_mE57BC0F6775AF37F06B9F21A43D3389C66D90080,
	U3CU3Ec__DisplayClass12_0__ctor_mE606C680B1CBA55E547580130862D84B29E9F3B1,
	U3CU3Ec__DisplayClass12_0_U3CDOShakeRotationU3Eb__0_m1FB86C3985B5CDD89095BB7FD62504D40781B53C,
	U3CU3Ec__DisplayClass12_0_U3CDOShakeRotationU3Eb__1_mED8B09C58683856F429C145CFD0D4A640BE2F452,
	U3CU3Ec__DisplayClass13_0__ctor_m8EB6CBD11BA0AA6BF04F64914D4AF92CDE0446C9,
	U3CU3Ec__DisplayClass13_0_U3CDOShakeRotationU3Eb__0_mD2CD3CF51252C5E126627D2E21C19E6B5472B10C,
	U3CU3Ec__DisplayClass13_0_U3CDOShakeRotationU3Eb__1_mE75DCD31E11574BE539EEB98C53F60BB5C812FAE,
	U3CU3Ec__DisplayClass14_0__ctor_mB398AE4CFAE33F76C5158CD09F27890593BA677A,
	U3CU3Ec__DisplayClass14_0_U3CDOColorU3Eb__0_mF32FF3E3299F0271867C6F256735BFD646279222,
	U3CU3Ec__DisplayClass14_0_U3CDOColorU3Eb__1_m0EB9EB4B06BFFC13EECDB01DFC1F13AF8D8DECED,
	U3CU3Ec__DisplayClass15_0__ctor_mB754754F91F38FD7CDB4DE49EA1117DE804FA44A,
	U3CU3Ec__DisplayClass15_0_U3CDOIntensityU3Eb__0_mBA3B54906DC9A96DA27D63EEC74A1596DCB77FFE,
	U3CU3Ec__DisplayClass15_0_U3CDOIntensityU3Eb__1_mF6CD8062864F9FF02A47237C5BC407801F3A4C56,
	U3CU3Ec__DisplayClass16_0__ctor_m8C4AD7E5A27A938FBE1FFF399D9856E1F70B686F,
	U3CU3Ec__DisplayClass16_0_U3CDOShadowStrengthU3Eb__0_m040BF1588DB18DCEB51D6B85B6F5A4BE61AA618C,
	U3CU3Ec__DisplayClass16_0_U3CDOShadowStrengthU3Eb__1_mB8E5409C50C751D2C46781038CE0CED964FE3FE6,
	U3CU3Ec__DisplayClass17_0__ctor_m88C223C17347C554FE5C0DC8B21B282C230DA9E9,
	U3CU3Ec__DisplayClass17_0_U3CDOColorU3Eb__0_mFA4A452D27E1DA131356FF322CA1A0973F3285B6,
	U3CU3Ec__DisplayClass17_0_U3CDOColorU3Eb__1_m45099F313C593379EA8CBDBD5BF9A5C8B2E65737,
	U3CU3Ec__DisplayClass18_0__ctor_m8E1C09B3484478E1EAB563E857CA0EF71D6EDDA1,
	U3CU3Ec__DisplayClass18_0_U3CDOColorU3Eb__0_m3D180482F1BD66D1AAA4284C531D909D0C00D744,
	U3CU3Ec__DisplayClass18_0_U3CDOColorU3Eb__1_mBBC0089428F3A9C8187E3A7F828F23E1088DF885,
	U3CU3Ec__DisplayClass19_0__ctor_m2A670B0E82C4AED6934BE4C3CE5300965A6ABCFC,
	U3CU3Ec__DisplayClass19_0_U3CDOColorU3Eb__0_mEE514D6E989E5D0F0C0B77F2ED9E16C4A13F863C,
	U3CU3Ec__DisplayClass19_0_U3CDOColorU3Eb__1_m80042F78DB607C56416F5BC5E425A0B994E5CE76,
	U3CU3Ec__DisplayClass20_0__ctor_m55A4ABB994BC5C802D7E728EB3BE384C0101579C,
	U3CU3Ec__DisplayClass20_0_U3CDOFadeU3Eb__0_mE41D0EC1CB35FE35D493977A0578F8E7418F4C8D,
	U3CU3Ec__DisplayClass20_0_U3CDOFadeU3Eb__1_mB6CB8EC51BC054284E08BA64531C2C9A4127A6A5,
	U3CU3Ec__DisplayClass21_0__ctor_mF47F23D94A30DECD1C4BF2C7CB57FEAC0B78CAF8,
	U3CU3Ec__DisplayClass21_0_U3CDOFadeU3Eb__0_m772703E02845ED92E977F02CF9DA7535A7C1A077,
	U3CU3Ec__DisplayClass21_0_U3CDOFadeU3Eb__1_m429B3D8C806B05AF89BCAF3B48FAB2C0C7481669,
	U3CU3Ec__DisplayClass22_0__ctor_m55929F023175FABA6C1479BE42AA987BE28D0364,
	U3CU3Ec__DisplayClass22_0_U3CDOFloatU3Eb__0_mB928BE369B1D25CCD549D8F66EEBD141670AA57C,
	U3CU3Ec__DisplayClass22_0_U3CDOFloatU3Eb__1_mF39934108EB8BA7C4562FFA735B8C3A5719CAF47,
	U3CU3Ec__DisplayClass23_0__ctor_m80BB7A10B09AC55F0DCFF3DC4176250C0C2ACBBD,
	U3CU3Ec__DisplayClass23_0_U3CDOOffsetU3Eb__0_mDCAF44A5CE750A55C8B3036FFE5840DA517F7187,
	U3CU3Ec__DisplayClass23_0_U3CDOOffsetU3Eb__1_mB0D58FA46EC38462421C4CF778D0385B93C7E4C0,
	U3CU3Ec__DisplayClass24_0__ctor_m22D05B57D4DC7ECBBFC9C89CD090F01BC5681718,
	U3CU3Ec__DisplayClass24_0_U3CDOOffsetU3Eb__0_m3CD2D18FB45199EB95923126EFB3CAEE5D516624,
	U3CU3Ec__DisplayClass24_0_U3CDOOffsetU3Eb__1_m27BB1B04A6CEE67B271E355AD7CEBEB7F23E11B7,
	U3CU3Ec__DisplayClass25_0__ctor_mEFA552EA3C088F7C20B12C7067495D2975C327BA,
	U3CU3Ec__DisplayClass25_0_U3CDOTilingU3Eb__0_mFBFE59634B5642736929B22E5E301F7379DF88AA,
	U3CU3Ec__DisplayClass25_0_U3CDOTilingU3Eb__1_m85ED680808DCD2E7615F2B5A94887D757D8B44FF,
	U3CU3Ec__DisplayClass26_0__ctor_m7F47E1F7EC24B6EF21EF5E6D31A46BA5E4E11F98,
	U3CU3Ec__DisplayClass26_0_U3CDOTilingU3Eb__0_mEEAB79D1D1A659252E96B2C8F1CCE2C5C3341E07,
	U3CU3Ec__DisplayClass26_0_U3CDOTilingU3Eb__1_m7C8B83804672258A8D59F587A7ED0561B94DB643,
	U3CU3Ec__DisplayClass27_0__ctor_m6CF10B7FA9063C154564292CBA53952991737A1A,
	U3CU3Ec__DisplayClass27_0_U3CDOVectorU3Eb__0_m8377D5B9A8B02DE5B5348E50756D14DE913D0303,
	U3CU3Ec__DisplayClass27_0_U3CDOVectorU3Eb__1_mAF61404889DF199C5192394624ADFB1E2518624E,
	U3CU3Ec__DisplayClass28_0__ctor_m90A534D792092C098AC98F431D3AD3CF5C2E827C,
	U3CU3Ec__DisplayClass28_0_U3CDOMoveU3Eb__0_m03B19B10C49540DED743F03EE4558ECCFA615E95,
	U3CU3Ec__DisplayClass29_0__ctor_mCDF281D408DA52B7C76F075088B67D5A03034C9F,
	U3CU3Ec__DisplayClass29_0_U3CDOMoveXU3Eb__0_m6DED4DC130935B09B23EC98A17F052A519615911,
	U3CU3Ec__DisplayClass30_0__ctor_mBE3EB40629AF2DD8E24480A6D3D4209A7DA4FC45,
	U3CU3Ec__DisplayClass30_0_U3CDOMoveYU3Eb__0_m20FCFCCB2B3B2164568452FAFB019A22451A4675,
	U3CU3Ec__DisplayClass31_0__ctor_m3E8471A32AFDA8B9A8AF3268FB5612B3CF0132CD,
	U3CU3Ec__DisplayClass31_0_U3CDOMoveZU3Eb__0_m1CABD72CD35F76BC8CC04F4B2FEBA1EB87C7C819,
	U3CU3Ec__DisplayClass32_0__ctor_mE023046BD5A6D330DAAE4135185540A905CD713C,
	U3CU3Ec__DisplayClass32_0_U3CDORotateU3Eb__0_m671F3CF957422B3BEB125C0ED6883FF39CF5F564,
	U3CU3Ec__DisplayClass33_0__ctor_mE182EFDA4B21E2565475EA75B711210E1FC6ADBD,
	U3CU3Ec__DisplayClass33_0_U3CDOLookAtU3Eb__0_m5E92533756830B20661BAB402472DCAF93B0F757,
	U3CU3Ec__DisplayClass34_0__ctor_m87947EDBE476EB119E99E2BE328DED6A0BB008DE,
	U3CU3Ec__DisplayClass34_0_U3CDOJumpU3Eb__0_m6A69B42B7E23D4EE27749E23CA88429CDABCA82C,
	U3CU3Ec__DisplayClass34_0_U3CDOJumpU3Eb__1_m5075456A1FF9C6AE85D98C033A3788F8FC277BBD,
	U3CU3Ec__DisplayClass34_0_U3CDOJumpU3Eb__2_mB56ECAF435FC0811D32DC87D69644B95846BF37C,
	U3CU3Ec__DisplayClass34_0_U3CDOJumpU3Eb__3_m3A8B537660EFDE07AD9029E226C9844E88C11F41,
	U3CU3Ec__DisplayClass35_0__ctor_mD01D581412BA53D097FA2768561DA4BBB711C3E1,
	U3CU3Ec__DisplayClass35_0_U3CDOPathU3Eb__0_mAC5CB1AAF81C8BD908107AA603AF78B6EE7C93AD,
	U3CU3Ec__DisplayClass36_0__ctor_mB269FDF5927C88DF46FFFD0247C99D03291E8B32,
	U3CU3Ec__DisplayClass36_0_U3CDOLocalPathU3Eb__0_mACB424383CABC4A3AECC6F59581A51F533840C76,
	U3CU3Ec__DisplayClass36_0_U3CDOLocalPathU3Eb__1_mAC32E7674A9FEC018D839654C1C1B452831857E0,
	U3CU3Ec__DisplayClass37_0__ctor_m1A1FC7FDC72AB2D61EA947D07507BE7BE6415D7A,
	U3CU3Ec__DisplayClass37_0_U3CDOPathU3Eb__0_m2D020D59442E575F70DE9F8CE2D73E4DC7433265,
	U3CU3Ec__DisplayClass38_0__ctor_mA37B06178CB3001538BB5FB3720BC916DC8A3CA5,
	U3CU3Ec__DisplayClass38_0_U3CDOLocalPathU3Eb__0_m43AC6EFB5F6CC305225A7D39465A6735D2BF842F,
	U3CU3Ec__DisplayClass38_0_U3CDOLocalPathU3Eb__1_mECA11BC4C6356B85D38D6493DFB92EC20AF383A5,
	U3CU3Ec__DisplayClass39_0__ctor_m61F78B5D0D38DD49E8DFC7136A45A76E5514F1F5,
	U3CU3Ec__DisplayClass39_0_U3CDOResizeU3Eb__0_m4B15595A7CBB995D0E0FA337A70F4012BF1E5C5A,
	U3CU3Ec__DisplayClass39_0_U3CDOResizeU3Eb__1_mE328EED94BD32D2E8DC98AC6DA5077FC3D6F7C3D,
	U3CU3Ec__DisplayClass40_0__ctor_m7FEF1C22F05CBE0DC9AFE20937F1E9F9DAD0FD47,
	U3CU3Ec__DisplayClass40_0_U3CDOTimeU3Eb__0_mB5FF71296F11C254CAF01B96108BC7BA70A860E4,
	U3CU3Ec__DisplayClass40_0_U3CDOTimeU3Eb__1_m1BA3BBB4989B1FD5441C5FCB743EDBC5D382CD06,
	U3CU3Ec__DisplayClass41_0__ctor_mD71DAF48B5C0BDD08D77E1E0A86CFB3D690D8CE7,
	U3CU3Ec__DisplayClass41_0_U3CDOMoveU3Eb__0_m62C57A489D46DF0624110F9F8AF38C940827CD9A,
	U3CU3Ec__DisplayClass41_0_U3CDOMoveU3Eb__1_m21FC6CBB1B56045861BDCDC4644F41D462715151,
	U3CU3Ec__DisplayClass42_0__ctor_m5DD5B409BA6AD7353C13BA706CAD84F1ED9141B1,
	U3CU3Ec__DisplayClass42_0_U3CDOMoveXU3Eb__0_m69551D55F7FD01A37B60C2AB469B15C49E425E5B,
	U3CU3Ec__DisplayClass42_0_U3CDOMoveXU3Eb__1_m727D19EC1CD2D4E60A573063ACD167340AECA556,
	U3CU3Ec__DisplayClass43_0__ctor_mA2661B2DBF8E1CF340B1C860E1260BFCF82A5AB4,
	U3CU3Ec__DisplayClass43_0_U3CDOMoveYU3Eb__0_mA65876046B07353BB36FE0020D00E0E6D2F0F65C,
	U3CU3Ec__DisplayClass43_0_U3CDOMoveYU3Eb__1_mFCE7900F15A63DEC781791B2DEBB79396EFF595A,
	U3CU3Ec__DisplayClass44_0__ctor_m561007304BF22CD59E5FA30E69B514E603ECA46D,
	U3CU3Ec__DisplayClass44_0_U3CDOMoveZU3Eb__0_m748CB8A802995E5DB49F286B8857F9A9B5436767,
	U3CU3Ec__DisplayClass44_0_U3CDOMoveZU3Eb__1_mAAD584B94AD32709950B6FC3389F8744826A4E5E,
	U3CU3Ec__DisplayClass45_0__ctor_mDCA06E2C93223BDAF428170FBDF87029F4618D66,
	U3CU3Ec__DisplayClass45_0_U3CDOLocalMoveU3Eb__0_m2457A3ECE4412BE02D37D269867D76DE446F4EFF,
	U3CU3Ec__DisplayClass45_0_U3CDOLocalMoveU3Eb__1_m1D80C2BDA35BF4EDF80C029768E86213C2245C5F,
	U3CU3Ec__DisplayClass46_0__ctor_m28C791D4CFCA8E98EE753EEE233A233E4D077AEF,
	U3CU3Ec__DisplayClass46_0_U3CDOLocalMoveXU3Eb__0_m4E8CAE10E0A9B31C090AEFAD12B649AB319BD7F6,
	U3CU3Ec__DisplayClass46_0_U3CDOLocalMoveXU3Eb__1_m3C14E3D5B7194ED766ABB158F6A83524FB7B0C73,
	U3CU3Ec__DisplayClass47_0__ctor_mAE5DA37BC6914CD1A7A5839B9DB3CE0D5DC590A0,
	U3CU3Ec__DisplayClass47_0_U3CDOLocalMoveYU3Eb__0_m4CB694090CF0C2CB804B36B7F1E98BA14C751FE1,
	U3CU3Ec__DisplayClass47_0_U3CDOLocalMoveYU3Eb__1_m73B703EC4295353CBBCC58C74A011775AF43CF2B,
	U3CU3Ec__DisplayClass48_0__ctor_m8FEB64841ECF0DE83AD8256E8B7842ADF0A6C28C,
	U3CU3Ec__DisplayClass48_0_U3CDOLocalMoveZU3Eb__0_mA28BCFFE9F0A4BF59E3FD5FA08BEA69DB9AB94D9,
	U3CU3Ec__DisplayClass48_0_U3CDOLocalMoveZU3Eb__1_m03009B09B9C01B4EEC01717742AF2E2F7A8081FE,
	U3CU3Ec__DisplayClass49_0__ctor_m8441E58EFFA3A7A22ACE18A36BADFCBB9FFDA121,
	U3CU3Ec__DisplayClass49_0_U3CDORotateU3Eb__0_m0C28D7A399552C203977CFFD04D0476F1A5F53CF,
	U3CU3Ec__DisplayClass49_0_U3CDORotateU3Eb__1_m3D15872923950681AB4AC36CE040BA6F51BBDB54,
	U3CU3Ec__DisplayClass50_0__ctor_mE7A05719EB9FF09713486D350D640371AE8C8A12,
	U3CU3Ec__DisplayClass50_0_U3CDORotateQuaternionU3Eb__0_mBB5410D8340A681E2F5F2021DC83506088065040,
	U3CU3Ec__DisplayClass50_0_U3CDORotateQuaternionU3Eb__1_mE3A3A35B3B211DE430751CBBCB88A823779A2E60,
	U3CU3Ec__DisplayClass51_0__ctor_m88B12570D3FA8DA4AB6FEA6C9C459B9D6FBEF02E,
	U3CU3Ec__DisplayClass51_0_U3CDOLocalRotateU3Eb__0_mBDBC3F6AECA66EDFF7D7E0E4DE1C030919172006,
	U3CU3Ec__DisplayClass51_0_U3CDOLocalRotateU3Eb__1_m8601D9DB188B2BD6BBA5988744B7A732309F223D,
	U3CU3Ec__DisplayClass52_0__ctor_mDDAEA1236B676F6073AE29B64A1403AB26ACE6DB,
	U3CU3Ec__DisplayClass52_0_U3CDOLocalRotateQuaternionU3Eb__0_m0ED78CF5D8886A592F6802A51A22218A2ED92280,
	U3CU3Ec__DisplayClass52_0_U3CDOLocalRotateQuaternionU3Eb__1_mB53D41BC4DB551A85FB23A8D93D8E84EABF5D9A5,
	U3CU3Ec__DisplayClass53_0__ctor_m1C514889BDCD73E4A3A811A9C58465B7805456A8,
	U3CU3Ec__DisplayClass53_0_U3CDOScaleU3Eb__0_mCC8D7AC358069E730FD972ADF1CFA9CC5C2BED6F,
	U3CU3Ec__DisplayClass53_0_U3CDOScaleU3Eb__1_m270B1F6D3CD2992C7421A85EE2685D8536B155F9,
	U3CU3Ec__DisplayClass54_0__ctor_mB3C0B1483FE3E60BA677904B8ADF8B62E907969B,
	U3CU3Ec__DisplayClass54_0_U3CDOScaleU3Eb__0_mD5D6708251F75D9FC6B94B133BE437F98266D942,
	U3CU3Ec__DisplayClass54_0_U3CDOScaleU3Eb__1_m660620CCE701A1AA2FD58871D52238F645041146,
	U3CU3Ec__DisplayClass55_0__ctor_mF5E9434DE1248F815C8D7EF032AA1ABB5E90C16E,
	U3CU3Ec__DisplayClass55_0_U3CDOScaleXU3Eb__0_mBC916D4554B9A2194AF6B59CEB88A66C2121AC37,
	U3CU3Ec__DisplayClass55_0_U3CDOScaleXU3Eb__1_m7AC8820060FE91213F073BF1533D7870E2ADA8E6,
	U3CU3Ec__DisplayClass56_0__ctor_m0A4482699B086B67BDF70929CAEB6F007F8D17E6,
	U3CU3Ec__DisplayClass56_0_U3CDOScaleYU3Eb__0_mA87DF9618A52B6AB69DE684873CF0B4094185B85,
	U3CU3Ec__DisplayClass56_0_U3CDOScaleYU3Eb__1_mDA025DBA85781F57F5BB388C2C0336D4BF092CBC,
	U3CU3Ec__DisplayClass57_0__ctor_m1506C65FA353A0180FDC50712E034200632DCB6B,
	U3CU3Ec__DisplayClass57_0_U3CDOScaleZU3Eb__0_m1F774ED5B93D0EE161CCBD92F3848659882AAA1B,
	U3CU3Ec__DisplayClass57_0_U3CDOScaleZU3Eb__1_m1765257B1EAC7707BF59A1D1161F6EC582330EDE,
	U3CU3Ec__DisplayClass58_0__ctor_mED96E830E7A4E8CE92146A98904772D41E084636,
	U3CU3Ec__DisplayClass58_0_U3CDOLookAtU3Eb__0_mEDFDB547323BBF70F23947C1E7338A3A8AE7771F,
	U3CU3Ec__DisplayClass58_0_U3CDOLookAtU3Eb__1_m8EDBD1ECB1362EECCB8C48F307BF8AA9D3665589,
	U3CU3Ec__DisplayClass59_0__ctor_mF3B3C7DFA8827D5BCDF897D0B161C5CE19814554,
	U3CU3Ec__DisplayClass59_0_U3CDOPunchPositionU3Eb__0_mC3226C94783A791C7391793FBC469C15BC1A4894,
	U3CU3Ec__DisplayClass59_0_U3CDOPunchPositionU3Eb__1_m93B4693EE386918AACCCC4CFDD7359AF7E2C2AAD,
	U3CU3Ec__DisplayClass60_0__ctor_m28DEA3D1E5A20A170AC0824BBF3AC5E8579BA3DC,
	U3CU3Ec__DisplayClass60_0_U3CDOPunchScaleU3Eb__0_m00A612D7AD8561E87A479BCA800D40E3D074B706,
	U3CU3Ec__DisplayClass60_0_U3CDOPunchScaleU3Eb__1_mBFC473AA6F7BB9D390AEAE259F4E85DFFF9AC608,
	U3CU3Ec__DisplayClass61_0__ctor_mC3F96960F74D9ADADC62E2B2A78309E847E36084,
	U3CU3Ec__DisplayClass61_0_U3CDOPunchRotationU3Eb__0_m651AA8503EEEDB24A7A99CABA333D986F5B19490,
	U3CU3Ec__DisplayClass61_0_U3CDOPunchRotationU3Eb__1_m898D2C59027CFD120D2F3401CAD3DA541D42AC09,
	U3CU3Ec__DisplayClass62_0__ctor_mE7575A65CFB8E2A6CD8D303EC360759D6C49C29A,
	U3CU3Ec__DisplayClass62_0_U3CDOShakePositionU3Eb__0_m556A87CE6DB7638B9257ED9657716BBA8B630317,
	U3CU3Ec__DisplayClass62_0_U3CDOShakePositionU3Eb__1_mDA035DF61C90285E91BBE309137E1CC8A0B39B76,
	U3CU3Ec__DisplayClass63_0__ctor_m751095F9FBF992B99188F0DD2A59B05C59F45FC6,
	U3CU3Ec__DisplayClass63_0_U3CDOShakePositionU3Eb__0_m9BA11BB61DA5F013C2242EC75E31394F5DDD1B35,
	U3CU3Ec__DisplayClass63_0_U3CDOShakePositionU3Eb__1_mC42437BCBF7B7E8A5791EABFA940FC23D2D904C5,
	U3CU3Ec__DisplayClass64_0__ctor_m93FA757C2F100A9B98DF73369A358E2B80BC0247,
	U3CU3Ec__DisplayClass64_0_U3CDOShakeRotationU3Eb__0_m75D4F7FB68D23D9B16494147086BD25511519323,
	U3CU3Ec__DisplayClass64_0_U3CDOShakeRotationU3Eb__1_mF39C403B79DF9FEA3EF47E6AEE2CAF3222D98843,
	U3CU3Ec__DisplayClass65_0__ctor_mB5D4F70938E7752219827AB7C3E0F4A88DBB5300,
	U3CU3Ec__DisplayClass65_0_U3CDOShakeRotationU3Eb__0_mA6C8154BFB8700E4F91DB116F6E752C1D693F1A4,
	U3CU3Ec__DisplayClass65_0_U3CDOShakeRotationU3Eb__1_m5DF0A97E2EB382969BECD80858328D56F4641BA5,
	U3CU3Ec__DisplayClass66_0__ctor_mB3E82C541A24DA7A41932576C1FA9DB1E54A6D0E,
	U3CU3Ec__DisplayClass66_0_U3CDOShakeScaleU3Eb__0_mEBC36ACEBE6CE4B1E8A81D470928593DA2A4361E,
	U3CU3Ec__DisplayClass66_0_U3CDOShakeScaleU3Eb__1_m96237B5163EB288592FC551A6BF3E4B669537A92,
	U3CU3Ec__DisplayClass67_0__ctor_m9452D35C9DCEA16B9778C855E5FA8B4D3A2401DA,
	U3CU3Ec__DisplayClass67_0_U3CDOShakeScaleU3Eb__0_mAA98EAA2A062CCC8A4FC4DC9565B28B7E6973034,
	U3CU3Ec__DisplayClass67_0_U3CDOShakeScaleU3Eb__1_mD696AC0C6F961F707AC1EAB2397A1C0B42419C71,
	U3CU3Ec__DisplayClass68_0__ctor_mE195658976E1930732C2B2F3D0CC128E49CAFF1C,
	U3CU3Ec__DisplayClass68_0_U3CDOJumpU3Eb__0_mA1F36D5344D3D488C71733ED54C0B6B54152FFD2,
	U3CU3Ec__DisplayClass68_0_U3CDOJumpU3Eb__1_m50605D00E3CD9268DE5D79AE29D0869AE6E45161,
	U3CU3Ec__DisplayClass68_0_U3CDOJumpU3Eb__2_mFA8683E8AE0FA11EEC7A95F5EDB0F13C17A8608C,
	U3CU3Ec__DisplayClass68_0_U3CDOJumpU3Eb__3_m61E575832ECE4F9649A978B0D5739B6392698E9F,
	U3CU3Ec__DisplayClass68_0_U3CDOJumpU3Eb__4_mD562A20F84AFB44CCF566557B47961A403C6A6AE,
	U3CU3Ec__DisplayClass68_0_U3CDOJumpU3Eb__5_mB76CBD86CBB3CAEBD87CE85F8EBDF6D8E34A8B2E,
	U3CU3Ec__DisplayClass68_0_U3CDOJumpU3Eb__6_m23BE9D64DC96D25D9CF432C5321A219D7D1985B6,
	U3CU3Ec__DisplayClass69_0__ctor_m168BC1CFB89D50382CF943A5485D5E42862021EF,
	U3CU3Ec__DisplayClass69_0_U3CDOLocalJumpU3Eb__0_m9ACFEFEC7CB8E1F5E58296479FC252A213E76054,
	U3CU3Ec__DisplayClass69_0_U3CDOLocalJumpU3Eb__1_m6F3DFDC993896028642050DD559167D530C7A0B4,
	U3CU3Ec__DisplayClass69_0_U3CDOLocalJumpU3Eb__2_m26DFFD6A4599ECEA1E9861B8D77AFEB635C09B1D,
	U3CU3Ec__DisplayClass69_0_U3CDOLocalJumpU3Eb__3_mBF5ABF833F09FAA2A4F60541FCC437D4BE88A457,
	U3CU3Ec__DisplayClass69_0_U3CDOLocalJumpU3Eb__4_mB1A8C8DD403AE1830C71CC24ACC5DBBB87F865AE,
	U3CU3Ec__DisplayClass69_0_U3CDOLocalJumpU3Eb__5_m7A12906348DA7ED8E82519DCD9E86A23617825C4,
	U3CU3Ec__DisplayClass69_0_U3CDOLocalJumpU3Eb__6_mE534589F1F34065D31ACD68C537D896751DE5BC1,
	U3CU3Ec__DisplayClass70_0__ctor_m3FFF8D0AAEC236077E9CF42D4E14CA273987EAD4,
	U3CU3Ec__DisplayClass70_0_U3CDOPathU3Eb__0_m9A0C0A67AD6839BB579ED12F821092B8DB23ECEA,
	U3CU3Ec__DisplayClass70_0_U3CDOPathU3Eb__1_m3880CA477A405C9010D0A70F0EA14B1623A6E1FC,
	U3CU3Ec__DisplayClass71_0__ctor_m8984100650DE98B17C87FB26DE5E6E7EF4CA30A5,
	U3CU3Ec__DisplayClass71_0_U3CDOLocalPathU3Eb__0_mE1D5F3EB1B500B51BE3A56D3336F03EA2F9D936F,
	U3CU3Ec__DisplayClass71_0_U3CDOLocalPathU3Eb__1_m82CD11B88E1148540B1C87E156DA78CFADC54182,
	U3CU3Ec__DisplayClass72_0__ctor_m8EA9B1DDB9F19E37B8EA9234BDEC50CD76096CFE,
	U3CU3Ec__DisplayClass72_0_U3CDOPathU3Eb__0_mD354490B9D2FCCEFA61AA07CE969A25E7BC744B3,
	U3CU3Ec__DisplayClass72_0_U3CDOPathU3Eb__1_m9FC9CC3F1BC6A9825C6AAAACC009A80B9002601E,
	U3CU3Ec__DisplayClass73_0__ctor_mBA657FF3A1A66BAD348402D55C0CBF907EF04500,
	U3CU3Ec__DisplayClass73_0_U3CDOLocalPathU3Eb__0_m6F62BCA3F8EA1C0B289DAE5D29BB98B91637182D,
	U3CU3Ec__DisplayClass73_0_U3CDOLocalPathU3Eb__1_mB5E0EB45470B749C89A1AFB16B94A5A4DD70C8A7,
	U3CU3Ec__DisplayClass74_0__ctor_mB149BC27C91715D1390070AC1611EC0B5E407DA4,
	U3CU3Ec__DisplayClass74_0_U3CDOTimeScaleU3Eb__0_m3E6D393D63F227A1FE0344D251FE6062A7733798,
	U3CU3Ec__DisplayClass74_0_U3CDOTimeScaleU3Eb__1_m23DE9C348FA50B978BCBE8F82F0AEB0685D2EB85,
	U3CU3Ec__DisplayClass75_0__ctor_m3E8EEFA5EFC6F9B932892AF9DEF001B386A742C4,
	U3CU3Ec__DisplayClass75_0_U3CDOBlendableColorU3Eb__0_m95C20EC251CB95704A7CF56D5C44AF4452A31C40,
	U3CU3Ec__DisplayClass75_0_U3CDOBlendableColorU3Eb__1_m8C706B706D7E3413BDB9E78A29B3E12D21C7F405,
	U3CU3Ec__DisplayClass76_0__ctor_m21AC20B4D851EC2183AACE433C2FC734EBF9DE3E,
	U3CU3Ec__DisplayClass76_0_U3CDOBlendableColorU3Eb__0_m4700D2A464D3BA139AB08AE4E8B7C371A9C9DFA3,
	U3CU3Ec__DisplayClass76_0_U3CDOBlendableColorU3Eb__1_mBD3E852C2C7DE134BDD399079EC8AD6D414F73F3,
	U3CU3Ec__DisplayClass77_0__ctor_mE3F1213264B3DAB7BDB42F3B7AD72F0B454EF3CC,
	U3CU3Ec__DisplayClass77_0_U3CDOBlendableColorU3Eb__0_mCE3589B0887759940AEB1BE9487320F338E92BFB,
	U3CU3Ec__DisplayClass77_0_U3CDOBlendableColorU3Eb__1_mB9076AE89488405260928B8AFB8298C2E29898CD,
	U3CU3Ec__DisplayClass78_0__ctor_mC945331E7A543C50DB1C06B2DDE915BDA17445E3,
	U3CU3Ec__DisplayClass78_0_U3CDOBlendableMoveByU3Eb__0_m34327B601092B8EC0063433ACF4B675B1916F262,
	U3CU3Ec__DisplayClass78_0_U3CDOBlendableMoveByU3Eb__1_mBA2C42167D25EA510E5F45D475F6069D3BE07FB0,
	U3CU3Ec__DisplayClass79_0__ctor_m9ACAC8B9F78745771E6F5D79135A2F48D4E45FF3,
	U3CU3Ec__DisplayClass79_0_U3CDOBlendableLocalMoveByU3Eb__0_mA7417C35BC4493F589D21ED50D2DB534B89C0720,
	U3CU3Ec__DisplayClass79_0_U3CDOBlendableLocalMoveByU3Eb__1_m3F206A1C3000246E24EF27FA9AD2A6A40236FE6B,
	U3CU3Ec__DisplayClass80_0__ctor_m206F5342AD73218D71E40A75225296C8283248A1,
	U3CU3Ec__DisplayClass80_0_U3CDOBlendableRotateByU3Eb__0_m27F0458B5FA3804D85B104E6A04BE1110F9D8429,
	U3CU3Ec__DisplayClass80_0_U3CDOBlendableRotateByU3Eb__1_mD2AA7CB2FF9EBE3C2B0327F7C166BFD268CB049B,
	U3CU3Ec__DisplayClass81_0__ctor_m42E5D2CB7CCD8208A59348F69263245C461C9D93,
	U3CU3Ec__DisplayClass81_0_U3CDOBlendableLocalRotateByU3Eb__0_m161F5C4A89F61BEE9C07983B244A0C9CB9A0C0E2,
	U3CU3Ec__DisplayClass81_0_U3CDOBlendableLocalRotateByU3Eb__1_m0B659E64749A5E5E4B4851CEC3558108DE8944B7,
	U3CU3Ec__DisplayClass82_0__ctor_m392015F9097A9F33F12F6B392F6C3E4C7C47BD91,
	U3CU3Ec__DisplayClass82_0_U3CDOBlendableScaleByU3Eb__0_m73E4E201C3B816C8BD38045B19B630CD56FB0B41,
	U3CU3Ec__DisplayClass82_0_U3CDOBlendableScaleByU3Eb__1_m9342366ED6735575E70FA58CFE905D2978E32D26,
	U3CWaitForCompletionU3Ed__14__ctor_mE7533C100A0B5404B3FC93CEA6A0C37827870A87,
	U3CWaitForCompletionU3Ed__14_System_IDisposable_Dispose_m303A8F433405425BDC09AAE0F023A63D315B1EF9,
	U3CWaitForCompletionU3Ed__14_MoveNext_mFEF5F6183204655EDB9653AB2411907380008298,
	U3CWaitForCompletionU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE19223304A68A35794CA6F24216AAFD892EA5AC1,
	U3CWaitForCompletionU3Ed__14_System_Collections_IEnumerator_Reset_mF6C6790F450FE739E25B336C52E64226C575BD95,
	U3CWaitForCompletionU3Ed__14_System_Collections_IEnumerator_get_Current_mC04EEB82100E6C38A307534B0B9C28060021DB16,
	U3CWaitForRewindU3Ed__15__ctor_m37784760A70E04AC02EC1D582B83F4668111A5A0,
	U3CWaitForRewindU3Ed__15_System_IDisposable_Dispose_m3B4A63F07D1DD69D384626E147834AD7F9149A26,
	U3CWaitForRewindU3Ed__15_MoveNext_m1E84ABD217A5D6C6C72C96771EE8F8C9B2F76D8A,
	U3CWaitForRewindU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD0CB3F77C35B9E9981BA568AF23A35F479B91C53,
	U3CWaitForRewindU3Ed__15_System_Collections_IEnumerator_Reset_m9C0EC279DBA63D45FEB58C3E702A8BB4858BA619,
	U3CWaitForRewindU3Ed__15_System_Collections_IEnumerator_get_Current_m750B3811AE0DB4A6F9805BECB8DF2596A2C54F3B,
	U3CWaitForKillU3Ed__16__ctor_m6EE0C594B82600E5E99AD05056367F5325ABD2FB,
	U3CWaitForKillU3Ed__16_System_IDisposable_Dispose_m3732D9849EC07BDD486028486DEF03885C650C09,
	U3CWaitForKillU3Ed__16_MoveNext_m2159DC2ED05D95253A737C47F71D27C147E85441,
	U3CWaitForKillU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD22099F2EB9F7275F4C9664AD9657CF571A45DC6,
	U3CWaitForKillU3Ed__16_System_Collections_IEnumerator_Reset_mDA1D8BF979E23C98EEA6DAF7B2C2230ABB1B10F4,
	U3CWaitForKillU3Ed__16_System_Collections_IEnumerator_get_Current_mBDACB19F06D62D737C514DAC0CCBC697F44DAA9B,
	U3CWaitForElapsedLoopsU3Ed__17__ctor_m1AEA52887D651620F95BF3075BFB69D4D91747CC,
	U3CWaitForElapsedLoopsU3Ed__17_System_IDisposable_Dispose_m5B505953396B4222005CF5E9C20CC9D4B6C5C041,
	U3CWaitForElapsedLoopsU3Ed__17_MoveNext_m2FE7359E892D86FB3A1C828A321EFA8C94B7DE29,
	U3CWaitForElapsedLoopsU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB6B87B9867FA04AA0FC215032805FEC243A46FDC,
	U3CWaitForElapsedLoopsU3Ed__17_System_Collections_IEnumerator_Reset_m67224BDA06DBD0C7B2A8465BF9B0B732C2A4B394,
	U3CWaitForElapsedLoopsU3Ed__17_System_Collections_IEnumerator_get_Current_m8801CA279BCD694AC804BA6B9702C486A2567CCC,
	U3CWaitForPositionU3Ed__18__ctor_m0265088734E4649A7C2DA707563A5BB4C5D1ED2E,
	U3CWaitForPositionU3Ed__18_System_IDisposable_Dispose_m1D8512DD5734B0F334C7D86B5189495981BB9067,
	U3CWaitForPositionU3Ed__18_MoveNext_m248E3EAEE5300311FF2BE2AE6AADB8B567CE1AA8,
	U3CWaitForPositionU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF944285E4CEC654F6F2A4ED68C5BD7C37BAF360,
	U3CWaitForPositionU3Ed__18_System_Collections_IEnumerator_Reset_m5199C8176C24AB72FCAC16AA9CA6997EEC5C36B5,
	U3CWaitForPositionU3Ed__18_System_Collections_IEnumerator_get_Current_m1230933A96DB7238797F1C0B3E1592669D367581,
	U3CWaitForStartU3Ed__19__ctor_mC5BB3CB2C97AA15519D6CB641CAE394F1DB11C2E,
	U3CWaitForStartU3Ed__19_System_IDisposable_Dispose_mDC8918991DDE6C8EBA355B49557FEB6EDC331B31,
	U3CWaitForStartU3Ed__19_MoveNext_m7273BB46ADA2EEB3D45328F92A20677718E8F3E4,
	U3CWaitForStartU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51827FF406E046E75CB7BB1FBCAD94D5CFD4E6A4,
	U3CWaitForStartU3Ed__19_System_Collections_IEnumerator_Reset_mE24D88E9B004C8AB98E432B4963736DC69468813,
	U3CWaitForStartU3Ed__19_System_Collections_IEnumerator_get_Current_mE1637A2CE0B2145054A0E512DE310A31C8FB4A57,
	U3CU3Ec__cctor_mB7D6B119DEABF2D27B0E2DF78C19745EAE02C6B5,
	U3CU3Ec__ctor_mCEBEFF6B6FECAAFB0E1D7744298A0C816DCC42BB,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_0_m8422CB8EE877F7AC7975590F4430CD16467FD2D5,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_1_mA4FA951639F14C84A46BCF3DEF064594BED04FF0,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_2_mC63DDF643E5EE7DA17E58A8E0ABD2881DA51623C,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_3_m92C37170BA2074145756259E716DD6CBDAA58928,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_4_m79B14CBC25F458C1C144E7CA6D4E00D0078FFEF9,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_5_mF42AEF55B3EAEBB5F69155345B050A57732F13CE,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_6_m6F5CCA6C15982F075F52ACE4D1DD88118DF2FB3B,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_7_m7C0A9ABB7AB2FA66626A3C422EA0A0734F192F3C,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_8_mAE66D99FB7EDBC75B1EB6B52BF220931C078007E,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_9_mD12823F20B4DC939D2E6537779DA5B820A6BECF8,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_10_mEB2A499C6F094747DB9C46ECFF0EEB6405A831F6,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_11_m189CD77E0724EFC30E23444610543605B004B37F,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_12_mA9B325A277839DED4276CDC80B7E234C576E3099,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_13_m996454852D07FED1E1D94880957E26CFD30D0315,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_14_m42C5B6ECB50310C06836FE9D91205D91906E38F8,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_15_mEF5B627D4123E82693BF46ACA2AE47E1299A4265,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_16_m2A5850539FDC19F19F1B8E6F4310574B8C4BDB17,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_17_m7AB0A1C30ACEB82AAEBF34F22272FCD2B1A959E9,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_18_m554E547B8438F3336855FCBA3236A0B08BFCA6FF,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_19_m05EB95C055B75FFDEAA77F159B4DBB1FFAEAE01E,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_20_m6BE84CDF139FB97FCCC64EE13D69B8CD99662F77,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_21_m3CEBD0982B0561CB39EA0F295F7E253669F9984B,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_22_m6D23875F0A658C5DFE6BFF8D3E37825D93105522,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_23_mA4141804D1FFA686B4DA2F128E8B1C6AD271D69A,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_24_m952E9771551239F0DBA53B7ADA44E7DA0F70A36F,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_25_m5A35078F32346ECF7C0125AAF8F2295E5A58B128,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_26_m2349F372B4B11CDD4512EB2616553C553D81CC77,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_27_mC2192BD27C605C82942AC991125E69E62557E51E,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_28_mD8A7A5269E41AEA94F2008884A31D7E76447C204,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_29_m951B17D9BE05EFF08958ED094B075CD6DDF6CB68,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_30_mC14074E2AFB650112DFB01AFAA1E626E344B8A60,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_31_m4B774FB374D0C7CF5024F7F311794E0FC4CF38FA,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_32_m92DC339E14ACB0D97A8E37F518919DDA2D5AE081,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_33_m4A09D0C05A6647EF50C04183DB7C7ABB468F89A8,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_34_m958048AD5E79C4FE563F0738EECABD4AC9383ADC,
	U3CU3Ec_U3CToEaseFunctionU3Eb__4_35_m26D1751E54BD8E843A0647A57379070FD1822ABF,
};
extern void Color2__ctor_mB17DD3F311A7AA9AC3FA318105CF03900034C65B_AdjustorThunk (void);
extern void PathOptions_Reset_mAE569F0A485C767B02D5A39FE73FBB99F499181E_AdjustorThunk (void);
extern void QuaternionOptions_Reset_m9F3925AC48AC22071B77E36A944E061E0C46A134_AdjustorThunk (void);
extern void UintOptions_Reset_mC2110A83EF78588DC8E220C98D2ABE7971110602_AdjustorThunk (void);
extern void Vector3ArrayOptions_Reset_m3B9A2BB478088F79B5B9BF318FC8BBAE1799453F_AdjustorThunk (void);
extern void NoOptions_Reset_m27971DF59C8A9C56F0DB40B6ED3C01F5816F8BE6_AdjustorThunk (void);
extern void ColorOptions_Reset_mBFFB7CD03418E47DEE6B7CD3EDB3BF38D2FE3714_AdjustorThunk (void);
extern void FloatOptions_Reset_mA56EA5E336B757B6DFEF7C512FAB46E7D491BAA4_AdjustorThunk (void);
extern void RectOptions_Reset_m76EC7D6D0CA41EAAE53344226F46FDFF5FB2578A_AdjustorThunk (void);
extern void StringOptions_Reset_m2ECE10EBDC0E574ED2D50230BB2D49ED57C230F0_AdjustorThunk (void);
extern void VectorOptions_Reset_mF46B9B9771CBDA05DD32C4396C611D9D8E86BCD4_AdjustorThunk (void);
extern void ControlPoint__ctor_m3E47DB0F5677F7DF493D707670BC3A853B24E708_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[12] = 
{
	{ 0x06000001, Color2__ctor_mB17DD3F311A7AA9AC3FA318105CF03900034C65B_AdjustorThunk },
	{ 0x06000208, PathOptions_Reset_mAE569F0A485C767B02D5A39FE73FBB99F499181E_AdjustorThunk },
	{ 0x06000209, QuaternionOptions_Reset_m9F3925AC48AC22071B77E36A944E061E0C46A134_AdjustorThunk },
	{ 0x0600020A, UintOptions_Reset_mC2110A83EF78588DC8E220C98D2ABE7971110602_AdjustorThunk },
	{ 0x0600020B, Vector3ArrayOptions_Reset_m3B9A2BB478088F79B5B9BF318FC8BBAE1799453F_AdjustorThunk },
	{ 0x0600020C, NoOptions_Reset_m27971DF59C8A9C56F0DB40B6ED3C01F5816F8BE6_AdjustorThunk },
	{ 0x0600020D, ColorOptions_Reset_mBFFB7CD03418E47DEE6B7CD3EDB3BF38D2FE3714_AdjustorThunk },
	{ 0x0600020E, FloatOptions_Reset_mA56EA5E336B757B6DFEF7C512FAB46E7D491BAA4_AdjustorThunk },
	{ 0x0600020F, RectOptions_Reset_m76EC7D6D0CA41EAAE53344226F46FDFF5FB2578A_AdjustorThunk },
	{ 0x06000210, StringOptions_Reset_m2ECE10EBDC0E574ED2D50230BB2D49ED57C230F0_AdjustorThunk },
	{ 0x06000211, VectorOptions_Reset_mF46B9B9771CBDA05DD32C4396C611D9D8E86BCD4_AdjustorThunk },
	{ 0x06000225, ControlPoint__ctor_m3E47DB0F5677F7DF493D707670BC3A853B24E708_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1032] = 
{
	1304,
	4683,
	4683,
	4684,
	1545,
	3418,
	1097,
	2770,
	-1,
	-1,
	-1,
	-1,
	1545,
	491,
	119,
	2501,
	5759,
	5681,
	5797,
	4365,
	5797,
	4077,
	5149,
	5688,
	5797,
	5759,
	5229,
	4118,
	4105,
	4108,
	4108,
	4110,
	4110,
	4114,
	4119,
	4120,
	4121,
	4120,
	4103,
	4115,
	4114,
	-1,
	3768,
	4118,
	4133,
	3598,
	3533,
	3553,
	3522,
	4112,
	4104,
	5771,
	5455,
	4747,
	5759,
	5454,
	5454,
	5759,
	5454,
	4751,
	4340,
	5455,
	4750,
	4747,
	5759,
	5454,
	5759,
	5454,
	4746,
	5759,
	5454,
	4746,
	5759,
	5454,
	4746,
	5455,
	4339,
	4015,
	5455,
	4747,
	5759,
	5454,
	5759,
	5454,
	4967,
	5759,
	5771,
	5771,
	4814,
	4814,
	5797,
	-1,
	3418,
	4146,
	4198,
	3800,
	3637,
	4199,
	4413,
	4798,
	4801,
	4801,
	3418,
	1089,
	5685,
	5218,
	5685,
	5685,
	4646,
	5218,
	-1,
	-1,
	5685,
	5685,
	4644,
	5218,
	5685,
	5685,
	4627,
	5586,
	5586,
	5586,
	4810,
	4815,
	5586,
	5454,
	5649,
	5046,
	5046,
	5046,
	5649,
	5626,
	5626,
	5626,
	5626,
	5626,
	5454,
	5074,
	4810,
	5649,
	3418,
	4813,
	4399,
	4815,
	4815,
	4399,
	3418,
	3395,
	3395,
	123,
	5685,
	5626,
	3630,
	3564,
	4746,
	4405,
	4405,
	4405,
	4381,
	4405,
	4405,
	4405,
	4405,
	4401,
	4401,
	3599,
	3600,
	3599,
	3600,
	4381,
	4405,
	4405,
	4085,
	4381,
	4083,
	4405,
	4131,
	4131,
	4407,
	4135,
	4407,
	4135,
	4142,
	4140,
	4132,
	4132,
	4132,
	4139,
	3773,
	3603,
	3552,
	3552,
	4116,
	4116,
	4133,
	4405,
	4140,
	4132,
	4132,
	4132,
	4140,
	4132,
	4132,
	4132,
	4139,
	4400,
	4139,
	4400,
	4409,
	4405,
	4405,
	4405,
	4405,
	3773,
	3603,
	3774,
	3774,
	3556,
	3558,
	3599,
	3600,
	3599,
	3600,
	3603,
	3603,
	3552,
	3552,
	4116,
	4116,
	4405,
	4381,
	4381,
	4083,
	4140,
	4140,
	4139,
	4139,
	4409,
	4747,
	4747,
	4747,
	4747,
	5454,
	5454,
	4340,
	4340,
	5454,
	5454,
	5454,
	5454,
	5454,
	5454,
	5454,
	5454,
	4747,
	4747,
	4747,
	4747,
	5454,
	5454,
	5454,
	5454,
	3418,
	3371,
	2040,
	2038,
	2038,
	1085,
	648,
	2038,
	2038,
	2040,
	2040,
	1091,
	2038,
	2038,
	2038,
	2038,
	2038,
	2038,
	2038,
	2038,
	2041,
	2040,
	2040,
	5797,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4813,
	4813,
	4813,
	4404,
	4815,
	4815,
	4813,
	4813,
	4404,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4814,
	4814,
	4389,
	4814,
	4389,
	4814,
	4389,
	4814,
	4814,
	4814,
	4129,
	4814,
	4389,
	4387,
	4128,
	4138,
	4101,
	4130,
	4614,
	3398,
	2793,
	3418,
	3395,
	2502,
	3395,
	123,
	4185,
	5626,
	-1,
	3418,
	3418,
	1100,
	675,
	1099,
	673,
	2040,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2770,
	1552,
	940,
	2770,
	2770,
	778,
	7,
	3418,
	2770,
	1552,
	944,
	2770,
	2770,
	781,
	10,
	3418,
	2770,
	1552,
	1072,
	2770,
	2770,
	783,
	12,
	3418,
	2770,
	1552,
	1072,
	2770,
	2770,
	783,
	12,
	3418,
	2770,
	1552,
	1101,
	2770,
	2770,
	791,
	20,
	3418,
	2770,
	1552,
	5771,
	1101,
	2770,
	2770,
	786,
	15,
	153,
	3418,
	2770,
	1552,
	938,
	2770,
	2770,
	777,
	6,
	3418,
	2770,
	1552,
	994,
	2770,
	2770,
	782,
	11,
	3418,
	2770,
	1552,
	1245,
	2770,
	2770,
	787,
	16,
	3418,
	2770,
	1552,
	1097,
	2770,
	2770,
	784,
	13,
	3418,
	5797,
	2770,
	1552,
	1116,
	2770,
	2770,
	788,
	17,
	3418,
	2770,
	1552,
	994,
	2770,
	2770,
	790,
	19,
	3418,
	2770,
	1552,
	1242,
	2770,
	2770,
	792,
	21,
	3418,
	2770,
	1552,
	1250,
	2770,
	2770,
	794,
	23,
	3418,
	1552,
	2770,
	1097,
	2770,
	2770,
	789,
	18,
	421,
	2043,
	3418,
	5797,
	5797,
	5685,
	4388,
	2770,
	1552,
	1237,
	2770,
	2770,
	779,
	8,
	3418,
	2770,
	1552,
	1246,
	2770,
	2770,
	793,
	22,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	3418,
	5626,
	5626,
	5626,
	5626,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5797,
	1601,
	4687,
	876,
	492,
	3418,
	876,
	492,
	1543,
	1543,
	3418,
	876,
	492,
	1543,
	1543,
	3418,
	526,
	3418,
	897,
	1247,
	2502,
	1005,
	4810,
	5685,
	3418,
	2032,
	1552,
	2743,
	3418,
	5685,
	5771,
	2770,
	1552,
	1111,
	2770,
	2770,
	785,
	14,
	3418,
	3418,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5685,
	5685,
	5685,
	5685,
	5685,
	5685,
	5685,
	5685,
	5685,
	5685,
	5681,
	3418,
	3418,
	3418,
	3418,
	3418,
	1588,
	3418,
	3418,
	3418,
	1089,
	2038,
	2038,
	2038,
	1096,
	1100,
	2038,
	5797,
	5797,
	3418,
	3418,
	-1,
	-1,
	-1,
	1586,
	-1,
	5771,
	4627,
	5685,
	5759,
	5218,
	5797,
	5797,
	5797,
	5149,
	5759,
	4606,
	3544,
	4452,
	5626,
	5218,
	4186,
	5626,
	5626,
	5626,
	5626,
	4453,
	4967,
	5626,
	5626,
	5759,
	5759,
	5591,
	4814,
	4814,
	5685,
	5685,
	5797,
	5685,
	5685,
	5685,
	5681,
	5797,
	5078,
	5051,
	4991,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4200,
	4200,
	4200,
	3799,
	3634,
	5578,
	5622,
	2770,
	491,
	4200,
	4200,
	4200,
	4200,
	3636,
	3418,
	3398,
	2793,
	3418,
	3398,
	2793,
	3418,
	3418,
	491,
	3418,
	3398,
	2793,
	3418,
	3398,
	2793,
	3418,
	3398,
	2793,
	3418,
	3300,
	2700,
	3418,
	3398,
	2793,
	3418,
	3398,
	2793,
	3418,
	3398,
	2793,
	3418,
	3398,
	2793,
	3418,
	3387,
	2787,
	3418,
	3387,
	2787,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3300,
	2700,
	3418,
	3398,
	2793,
	3418,
	3398,
	2793,
	3418,
	3301,
	2701,
	3418,
	3300,
	2700,
	3418,
	3300,
	2700,
	3418,
	3300,
	2700,
	3418,
	3300,
	2700,
	3418,
	3398,
	2793,
	3418,
	3414,
	2808,
	3418,
	3414,
	2808,
	3418,
	3414,
	2808,
	3418,
	3414,
	2808,
	3418,
	3417,
	2811,
	3418,
	3416,
	3418,
	3416,
	3418,
	3416,
	3418,
	3416,
	3418,
	3381,
	3418,
	3381,
	3418,
	3416,
	3418,
	3416,
	3416,
	3418,
	3416,
	3418,
	3416,
	2810,
	3418,
	3416,
	3418,
	3416,
	2810,
	3418,
	3414,
	2808,
	3418,
	3398,
	2793,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3381,
	2781,
	3418,
	3381,
	2781,
	3418,
	3381,
	2781,
	3418,
	3381,
	2781,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3381,
	2781,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3398,
	2793,
	3418,
	3300,
	2700,
	3418,
	3300,
	2700,
	3418,
	3300,
	2700,
	3418,
	3416,
	2810,
	3418,
	3416,
	2810,
	3418,
	3381,
	2781,
	3418,
	3381,
	2781,
	3418,
	3416,
	2810,
	2743,
	3418,
	3395,
	3371,
	3418,
	3371,
	2743,
	3418,
	3395,
	3371,
	3418,
	3371,
	2743,
	3418,
	3395,
	3371,
	3418,
	3371,
	2743,
	3418,
	3395,
	3371,
	3418,
	3371,
	2743,
	3418,
	3395,
	3371,
	3418,
	3371,
	2743,
	3418,
	3395,
	3371,
	3418,
	3371,
	5797,
	3418,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
	491,
};
static const Il2CppTokenRangePair s_rgctxIndices[50] = 
{
	{ 0x02000050, { 72, 14 } },
	{ 0x0600002A, { 0, 1 } },
	{ 0x0600005C, { 1, 2 } },
	{ 0x0600006F, { 3, 1 } },
	{ 0x06000070, { 4, 1 } },
	{ 0x0600011F, { 5, 1 } },
	{ 0x06000120, { 6, 1 } },
	{ 0x06000121, { 7, 1 } },
	{ 0x06000122, { 8, 1 } },
	{ 0x06000123, { 9, 1 } },
	{ 0x06000124, { 10, 1 } },
	{ 0x06000125, { 11, 1 } },
	{ 0x06000126, { 12, 1 } },
	{ 0x06000127, { 13, 1 } },
	{ 0x06000128, { 14, 1 } },
	{ 0x06000129, { 15, 1 } },
	{ 0x0600012A, { 16, 1 } },
	{ 0x0600012B, { 17, 1 } },
	{ 0x0600012C, { 18, 1 } },
	{ 0x0600012D, { 19, 1 } },
	{ 0x0600012E, { 20, 1 } },
	{ 0x0600012F, { 21, 1 } },
	{ 0x06000130, { 22, 1 } },
	{ 0x06000131, { 23, 1 } },
	{ 0x06000132, { 24, 1 } },
	{ 0x06000133, { 25, 1 } },
	{ 0x06000134, { 26, 1 } },
	{ 0x06000135, { 27, 1 } },
	{ 0x06000136, { 28, 1 } },
	{ 0x06000137, { 29, 1 } },
	{ 0x06000138, { 30, 1 } },
	{ 0x06000139, { 31, 1 } },
	{ 0x06000143, { 32, 1 } },
	{ 0x06000144, { 33, 1 } },
	{ 0x06000145, { 34, 1 } },
	{ 0x06000146, { 35, 1 } },
	{ 0x06000147, { 36, 1 } },
	{ 0x06000148, { 37, 1 } },
	{ 0x06000149, { 38, 1 } },
	{ 0x06000166, { 39, 1 } },
	{ 0x0600016E, { 40, 1 } },
	{ 0x06000170, { 41, 6 } },
	{ 0x06000171, { 47, 3 } },
	{ 0x06000172, { 50, 5 } },
	{ 0x06000173, { 55, 3 } },
	{ 0x06000175, { 58, 1 } },
	{ 0x06000222, { 59, 3 } },
	{ 0x06000223, { 62, 4 } },
	{ 0x06000273, { 66, 1 } },
	{ 0x06000277, { 67, 5 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[86] = 
{
	{ (Il2CppRGCTXDataType)3, 20841 },
	{ (Il2CppRGCTXDataType)3, 21883 },
	{ (Il2CppRGCTXDataType)3, 22130 },
	{ (Il2CppRGCTXDataType)2, 322 },
	{ (Il2CppRGCTXDataType)2, 323 },
	{ (Il2CppRGCTXDataType)2, 337 },
	{ (Il2CppRGCTXDataType)2, 338 },
	{ (Il2CppRGCTXDataType)2, 345 },
	{ (Il2CppRGCTXDataType)2, 354 },
	{ (Il2CppRGCTXDataType)2, 346 },
	{ (Il2CppRGCTXDataType)2, 347 },
	{ (Il2CppRGCTXDataType)2, 340 },
	{ (Il2CppRGCTXDataType)2, 341 },
	{ (Il2CppRGCTXDataType)2, 342 },
	{ (Il2CppRGCTXDataType)2, 344 },
	{ (Il2CppRGCTXDataType)2, 343 },
	{ (Il2CppRGCTXDataType)2, 348 },
	{ (Il2CppRGCTXDataType)2, 349 },
	{ (Il2CppRGCTXDataType)2, 357 },
	{ (Il2CppRGCTXDataType)2, 355 },
	{ (Il2CppRGCTXDataType)2, 356 },
	{ (Il2CppRGCTXDataType)2, 331 },
	{ (Il2CppRGCTXDataType)2, 329 },
	{ (Il2CppRGCTXDataType)2, 328 },
	{ (Il2CppRGCTXDataType)2, 330 },
	{ (Il2CppRGCTXDataType)2, 333 },
	{ (Il2CppRGCTXDataType)2, 332 },
	{ (Il2CppRGCTXDataType)2, 326 },
	{ (Il2CppRGCTXDataType)2, 327 },
	{ (Il2CppRGCTXDataType)2, 334 },
	{ (Il2CppRGCTXDataType)2, 335 },
	{ (Il2CppRGCTXDataType)2, 336 },
	{ (Il2CppRGCTXDataType)2, 324 },
	{ (Il2CppRGCTXDataType)2, 325 },
	{ (Il2CppRGCTXDataType)2, 339 },
	{ (Il2CppRGCTXDataType)2, 350 },
	{ (Il2CppRGCTXDataType)2, 351 },
	{ (Il2CppRGCTXDataType)2, 352 },
	{ (Il2CppRGCTXDataType)2, 353 },
	{ (Il2CppRGCTXDataType)3, 17161 },
	{ (Il2CppRGCTXDataType)3, 21698 },
	{ (Il2CppRGCTXDataType)3, 22016 },
	{ (Il2CppRGCTXDataType)3, 2481 },
	{ (Il2CppRGCTXDataType)3, 96 },
	{ (Il2CppRGCTXDataType)3, 98 },
	{ (Il2CppRGCTXDataType)3, 97 },
	{ (Il2CppRGCTXDataType)3, 21994 },
	{ (Il2CppRGCTXDataType)3, 22014 },
	{ (Il2CppRGCTXDataType)3, 94 },
	{ (Il2CppRGCTXDataType)3, 21992 },
	{ (Il2CppRGCTXDataType)3, 22013 },
	{ (Il2CppRGCTXDataType)3, 2480 },
	{ (Il2CppRGCTXDataType)3, 92 },
	{ (Il2CppRGCTXDataType)3, 93 },
	{ (Il2CppRGCTXDataType)3, 21991 },
	{ (Il2CppRGCTXDataType)3, 22015 },
	{ (Il2CppRGCTXDataType)3, 95 },
	{ (Il2CppRGCTXDataType)3, 21993 },
	{ (Il2CppRGCTXDataType)3, 99 },
	{ (Il2CppRGCTXDataType)1, 435 },
	{ (Il2CppRGCTXDataType)1, 700 },
	{ (Il2CppRGCTXDataType)2, 850 },
	{ (Il2CppRGCTXDataType)1, 445 },
	{ (Il2CppRGCTXDataType)2, 854 },
	{ (Il2CppRGCTXDataType)3, 18086 },
	{ (Il2CppRGCTXDataType)2, 445 },
	{ (Il2CppRGCTXDataType)2, 146 },
	{ (Il2CppRGCTXDataType)1, 436 },
	{ (Il2CppRGCTXDataType)1, 701 },
	{ (Il2CppRGCTXDataType)1, 791 },
	{ (Il2CppRGCTXDataType)2, 3281 },
	{ (Il2CppRGCTXDataType)3, 17186 },
	{ (Il2CppRGCTXDataType)1, 574 },
	{ (Il2CppRGCTXDataType)1, 748 },
	{ (Il2CppRGCTXDataType)1, 817 },
	{ (Il2CppRGCTXDataType)2, 748 },
	{ (Il2CppRGCTXDataType)3, 22054 },
	{ (Il2CppRGCTXDataType)3, 22035 },
	{ (Il2CppRGCTXDataType)3, 22073 },
	{ (Il2CppRGCTXDataType)3, 102 },
	{ (Il2CppRGCTXDataType)3, 101 },
	{ (Il2CppRGCTXDataType)2, 817 },
	{ (Il2CppRGCTXDataType)3, 2482 },
	{ (Il2CppRGCTXDataType)3, 22111 },
	{ (Il2CppRGCTXDataType)3, 22092 },
	{ (Il2CppRGCTXDataType)3, 100 },
};
extern const CustomAttributesCacheGenerator g_DOTween_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_DOTween_CodeGenModule;
const Il2CppCodeGenModule g_DOTween_CodeGenModule = 
{
	"DOTween.dll",
	1032,
	s_methodPointers,
	12,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	50,
	s_rgctxIndices,
	86,
	s_rgctxValues,
	NULL,
	g_DOTween_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
