﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4;
// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7;
// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2;
// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// System.Runtime.InteropServices.GuidAttribute
struct GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct  AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Reflection.AssemblyCompanyAttribute
struct  AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::m_company
	String_t* ___m_company_0;

public:
	inline static int32_t get_offset_of_m_company_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4, ___m_company_0)); }
	inline String_t* get_m_company_0() const { return ___m_company_0; }
	inline String_t** get_address_of_m_company_0() { return &___m_company_0; }
	inline void set_m_company_0(String_t* value)
	{
		___m_company_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_company_0), (void*)value);
	}
};


// System.Reflection.AssemblyConfigurationAttribute
struct  AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyConfigurationAttribute::m_configuration
	String_t* ___m_configuration_0;

public:
	inline static int32_t get_offset_of_m_configuration_0() { return static_cast<int32_t>(offsetof(AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C, ___m_configuration_0)); }
	inline String_t* get_m_configuration_0() const { return ___m_configuration_0; }
	inline String_t** get_address_of_m_configuration_0() { return &___m_configuration_0; }
	inline void set_m_configuration_0(String_t* value)
	{
		___m_configuration_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_configuration_0), (void*)value);
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct  AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyDescriptionAttribute
struct  AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::m_description
	String_t* ___m_description_0;

public:
	inline static int32_t get_offset_of_m_description_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3, ___m_description_0)); }
	inline String_t* get_m_description_0() const { return ___m_description_0; }
	inline String_t** get_address_of_m_description_0() { return &___m_description_0; }
	inline void set_m_description_0(String_t* value)
	{
		___m_description_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct  AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct  AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Reflection.AssemblyTitleAttribute
struct  AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::m_title
	String_t* ___m_title_0;

public:
	inline static int32_t get_offset_of_m_title_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7, ___m_title_0)); }
	inline String_t* get_m_title_0() const { return ___m_title_0; }
	inline String_t** get_address_of_m_title_0() { return &___m_title_0; }
	inline void set_m_title_0(String_t* value)
	{
		___m_title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_title_0), (void*)value);
	}
};


// System.Reflection.AssemblyTrademarkAttribute
struct  AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTrademarkAttribute::m_trademark
	String_t* ___m_trademark_0;

public:
	inline static int32_t get_offset_of_m_trademark_0() { return static_cast<int32_t>(offsetof(AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2, ___m_trademark_0)); }
	inline String_t* get_m_trademark_0() const { return ___m_trademark_0; }
	inline String_t** get_address_of_m_trademark_0() { return &___m_trademark_0; }
	inline void set_m_trademark_0(String_t* value)
	{
		___m_trademark_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_trademark_0), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.InteropServices.ComVisibleAttribute
struct  ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.InteropServices.ComVisibleAttribute::_val
	bool ____val_0;

public:
	inline static int32_t get_offset_of__val_0() { return static_cast<int32_t>(offsetof(ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A, ____val_0)); }
	inline bool get__val_0() const { return ____val_0; }
	inline bool* get_address_of__val_0() { return &____val_0; }
	inline void set__val_0(bool value)
	{
		____val_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct  ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.FlagsAttribute
struct  FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.InteropServices.GuidAttribute
struct  GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.InteropServices.GuidAttribute::_val
	String_t* ____val_0;

public:
	inline static int32_t get_offset_of__val_0() { return static_cast<int32_t>(offsetof(GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063, ____val_0)); }
	inline String_t* get__val_0() const { return ____val_0; }
	inline String_t** get_address_of__val_0() { return &____val_0; }
	inline void set__val_0(String_t* value)
	{
		____val_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____val_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct  InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.CompilerServices.InternalsVisibleToAttribute::_assemblyName
	String_t* ____assemblyName_0;
	// System.Boolean System.Runtime.CompilerServices.InternalsVisibleToAttribute::_allInternalsVisible
	bool ____allInternalsVisible_1;

public:
	inline static int32_t get_offset_of__assemblyName_0() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____assemblyName_0)); }
	inline String_t* get__assemblyName_0() const { return ____assemblyName_0; }
	inline String_t** get_address_of__assemblyName_0() { return &____assemblyName_0; }
	inline void set__assemblyName_0(String_t* value)
	{
		____assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____assemblyName_0), (void*)value);
	}

	inline static int32_t get_offset_of__allInternalsVisible_1() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____allInternalsVisible_1)); }
	inline bool get__allInternalsVisible_1() const { return ____allInternalsVisible_1; }
	inline bool* get_address_of__allInternalsVisible_1() { return &____allInternalsVisible_1; }
	inline void set__allInternalsVisible_1(bool value)
	{
		____allInternalsVisible_1 = value;
	}
};


// System.ParamArrayAttribute
struct  ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct  SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9 (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * __this, String_t* ___assemblyName0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * __this, String_t* ___title0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25 (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757 (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * __this, String_t* ___configuration0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0 (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * __this, String_t* ___company0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02 (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * __this, String_t* ___trademark0, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.ComVisibleAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172 (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * __this, bool ___visibility0, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.GuidAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GuidAttribute__ctor_mCCEF3938DF601B23B5791CEE8F7AF05C98B6AFEA (GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 * __this, String_t* ___guid0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
static void DOTween_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[3];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[4];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x44\x4F\x54\x77\x65\x65\x6E\x45\x64\x69\x74\x6F\x72"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[5];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x44\x4F\x54\x77\x65\x65\x6E\x34\x33"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[6];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x44\x4F\x54\x77\x65\x65\x6E\x34\x36"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[7];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x44\x4F\x54\x77\x65\x65\x6E\x50\x72\x6F"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[8];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x44\x4F\x54\x77\x65\x65\x6E\x50\x72\x6F\x45\x64\x69\x74\x6F\x72"), NULL);
	}
	{
		AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * tmp = (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 *)cache->attributes[9];
		AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x4F\x54\x77\x65\x65\x6E"), NULL);
	}
	{
		AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * tmp = (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 *)cache->attributes[10];
		AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * tmp = (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C *)cache->attributes[11];
		AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * tmp = (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 *)cache->attributes[12];
		AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x6D\x69\x67\x69\x61\x6E\x74"), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[13];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x44\x4F\x54\x77\x65\x65\x6E"), NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[14];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\xC2\xA9\x20\x44\x61\x6E\x69\x65\x6C\x65\x20\x47\x69\x61\x72\x64\x69\x6E\x69\x2C\x20\x32\x30\x31\x34"), NULL);
	}
	{
		AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * tmp = (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 *)cache->attributes[15];
		AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * tmp = (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A *)cache->attributes[16];
		ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172(tmp, false, NULL);
	}
	{
		GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 * tmp = (GuidAttribute_tBB494B31270577CCD589ABBB159C18CDAE20D063 *)cache->attributes[17];
		GuidAttribute__ctor_mCCEF3938DF601B23B5791CEE8F7AF05C98B6AFEA(tmp, il2cpp_codegen_string_new_wrapper("\x38\x30\x37\x65\x30\x36\x38\x63\x2D\x32\x61\x30\x65\x2D\x34\x63\x38\x31\x2D\x61\x33\x30\x33\x2D\x34\x62\x34\x66\x64\x33\x39\x32\x34\x35\x31\x31"), NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[18];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x31\x2E\x30\x2E\x30\x2E\x30"), NULL);
	}
}
static void AxisConstraint_tA0D384964013674923F26C7DF2618FB76CD3D7F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_CustomAttributesCacheGenerator_DOTween_CompleteAndReturnKilledTotExceptFor_m1B34E13253A3590FD3BB5FCAB4091C6F7D84EC5A____excludeTargetsOrIds0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_CustomAttributesCacheGenerator_DOTween_KillAll_mC78D7B22BB32344450F9A3E7BDBCC36E4B3A6E3A____idsOrTargetsToExclude1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass54_0_t5CA40F778CB3B9321133596D585FE154AAEB55C4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t94C2A795634E6243038D09A271EB7F4DDBC663D0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_tFF384B69897CCFA60AF1FC3FF0E159508024A89C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Complete_m1006012CD6C2AED2F84C6F04C16FF5D6DCEB0188(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Complete_mBB5115AC82EEA28A91F897A767E69D58272EA926(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Flip_m53777A3B2C663F559F961A6D90AFBA6910C7EEF1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_ForceInit_m25AFF7EA6A03406F1080E11292F6C8923E082836(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Goto_mFA79DC2B3D9D9B253F4AB7612CFD5E71CA3E41B2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Kill_m8A79B9D5D31C46E9669C2EFEDF26BF4F7EB02D10(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Pause_m13EAC3F2C8FF10C8D3225156F8289DDF8BE427B0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Play_mABAB33DD47D993F5CD53090A59A957DFC857C767(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_PlayBackwards_m147A9C8A47E881FA94B137394EF6F4EA53BFD1ED(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_PlayForward_m72684DEC2FE49A30A71B00FF442CCBBE3933F547(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Restart_mE904E3AB313B9CF8977E8BA3A2A191B821FC95CC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Rewind_m911EFDC4DAAEB2DC3C80112FE4C14F01BCF558C8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_SmoothRewind_mE58E999B4138952F9B6252DBFC2829E961166922(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_TogglePause_mF6A1C2093B76873D90C8C36DF3E16528B227960B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_GotoWaypoint_m1D883C58BF3D55F0C88F06E5BD4AFBCFD00C78A4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_WaitForCompletion_m19F0941B6FEF13B652521ECE7BCE7742FDC1E925(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_WaitForRewind_m66036030592B041B2CF97D4FEDEED7E3F999E230(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_WaitForKill_mA95646AE5DAE73DFEE9D545BCA488AC47D6BDE32(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_WaitForElapsedLoops_mE72913438B63E226ADCB3A53AD3DF1D91AA2A341(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_WaitForPosition_m4B51613BD949DD254307AAA4CD69C63D402833BE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_WaitForStart_m3F13AED0EB2811F6AA57F960ADD807944ADDBBF9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_CompletedLoops_m067EA5E0CFEEF70813063DC6336000D02E5C2E87(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Delay_m65CC3BC72D7A467348E7873E2DB7600038F0D3B7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Duration_mB449EF1960C6AC6818CAFAEF32A1E6B1130AC6A0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Elapsed_m63AF61BC4F010D9195A174ED86D42D357753EA80(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_ElapsedPercentage_mC12ED1EAF1BEBF089D80B420DC6D987DE9C7C618(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_ElapsedDirectionalPercentage_m478ED527B4F86DCC0240987C317231168587F5DF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_IsActive_m19E1D79E2590A189C95DADED68FFDB43A7F3B2A2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_IsBackwards_m8266302C8957C3266BCA3BEF8B5F0A29362BEE0E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_IsComplete_mC87F29FE0746C793F0C11175A9B37087C1C436FD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_IsInitialized_mE92123AF4FE069B2F175193367CAFF218B552CEB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_IsPlaying_m3B9AB4389AA78D1EB5384BB33BF24640E0005F6D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Loops_mF14C87CC213FA4DA5113F6A645BB17577214F1BC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_PathGetPoint_m8DDEC2EABC0BC6B6BC1B0CC6CF396D099C23EDF8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_PathGetDrawPoints_mB97A95B99626040449A5F1B4B4098B4E6D4E6BA9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_PathLength_mCEE0E32D47307DA4C408E9705B8054B540093C82(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOFade_m3AC0CE0E603F8F3A55796F07C2AB3C68A9F480AF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPitch_m105F8A473434A2950C30CC3ABDB8FFD1B96AB55F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOAspect_mCD85FE3D0E67452AC9682825549AD6C973417F70(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOColor_m406B84ECF991C880D866C555B3AE4DF41274D203(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOFarClipPlane_m0956009131C00C1D3B6ABF39C23EEFB29509A913(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOFieldOfView_m2FDB010FDA9B4164709FE0711B349C64FD73B498(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DONearClipPlane_mD6C4FD5A86B7D8E512D412FC687B564F987B8B2E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOOrthoSize_m6B046F2A9CB65F5C33F7E05E404C81BE5FCC1468(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPixelRect_m18CCEED227266203D749048A9959857D56EBDEEB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DORect_m9D52CABC1C9DACDFE1EF0FB52D22BE293536B39E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakePosition_mE45C809671BA1C7EAFD000988706740A530AA1FA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakePosition_mD9DF263BD711C7BC12E152F014ED9998A6622FFA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakeRotation_m1DE1BCBE8A8E4B236206932EE171B894F9ABDC80(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakeRotation_m09BA15DFC0073FCB0B0C4DC820B91BC5723C9E51(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOColor_mE6D48DDA2B16225D9DBF40D67D8716992016BE5E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOIntensity_m4F6889CB6C25B1B30099075C7AC9F6E3B24FC080(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShadowStrength_mFA55E1DB84B84DF7DD91BB12E75225630A4BE3C7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOColor_m95F7C9CF81DFA68FDC0048E17587A94E0AF83828(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOColor_mE6140CF9E1B3D3B559F6751F9B8C3B1602826710(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOColor_mDA1E755EC546D05D85BB8AB560DBE5D7F0BB2F03(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOFade_mB5B3CAC06223B35C8E666ADC139D93FF17A31C48(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOFade_m26A4C55A80B031F02DBE0E88980E528FE5A5D0AF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOFloat_m680DEA336497C8D581C407635DFDB4C98E110981(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOOffset_m86FF955A9303A7AA5719B0AC2552A65096FB2071(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOOffset_mF11F80175BBC7A3F9506A1B926DF993306154C0C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOTiling_m1A4955BC0ED3D1F77EC53FAEF2EBF114CDC38DE8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOTiling_m92B0C39EAB8BF112EAF1108D0FFDCCD63DE4734F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOVector_mD77F3BC35D05D7FE67869842889048EE5C33F57D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOMove_mD9817CCAE0681D620067D21B4E868428FE2533D8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveX_mAFE787202DE6B5E0DDFC2EED98DBA533040C4A10(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveY_m5A82F6418B09E4930707934723C4DA82C7D802C3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveZ_m07BB95E8FFBD4CB8DB710B5771F88FE8F57211B4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DORotate_m578E0422C09350DCB3298AA920B6F9684BF09CAD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLookAt_m77EF2702C9BC226F7BC85636AE000C661657A114(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOJump_mBFA6A1181AC16A62DC1F587E8593D6AA2C816293(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPath_mDDB1604C7FB47EB928B6BC63A1DD7CF9A85D3164(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalPath_mC7A563E0098ACD6BECF7380308E9507831696FDA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPath_m01EDCCA0EE21B7E554FF28BEA409198E50CFD1FC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalPath_mE0136EA4E054DF88A8322A664D3CCEFAFABEAE4C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOResize_mA7C52079F18C8A5344776456730E4C776757B86B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOTime_m69CE5A2BEC68B0F0BF8528BBAAD49B4A68C6A3CB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOMove_mA83A2C1D4DFC55CB4D2B2763FD1CF8D4ADCD634D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveX_m5C08EC5E31AB6EE00A812577014611B581402B5F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveY_m27EE0286BBFE2B1586BE88A660B15B55C907A27C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveZ_m7CAC2050DB7E8E421FC2682C3046A4284ACEA754(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalMove_m0F6957D951BA19FEA0A48C7CDA52240AC7CD9369(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalMoveX_m2003324ABEB7FCE822E7A47F9E92F4ABE1CA29CC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalMoveY_m180E4334F1CC2C6E1BE72ECE2DE8B21DD4AA9469(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalMoveZ_m68386131CD5762AAAF410F045F6AAEF8835A05CB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DORotate_mBC72ACEECA2351B7CE6491E5FE0B76469609DEFD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DORotateQuaternion_m2BDDEBEAC8361BE98535A1A1B67F6C7FA88E1A47(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalRotate_m0A3A0561A8DFA471BCA68891686EE51BD6CD2F28(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalRotateQuaternion_m287F445C8974F85EE58DF0752443EF0258716F09(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOScale_m68874CDE5A501CD5EBFD9021D689A8E3029A7797(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOScale_m295B0D8C9518B001C1B6A9A77FBF7A4A6621488F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOScaleX_m9665F244A1551D37A9B7F089DD5591C45D67D0A9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOScaleY_mE82DE29207C5EEC076B9CDB1E08001D4336A9923(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOScaleZ_mC574D66C01E03AE4C8DFBC7381CA6F1A0AE3D2CF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLookAt_mDAA64D1B1CD9AFC1E351C021602C3BE102CC548B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPunchPosition_m9EEF18AFCE150A50A4F79AA85982B9CE70DAD0DA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPunchScale_m351E39CC16A4CC9692955C502F350D56825B17E4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPunchRotation_mC1D5DA0D6218A579D04BB373DDF1264E0AC4736A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakePosition_mEC6EF5C734CA59D5C7A7808204E380B5E1C25EF5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakePosition_mDEC0E682E9A8D470D6FD1F825161C3E6087DC3B8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakeRotation_m290A50BA6F51EFC713882E1CFCE747B4B932FCB6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakeRotation_m35DC1BCA976A9A2E1C7A6CA4359EB5F30695F1EE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakeScale_mB136215C0FAAD4193DD01A5917384145C0368231(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakeScale_m39430B83048A4F601B8B206224E792F7F075D22F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOJump_m65381EFDC430C4FA504C2656A4E302D6844CBECB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalJump_mA83D2B1991F192D59F5953D1D5FC64ADBF728037(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPath_mCEB7849F32E0448CF8ACFEE2871F11796D4A86F2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalPath_m38EEE83BE829F79550424AD767CFCA1862E615BE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPath_m7AB84619455A541B79164E1D270A5AB511533B24(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalPath_m28185CD9787640FBB679A0B46F144E1BB0DB43F8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOTimeScale_m1776BEFED215BFE01F622383F7AF35A99FA972BF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOBlendableColor_mA127ED765FB1A2BA042A9C91A678AAAC279F493F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOBlendableColor_mA8441733C425378F3A3D1F4FFAE7A9F940DB0147(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOBlendableColor_m41015F46280C6C7E8649140192B28CD0E8D4DDD3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOBlendableMoveBy_m5EF65D43D7282E6CD69AB3470DDECF36EF85D96E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOBlendableLocalMoveBy_m305FC1B99A29FB0B2A114E402906F61DE36DB63D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOBlendableRotateBy_m2B26C8D41D59A77FE5315731CF33214DE82A4C59(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOBlendableLocalRotateBy_mA0ADE3AD501FE8B31B14B68C78C4BAEEA709342F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOBlendableScaleBy_m8F7EB3E007F5AC240CC5C483EB44BC0524C9A578(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOComplete_m1939237DDDE3FA508E979D2AE3DC21523918EE40(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOComplete_mB2018D4F1EEACAA7B3F43262CE9F1E3D6BCEC693(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOKill_m4E7F53B09D2B06D72DE5783E8BD6AB995E31F390(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOKill_m6C9A4C28424CA62C5EF0A9DF28D1DF8082708BD8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOFlip_m88EB2005DB078C4E3BF3D8C6714A3A0F2E529587(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOFlip_m9F4FA956DDCD67EDBBF5160C2E2718DF55733358(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOGoto_mB9FEB565E815B21A5C66B3EB71C1EBA36894F46B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOGoto_mBA4DBCFA8B1BEC3B2D9D5757331843042CABF6CA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPause_m2A116931A03160BE68769455EB747AF8DA491836(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPause_mFC159FACC09AF2E17B76F8AF4412F57A84786189(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPlay_m5B3FFE8CE9F5A2FCADDA69A3D1DA22D733E126E3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPlay_m01B0C62F69368262EDD6E6C03865B3DBBAA80B3B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPlayBackwards_mAF952391CB3262C21B8DC73C34084F5788EDC54B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPlayBackwards_mC6F2B1B13752B08CCD27DB1B0532207C5482DB85(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPlayForward_m1F20FCE79D40810AE5E361925CE872790D43025A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPlayForward_m5147ABDF037463B1268132EB003F81BE1FDC2B0D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DORestart_mC1F7568D09A231DA92F1E997CCC2C4A00DC753C3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DORestart_mBB4474BF78311CF0497452FDECC3A78515ECC28A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DORewind_m323E06FBD4B7B6F871A4D2F24D345D126824F6E9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DORewind_m733779A82524CD33F9602588C940048C18A03920(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOSmoothRewind_m60199BDB37313EFFA85AAB25B5C280AEFA9DA436(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOSmoothRewind_mA39884D144593E20BB05E04A1FF62E7573A3DFAB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOTogglePause_m95176BDF60314FD5D78055896D36C8573F93698D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOTogglePause_m6A532FF78795DAB72A338B410F7D024874F526B4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_tB40E11B852037879808C92BECD7876A16BB66AAD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_t4ADC6D787273413B12DA16C6A9FF7634CFEEC5EA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t38264080302DF9BF0BB41063085294F621CC4A38_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_tDBCE13845A0DCA0162D051A9F411E372DB36F3E6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t7FCF4513CF1F07215127C203C9FEA14AAFDC9F50_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_t3E49EC8252D6DB8CEB99D6C9D21EDA4FE7D8BE18_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_t8AEF0BB1862E14B21B028DAD7EF5C402264499A8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_t11A4EF733655292B9E7C095F4A758D73B65D6A3F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_tFA1BEC670AA44A3425581607E62689617A5B3401_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_tAEF6C9694F8394E48268EB50E3C1464E0B60FB6D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_tB1724A4C3C041227693B2D436C665B8E1D0AC0CF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_tEB6F5744358D6A125915DB13390F110FE82C787D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_t7F679F15C2CB3784367D8D8BF022E0077B26A487_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_t039CA28E39A03F04200549CE54814453A0719E7E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass14_0_t2476DE91421EEAC3A7446513131387BF08003760_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_tBEC9107837D687EBB5CDB5EDFFABB536BD4A10A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_t9CB4882EBED18DDDF1D7CD1D0357BC4F6E9858CC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_0_t205C146EA04CB33EC286F3E1CE7BF71A84343E75_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass18_0_t72640789D2C8F7572BF20C9AB1B3716FA6768E1F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass19_0_tC8022EDFD3425768C4E03243E018004D4C9ACBD9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass20_0_t099D84F496135601F3D2601D80BFD66757B0268A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass21_0_tF16CE8F598074E095A34C5DD25A4774532CD4CD8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_0_tEB5FA4ABC812BF9861B0AF7E1DD4B6423793ECF2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass23_0_tD326E914D6FDF53F8A2FF25A22C3D0C65CAE6D89_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass24_0_t096DF9ABE6C1349E041D1EA2B49CFA1A43161374_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass25_0_t03D4E1A9D3AC2BB3ABB8594B2C273D6D674C95A2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass26_0_tC865C8E4576535A1D73324B6F21501817561F7B2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass27_0_tDA6B27949F5F36C069878A4E67ACBBB485CFACE9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass28_0_tD8DCB77C11ADB2F5EB2F5FC4332BEE3124F05464_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass29_0_t1B6AF445A08375194E5E017B92719581E3C9F5A2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass30_0_tFEC9D7C41DE7FFA666B325AE1C362EC685A65E0E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass31_0_t6F0E0692F19CC45D16AB033AC896C22ECCA09B88_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass32_0_t8247DD53263B3A6FF2B0A2241E29602140C7380C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass33_0_t765B45EA4597AE8893DD9470FB2455A6FC64804A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass34_0_t631306492A118761A64A0236B75E91D2CE2F2F22_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass35_0_t9FFA54157CF9F3C92A523EEE76CB926F21CFF054_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass36_0_t74E6F4F83D614A64AC7F3D366759AB9FC0658BCA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass37_0_t16BD3AABF6319FF6BC900088E44E10D629554F0E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass38_0_tFEF5A203AECAEADD9F70F17330888855E1607115_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass39_0_t9B3349FB9C25533D1448DF5D2FE127C5565A9282_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass40_0_t61317F89A801E2B92E3BD91E0C9F9D7A2E106150_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass41_0_t3AA536533CAD546209A8BAB1D874B7A77001E33B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass42_0_t5A980AA5C6462DA8D3261F56B05DC2DFDE1DEE0B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass43_0_t57A062A9F0E130A73E2537AC1398854DBBAB6EAA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass44_0_t35D5B1A764645B9277BC2C3E72D2AB8AB2DC0468_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass45_0_t63268709B7CA79A5184AB8CD0FE54F29230DD0C7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass46_0_t41D23C61E71A27B1AC01175000364B9C262D66CC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass47_0_tB2CA530F359BCEFCDB853EF86C7C2D5741AB2B90_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass48_0_t84E8D844740851285C164840939C4F303380BEFA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass49_0_t191A682E7710F43850AF276DD75B0A0A25E2CB48_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass50_0_t47081FCA9FF456DB61D24E10E04402E515B55690_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass51_0_tD98EE766E6E9387AB14873319D8FCFFB0A607286_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass52_0_t51FBA15D34D087CCAAD7A55F56FEEBCBC5934AB9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass53_0_tC1BEC5FE83906FF603A44BE85789B191DAAD74FB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass54_0_tC0242F8B0F6F558FAC19D924BFC40F446F51C8E5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass55_0_t0C6D9F57F49BDBE4767A915B6EB112D3A546BE45_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass56_0_tF8F4550D9DDE0683D73D39CE6F83B11445E3DB53_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass57_0_t3EFE56B26C4F97F8EC63A875565CCA48602B597C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass58_0_t31DCD02618339E466511545CC8757F2CA77E38AF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass59_0_t85D3B29416C541E333498DFB9C72E46B6B4017C9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass60_0_tDD836C405EE2650F09F66CED491E61BF393B81FB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass61_0_t81E28B7AD572DEB90D667B1B7D24840BF924C804_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass62_0_t515820AEA2B6317217164C21AE194CFAE77EFABC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass63_0_t5AF9A4B60BF689C27E034A7154589EA945B1781F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass64_0_tF83478EF468693DBD75CA37525CC0908159528FB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass65_0_tA1CAEFF22CB6BFF3148E484DBD43AE077FC32171_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass66_0_t3EF239B9367D927C00E0FFBCCDBBBF72164BD3D9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass67_0_t3E363EA12481E34709704C52830BE98F1E0F32C7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass68_0_t85231F1484B378760E157E570CEF9C306625724D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass69_0_t492F9FAC0085552B88452C309F8F32550A7E3EFC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass70_0_t5C01264398B7E89B2A9F2987C87CD4A30FB82336_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass71_0_tE30A9C42D37DD4DF234252ECA5B1AF7FC9954593_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass72_0_tEEE9D318DE74F408B53EABEC2911D3EE48A9D4D7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass73_0_t6F5BEC03BEA36492BE8362AC5944EEC1EA22D54A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass74_0_tEA5E6205658FCA4217542E5E669FA08D8B5F43AE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass75_0_tF7C57165DFACD6EFC5A0DE6B947348FD1344EB1E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass76_0_t35A28073B391C02D85927A47CA7288A4537F592C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass77_0_t54165AC5269DD09B5C2BFBA59525F530C90779E3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass78_0_t8FF467ED4F60D9A7736BFF4092D33D28C3C3B257_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass79_0_tD225F1FE8C9E4836480E73AD6817819C0929B361_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass80_0_t1B2F6D82ABB39AADED1B4DA6FF8EFAF2C2171C9F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass81_0_tBDFFB0F6393D3C7C23083F24CBD7BA647A59887D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass82_0_t21E3CD943D58B61AE6956A7AE46C846F006F9D5C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetAutoKill_m0F63077E3750588A176EB71D336747523F446A19(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetAutoKill_m21D49E7D6C94061CE792D166EB85E72EB371F1EB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetId_mBBA0D4BF1C99EC4DE10F3CAF8EB073E6DAD79F25(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetTarget_mB9458523F6ACC00258FC1826941CC216BB9750AB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetLoops_m6135CE068BAB0AF20E372C1631A1E4DFAC172DA1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetLoops_mBC93AC297B1F4FC51486A34CC21D1275D5F05B19(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetEase_m15F52EA199298028A4902CB8C22D6BF573651466(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetEase_m7A7666437F13B0DE10917887B66A5BAD0FC11F18(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetEase_mA24A6BC910E0D5C522C479CA830600EA6DBA6911(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetEase_m6F6B3570BC12F6D12DD429F362C599ADB527825D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetEase_m6892BD08C18F23C6580B86EEDF579C46C3E1F5CD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetRecyclable_m33D0108686BE50284A2C5803D545152AB7390B39(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetRecyclable_m09B82A06A7D351A90E7B1A3F6C5B1D77D697F467(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetUpdate_m77530AE9A8ECCDE518542F4B0697B42C620DEA44(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetUpdate_mBEB5F0C843A574FF765F20871E3789958955CB4A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetUpdate_m9941D7A05C74D4D2792073D1F2FF080A9563E065(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnStart_m651FF6FF4CD6B69A04A79B68D6E2F3C6DC496DF2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnPlay_m8C5A3EF0FE5A53E53199BFB078985DCF8290C191(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnPause_m0F93BAB9BDB9499C860C14F4F3F1B93851F01224(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnRewind_mEDC5338C4968FDDF15A685C68D4866023B506DC8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnUpdate_m1485D6222A6B23DB759289BDC4D007D6DF7EB5B0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnStepComplete_m7569E16F4DA8201E4AE78893FCC1B4DB7F694D1D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnComplete_mC30E8CFFAAB640E8D9A5D05628E1B71F7EEAEAD6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnKill_m07DE908CB3EA433603FA03AFD49342849DF8E9FC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnWaypointChange_mCDD63513A19AB7BBA232D05559C8662B5FAFA315(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetAs_m783B780BCB9B2FE126F1198FABD86EB043A4ACC8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetAs_mBDAEC5E95E17B15C522BA36FB69A4B105BCC07F3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_Append_m045B3A5C557D2007A05A55F1D5B86A26AA5F13D5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_Prepend_mA5ACFDDF27607E1D6A1556A18C4467099A25ABDD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_Join_m33E665745C1F091AAD10AD2C3FDD91017861D89F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_Insert_m6C8C4CAFC8CC4C5F2120879AE79BD01E3903422A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_AppendInterval_m20E76B7FD0B9E0FD85D5E55444177E412D2A39FE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_PrependInterval_mF91C39268775A123FCCE526262C391CB826B8B9B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_AppendCallback_m056A9F025C67DB07A403E70BED916DCDDF8671E6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_PrependCallback_m430A6A2B38D293E34439857D757394BAF37D9EEA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_InsertCallback_mCEC5B490A95472ED358A6A930F69941843B21826(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_From_m41B352F2643EEF5F8602D32C51D373FE8A8E0964(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_From_m5B8D672BC35BE5D48ED7AC4673089451106EA93B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetDelay_m87802322925DBE8186D81420CA984CCBCD6074D9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetRelative_mF9232D71791D5356137306A230990BC61BB0447C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetRelative_m32323B523CF668B9D5415BACD1DF23B3DFC93B2C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetSpeedBased_m12A4DF33634764113DA53C919AAEDF903CA9B14C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetSpeedBased_mA86309BC9CC816410F8D00BC64DDE78CFACE42AD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m619178CD390C9F59543EBF056E54CECFF4BA95CD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m9C45DDF251D2CD1BC96529E3480E9A91B46AF1E4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m9B154343AFE2B086C2BE39C9E913F51186A10BAF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mA202FF383BEAAE03292420F1163032B5D0566742(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mE2CF0CEA2DE42858A86D1C736E162AC6814256AE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m527D580A508B8297888953E59601C95C399E98F4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mAF32D7903868EB663666CB91E101BF2477234647(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mF1E28DA2E05779F1E161A62F103038250446302D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mDA3551543550914D61FBDAF7F749A5FC69889AA7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mF4CB8F4A815A8CF310CB76FA85D22E854A65319A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mB198DDCA53C32A43F7EA9213244369D8B55F9A40(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mF4577F3599CEDC3C1AF2F31CD109481C229C4994(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mD2E5E89120A9CD6676C15DB603DC72BFE7EC126F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mC7FC2851A87231470309BC2F740335EC663BD21F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m232500C0CAFAD89AFE0A437A6015F2E59ED3D8FF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetLookAt_m31B2E7ECAADA53B8C6EDA9BC760AFA2F6155B382(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetLookAt_m3BFF34B7765FCB4B3DDE2B3B90869FE9086DDD01(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetLookAt_mAD3CC7329671C9A6D0FA41A9AB2D3B236769C2AF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetPathForwardDirection_m8EF4B32280222570CCC96CE57E9D0C4936F1F732(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringPluginExtensions_t3020868E50CDBE4754D7A24FACC98D90650F01DC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringPluginExtensions_t3020868E50CDBE4754D7A24FACC98D90650F01DC_CustomAttributesCacheGenerator_StringPluginExtensions_ScrambleChars_m3177B26808D6532A66A40AA9292B134A216D4C90(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringPluginExtensions_t3020868E50CDBE4754D7A24FACC98D90650F01DC_CustomAttributesCacheGenerator_StringPluginExtensions_AppendScrambledChars_m8A75B95AF635D8479043492E1DF7084C4A9E4870(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_type(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_subdivisionsXSegment(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_subdivisions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_wps(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_controlPoints(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_length(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_isFinalized(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_timesTable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_lengthsTable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DOTweenComponent_tEFFB81B283458D3C607F5D8960C4E28E10764F29_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void U3CWaitForCompletionU3Ed__14_tB45859C2358BD11663DD63650731B7149A0DFB23_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForCompletionU3Ed__14_tB45859C2358BD11663DD63650731B7149A0DFB23_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__14__ctor_mE7533C100A0B5404B3FC93CEA6A0C37827870A87(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCompletionU3Ed__14_tB45859C2358BD11663DD63650731B7149A0DFB23_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__14_System_IDisposable_Dispose_m303A8F433405425BDC09AAE0F023A63D315B1EF9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCompletionU3Ed__14_tB45859C2358BD11663DD63650731B7149A0DFB23_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE19223304A68A35794CA6F24216AAFD892EA5AC1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCompletionU3Ed__14_tB45859C2358BD11663DD63650731B7149A0DFB23_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__14_System_Collections_IEnumerator_Reset_mF6C6790F450FE739E25B336C52E64226C575BD95(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCompletionU3Ed__14_tB45859C2358BD11663DD63650731B7149A0DFB23_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__14_System_Collections_IEnumerator_get_Current_mC04EEB82100E6C38A307534B0B9C28060021DB16(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForRewindU3Ed__15_tAD6D817368CB8A458F47A994A7103A4CE6F3DA71_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForRewindU3Ed__15_tAD6D817368CB8A458F47A994A7103A4CE6F3DA71_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__15__ctor_m37784760A70E04AC02EC1D582B83F4668111A5A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForRewindU3Ed__15_tAD6D817368CB8A458F47A994A7103A4CE6F3DA71_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__15_System_IDisposable_Dispose_m3B4A63F07D1DD69D384626E147834AD7F9149A26(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForRewindU3Ed__15_tAD6D817368CB8A458F47A994A7103A4CE6F3DA71_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD0CB3F77C35B9E9981BA568AF23A35F479B91C53(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForRewindU3Ed__15_tAD6D817368CB8A458F47A994A7103A4CE6F3DA71_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__15_System_Collections_IEnumerator_Reset_m9C0EC279DBA63D45FEB58C3E702A8BB4858BA619(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForRewindU3Ed__15_tAD6D817368CB8A458F47A994A7103A4CE6F3DA71_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__15_System_Collections_IEnumerator_get_Current_m750B3811AE0DB4A6F9805BECB8DF2596A2C54F3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForKillU3Ed__16_t552D031056DD4DB8BDA1BB638EA41E6A1131C8BE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForKillU3Ed__16_t552D031056DD4DB8BDA1BB638EA41E6A1131C8BE_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__16__ctor_m6EE0C594B82600E5E99AD05056367F5325ABD2FB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForKillU3Ed__16_t552D031056DD4DB8BDA1BB638EA41E6A1131C8BE_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__16_System_IDisposable_Dispose_m3732D9849EC07BDD486028486DEF03885C650C09(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForKillU3Ed__16_t552D031056DD4DB8BDA1BB638EA41E6A1131C8BE_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD22099F2EB9F7275F4C9664AD9657CF571A45DC6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForKillU3Ed__16_t552D031056DD4DB8BDA1BB638EA41E6A1131C8BE_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__16_System_Collections_IEnumerator_Reset_mDA1D8BF979E23C98EEA6DAF7B2C2230ABB1B10F4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForKillU3Ed__16_t552D031056DD4DB8BDA1BB638EA41E6A1131C8BE_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__16_System_Collections_IEnumerator_get_Current_mBDACB19F06D62D737C514DAC0CCBC697F44DAA9B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForElapsedLoopsU3Ed__17_tCB517C96BA7776F1E3CD6A8A4B5D50AAA076AACD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForElapsedLoopsU3Ed__17_tCB517C96BA7776F1E3CD6A8A4B5D50AAA076AACD_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__17__ctor_m1AEA52887D651620F95BF3075BFB69D4D91747CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForElapsedLoopsU3Ed__17_tCB517C96BA7776F1E3CD6A8A4B5D50AAA076AACD_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__17_System_IDisposable_Dispose_m5B505953396B4222005CF5E9C20CC9D4B6C5C041(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForElapsedLoopsU3Ed__17_tCB517C96BA7776F1E3CD6A8A4B5D50AAA076AACD_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB6B87B9867FA04AA0FC215032805FEC243A46FDC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForElapsedLoopsU3Ed__17_tCB517C96BA7776F1E3CD6A8A4B5D50AAA076AACD_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__17_System_Collections_IEnumerator_Reset_m67224BDA06DBD0C7B2A8465BF9B0B732C2A4B394(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForElapsedLoopsU3Ed__17_tCB517C96BA7776F1E3CD6A8A4B5D50AAA076AACD_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__17_System_Collections_IEnumerator_get_Current_m8801CA279BCD694AC804BA6B9702C486A2567CCC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPositionU3Ed__18_t170F9F5656C1BE5986CDA4AEF56179230EFA0BF4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForPositionU3Ed__18_t170F9F5656C1BE5986CDA4AEF56179230EFA0BF4_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__18__ctor_m0265088734E4649A7C2DA707563A5BB4C5D1ED2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPositionU3Ed__18_t170F9F5656C1BE5986CDA4AEF56179230EFA0BF4_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__18_System_IDisposable_Dispose_m1D8512DD5734B0F334C7D86B5189495981BB9067(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPositionU3Ed__18_t170F9F5656C1BE5986CDA4AEF56179230EFA0BF4_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF944285E4CEC654F6F2A4ED68C5BD7C37BAF360(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPositionU3Ed__18_t170F9F5656C1BE5986CDA4AEF56179230EFA0BF4_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__18_System_Collections_IEnumerator_Reset_m5199C8176C24AB72FCAC16AA9CA6997EEC5C36B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPositionU3Ed__18_t170F9F5656C1BE5986CDA4AEF56179230EFA0BF4_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__18_System_Collections_IEnumerator_get_Current_m1230933A96DB7238797F1C0B3E1592669D367581(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForStartU3Ed__19_t16390E0C9E1AF0550E16FC22511D16C8199F1C7D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForStartU3Ed__19_t16390E0C9E1AF0550E16FC22511D16C8199F1C7D_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__19__ctor_mC5BB3CB2C97AA15519D6CB641CAE394F1DB11C2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForStartU3Ed__19_t16390E0C9E1AF0550E16FC22511D16C8199F1C7D_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__19_System_IDisposable_Dispose_mDC8918991DDE6C8EBA355B49557FEB6EDC331B31(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForStartU3Ed__19_t16390E0C9E1AF0550E16FC22511D16C8199F1C7D_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51827FF406E046E75CB7BB1FBCAD94D5CFD4E6A4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForStartU3Ed__19_t16390E0C9E1AF0550E16FC22511D16C8199F1C7D_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__19_System_Collections_IEnumerator_Reset_mE24D88E9B004C8AB98E432B4963736DC69468813(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForStartU3Ed__19_t16390E0C9E1AF0550E16FC22511D16C8199F1C7D_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__19_System_Collections_IEnumerator_get_Current_mE1637A2CE0B2145054A0E512DE310A31C8FB4A57(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Extensions_t8682B038AE47AF4667FE89846FD3230EA5B01B1E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Extensions_t8682B038AE47AF4667FE89846FD3230EA5B01B1E_CustomAttributesCacheGenerator_Extensions_SetSpecialStartupMode_m4C63EA84189CA6ECED72CB45F4A00167CBFA4C05(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Extensions_t8682B038AE47AF4667FE89846FD3230EA5B01B1E_CustomAttributesCacheGenerator_Extensions_NoFrom_m0A51B97805F16F5781068B840863F7509C043153(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Extensions_t8682B038AE47AF4667FE89846FD3230EA5B01B1E_CustomAttributesCacheGenerator_Extensions_Blendable_mE98051CC7CB19CEA24937AF3EFB70E0A03FD87F7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec_t27371799CCD91EBD879972E7C864BE29699C0857_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_tBFBC027F1C52FDD6D5DCC510B835C581D2AEAF12_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_DOTween_AttributeGenerators[];
const CustomAttributesCacheGenerator g_DOTween_AttributeGenerators[353] = 
{
	AxisConstraint_tA0D384964013674923F26C7DF2618FB76CD3D7F4_CustomAttributesCacheGenerator,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator,
	StringPluginExtensions_t3020868E50CDBE4754D7A24FACC98D90650F01DC_CustomAttributesCacheGenerator,
	DOTweenComponent_tEFFB81B283458D3C607F5D8960C4E28E10764F29_CustomAttributesCacheGenerator,
	Extensions_t8682B038AE47AF4667FE89846FD3230EA5B01B1E_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_tBFBC027F1C52FDD6D5DCC510B835C581D2AEAF12_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass54_0_t5CA40F778CB3B9321133596D585FE154AAEB55C4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t94C2A795634E6243038D09A271EB7F4DDBC663D0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_tFF384B69897CCFA60AF1FC3FF0E159508024A89C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_tB40E11B852037879808C92BECD7876A16BB66AAD_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_t4ADC6D787273413B12DA16C6A9FF7634CFEEC5EA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t38264080302DF9BF0BB41063085294F621CC4A38_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_tDBCE13845A0DCA0162D051A9F411E372DB36F3E6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t7FCF4513CF1F07215127C203C9FEA14AAFDC9F50_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_t3E49EC8252D6DB8CEB99D6C9D21EDA4FE7D8BE18_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_t8AEF0BB1862E14B21B028DAD7EF5C402264499A8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_t11A4EF733655292B9E7C095F4A758D73B65D6A3F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_tFA1BEC670AA44A3425581607E62689617A5B3401_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_tAEF6C9694F8394E48268EB50E3C1464E0B60FB6D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_tB1724A4C3C041227693B2D436C665B8E1D0AC0CF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_tEB6F5744358D6A125915DB13390F110FE82C787D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass12_0_t7F679F15C2CB3784367D8D8BF022E0077B26A487_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_t039CA28E39A03F04200549CE54814453A0719E7E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass14_0_t2476DE91421EEAC3A7446513131387BF08003760_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_tBEC9107837D687EBB5CDB5EDFFABB536BD4A10A3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_t9CB4882EBED18DDDF1D7CD1D0357BC4F6E9858CC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_0_t205C146EA04CB33EC286F3E1CE7BF71A84343E75_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass18_0_t72640789D2C8F7572BF20C9AB1B3716FA6768E1F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass19_0_tC8022EDFD3425768C4E03243E018004D4C9ACBD9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass20_0_t099D84F496135601F3D2601D80BFD66757B0268A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass21_0_tF16CE8F598074E095A34C5DD25A4774532CD4CD8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_0_tEB5FA4ABC812BF9861B0AF7E1DD4B6423793ECF2_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass23_0_tD326E914D6FDF53F8A2FF25A22C3D0C65CAE6D89_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass24_0_t096DF9ABE6C1349E041D1EA2B49CFA1A43161374_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass25_0_t03D4E1A9D3AC2BB3ABB8594B2C273D6D674C95A2_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass26_0_tC865C8E4576535A1D73324B6F21501817561F7B2_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass27_0_tDA6B27949F5F36C069878A4E67ACBBB485CFACE9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass28_0_tD8DCB77C11ADB2F5EB2F5FC4332BEE3124F05464_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass29_0_t1B6AF445A08375194E5E017B92719581E3C9F5A2_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass30_0_tFEC9D7C41DE7FFA666B325AE1C362EC685A65E0E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass31_0_t6F0E0692F19CC45D16AB033AC896C22ECCA09B88_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass32_0_t8247DD53263B3A6FF2B0A2241E29602140C7380C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass33_0_t765B45EA4597AE8893DD9470FB2455A6FC64804A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass34_0_t631306492A118761A64A0236B75E91D2CE2F2F22_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass35_0_t9FFA54157CF9F3C92A523EEE76CB926F21CFF054_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass36_0_t74E6F4F83D614A64AC7F3D366759AB9FC0658BCA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass37_0_t16BD3AABF6319FF6BC900088E44E10D629554F0E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass38_0_tFEF5A203AECAEADD9F70F17330888855E1607115_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass39_0_t9B3349FB9C25533D1448DF5D2FE127C5565A9282_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass40_0_t61317F89A801E2B92E3BD91E0C9F9D7A2E106150_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass41_0_t3AA536533CAD546209A8BAB1D874B7A77001E33B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass42_0_t5A980AA5C6462DA8D3261F56B05DC2DFDE1DEE0B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass43_0_t57A062A9F0E130A73E2537AC1398854DBBAB6EAA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass44_0_t35D5B1A764645B9277BC2C3E72D2AB8AB2DC0468_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass45_0_t63268709B7CA79A5184AB8CD0FE54F29230DD0C7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass46_0_t41D23C61E71A27B1AC01175000364B9C262D66CC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass47_0_tB2CA530F359BCEFCDB853EF86C7C2D5741AB2B90_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass48_0_t84E8D844740851285C164840939C4F303380BEFA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass49_0_t191A682E7710F43850AF276DD75B0A0A25E2CB48_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass50_0_t47081FCA9FF456DB61D24E10E04402E515B55690_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass51_0_tD98EE766E6E9387AB14873319D8FCFFB0A607286_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass52_0_t51FBA15D34D087CCAAD7A55F56FEEBCBC5934AB9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass53_0_tC1BEC5FE83906FF603A44BE85789B191DAAD74FB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass54_0_tC0242F8B0F6F558FAC19D924BFC40F446F51C8E5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass55_0_t0C6D9F57F49BDBE4767A915B6EB112D3A546BE45_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass56_0_tF8F4550D9DDE0683D73D39CE6F83B11445E3DB53_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass57_0_t3EFE56B26C4F97F8EC63A875565CCA48602B597C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass58_0_t31DCD02618339E466511545CC8757F2CA77E38AF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass59_0_t85D3B29416C541E333498DFB9C72E46B6B4017C9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass60_0_tDD836C405EE2650F09F66CED491E61BF393B81FB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass61_0_t81E28B7AD572DEB90D667B1B7D24840BF924C804_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass62_0_t515820AEA2B6317217164C21AE194CFAE77EFABC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass63_0_t5AF9A4B60BF689C27E034A7154589EA945B1781F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass64_0_tF83478EF468693DBD75CA37525CC0908159528FB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass65_0_tA1CAEFF22CB6BFF3148E484DBD43AE077FC32171_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass66_0_t3EF239B9367D927C00E0FFBCCDBBBF72164BD3D9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass67_0_t3E363EA12481E34709704C52830BE98F1E0F32C7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass68_0_t85231F1484B378760E157E570CEF9C306625724D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass69_0_t492F9FAC0085552B88452C309F8F32550A7E3EFC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass70_0_t5C01264398B7E89B2A9F2987C87CD4A30FB82336_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass71_0_tE30A9C42D37DD4DF234252ECA5B1AF7FC9954593_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass72_0_tEEE9D318DE74F408B53EABEC2911D3EE48A9D4D7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass73_0_t6F5BEC03BEA36492BE8362AC5944EEC1EA22D54A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass74_0_tEA5E6205658FCA4217542E5E669FA08D8B5F43AE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass75_0_tF7C57165DFACD6EFC5A0DE6B947348FD1344EB1E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass76_0_t35A28073B391C02D85927A47CA7288A4537F592C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass77_0_t54165AC5269DD09B5C2BFBA59525F530C90779E3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass78_0_t8FF467ED4F60D9A7736BFF4092D33D28C3C3B257_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass79_0_tD225F1FE8C9E4836480E73AD6817819C0929B361_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass80_0_t1B2F6D82ABB39AADED1B4DA6FF8EFAF2C2171C9F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass81_0_tBDFFB0F6393D3C7C23083F24CBD7BA647A59887D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass82_0_t21E3CD943D58B61AE6956A7AE46C846F006F9D5C_CustomAttributesCacheGenerator,
	U3CWaitForCompletionU3Ed__14_tB45859C2358BD11663DD63650731B7149A0DFB23_CustomAttributesCacheGenerator,
	U3CWaitForRewindU3Ed__15_tAD6D817368CB8A458F47A994A7103A4CE6F3DA71_CustomAttributesCacheGenerator,
	U3CWaitForKillU3Ed__16_t552D031056DD4DB8BDA1BB638EA41E6A1131C8BE_CustomAttributesCacheGenerator,
	U3CWaitForElapsedLoopsU3Ed__17_tCB517C96BA7776F1E3CD6A8A4B5D50AAA076AACD_CustomAttributesCacheGenerator,
	U3CWaitForPositionU3Ed__18_t170F9F5656C1BE5986CDA4AEF56179230EFA0BF4_CustomAttributesCacheGenerator,
	U3CWaitForStartU3Ed__19_t16390E0C9E1AF0550E16FC22511D16C8199F1C7D_CustomAttributesCacheGenerator,
	U3CU3Ec_t27371799CCD91EBD879972E7C864BE29699C0857_CustomAttributesCacheGenerator,
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_type,
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_subdivisionsXSegment,
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_subdivisions,
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_wps,
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_controlPoints,
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_length,
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_isFinalized,
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_timesTable,
	Path_t55184BB3F1BD04C08B77EB5322B0FC2F28B05DE5_CustomAttributesCacheGenerator_lengthsTable,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Complete_m1006012CD6C2AED2F84C6F04C16FF5D6DCEB0188,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Complete_mBB5115AC82EEA28A91F897A767E69D58272EA926,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Flip_m53777A3B2C663F559F961A6D90AFBA6910C7EEF1,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_ForceInit_m25AFF7EA6A03406F1080E11292F6C8923E082836,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Goto_mFA79DC2B3D9D9B253F4AB7612CFD5E71CA3E41B2,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Kill_m8A79B9D5D31C46E9669C2EFEDF26BF4F7EB02D10,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Pause_m13EAC3F2C8FF10C8D3225156F8289DDF8BE427B0,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Play_mABAB33DD47D993F5CD53090A59A957DFC857C767,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_PlayBackwards_m147A9C8A47E881FA94B137394EF6F4EA53BFD1ED,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_PlayForward_m72684DEC2FE49A30A71B00FF442CCBBE3933F547,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Restart_mE904E3AB313B9CF8977E8BA3A2A191B821FC95CC,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Rewind_m911EFDC4DAAEB2DC3C80112FE4C14F01BCF558C8,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_SmoothRewind_mE58E999B4138952F9B6252DBFC2829E961166922,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_TogglePause_mF6A1C2093B76873D90C8C36DF3E16528B227960B,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_GotoWaypoint_m1D883C58BF3D55F0C88F06E5BD4AFBCFD00C78A4,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_WaitForCompletion_m19F0941B6FEF13B652521ECE7BCE7742FDC1E925,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_WaitForRewind_m66036030592B041B2CF97D4FEDEED7E3F999E230,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_WaitForKill_mA95646AE5DAE73DFEE9D545BCA488AC47D6BDE32,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_WaitForElapsedLoops_mE72913438B63E226ADCB3A53AD3DF1D91AA2A341,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_WaitForPosition_m4B51613BD949DD254307AAA4CD69C63D402833BE,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_WaitForStart_m3F13AED0EB2811F6AA57F960ADD807944ADDBBF9,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_CompletedLoops_m067EA5E0CFEEF70813063DC6336000D02E5C2E87,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Delay_m65CC3BC72D7A467348E7873E2DB7600038F0D3B7,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Duration_mB449EF1960C6AC6818CAFAEF32A1E6B1130AC6A0,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Elapsed_m63AF61BC4F010D9195A174ED86D42D357753EA80,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_ElapsedPercentage_mC12ED1EAF1BEBF089D80B420DC6D987DE9C7C618,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_ElapsedDirectionalPercentage_m478ED527B4F86DCC0240987C317231168587F5DF,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_IsActive_m19E1D79E2590A189C95DADED68FFDB43A7F3B2A2,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_IsBackwards_m8266302C8957C3266BCA3BEF8B5F0A29362BEE0E,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_IsComplete_mC87F29FE0746C793F0C11175A9B37087C1C436FD,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_IsInitialized_mE92123AF4FE069B2F175193367CAFF218B552CEB,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_IsPlaying_m3B9AB4389AA78D1EB5384BB33BF24640E0005F6D,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_Loops_mF14C87CC213FA4DA5113F6A645BB17577214F1BC,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_PathGetPoint_m8DDEC2EABC0BC6B6BC1B0CC6CF396D099C23EDF8,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_PathGetDrawPoints_mB97A95B99626040449A5F1B4B4098B4E6D4E6BA9,
	TweenExtensions_tC2477F9EC2A8E1F6E27D76602F023F49E0693E46_CustomAttributesCacheGenerator_TweenExtensions_PathLength_mCEE0E32D47307DA4C408E9705B8054B540093C82,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOFade_m3AC0CE0E603F8F3A55796F07C2AB3C68A9F480AF,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPitch_m105F8A473434A2950C30CC3ABDB8FFD1B96AB55F,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOAspect_mCD85FE3D0E67452AC9682825549AD6C973417F70,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOColor_m406B84ECF991C880D866C555B3AE4DF41274D203,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOFarClipPlane_m0956009131C00C1D3B6ABF39C23EEFB29509A913,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOFieldOfView_m2FDB010FDA9B4164709FE0711B349C64FD73B498,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DONearClipPlane_mD6C4FD5A86B7D8E512D412FC687B564F987B8B2E,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOOrthoSize_m6B046F2A9CB65F5C33F7E05E404C81BE5FCC1468,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPixelRect_m18CCEED227266203D749048A9959857D56EBDEEB,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DORect_m9D52CABC1C9DACDFE1EF0FB52D22BE293536B39E,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakePosition_mE45C809671BA1C7EAFD000988706740A530AA1FA,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakePosition_mD9DF263BD711C7BC12E152F014ED9998A6622FFA,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakeRotation_m1DE1BCBE8A8E4B236206932EE171B894F9ABDC80,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakeRotation_m09BA15DFC0073FCB0B0C4DC820B91BC5723C9E51,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOColor_mE6D48DDA2B16225D9DBF40D67D8716992016BE5E,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOIntensity_m4F6889CB6C25B1B30099075C7AC9F6E3B24FC080,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShadowStrength_mFA55E1DB84B84DF7DD91BB12E75225630A4BE3C7,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOColor_m95F7C9CF81DFA68FDC0048E17587A94E0AF83828,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOColor_mE6140CF9E1B3D3B559F6751F9B8C3B1602826710,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOColor_mDA1E755EC546D05D85BB8AB560DBE5D7F0BB2F03,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOFade_mB5B3CAC06223B35C8E666ADC139D93FF17A31C48,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOFade_m26A4C55A80B031F02DBE0E88980E528FE5A5D0AF,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOFloat_m680DEA336497C8D581C407635DFDB4C98E110981,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOOffset_m86FF955A9303A7AA5719B0AC2552A65096FB2071,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOOffset_mF11F80175BBC7A3F9506A1B926DF993306154C0C,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOTiling_m1A4955BC0ED3D1F77EC53FAEF2EBF114CDC38DE8,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOTiling_m92B0C39EAB8BF112EAF1108D0FFDCCD63DE4734F,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOVector_mD77F3BC35D05D7FE67869842889048EE5C33F57D,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOMove_mD9817CCAE0681D620067D21B4E868428FE2533D8,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveX_mAFE787202DE6B5E0DDFC2EED98DBA533040C4A10,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveY_m5A82F6418B09E4930707934723C4DA82C7D802C3,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveZ_m07BB95E8FFBD4CB8DB710B5771F88FE8F57211B4,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DORotate_m578E0422C09350DCB3298AA920B6F9684BF09CAD,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLookAt_m77EF2702C9BC226F7BC85636AE000C661657A114,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOJump_mBFA6A1181AC16A62DC1F587E8593D6AA2C816293,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPath_mDDB1604C7FB47EB928B6BC63A1DD7CF9A85D3164,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalPath_mC7A563E0098ACD6BECF7380308E9507831696FDA,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPath_m01EDCCA0EE21B7E554FF28BEA409198E50CFD1FC,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalPath_mE0136EA4E054DF88A8322A664D3CCEFAFABEAE4C,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOResize_mA7C52079F18C8A5344776456730E4C776757B86B,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOTime_m69CE5A2BEC68B0F0BF8528BBAAD49B4A68C6A3CB,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOMove_mA83A2C1D4DFC55CB4D2B2763FD1CF8D4ADCD634D,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveX_m5C08EC5E31AB6EE00A812577014611B581402B5F,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveY_m27EE0286BBFE2B1586BE88A660B15B55C907A27C,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOMoveZ_m7CAC2050DB7E8E421FC2682C3046A4284ACEA754,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalMove_m0F6957D951BA19FEA0A48C7CDA52240AC7CD9369,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalMoveX_m2003324ABEB7FCE822E7A47F9E92F4ABE1CA29CC,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalMoveY_m180E4334F1CC2C6E1BE72ECE2DE8B21DD4AA9469,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalMoveZ_m68386131CD5762AAAF410F045F6AAEF8835A05CB,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DORotate_mBC72ACEECA2351B7CE6491E5FE0B76469609DEFD,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DORotateQuaternion_m2BDDEBEAC8361BE98535A1A1B67F6C7FA88E1A47,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalRotate_m0A3A0561A8DFA471BCA68891686EE51BD6CD2F28,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalRotateQuaternion_m287F445C8974F85EE58DF0752443EF0258716F09,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOScale_m68874CDE5A501CD5EBFD9021D689A8E3029A7797,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOScale_m295B0D8C9518B001C1B6A9A77FBF7A4A6621488F,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOScaleX_m9665F244A1551D37A9B7F089DD5591C45D67D0A9,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOScaleY_mE82DE29207C5EEC076B9CDB1E08001D4336A9923,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOScaleZ_mC574D66C01E03AE4C8DFBC7381CA6F1A0AE3D2CF,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLookAt_mDAA64D1B1CD9AFC1E351C021602C3BE102CC548B,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPunchPosition_m9EEF18AFCE150A50A4F79AA85982B9CE70DAD0DA,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPunchScale_m351E39CC16A4CC9692955C502F350D56825B17E4,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPunchRotation_mC1D5DA0D6218A579D04BB373DDF1264E0AC4736A,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakePosition_mEC6EF5C734CA59D5C7A7808204E380B5E1C25EF5,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakePosition_mDEC0E682E9A8D470D6FD1F825161C3E6087DC3B8,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakeRotation_m290A50BA6F51EFC713882E1CFCE747B4B932FCB6,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakeRotation_m35DC1BCA976A9A2E1C7A6CA4359EB5F30695F1EE,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakeScale_mB136215C0FAAD4193DD01A5917384145C0368231,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOShakeScale_m39430B83048A4F601B8B206224E792F7F075D22F,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOJump_m65381EFDC430C4FA504C2656A4E302D6844CBECB,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalJump_mA83D2B1991F192D59F5953D1D5FC64ADBF728037,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPath_mCEB7849F32E0448CF8ACFEE2871F11796D4A86F2,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalPath_m38EEE83BE829F79550424AD767CFCA1862E615BE,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPath_m7AB84619455A541B79164E1D270A5AB511533B24,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOLocalPath_m28185CD9787640FBB679A0B46F144E1BB0DB43F8,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOTimeScale_m1776BEFED215BFE01F622383F7AF35A99FA972BF,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOBlendableColor_mA127ED765FB1A2BA042A9C91A678AAAC279F493F,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOBlendableColor_mA8441733C425378F3A3D1F4FFAE7A9F940DB0147,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOBlendableColor_m41015F46280C6C7E8649140192B28CD0E8D4DDD3,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOBlendableMoveBy_m5EF65D43D7282E6CD69AB3470DDECF36EF85D96E,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOBlendableLocalMoveBy_m305FC1B99A29FB0B2A114E402906F61DE36DB63D,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOBlendableRotateBy_m2B26C8D41D59A77FE5315731CF33214DE82A4C59,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOBlendableLocalRotateBy_mA0ADE3AD501FE8B31B14B68C78C4BAEEA709342F,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOBlendableScaleBy_m8F7EB3E007F5AC240CC5C483EB44BC0524C9A578,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOComplete_m1939237DDDE3FA508E979D2AE3DC21523918EE40,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOComplete_mB2018D4F1EEACAA7B3F43262CE9F1E3D6BCEC693,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOKill_m4E7F53B09D2B06D72DE5783E8BD6AB995E31F390,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOKill_m6C9A4C28424CA62C5EF0A9DF28D1DF8082708BD8,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOFlip_m88EB2005DB078C4E3BF3D8C6714A3A0F2E529587,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOFlip_m9F4FA956DDCD67EDBBF5160C2E2718DF55733358,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOGoto_mB9FEB565E815B21A5C66B3EB71C1EBA36894F46B,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOGoto_mBA4DBCFA8B1BEC3B2D9D5757331843042CABF6CA,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPause_m2A116931A03160BE68769455EB747AF8DA491836,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPause_mFC159FACC09AF2E17B76F8AF4412F57A84786189,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPlay_m5B3FFE8CE9F5A2FCADDA69A3D1DA22D733E126E3,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPlay_m01B0C62F69368262EDD6E6C03865B3DBBAA80B3B,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPlayBackwards_mAF952391CB3262C21B8DC73C34084F5788EDC54B,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPlayBackwards_mC6F2B1B13752B08CCD27DB1B0532207C5482DB85,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPlayForward_m1F20FCE79D40810AE5E361925CE872790D43025A,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOPlayForward_m5147ABDF037463B1268132EB003F81BE1FDC2B0D,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DORestart_mC1F7568D09A231DA92F1E997CCC2C4A00DC753C3,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DORestart_mBB4474BF78311CF0497452FDECC3A78515ECC28A,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DORewind_m323E06FBD4B7B6F871A4D2F24D345D126824F6E9,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DORewind_m733779A82524CD33F9602588C940048C18A03920,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOSmoothRewind_m60199BDB37313EFFA85AAB25B5C280AEFA9DA436,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOSmoothRewind_mA39884D144593E20BB05E04A1FF62E7573A3DFAB,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOTogglePause_m95176BDF60314FD5D78055896D36C8573F93698D,
	ShortcutExtensions_t667C55CFEF2FE2AE6573A63FE7FA606B98E0F4BE_CustomAttributesCacheGenerator_ShortcutExtensions_DOTogglePause_m6A532FF78795DAB72A338B410F7D024874F526B4,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetAutoKill_m0F63077E3750588A176EB71D336747523F446A19,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetAutoKill_m21D49E7D6C94061CE792D166EB85E72EB371F1EB,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetId_mBBA0D4BF1C99EC4DE10F3CAF8EB073E6DAD79F25,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetTarget_mB9458523F6ACC00258FC1826941CC216BB9750AB,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetLoops_m6135CE068BAB0AF20E372C1631A1E4DFAC172DA1,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetLoops_mBC93AC297B1F4FC51486A34CC21D1275D5F05B19,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetEase_m15F52EA199298028A4902CB8C22D6BF573651466,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetEase_m7A7666437F13B0DE10917887B66A5BAD0FC11F18,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetEase_mA24A6BC910E0D5C522C479CA830600EA6DBA6911,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetEase_m6F6B3570BC12F6D12DD429F362C599ADB527825D,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetEase_m6892BD08C18F23C6580B86EEDF579C46C3E1F5CD,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetRecyclable_m33D0108686BE50284A2C5803D545152AB7390B39,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetRecyclable_m09B82A06A7D351A90E7B1A3F6C5B1D77D697F467,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetUpdate_m77530AE9A8ECCDE518542F4B0697B42C620DEA44,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetUpdate_mBEB5F0C843A574FF765F20871E3789958955CB4A,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetUpdate_m9941D7A05C74D4D2792073D1F2FF080A9563E065,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnStart_m651FF6FF4CD6B69A04A79B68D6E2F3C6DC496DF2,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnPlay_m8C5A3EF0FE5A53E53199BFB078985DCF8290C191,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnPause_m0F93BAB9BDB9499C860C14F4F3F1B93851F01224,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnRewind_mEDC5338C4968FDDF15A685C68D4866023B506DC8,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnUpdate_m1485D6222A6B23DB759289BDC4D007D6DF7EB5B0,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnStepComplete_m7569E16F4DA8201E4AE78893FCC1B4DB7F694D1D,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnComplete_mC30E8CFFAAB640E8D9A5D05628E1B71F7EEAEAD6,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnKill_m07DE908CB3EA433603FA03AFD49342849DF8E9FC,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_OnWaypointChange_mCDD63513A19AB7BBA232D05559C8662B5FAFA315,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetAs_m783B780BCB9B2FE126F1198FABD86EB043A4ACC8,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetAs_mBDAEC5E95E17B15C522BA36FB69A4B105BCC07F3,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_Append_m045B3A5C557D2007A05A55F1D5B86A26AA5F13D5,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_Prepend_mA5ACFDDF27607E1D6A1556A18C4467099A25ABDD,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_Join_m33E665745C1F091AAD10AD2C3FDD91017861D89F,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_Insert_m6C8C4CAFC8CC4C5F2120879AE79BD01E3903422A,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_AppendInterval_m20E76B7FD0B9E0FD85D5E55444177E412D2A39FE,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_PrependInterval_mF91C39268775A123FCCE526262C391CB826B8B9B,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_AppendCallback_m056A9F025C67DB07A403E70BED916DCDDF8671E6,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_PrependCallback_m430A6A2B38D293E34439857D757394BAF37D9EEA,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_InsertCallback_mCEC5B490A95472ED358A6A930F69941843B21826,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_From_m41B352F2643EEF5F8602D32C51D373FE8A8E0964,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_From_m5B8D672BC35BE5D48ED7AC4673089451106EA93B,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetDelay_m87802322925DBE8186D81420CA984CCBCD6074D9,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetRelative_mF9232D71791D5356137306A230990BC61BB0447C,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetRelative_m32323B523CF668B9D5415BACD1DF23B3DFC93B2C,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetSpeedBased_m12A4DF33634764113DA53C919AAEDF903CA9B14C,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetSpeedBased_mA86309BC9CC816410F8D00BC64DDE78CFACE42AD,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m619178CD390C9F59543EBF056E54CECFF4BA95CD,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m9C45DDF251D2CD1BC96529E3480E9A91B46AF1E4,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m9B154343AFE2B086C2BE39C9E913F51186A10BAF,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mA202FF383BEAAE03292420F1163032B5D0566742,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mE2CF0CEA2DE42858A86D1C736E162AC6814256AE,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m527D580A508B8297888953E59601C95C399E98F4,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mAF32D7903868EB663666CB91E101BF2477234647,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mF1E28DA2E05779F1E161A62F103038250446302D,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mDA3551543550914D61FBDAF7F749A5FC69889AA7,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mF4CB8F4A815A8CF310CB76FA85D22E854A65319A,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mB198DDCA53C32A43F7EA9213244369D8B55F9A40,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mF4577F3599CEDC3C1AF2F31CD109481C229C4994,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mD2E5E89120A9CD6676C15DB603DC72BFE7EC126F,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_mC7FC2851A87231470309BC2F740335EC663BD21F,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetOptions_m232500C0CAFAD89AFE0A437A6015F2E59ED3D8FF,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetLookAt_m31B2E7ECAADA53B8C6EDA9BC760AFA2F6155B382,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetLookAt_m3BFF34B7765FCB4B3DDE2B3B90869FE9086DDD01,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetLookAt_mAD3CC7329671C9A6D0FA41A9AB2D3B236769C2AF,
	TweenSettingsExtensions_t4B9DB64BCCD3DADF0983000DB168374C35739106_CustomAttributesCacheGenerator_TweenSettingsExtensions_SetPathForwardDirection_m8EF4B32280222570CCC96CE57E9D0C4936F1F732,
	StringPluginExtensions_t3020868E50CDBE4754D7A24FACC98D90650F01DC_CustomAttributesCacheGenerator_StringPluginExtensions_ScrambleChars_m3177B26808D6532A66A40AA9292B134A216D4C90,
	StringPluginExtensions_t3020868E50CDBE4754D7A24FACC98D90650F01DC_CustomAttributesCacheGenerator_StringPluginExtensions_AppendScrambledChars_m8A75B95AF635D8479043492E1DF7084C4A9E4870,
	Extensions_t8682B038AE47AF4667FE89846FD3230EA5B01B1E_CustomAttributesCacheGenerator_Extensions_SetSpecialStartupMode_m4C63EA84189CA6ECED72CB45F4A00167CBFA4C05,
	Extensions_t8682B038AE47AF4667FE89846FD3230EA5B01B1E_CustomAttributesCacheGenerator_Extensions_NoFrom_m0A51B97805F16F5781068B840863F7509C043153,
	Extensions_t8682B038AE47AF4667FE89846FD3230EA5B01B1E_CustomAttributesCacheGenerator_Extensions_Blendable_mE98051CC7CB19CEA24937AF3EFB70E0A03FD87F7,
	U3CWaitForCompletionU3Ed__14_tB45859C2358BD11663DD63650731B7149A0DFB23_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__14__ctor_mE7533C100A0B5404B3FC93CEA6A0C37827870A87,
	U3CWaitForCompletionU3Ed__14_tB45859C2358BD11663DD63650731B7149A0DFB23_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__14_System_IDisposable_Dispose_m303A8F433405425BDC09AAE0F023A63D315B1EF9,
	U3CWaitForCompletionU3Ed__14_tB45859C2358BD11663DD63650731B7149A0DFB23_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE19223304A68A35794CA6F24216AAFD892EA5AC1,
	U3CWaitForCompletionU3Ed__14_tB45859C2358BD11663DD63650731B7149A0DFB23_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__14_System_Collections_IEnumerator_Reset_mF6C6790F450FE739E25B336C52E64226C575BD95,
	U3CWaitForCompletionU3Ed__14_tB45859C2358BD11663DD63650731B7149A0DFB23_CustomAttributesCacheGenerator_U3CWaitForCompletionU3Ed__14_System_Collections_IEnumerator_get_Current_mC04EEB82100E6C38A307534B0B9C28060021DB16,
	U3CWaitForRewindU3Ed__15_tAD6D817368CB8A458F47A994A7103A4CE6F3DA71_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__15__ctor_m37784760A70E04AC02EC1D582B83F4668111A5A0,
	U3CWaitForRewindU3Ed__15_tAD6D817368CB8A458F47A994A7103A4CE6F3DA71_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__15_System_IDisposable_Dispose_m3B4A63F07D1DD69D384626E147834AD7F9149A26,
	U3CWaitForRewindU3Ed__15_tAD6D817368CB8A458F47A994A7103A4CE6F3DA71_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD0CB3F77C35B9E9981BA568AF23A35F479B91C53,
	U3CWaitForRewindU3Ed__15_tAD6D817368CB8A458F47A994A7103A4CE6F3DA71_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__15_System_Collections_IEnumerator_Reset_m9C0EC279DBA63D45FEB58C3E702A8BB4858BA619,
	U3CWaitForRewindU3Ed__15_tAD6D817368CB8A458F47A994A7103A4CE6F3DA71_CustomAttributesCacheGenerator_U3CWaitForRewindU3Ed__15_System_Collections_IEnumerator_get_Current_m750B3811AE0DB4A6F9805BECB8DF2596A2C54F3B,
	U3CWaitForKillU3Ed__16_t552D031056DD4DB8BDA1BB638EA41E6A1131C8BE_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__16__ctor_m6EE0C594B82600E5E99AD05056367F5325ABD2FB,
	U3CWaitForKillU3Ed__16_t552D031056DD4DB8BDA1BB638EA41E6A1131C8BE_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__16_System_IDisposable_Dispose_m3732D9849EC07BDD486028486DEF03885C650C09,
	U3CWaitForKillU3Ed__16_t552D031056DD4DB8BDA1BB638EA41E6A1131C8BE_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD22099F2EB9F7275F4C9664AD9657CF571A45DC6,
	U3CWaitForKillU3Ed__16_t552D031056DD4DB8BDA1BB638EA41E6A1131C8BE_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__16_System_Collections_IEnumerator_Reset_mDA1D8BF979E23C98EEA6DAF7B2C2230ABB1B10F4,
	U3CWaitForKillU3Ed__16_t552D031056DD4DB8BDA1BB638EA41E6A1131C8BE_CustomAttributesCacheGenerator_U3CWaitForKillU3Ed__16_System_Collections_IEnumerator_get_Current_mBDACB19F06D62D737C514DAC0CCBC697F44DAA9B,
	U3CWaitForElapsedLoopsU3Ed__17_tCB517C96BA7776F1E3CD6A8A4B5D50AAA076AACD_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__17__ctor_m1AEA52887D651620F95BF3075BFB69D4D91747CC,
	U3CWaitForElapsedLoopsU3Ed__17_tCB517C96BA7776F1E3CD6A8A4B5D50AAA076AACD_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__17_System_IDisposable_Dispose_m5B505953396B4222005CF5E9C20CC9D4B6C5C041,
	U3CWaitForElapsedLoopsU3Ed__17_tCB517C96BA7776F1E3CD6A8A4B5D50AAA076AACD_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB6B87B9867FA04AA0FC215032805FEC243A46FDC,
	U3CWaitForElapsedLoopsU3Ed__17_tCB517C96BA7776F1E3CD6A8A4B5D50AAA076AACD_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__17_System_Collections_IEnumerator_Reset_m67224BDA06DBD0C7B2A8465BF9B0B732C2A4B394,
	U3CWaitForElapsedLoopsU3Ed__17_tCB517C96BA7776F1E3CD6A8A4B5D50AAA076AACD_CustomAttributesCacheGenerator_U3CWaitForElapsedLoopsU3Ed__17_System_Collections_IEnumerator_get_Current_m8801CA279BCD694AC804BA6B9702C486A2567CCC,
	U3CWaitForPositionU3Ed__18_t170F9F5656C1BE5986CDA4AEF56179230EFA0BF4_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__18__ctor_m0265088734E4649A7C2DA707563A5BB4C5D1ED2E,
	U3CWaitForPositionU3Ed__18_t170F9F5656C1BE5986CDA4AEF56179230EFA0BF4_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__18_System_IDisposable_Dispose_m1D8512DD5734B0F334C7D86B5189495981BB9067,
	U3CWaitForPositionU3Ed__18_t170F9F5656C1BE5986CDA4AEF56179230EFA0BF4_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF944285E4CEC654F6F2A4ED68C5BD7C37BAF360,
	U3CWaitForPositionU3Ed__18_t170F9F5656C1BE5986CDA4AEF56179230EFA0BF4_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__18_System_Collections_IEnumerator_Reset_m5199C8176C24AB72FCAC16AA9CA6997EEC5C36B5,
	U3CWaitForPositionU3Ed__18_t170F9F5656C1BE5986CDA4AEF56179230EFA0BF4_CustomAttributesCacheGenerator_U3CWaitForPositionU3Ed__18_System_Collections_IEnumerator_get_Current_m1230933A96DB7238797F1C0B3E1592669D367581,
	U3CWaitForStartU3Ed__19_t16390E0C9E1AF0550E16FC22511D16C8199F1C7D_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__19__ctor_mC5BB3CB2C97AA15519D6CB641CAE394F1DB11C2E,
	U3CWaitForStartU3Ed__19_t16390E0C9E1AF0550E16FC22511D16C8199F1C7D_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__19_System_IDisposable_Dispose_mDC8918991DDE6C8EBA355B49557FEB6EDC331B31,
	U3CWaitForStartU3Ed__19_t16390E0C9E1AF0550E16FC22511D16C8199F1C7D_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51827FF406E046E75CB7BB1FBCAD94D5CFD4E6A4,
	U3CWaitForStartU3Ed__19_t16390E0C9E1AF0550E16FC22511D16C8199F1C7D_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__19_System_Collections_IEnumerator_Reset_mE24D88E9B004C8AB98E432B4963736DC69468813,
	U3CWaitForStartU3Ed__19_t16390E0C9E1AF0550E16FC22511D16C8199F1C7D_CustomAttributesCacheGenerator_U3CWaitForStartU3Ed__19_System_Collections_IEnumerator_get_Current_mE1637A2CE0B2145054A0E512DE310A31C8FB4A57,
	DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_CustomAttributesCacheGenerator_DOTween_CompleteAndReturnKilledTotExceptFor_m1B34E13253A3590FD3BB5FCAB4091C6F7D84EC5A____excludeTargetsOrIds0,
	DOTween_t7BE7AEC9D5268B0C5F1193D8F20B01B7F18CC203_CustomAttributesCacheGenerator_DOTween_KillAll_mC78D7B22BB32344450F9A3E7BDBCC36E4B3A6E3A____idsOrTargetsToExclude1,
	DOTween_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
